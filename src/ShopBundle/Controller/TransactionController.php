<?php

namespace ShopBundle\Controller;

use ShopBundle\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Transaction controller.
 *
 * @Route("{_locale}/transaction")
 */
class TransactionController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function indexAction(){
		$em = $this->getDoctrine()->getManager();

		$articles = $em->getRepository('ShopBundle:Transaction')->findAll();

		return $this->render('transaction/index.html.twig', array(
			'transaction' => $transaction,
		));
	}

	/**
	 * Add a new order transaction
	 * @param Request $request
	 * 
	 */
	public function addAction(Request $request){

	}
}