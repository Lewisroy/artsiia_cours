<?php

namespace ShopBundle\Controller;

use AppBundle\Services\MangoPayServices;
use ShopBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Article controller.
 *
 * @Route("{_locale}/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="article_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('ShopBundle:Article')->findAll();

        return $this->render('article/index.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * Creates a new article entity.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/new", name="article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $user = $this->getUser();

        $article = new Article();
        $form = $this->createForm('ShopBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $availability = $form['availability']->getData();
            switch ($availability) {
                case 'en stock':
                    $quantity = $form['quantity']->getData();
                    if ( is_nan($quantity) || $quantity == 0 ){
                        //error : you should enter a quantity
                        $form->get('quantity')->addError(new FormError('You should enter a quantity'));
                        return $this->render('article/new.html.twig', array(
                            'article' => $article,
                            'form'    => $form->createView(),
                        ));
                    } else {
                        $article->setQuantity($quantity);
                    }
                    break;
                case 'article unique':
                    $quantity = 1;
                    $article->setQuantity($quantity);
                    break;
                case 'commande':
                    $creationTime = $form['creationTime']->getData();
                    if (empty($creationTime)){
                        //error : you should enter a creationTime
                        $form->get('creationTime')->addError(new FormError('You should enter a creation time'));
                        return $this->render('article/new.html.twig', array(
                            'article' => $article,
                            'form'    => $form->createView(),
                        ));
                    } else {
                        $article->setCreationTime($creationTime);
                    }
                    break;
                case 'indisponible':
                    $quantity = 0;
                    $article->setQuantity($quantity);
                    break;
                default:
                    //error : don't try to cheat on us !
                    return $this->redirectToRoute('article_new');
                    break;
            }

            $article->setOwner($user);

            // Downloading the cover
            $cover = $form['cover']->getData();
            if(!empty($article->getCover()) && $cover)
            {
                $file     = $article->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->getParameter('uploads_article_cover').$user,
                    $fileName
                );
                $article->setCover($fileName);
            }

            for ($i=2; $i <= 6; $i++) {
                $picture = $form['image'.$i]->getData();
                $getImage = "getImage".$i;
                $setImage = "setImage".$i;
                if(!empty($article->$getImage()) && $picture)
                {
                    $file     = $article->$getImage();
                    $fileName = md5(uniqid()).'.'.$file->guessExtension();
                    $file->move(
                        $this->getParameter('uploads_shop').$user,
                        $fileName
                    );
                    $article->$setImage($fileName);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('article_show', array('id' => $article->getId()));
        }

        return $this->render('article/new.html.twig', array(
            'article' => $article,
            'form'    => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}", name="article_show")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('article/show.html.twig', array(
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/{id}/edit", name="article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('ShopBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_edit', array('id' => $article->getId()));
        }

        dump($article);

        return $this->render('article/edit.html.twig', array(
            'article'     => $article,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *
     * @param Request $request
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('article_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('article_delete', array('id' => $article->getId())))
        ->setMethod('DELETE')
        ->getForm();
    }

    /**
     * We get all articles of an user
     * @param  Integer $user_id the user's we want to get articles
     * @return \Symfony\Component\HttpFoundation\Response
     * @return Article $articles the articles user
     * @Route("/{id}/articles", name="general_profile_articles")
     * @Method("GET")
     */
    public function articlesAction($user_id)
    {
        $em       = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('ShopBundle:Article')->getUserArticles($user_id);

        return $this->render('default/articles.html.twig',array(
            'articles' => $articles
        ));
    }

    /**
     * Like an article
     *
     * @param Request $request
     * @return JsonResponse
     * @Route("/like", name="article_like")
     * @Method("POST")
     */
    public function likeAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $return = array();
        $return['minus_one'] = false;
        $return['plus_one'] = false;
        $return['id'] = $id;
        $user = $this->getUser();
        $post = $em->getRepository('ArticleBundle:Activity')->findOneBy(array('id' => $id));
        $inLiker = false;
        foreach ($post->getLikers() as $liker) {
            if ($liker->getId() == $user->getId()) {
                $inLiker = true;
                break;
            }
        }
        if (!$inLiker) {
            $post->addLiker($user);
            $return['plus_one'] = true;
            $em->persist($post);
            $em->flush();
        } else {
            $post->removeLiker($user);
            $return['minus_one'] = true;
            $em->persist($post);
            $em->flush();
        }
        return new JsonResponse($return);
    }

    /**
     * Buy a article
     *
     * @Route("/buy", name="article_buy")
     * @Method("POST")
     */
    public function buyAction(Request $request){

        $mangoPayApi = new MangoPayServices();

        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        $buyer = $this->getUser();
        $buyerId = $buyer->getId();
        $buyerMangoUserId = $buyer->getMangopayUserId();
        $buyerMangoWalletId = $buyer->getMangopayWalletId();
        $buyerBalance = $mangoPayApi->getWalletUser($buyerMangoWalletId)->Balance->Amount;

        $article = $em->getRepository('ShopBundle:Article')->findOneBy(array('id' => $id));

        $seller = $article->getOwner();
        $sellerId = $seller->getId();
        $sellerMangoUserId = $seller->getMangopayUserId();
        $sellerMangoWalletId = $seller->getMangopayWalletId();

        $articlePrice = $article->getPrice();

        $return['buyer_id'] = $buyerId;
        $return['buyer_mango_user_id'] = $buyerMangoUserId;
        $return['buyer_mango_wallet_id'] = $buyerMangoWalletId;

        $return['seller_id'] = $sellerId;
        $return['seller_mango_user_id'] = $sellerMangoUserId;
        $return['seller_mango_wallet_id'] = $sellerMangoWalletId;

        if ($buyerBalance > $articlePrice){
            $transfer = $mangoPayApi->doTransfer($buyerMangoUserId, str_replace(".","",$articlePrice), $buyerMangoWalletId, $sellerMangoWalletId);
            $return['transfert'] = $transfer;
        } else{
            $return['error'] = "Montant insuffisant";
        }

        return new JsonResponse($return);
    }

}
