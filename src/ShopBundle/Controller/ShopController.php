<?php

namespace ShopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Services\MangoPayServices;

/**
* Shop controller.
*
* @Route("/{_locale}/shop")
*/
class ShopController extends Controller {
	/**
	* View of the shop with the list of all Article entities.
	*
	* @Route("/", name="shop_index")
     * @return \Symfony\Component\HttpFoundation\Response
	* @Method("GET")
	*/
	public function indexAction() {
		$em = $this->getDoctrine()->getManager();
		$articles = $em->getRepository('ShopBundle:Article')->findAll();
		$categories = $em->getRepository('LectureBundle:Category')->findAll();

		return $this->render('shop/index.html.twig',array (
			'articles' => $articles,
			'categories' => $categories
			)
		);
	}

	/**
	 * Delete article on Shop
	 *
     * @param Request $request
     * @return JsonResponse
	 * @Route("/", name="delete_article")
	 * @Method("POST")
	 */
	public function delete_articleAction(Request $request){
		$em = $this->getDoctrine()->getManager();

		$article = $em->getRepository('ShopBundle:Article')->find($request->get('id'));

		$em->remove($article);
		$em->flush($article);
		
		return new JsonResponse(array("Article supprimé avec succes."));
	}

    /**
     * Buy a article
     *
     * @Route("/buy", name="shop_buy")
     * @Method("POST")
     */
    public function buyAction(Request $request){

        $mangoPayApi = new MangoPayServices();

        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();

        $buyer = $this->getUser();
        $buyerId = $buyer->getId();
        $buyerMangoUserId = $buyer->getMangopayUserId();
        $buyerMangoWalletId = $buyer->getMangopayWalletId();
        $buyerBalance = $mangoPayApi->getWalletUser($buyerMangoWalletId)->Balance->Amount;

        $article = $em->getRepository('ShopBundle:Article')->findOneBy(array('id' => $id));

        $seller = $article->getOwner();
        $sellerId = $seller->getId();
        $sellerMangoUserId = $seller->getMangopayUserId();
        $sellerMangoWalletId = $seller->getMangopayWalletId();

        $articlePrice = $article->getPrice();

        $return['buyer_id'] = $buyerId;
        $return['buyer_mango_user_id'] = $buyerMangoUserId;
        $return['buyer_mango_wallet_id'] = $buyerMangoWalletId;

        $return['seller_id'] = $sellerId;
        $return['seller_mango_user_id'] = $sellerMangoUserId;
        $return['seller_mango_wallet_id'] = $sellerMangoWalletId;

        if ($buyerBalance > $articlePrice){
            $transfer = $mangoPayApi->doTransfer($buyerMangoUserId, str_replace(".","",$articlePrice), $buyerMangoWalletId, $sellerMangoWalletId);
            $return['transfert'] = $transfer;
        } else{
            $return['error'] = "Montant insuffisant";
        }

        return new JsonResponse($return);
    }

}
