<?php

namespace ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Datetime;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="ShopBundle\Repository\ArticleRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 * @ORM\HasLifecycleCallbacks
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="decimal", scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=250)
     */
    private $short_description;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateUpdated", type="datetime", nullable=true)
     */
    private $dateUpdated;

    /**
     * @ORM\ManyToOne(targetEntity="LectureBundle\Entity\Category",cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var array
     *
     * @ORM\Column(name="tags", type="array")
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="availability", type="string", length=100)
     */
    private $availability;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var datetime
     *
     * @ORM\Column(name="creationTime", type="string", nullable=true)
     */
    private $creationTime;

    /**
     * @var string
     *
     * @ORM\Column(name="shippingMode", type="string", length=100, nullable=true)
     */
    private $shippingMode;

    /**
     * @var string
     *
     * @ORM\Column(name="shippingCountryFrom", type="string", nullable=true)
     */
    private $shippingCountryFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="shippingRegionFrom", type="string", nullable=true)
     */
    private $shippingRegionFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="image2", type="string", length=255, nullable=true)
     */
    private $image2;

    /**
     * @var string
     *
     * @ORM\Column(name="image3", type="string", length=255, nullable=true)
     */
    private $image3;

    /**
     * @var string
     *
     * @ORM\Column(name="image4", type="string", length=255, nullable=true)
     */
    private $image4;

    /**
     * @var string
     *
     * @ORM\Column(name="image5", type="string", length=255, nullable=true)
     */
    private $image5;

    /**
     * @var string
     *
     * @ORM\Column(name="image6", type="string", length=255, nullable=true)
     */
    private $image6;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="article_likers")
     *
     */
    private $likers;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="article_viewers")
     *
     */
    private $viewers;

    /**
     * @ORM\OneToMany(targetEntity="LectureBundle\Entity\Comment",mappedBy="article")
     */
    private $comments;

    /** 
     *  @ORM\PrePersist 
     */
    public function doStuffOnPrePersist()
    {
        $this->dateCreated = new DateTime();
    }

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return Article
     */
    public function setShortDescription($shortDescription)
    {
        $this->short_description = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Article
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return Article
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set tags
     *
     * @param array $tags
     *
     * @return Article
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Article
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set image2
     *
     * @param string $image2
     *
     * @return Article
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;

        return $this;
    }
    
    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Get image2
     *
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * Set image3
     *
     * @param string $image3
     *
     * @return Article
     */
    public function setImage3($image3)
    {
        $this->image3 = $image3;

        return $this;
    }

    /**
     * Get image3
     *
     * @return string
     */
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * Set image4
     *
     * @param string $image4
     *
     * @return Article
     */
    public function setImage4($image4)
    {
        $this->image4 = $image4;

        return $this;
    }

    /**
     * Get image4
     *
     * @return string
     */
    public function getImage4()
    {
        return $this->image4;
    }

    /**
     * Set image5
     *
     * @param string $image5
     *
     * @return Article
     */
    public function setImage5($image5)
    {
        $this->image5 = $image5;

        return $this;
    }

    /**
     * Get image5
     *
     * @return string
     */
    public function getImage5()
    {
        return $this->image5;
    }

    /**
     * Set image6
     *
     * @param string $image6
     *
     * @return Article
     */
    public function setImage6($image6)
    {
        $this->image6 = $image6;

        return $this;
    }

    /**
     * Get image6
     *
     * @return string
     */
    public function getImage6()
    {
        return $this->image6;
    }

    /**
     * Set category
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return Article
     */
    public function setCategory(\LectureBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \LectureBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set availability
     *
     * @param string $availability
     *
     * @return Article
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * Get availability
     *
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * Get quantity
     *
     * @return \int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set shippingMode
     *
     * @param string $shippingMode
     *
     * @return Article
     */
    public function setShippingMode($shippingMode)
    {
        $this->shippingMode = $shippingMode;

        return $this;
    }

    /**
     * Get shippingMode
     *
     * @return string
     */
    public function getShippingMode()
    {
        return $this->shippingMode;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Article
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Article
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set owner
     *
     * @param \UserBundle\Entity\User $owner
     *
     * @return Article
     */
    public function setOwner(\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \UserBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set shippingCountryFrom
     *
     * @param string $shippingCountryFrom
     *
     * @return Article
     */
    public function setShippingCountryFrom($shippingCountryFrom)
    {
        $this->shippingCountryFrom = $shippingCountryFrom;

        return $this;
    }

    /**
     * Get shippingCountryFrom
     *
     * @return string
     */
    public function getShippingCountryFrom()
    {
        return $this->shippingCountryFrom;
    }

    /**
     * Set shippingRegionFrom
     *
     * @param string $shippingRegionFrom
     *
     * @return Article
     */
    public function setShippingRegionFrom($shippingRegionFrom)
    {
        $this->shippingRegionFrom = $shippingRegionFrom;

        return $this;
    }

    /**
     * Get shippingRegionFrom
     *
     * @return string
     */
    public function getShippingRegionFrom()
    {
        return $this->shippingRegionFrom;
    }

    /**
     * Add liker
     *
     * @param \UserBundle\Entity\User $liker
     *
     * @return Article
     */
    public function addLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers[] = $liker;

        return $this;
    }

    /**
     * Remove liker
     *
     * @param \UserBundle\Entity\User $liker
     */
    public function removeLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Get likers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * Add viewer
     *
     * @param \UserBundle\Entity\User $viewer
     *
     * @return Article
     */
    public function addViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers[] = $viewer;

        return $this;
    }

    /**
     * Remove viewer
     *
     * @param \UserBundle\Entity\User $viewer
     */
    public function removeViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers->removeElement($viewer);
    }

    /**
     * Get viewers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewers()
    {
        return $this->viewers;
    }

    /**
     * Add comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     *
     * @return Article
     */
    public function addComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     */
    public function removeComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set creationTime
     *
     * @param \DateTime $creationTime
     *
     * @return Article
     */
    public function setCreationTime($creationTime)
    {
        $this->creationTime = $creationTime;

        return $this;
    }

    /**
     * Get creationTime
     *
     * @return \DateTime
     */
    public function getCreationTime()
    {
        return $this->creationTime;
    }

    /**
     * Add category.
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return Article
     */
    public function addCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category.
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCategory(\LectureBundle\Entity\Category $category)
    {
        return $this->category->removeElement($category);
    }
}
