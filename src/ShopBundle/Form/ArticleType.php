<?php

namespace ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'market.article.product_name'))
            ->add('price', MoneyType::class, array('label' => 'market.article.price'))
            ->add('short_description', TextType::class, array('label' => 'market.article.short_description'))
            ->add('description', CKEditorType::class, array(
                    'label'    => 'market.article.description',
                    'required' => true,
                    'config'   => array(
                        'uiColor'                          => '#ffffff',
                        'filebrowserBrowseRoute'           => 'elfinder',
                        'filebrowserBrowseRouteParameters' => array(
                            'instance'   => 'default',
                            'homeFolder' => ''
                        )
                    )
                )
            )
            ->add('category', EntityType::class, array(
                    'label' => 'faq.category',
                    'class' => 'LectureBundle\Entity\Category',
                    'choice_label' => 'name',
                    'multiple' => false,
                    'expanded' => false,
                    'required' => true
                )
            )
            ->add('availability', ChoiceType::class, array(
                    'choices' => array(
                        ''                                        => '',
                        'market.article.availability.on_stock'      => 'en stock',
                        'market.article.availability.unique'        => 'article unique',
                        'market.article.availability.on_order'      => 'commande',
                        'market.article.availability.not_available' => 'indisponible'
                    ),
                    'label'   => 'article.create.availability'
                )
            )
            ->add('quantity', IntegerType::class, array(
                    'required' => false,
                    'label'    => 'market.article.quantity'
                )
            )
            ->add('creationTime', ChoiceType::class, array(
                    'choices'  => array(
                        'market.article.creationTime.week'            => '1 semaine',
                        'market.article.creationTime.1_2_weeks'       => '1 à 2 semaines',
                        'market.article.creationTime.2_weeks_1_month' => '2 semaines à 1 mois',
                        'market.article.creationTime.1_2_months'      => '1 à 2 mois',
                        'market.article.creationTime.2_4_months'      => '2 à 4 mois',
                        'market.article.creationTime.4_6_months'      => '4 à 6 mois'
                    ),
                    'required' => false,
                    'label'    => 'market.article.creationTime'
                )
            )
            ->add('shippingMode', ChoiceType::class, array(
                    'choices'  => array(
                        'market.article.shippingMode.delivery'     => 'colis',
                        'market.article.shippingMode.hand_to_hand' => 'remise en main',
                        'market.article.shippingMode.discuss'      => 'à discuter'
                    ),
                    'required' => false,
                    'label'    => 'market.article.shippingMode_name'
                )
            )
            ->add('cover', FileType::class, array(
                    'label'      => 'global.array.cover',
                    'data_class' => null,
                    'attr'       => array('accept' => 'image/*')
                )
            )
            ->add('image2', FileType::class, array(
                    'label'      => 'market.article.image.second',
                    'required'   => false,
                    'data_class' => null,
                    'attr'       => array('accept' => 'image/*')
                )
            )
            ->add('image3', FileType::class, array(
                    'label'      => 'market.article.image.third',
                    'required'   => false,
                    'data_class' => null,
                    'attr'       => array('accept' => 'image/*')
                )
            )
            ->add('image4', FileType::class, array(
                    'label'      => 'market.article.image.forth',
                    'required'   => false,
                    'data_class' => null,
                    'attr'       => array('accept' => 'image/*')
                )
            )
            ->add('image5', FileType::class, array(
                    'label'      => 'market.article.image.fifth',
                    'required'   => false,
                    'data_class' => null,
                    'attr'       => array('accept' => 'image/*')
                )
            )
            ->add('image6', FileType::class, array(
                    'label'      => 'market.article.image.sixth',
                    'required'   => false,
                    'data_class' => null,
                    'attr'       => array('accept' => 'image/*')
                )
            )
            ->add('tags')
            ->add('shippingCountryFrom', TextType::class, array(
                    'required' => false,
                    'label'    => 'market.article.shippingCountry'
                )
            )
            ->add('shippingRegionFrom', TextType::class, array(
                    'required' => false,
                    'label'    => 'market.article.shippingRegion'
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ShopBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'shopbundle_article';
    }
}
