<?php
/**
 * User: fehiniaina
 * Date: 11/10/2017
 * Time: 19:07
 */

namespace ArtworkBundle\Utils;


/**
 * Class ArtsiiaManager
 * @package ArtworkBundle\Services
 */
abstract class ArtsiiaUtil
{
    /**
     * Nombre de caractères maximum d'un message.
     */
    const NB_CHAR_MAX_MESSAGE = 70 ;

    /**
     * Nombre maximum de message.
     */
    const NB_MAX_MESSAGE = 8 ;

    /**
     * Nombre de page de message par défaut.
     */
    const NB_PAGE_DEFAULT_MSG = 1 ;

    /**
     * Nombre de page de message par défaut.
     */
    const OFFSET = 0 ;

    /**
     * Traitement date.
     *
     * @param string $inputDate
     * @return array
     */
    public static function getDateTime($inputDate)
    {
        $dateNow   = new \DateTime('now') ;
        $dateEntry = new \DateTime($inputDate) ;
        $seconde   = strtotime($dateNow->format('Y-m-d h:i:s')) - strtotime($dateEntry->format('Y-m-d h:i:s'))  ; // nombre de seconde

        $day       = intval($seconde / 86400) ;
        $seconde  -=  $day * 86400 ;
        $hour      = intval($seconde / 3600) ;
        $seconde  -= $hour * 3600;
        $minute    = intval($seconde / 60) ;
        $seconde  -= $minute * 60 ;

        return array(
            "day"     => $day,
            "hour"    => $hour,
            "minute"  => $minute,
            "seconde" => $seconde
        ) ;
    }
}