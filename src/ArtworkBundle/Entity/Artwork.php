<?php

namespace ArtworkBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Artwork
 *
 * @ORM\Table(name="artwork")
 * @ORM\Entity(repositoryClass="ArtworkBundle\Repository\ArtworkRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 */
class Artwork extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255, unique=true)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetimetz")
     */
    private $dateCreation;

    /**
     * @var string
     *
     * @ORM\Column(name="confidential", type="string", length=255)
     */
    private $confidential;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

   /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="author_artwork",
     *      joinColumns={@ORM\JoinColumn(name="lecture_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Category",cascade={"persist"})
     * @ORM\JoinTable(name="category_artwork")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\SubCategory",cascade={"persist"})
     * @ORM\JoinTable(name="sub_category_artwork")
     */
    private $sub_category;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="artwork_likers")
     */
    private $likers;

        /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="artwork_viewers")
     */
    private $viewers;

    /**
     * @ORM\OneToMany(targetEntity="LectureBundle\Entity\Comment",mappedBy="artwork")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="artwork")
     */
    public $reporting;

    /**
     * Add reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     *
     * @return Reporting
     */
    public function addReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting[] = $reporting;

        return $this;
    }

    /**
     * Remove reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     */
    public function removeReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting->removeElement($reporting);
    }

    /**
     * Get reporting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporting()
    {
        return $this->reporting;
    }

    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }


    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category[] = $category;
    }
    
    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Artwork
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    
        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Artwork
     */
    public function setFile($file)
    {
        $this->file = $file;
    
        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Artwork
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Artwork
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set confidential
     *
     * @param string $confidential
     *
     * @return Artwork
     */
    public function setConfidential($confidential)
    {
        $this->confidential = $confidential;
    
        return $this;
    }

    /**
     * Get confidential
     *
     * @return string
     */
    public function getConfidential()
    {
        return $this->confidential;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Artwork
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Artwork
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set author
     *
     * @param string $author
     *
     * @return Artwork
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        if (null === $this->dateCreation) {
            $this->dateCreation = new \DateTime('now') ;
        }

        $this->author = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sub_category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reporting = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add author
     *
     * @param \UserBundle\Entity\User $author
     *
     * @return Artwork
     */
    public function addAuthor(\UserBundle\Entity\User $author)
    {
        $this->author[] = $author;
    
        return $this;
    }

    /**
     * Remove author
     *
     * @param \UserBundle\Entity\User $author
     */
    public function removeAuthor(\UserBundle\Entity\User $author)
    {
        $this->author->removeElement($author);
    }

    /**
     * Add category
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return Artwork
     */
    public function addCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category[] = $category;
    
        return $this;
    }

    /**
     * Remove category
     *
     * @param \LectureBundle\Entity\Category $category
     */
    public function removeCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    

    /**
     * Add subCategory
     *
     * @param \LectureBundle\Entity\SubCategory $subCategory
     *
     * @return Artwork
     */
    public function addSubCategory(\LectureBundle\Entity\SubCategory $subCategory)
    {
        $this->sub_category[] = $subCategory;
    
        return $this;
    }

    /**
     * Remove subCategory
     *
     * @param \LectureBundle\Entity\SubCategory $subCategory
     */
    public function removeSubCategory(\LectureBundle\Entity\SubCategory $subCategory)
    {
        $this->sub_category->removeElement($subCategory);
    }

    /**
     * Get subCategory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubCategory()
    {
        return $this->sub_category;
    }

    /**
     * @param mixed $subCategory
     */
    public function setSubCategory($subCategory)
    {
        $this->sub_category[] = $subCategory;
    }

    /**
     * Add liker
     *
     * @param \UserBundle\Entity\User $liker
     *
     * @return Artwork
     */
    public function addLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers[] = $liker;

        return $this;
    }

    /**
     * Remove liker
     *
     * @param \UserBundle\Entity\User $liker
     */
    public function removeLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Get likers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * Add comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     *
     * @return Artwork
     */
    public function addComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     */
    public function removeComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }


    /**
     * Add viewer
     *
     * @param \UserBundle\Entity\User $viewer
     *
     * @return Artwork
     */
    public function addViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers[] = $viewer;
    
        return $this;
    }

    /**
     * Remove viewer
     *
     * @param \UserBundle\Entity\User $viewer
     */
    public function removeViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers->removeElement($viewer);
    }

    /**
     * Get viewers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewers()
    {
        return $this->viewers;
    }
}
