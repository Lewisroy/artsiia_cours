

$(function () {

    var PULL_INTERVAL = 30000; // 30 secondes
    if (typeof pullMessageNotReadUrl !== 'undefined' && typeof pullAllMessageNotReadUrl !== 'undefined') {
        var TChat = {
            pullMessageNotReadUrl: pullMessageNotReadUrl,
            pullAllMessageNotReadUrl: pullAllMessageNotReadUrl,
            pullMethod: 'POST',
            pullMessageNotRead: function () { // recuperer nombre des messages non lu.

                $.ajax({
                    url: TChat.pullMessageNotReadUrl,
                    method: TChat.pullMethod,
                    success: function (response) {
                        $('.messageCount').html((response.message > 9) ? '9+' : response.message);
                    }
                });
            },
            pullAllMessageNotRead: function () { // mise à jour des messages non lu.

                $.ajax({
                    url: TChat.pullAllMessageNotReadUrl,
                    method: TChat.pullMethod,
                    success: function (response) {
                        $('.dernierMessage').html(response);
                    }
                });
            }
        };

        // Mise à jour automatique nombre messages non lu.
        setInterval(function () {
            TChat.pullMessageNotRead()
        }, PULL_INTERVAL);

        // Mise à jour automatique des listes des messages.
        setInterval(function () {
            TChat.pullAllMessageNotRead()
        }, PULL_INTERVAL);
    }

    PNotify.prototype.options.styling = "bootstrap3";

    $('#artworkbundle_artwork_category').on('change', function (e) {
        $.ajax({
            url: Routing.generate('list_sub_category', {_locale: locale, id: this.value}),
            type: 'GET',
            success: function (response) {
                var options = "";
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i]['id'] + '">' + response[i]['name'] + '</option>';
                }
                var html = '<select id="artworkbundle_artwork_sub_category" name="artworkbundle_artwork[sub_category][]" class="form-control select" multiple="multiple">' +
                        options +
                        '</select>';
                $('#sub_category').html(html);
                $('select').select2({});
            }
        });
    });
    $(".drop-down-list li").on("click", function () {
        var parent = $('#sub-categories-list');
        var href = parent.attr('data-url');
        if ($('#category_filter').length > 0) {
            $.ajax({
                url: href,
                data: {'categories': $('#category_filter').val()},
                type: 'POST',
                success: function (data) {
                    var htmlOption = '';
                    var JsonData = JSON.parse(data);
                    if (JsonData.length > 0) {
                        for (var key in JsonData) {
                            htmlOption += '<a data-filter="' + JsonData[key].id + '" class="filter">' + JsonData[key].name + '</a>';
                        }
                    }
                    parent.html(htmlOption);
                }
            })
        }
    });

    $('.followers').off('click').on('click', function (event) {
        event.preventDefault();

        var url = $(this).attr('data-url');
        var followers = $(this).attr('data-followers');
        var userFollowers = $(this).attr('data-userFollowers');

        $.ajax({
            url: url,
            data: {
                'followers': followers,
                'userFollowers': userFollowers
            },
            type: 'POST',
            dataType: 'json',
            beforeSend: function () {
                var loader = $('.be-loader .spinner').clone();
                $('#container-mix').html(loader);
            },
            success: function (data) {
                if (data.status == 'OK') {
                    $('#followers').removeClass('fa fa-users');
                    $('#followers').addClass('fa fa-check-circle');
                    $('.followers').removeClass('followers');
                    $('.text_followers').text('Abonné');
                    new PNotify({
                        title: 'Notification',
                        type: 'success',
                        text: data.message,
                        animation: "fade",
                        delay: 5000,
                    });
                }
            },
            error: function (error) {
                // var response = JSON.parse(error.responseText).message ;
                //
                // new PNotify({
                //     title: 'Notification',
                //     type: 'error',
                //     text: response,
                //     animation: "fade",
                //     delay: 5000,
                // });
                return false;
            }
        })
    });
});


function loadArtWork() {
    var formObj = $('#form-artwork-filter')
    $.ajax({
        url: formObj.attr('action'),
        data: formObj.serialize(),
        type: 'POST',
        beforeSend: function () {
            var loader = $('.be-loader .spinner').clone();
            $('#container-mix').html(loader);
        },
        success: function (data) {
            $('#container-mix').html(data);
            $('#container-mix .mix').fadeIn();
        }
    })
}
function mixItUp(element) {
    //MIX UP
    if ($(element).length) {
        $(element).mixItUp(
                {
                    animation: {
                        duration: 400,
                        effects: 'fade translateZ(-360px) stagger(34ms)',
                        easing: 'ease'
                    }
                }
        );
    }
    ;
}

$(document).ready(function () {
    $.ajax({
        url: Routing.generate('list_sub_category', {_locale: locale, id: $('#artworkbundle_artwork_category').val()}),
        type: 'GET',
        success: function (response) {
            var options = "";
            for (var i = 0; i < response.length; i++) {
                options += '<option value="' + response[i]['id'] + '">' + response[i]['name'] + '</option>';
            }
            var html = '<select id="artworkbundle_artwork_sub_category" name="artworkbundle_artwork[sub_category][]" class="form-control select" multiple="multiple">' +
                    options +
                    '</select>';
            $('#sub_category').html(html);
            $('select').select2({});
        }
    });
});
