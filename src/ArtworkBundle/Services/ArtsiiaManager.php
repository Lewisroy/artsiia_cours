<?php

/**
 * User: fehiniaina
 * Date: 11/10/2017
 * Time: 19:07
 */

namespace ArtworkBundle\Services;

use ArtworkBundle\Utils\ArtsiiaUtil;
use Doctrine\Bundle\DoctrineBundle\Registry;
use UserBundle\Entity\User;
use UserBundle\Entity\Message;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Translation\DataCollectorTranslator;

/**
 * Class ArtsiiaManager
 * @package ArtworkBundle\Services
 */
class ArtsiiaManager extends \Twig_Extension {

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * @var DataCollectorTranslator
     */
    private $translator;

    /**
     * ArtsiiaManager constructor.
     * @param Registry $registry
     */
    public function __construct(Registry $registry, Kernel $kernel) {
        $this->registry = $registry;
        $this->kernel = $kernel;
        $this->translator = $this->kernel->getContainer()->get('translator');
    }

    /**
     * Transformation des dates.
     *
     * @param string $date
     * @return string
     */
    public function getTransDate($date) {
        $dates = ArtsiiaUtil::getDateTime($date);
        $d1 = new \DateTime();
        $d2 = new \DateTime($date);

        $minutes = (int) ceil((time() - strtotime($date)) / 60);

        $result = '';

        // S’il est envoyé depuis moins d’une heure (strictement)
        //  Il y a X minutes.

        if ($minutes < 60) {
            $result = ' '.$this->translator->trans('messages.minute', array('%value%' => $minutes));
        }

        // S’il est envoyé entre 60 minutes et 1h59.ss
        // Il y a plus d’une heure
        elseif ($minutes >= 60 && $minutes < 119) {
            $result = ' '.$this->translator->trans('messages.hour');
        }

        // S’il est envoyé entre 2h et 23h59 heures
        // Il y a X heures.
        elseif ($minutes >= 120 && $minutes < 1439) {
            $result = ' '.$this->translator->trans('messages.hours', array('%value%' => (int)($minutes/60)));
        }

        // Entre 24h et 47h59
        // Il y a 1 jour.
        elseif ($minutes >= 1440 && $minutes < 2879) {
            $result = ' '.$this->translator->trans('messages.day');
        }
        // else
        else {
            $result = ' '.$this->translator->trans('messages.days', array('%value%' => (int)($minutes / 60 / 24)));
        }

        return $result;
    }

    /**
     * Récuperer les nombres des messages non lu.
     *
     * @param User $user
     * @return int
     */
    public function getNbMessageNotRead(User $user) {
        $messagesNotRead = $this->registry->getRepository('UserBundle:Message')->checkoutMessageNotRead($user->getId());
        $nbMessage = $messagesNotRead[0]['nbMessage'];
        $nbMessage = $nbMessage > 9 ? $nbMessage . '+' : $nbMessage;

        return $nbMessage;
    }

    /**
     * Affichage photo de profil.
     *
     * @param int    $userId
     * @param string $picture
     * @return string
     */
    public function getProfilPicture($userId, $picture) {
        $path = 'uploads/users/';

        if (null === $picture) {
            $picture = 'themeforest/img/default_profile.png';
        } else {
            $picture = $path . $userId . '/' . $picture;
        }

        return (string) $picture;
    }

    /**
     * 70 caractères maximum.
     *
     * @param string $text
     * @return string
     */
    public function scrope($text) {
        if (strlen($text) > ArtsiiaUtil::NB_CHAR_MAX_MESSAGE) {
            $text = substr($text, 0, ArtsiiaUtil::NB_CHAR_MAX_MESSAGE);
            $text = substr($text, 0, strrpos($text, ' ')) . " ...";
        }

        return $text;
    }

    /**
     * Nombre maximal d'affichage des messagerie.
     */
    public function getNbMaxAffichage() {
        return ArtsiiaUtil::NB_MAX_MESSAGE;
    }

    /**
     * Récuperer les nombres des messages non lu.
     *
     * @param User $user
     * @return array
     */
    public function getMessageNotRead(User $user) {
        $messages = $this->registry->getRepository('UserBundle:Message')->messagesNotRead($user->getId());

        $data = [];
        $check = [];
        foreach ($messages as $message) {
            if (!in_array($message['sender_id'], $check) && $message['readByReceiver'] == false) {
                $count = 0;
                foreach ($messages as $m) {
                    if ($message['sender_id'] == $m['sender_id']) {
                        $count++;
                    }
                }
                $message['count'] = $count;
                array_push($data, $message);
                array_push($check, $message['sender_id']);
            }
        }

        return count($data) > 5 ? array_slice($data, 0, 5) : $data;
    }

    /**
     * Méthode obligatoire.
     *
     * @return string
     */
    public function getName() {
        return 'ArtsiiaManager';
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('getNbMessageNotRead', array($this, 'getNbMessageNotRead')),
            new \Twig_SimpleFilter('getMessageNotRead', array($this, 'getMessageNotRead')),
            new \Twig_SimpleFilter('getTransDate', array($this, 'getTransDate')),
            new \Twig_SimpleFilter('scrope', array($this, 'scrope')),
            new \Twig_SimpleFilter('getProfilPicture', array($this, 'getProfilPicture')),
            new \Twig_SimpleFilter('getNbMaxAffichage', array($this, 'getNbMaxAffichage')),
        );
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('getNbMessageNotRead', array($this, 'getNbMessageNotRead')),
            new \Twig_SimpleFunction('getMessageNotRead', array($this, 'getMessageNotRead')),
            new \Twig_SimpleFunction('getTransDate', array($this, 'getTransDate')),
            new \Twig_SimpleFunction('scrope', array($this, 'scrope')),
            new \Twig_SimpleFunction('getProfilPicture', array($this, 'getProfilPicture')),
            new \Twig_SimpleFunction('getNbMaxAffichage', array($this, 'getNbMaxAffichage')),
        );
    }

}
