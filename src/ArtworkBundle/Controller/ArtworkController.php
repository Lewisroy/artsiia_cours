<?php

namespace ArtworkBundle\Controller;

use UserBundle\Entity\User;
use ArtworkBundle\Entity\Artwork;
use LectureBundle\Entity\Comment;
use ArtworkBundle\Form\ArtworkType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use LectureBundle\Form\CommentType;

/**
 * Artwork controller.
 *
 * @Route("/{_locale}/artwork")
 */
class ArtworkController extends BaseController {

    /**
     * Lists all artwork entities.
     * 
     * @return Artwork $artworks All the Artworks
     * @Route("/", name="artwork_index")
     * @Method("GET")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $artworks = $em->getRepository('ArtworkBundle:Artwork')->findAll();

        return $this->render('artwork/index.html.twig', array(
                    'artworks' => $artworks,
        ));
    }

    /**
     * Lists artworks by user.
     *
     * @Route("/ajaxUserArtworks", name="ajax_get_user_artwork")
     * @Method("GET")
     *
     * @param  Request $request
     * @return Response in json
     */
    public function ajaxUserArtworkAction(Request $request) {
        $user = $this->getUser();

        if ($user === null) {
            return $this->redirectToRoute('au_homepage');
        }

        $tFilters = $this->getFilters($request);
        $tSortings = $this->getSortings($request, array(
            'a.id',
        ));

        $options = array(
            'search' => $request->query->get('sSearch'),
        );

        $productManager = $this->container->get('artwork.artwork_manager');

        $toOeuvres = $productManager->findByCriteria($options, $tFilters, $tSortings)->getResult();

        $tiNbOeuvres['nbOeuvre'] = count($toOeuvres);

        $content = $this->getDataJson(
                $request, $tiNbOeuvres['nbOeuvre'], $tiNbOeuvres['nbOeuvre'], $toOeuvres, 'ArtworkBundle:Artwork:index.json.html.twig'
        );

        $response = new Response($content);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Creates a new artwork entity.
     * @param Request $request
     * @return Artwork $artwork artwork to create or artwork create
     * @return \Symfony\Component\Form\Form The form
     * @Route("/new", name="artwork_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute('au_homepage');
        }
        $em = $this->getDoctrine()->getManager();

        $artwork = new Artwork();

        $form = $this->createForm(ArtworkType::class, $artwork, array('entityManager' => $em));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getUser();
            $artwork->addAuthor($user);
            // Ajout de la photo de couverture
            $uploaded_file = $form['cover']->getData();
            if (!empty($artwork->getCover()) && $uploaded_file) {
                $file = $artwork->getCover();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                        $this->getParameter('uploads_artwork_cover'), $fileName
                );
                $artwork->setCover($fileName);
            }

            $uploaded_file = $form['file']->getData();
            if (!empty($artwork->getFile()) && $uploaded_file) {
                $file = $artwork->getFile();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                        $this->getParameter('uploads_artwork_file'), $fileName
                );
                $artwork->setFile($fileName);
            }


            $em->persist($artwork);
            $user->addArtwork($artwork);
            $em->flush();

            return $this->redirectToRoute('artwork_show', array('id' => $artwork->getId()));
        }

        return $this->render('artwork/new.html.twig', array(
                    'artwork' => $artwork,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a artwork entity.
     * @param Request $request
     * @param Artwork $artwork to show
     * @return Artwork $artwork to show
     * @return \Symfony\Component\Form\Form $deleteForm Form of delete
     * @return User $user The current user
     * @return User $artwork_authors The artwork user
     * @return \Symfony\Component\Form\Form $commentsForm The form for comment
     * @return \Symfony\Component\Form\Form $replyForm The form for reply
     * @Route("/{id}", name="artwork_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, Artwork $artwork) {
        foreach ($artwork->getAuthor() as $user) {
            if ($user->getIsBanni()) {
                throw $this->createNotFoundException();
            }
        }
        $deleteForm = $this->createDeleteForm($artwork);
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if ($user) {
            if (!$artwork->getViewers()->contains($this->getUser())) {
                $artwork->addViewer($user);
                $em->flush();
            }
        }

        $artwork_authors = null;


        if (null != $user) {

            $artwork_authors = $em->getRepository('ArtworkBundle:Artwork')->getLatestUserArtworks($artwork->getAuthor()[0]->getId(), 3);
        }

//        $form = $this->createFormBuilder(null, array('method' => "POST", 'attr' => array('id' => 'authors')))
//                ->add('author', EntityType::class, array(
//                    'class' => 'UserBundle:User',
//                    'multiple' => true,
//                    'expanded' => false,
//                    'mapped' => false,
//                ))
//                ->getForm();
//
//        $form = $this->createForm('UserBundle\Form\SearchUserType', $user);
//        $form->get('author')->setData($artwork->getAuthor());

        $commentsForm = $this->createForm(CommentType::class);
        $form_reply = $this->createForm(CommentType::class);

        $commentsForm->handleRequest($request);

        if ($commentsForm->isSubmitted()) {
            $commentData = $commentsForm->get('comment')->getData();
            if ($commentData != null or $commentData != "") {
                $comment = new Comment();
                $comment->setContent($commentData);
                $comment->setDate(new \DateTime());
                $comment->setArtwork($artwork);
                $comment->setUser($this->getUser());
                $em->persist($comment);
                $em->flush();
                $notification = $this->get('app_notification_services');
                $notification->sendNotificationArtwork($comment, $artwork);
            }
            return $this->redirectToRoute('artwork_show', array('id' => $artwork->getId()));
        }

        $form_reply->handleRequest($request);

        if ($form_reply->isSubmitted()) {
            $replayData = $form_reply->get('comment')->getData();
            if ($replayData != null or $replayData != "") {

                $replay = new Comment();
                $comment = $em->getRepository('ArtworkBundle:Comment')->findOneBy(array('id' => intval($request->get('comment'))));
                $replay->setContent($replayData);
                $replay->setComment($comment);
                $replay->setDate(new \DateTime());
                $replay->setArtwork($artwork);
                $replay->setUser($this->getUser());
                $em->persist($replay);
                $em->flush();
            }
            return $this->redirectToRoute('artwork_show', array('id' => $artwork->getId()));
        }

        $liker = false;
        if ($this->getUser()) {
            if ($artwork->getLikers()->contains($user)) {
                $liker = true;
            }
        }

        if ($user) {
            $notification = $this->get('app_notification_services');
            $notification->removeNotificationArtwork($user, $artwork);
        }

        return $this->render('artwork/showme.html.twig', array(
                    'artwork' => $artwork,
                    'delete_form' => $deleteForm->createView(),
                    'user' => $user,
                    'liker' => $liker,
                    'artwork_authors' => $artwork_authors,
                    'commentsForm' => $commentsForm->createView(),
                    'replyForm' => $form_reply->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing artwork entity.
     * @param Request $request
     * @param Artwork $artwork to show
     * @return Artwork $artwork to show
     * @return \Symfony\Component\Form\Form $deleteForm Form of delete
     * @return \Symfony\Component\Form\Form $editForm Form of edition
     * @Route("/{id}/edit", name="artwork_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Artwork $artwork) {
        $deleteForm = $this->createDeleteForm($artwork);
        $editForm = $this->createForm('ArtworkBundle\Form\ArtworkType', $artwork);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('artwork_edit', array('id' => $artwork->getId()));
        }

        return $this->render('artwork/edit.html.twig', array(
                    'artwork' => $artwork,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * We get the artwork of an user
     * @param  Integer $id the user's we want to get artwork
     * @return Artwork $artworks the artworks' user
     * @Route("/{id}/artworks", name="general_profile_artworks")
     */
    public function ArtworksAction($id) {
        $em = $this->getDoctrine()->getManager();
        $artworks = $em->getRepository('ArtworkBundle:Artwork')->getUserArtworks($id);

        return $this->render('default/artworks.html.twig', array(
                    'artworks' => $artworks
        ));
    }

    /**
     * Deletes a artwork entity.
     * @param Request $request
     * @param Artwork $artwork to show
     * @return Link to artwork_index
     * @Route("/{id}", name="artwork_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Artwork $artwork) {

        $form = $this->createDeleteForm($artwork);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($artwork);
            $em->flush($artwork);
        }

        return $this->redirectToRoute('artwork_index');
    }

    /**
     * Creates a form to delete a artwork entity.
     *
     * @param Artwork $artwork The artwork entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Artwork $artwork) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('artwork_delete', array('id' => $artwork->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * Get ArtWorkByFilter
     * 
     * @param Request $request
     * @return Artwork $artworks
     * @Route("/ajax-list", name="artwork_list")
     * @Method("POST")
     * 
     */
    public function listArtWorkByFilter(Request $request) {
        $idCategory = $request->get('category');
        $aSubCategory = $request->get('sub_category');
        $sOrder = '' != $request->get('order') ? $request->get('order') : 'id';
        $em = $this->getDoctrine()->getManager();
        $artWorks = $em->getRepository('ArtworkBundle:Artwork')->listArtWorkbyFilter($idCategory, $aSubCategory, null, $sOrder);

        return $this->render('@Artwork/Artwork/ajax-list.html.twig', array(
                    'artworks' => $artWorks
        ));
    }

    /**
     * like artwork
     * @param  Artwork $artwork
     * @Route("/likemanager/{id}", name="artwork_like")
     * @Method({"POST"})
     */
    public function manageLikeAction(Artwork $artwork) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $check = false;

        if ($user) {
            if (!$artwork->getLikers()->contains($user)) {
                $artwork->addLiker($user);
                $check = true;
            } else {
                $artwork->removeLiker($user);
            }
        }
        if ($check) {
            $follow = '<i class="fa fa-thumbs-up"></i>' . $this->get('translator')->trans('global.already_like');
            $followCount = '<i class="fa fa-thumbs-up blue-style-1"></i> ' . count($artwork->getLikers());
        } else {
            $follow = '<i class="fa fa-thumbs-o-up"></i>' . $this->get('translator')->trans('global.like');
            $followCount = '<i class="fa fa-thumbs-o-up"></i> ' . count($artwork->getLikers());
        }
        $em->flush();

        return new JsonResponse([
            'status' => $check,
            'follow' => $follow,
            'followCount' => $followCount,
            'addClass' => $check ? 'like-manager-yes' : 'like-manager-no',
            'removeClass' => !$check ? 'like-manager-yes' : 'like-manager-no'
        ]);
    }


    /**
     * remove my artwork
     * @Route("/{id}/remove-my-artwork", name="remove_my_artwork")
     * @Method({"GET", "DELETE"})
     */
    public function removeMyArtworkAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ArtworkBundle:Artwork')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Artwork entity.');
        }

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('remove_my_artwork', array('id' => $id)))
                ->setMethod('DELETE')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($form->isValid()) {
            $em->remove($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('administration.reports.text.remove_artwork_success'));
            return $response;
        } else {
            $content = $this->renderView('ArtworkBundle:Artwork:delete.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.button.remove_artwork'),
                'button' => $this->get('translator')->trans('global.delete'),
                'content' => $content
                    ])
            );
            return $response;
        }
    }

}
