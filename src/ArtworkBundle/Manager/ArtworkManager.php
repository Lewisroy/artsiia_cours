<?php

namespace ArtworkBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpFoundation\RequestStack;


/**
 * Class ArtworkManager
 * @package ArtworkBundle\Manager
 */
class ArtworkManager extends BaseManager
{
    const REPOSITORY_TYPE = 'ArtworkBundle:Artwork' ;

    /**
     * PictureManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, Kernel $kernel, RequestStack $requestStack)
    {
        parent::__construct($entityManager, $kernel, $requestStack) ;
    }
}