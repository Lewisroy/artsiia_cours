<?php

namespace ArtworkBundle\Manager;

use UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Kernel;
use Doctrine\ORM\ORMException;
use Symfony\Component\HttpFoundation\RequestStack;
use ArtworkBundle\Entity\BaseEntity;


/**
 * Class BaseManager
 * @package ArtworkBundle\Manager
 */
abstract class BaseManager
{
    /**
     * @var REPOSITORY_TYPE
     */
    const REPOSITORY_TYPE = null ;

    /**
     * @var EntityManager $entityManager
     */
    protected static $entityManager;

    /**
     * @var Kernel $kernel
     */
    protected static $kernel;

    /**
     * @var RequestStack $requestStack
     */
    protected static $requestStack;

    /**
     * BaseManager constructor.
     * @param EntityManager $entityManager
     * @param Kernel $kernel
     */
    public function __construct(EntityManager $entityManager, Kernel $kernel, RequestStack $requestStack)
    {
        self::$entityManager = $entityManager;
        self::$kernel        = $kernel ;
        self::$requestStack  = $requestStack->getCurrentRequest() ;
    }

    /**
     * Save object.
     *
     * @param BaseEntity $entity
     * @throws ORMException
     */
    public static function save (BaseEntity $entity)
    {
        try {
            self::$entityManager->persist($entity);
            self::$entityManager->flush();
        } catch ( ORMException $e ) {
            throw $e ;
        }
    }

    /**
     * Find object.
     *
     * @param  $id
     * @return object
     */
    public function find($id)
    {
        return $this
            ->getRepository()
            ->find($id);
    }

    /**
     * Find all object.
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this
            ->getRepository()
            ->findAll();
    }

    /**
     * @param $id
     * @return null|object
     */
    public function findOneBy($id)
    {
        return $this
            ->getRepository()
            ->findOneBy(array('id' => $id));
    }

    /**
     * Load by.
     *
     * @param  array $params
     * @return null|object
     */
    public function findBy($params = array())
    {
        return $this
            ->getRepository()
            ->findBy($params);
    }

    /**
     * @param array $options
     * @param array $filters
     * @return mixed
     */
    public function countBy($options = array(), $filters = array())
    {
        $query = $this->getRepository()->getNbQueryBuilder($options);

        return $query->getQuery()->getSingleResult() ;
    }

    /**
     * @param $options
     * @param array $filters
     * @param array $sortings
     * @return mixed
     */
    public function findByCriteria( $options, $filters = array(), $sortings = array() )
    {
        $query = $this->getRepository()->findByCriteriaQueryBuilder($options, $this->getUser());
        $query = $this->getRepository()->addSortings($query, $sortings);

        if ($filters != null) {
            $query = $this->getRepository()->addLimit($query, $filters);
        }

        return $query->getQuery();
    }

    /**
     * @return mixed
     */
    public function getOneOrNullResult()
    {
        return $this
            ->getQueryBuilder()
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Query::HYDRATE_OBJECT
     * Query::HYDRATE_ARRAY
     * Query::HYDRATE_SCALAR
     * Query::HYDRATE_SINGLE_SCALAR
     */
    public function getSingleResult($hydrate=\Doctrine\ORM\Query::HYDRATE_OBJECT)
    {
        return $this
            ->getQueryBuilder()
            ->getQuery()
            ->getSingleResult($hydrate);
    }

    /**
     * Query::HYDRATE_OBJECT
     * Query::HYDRATE_ARRAY
     * Query::HYDRATE_SCALAR
     * Query::HYDRATE_SINGLE_SCALAR
     */
    public function getResult($hydrate=\Doctrine\ORM\Query::HYDRATE_OBJECT)
    {
        return $this
            ->getQueryBuilder()
            ->getQuery()
            ->getResult($hydrate);
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return self::$entityManager->getRepository($this::REPOSITORY_TYPE) ;
    }


    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     */
    protected function getUser()
    {
        if (!self::$kernel->getContainer()->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = self::$kernel->getContainer()->get('security.token_storage')->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

}