<?php

namespace ArtworkBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;

class ArtworkType extends AbstractType {

    private $entityManager = null;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->entityManager = $options['entityManager'];
        $builder
                ->add('cover', FileType::class, array(
                    'constraints' => [
                        new File([
                            'maxSize' => '140M',
                                ])
                    ],
                    'label' => 'Upload_file',
                    'required' => true,
                    'data_class' => null,
                    'attr' => array('accept' => 'image/*')
                ))
                ->add('file', FileType::class, array('constraints' => [
                        new File([
                            'maxSize' => '140M',
                                ])
                    ], 'required' => false, 'label' => "Upload_file", 'data_class' => null))
                ->add('title', TextType::class, array('label' => 'global.array.title', 'required' => true))
                ->add('confidential', ChoiceType::class, array('label' => 'artwork.confidential', 'required' => true,
                    'choices' => array(
                        'Public' => 'Public',
                        'Subscribers' => 'Subscribers',
                        'Friends' => 'Friends'
            )))
                ->add('description', TextareaType::class, array('label' => 'global.array.description', 'required' => true))
                ->add('text', CKEditorType::class, array(
                    'label' => ' ',
                    'required' => false,
                    'config' => array(
                        'uiColor' => '#ffffff',
                        'toolbar' => array(
                            array(
                                'name' => 'basicstyles',
                                'items' => array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo', 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor', 'Maximise')
                            )),
                        'height' => '280px',
                        'removeButtons' => 'elementspath',
                    )
                ))
                ->add('category', EntityType::class, array(
                    'label' => 'faq.category',
                    'class' => 'LectureBundle\Entity\Category',
                    'choice_label' => 'name',
                    'multiple' => false,
                    'expanded' => false,
                    'required' => true
                ))
                ->add('sub_category', EntityType::class, array(
                    'label' => 'subCategory.title',
                    'class' => 'LectureBundle\Entity\SubCategory',
                    'choice_label' => 'name',
                    'placeholder' => 'subCategory.choice',
                    'multiple' => true,
                    'required' => false
                ))
                ->addEventListener(
                        FormEvents::PRE_SET_DATA, array($this, 'onPreSetData')
                )
                ->addEventListener(
                        FormEvents::PRE_SUBMIT, array($this, 'onSubmit')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ArtworkBundle\Entity\Artwork',
            'entityManager' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'artworkbundle_artwork';
    }

    /**
     * @param FormEvent $event
     */
    function onPreSetData(FormEvent $event) {
        $form = $event->getForm();
        // This will be the Artwork entity
        $data = $event->getData();

        $aCategories = $data->getCategory();
        $this->addElements($form, $aCategories);
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event) {
        $form = $event->getForm();

        $data = $event->getData();

        $aCategories = $data['category'];
        $this->addElements($form, $aCategories);
    }

    /**
     * @param FormInterface $form
     * @param $aCategories
     */
    private function addElements(FormInterface $form, $aCategories) {
        $aSubCategory = array();
        if (count($aCategories)) {
            $aSubCategory = $this->entityManager->getRepository('LectureBundle:SubCategory')->listByCategorie($aCategories);
        }
    }

}
