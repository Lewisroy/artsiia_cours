<?php

namespace AppBundle\Controller;

use AdminBundle\Entity\LegalAgreement;
use AdminBundle\Entity\Page;
use AdminBundle\Entity\ValidationChart;
use AdminBundle\Entity\PollAdmin;
use UserBundle\Entity\Message;
use function GuzzleHttp\Psr7\build_query;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {

    /**
     * @return  Root of the website, which redirect on /{_locale} 
     * @Route("/", name="au_general_homepage")
     */
    public function indexAction() {
        return $this->redirectToRoute('au_homepage');
    }

    /**
     * @return Artwork $artWorks the artworks to display 
     * @return Category $categories categories of the filters
     * @return Subcategory $subcategories subcategories for the filters
     * \Configuration\Route return the Discover Page
     * @Route("/{_locale}/", name="au_homepage")
     */
    public function homeAction() {
        $em = $this->getDoctrine()->getManager();
        // utilisateur connecté.
        $user = $this->getUser();
        $messages = array();

        if (null !== $user) {
            // recupération de tous les messages non lu.
            $messages = $em->getRepository('UserBundle:Message')->messagesNotRead($user->getId());
        }


        $artWorks = $em->getRepository('ArtworkBundle:Artwork')->showMoreArtworks(0, 24);
        $aCategory = $em->getRepository('LectureBundle:Category')->findAll();
        $aSubCategory = $em->getRepository('LectureBundle:SubCategory')->findAll();


        return $this->render('@Artwork/Default/indexForest.html.twig', array(
                    'artworks' => $artWorks,
                    'categories' => $aCategory,
                    'subCategories' => $aSubCategory,
                    'messages' => $messages,
        ));
    }

    /**
     * search
     * @Route("/{_locale}/search", name="search_artworks")
     * @Method({"GET", "POST"})
     */
    public function searchAction(Request $request) {

        if ($request->getMethod() != "POST") {
            return $this->redirectToRoute('au_homepage');
        }
        $tag = $request->request->get('tag');
        $category = $request->request->get('category');
        $sort = $request->request->get('sort');
        $subcategory = $request->request->get('subcategory');
        $scope = $request->request->get('scope');

        $em = $this->getDoctrine()->getManager();
        // utilisateur connecté.
        $user = $this->getUser();
        $messages = array();

        if (null !== $user) {
            // recupération de tous les messages non lu.
            $messages = $em->getRepository('UserBundle:Message')->messagesNotRead($user->getId());
        }

        $friends = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFriends() as $friend) {
                array_push($friends, $friend->getId());
            }
        }
        $followings = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFollowings() as $following) {
                array_push($followings, $following->getId());
            }
        }
        $data_scope = [
            'friends' => $friends,
            'followings' => $followings
        ];
        $artWorks = $em->getRepository('ArtworkBundle:Artwork')->showMoreSearchArtworks(0, 24, $tag, $category, $sort, $subcategory, $scope, $data_scope);

        $aCategory = $em->getRepository('LectureBundle:Category')->findAll();
        $aSubCategory = [];
        if ($category != null) {
            $aSubCategory = $em->getRepository('LectureBundle:SubCategory')->getSubCategoriesByCateg($category);
        }
        return $this->render('@Artwork/Default/search.html.twig', array(
                    'artworks' => $artWorks,
                    'categories' => $aCategory,
                    'subCategories' => $aSubCategory,
                    'messages' => $messages,
                    'tag' => $tag,
                    'category' => $category,
                    'sort' => $sort,
                    'subcategory' => $subcategory,
                    'scope' => $scope,
        ));
    }

    /**
     * @Route("/{_locale}/show-more-artworks", name="show_more_artworks")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMoreArtworksAction(Request $request) {
        $first = $request->request->get('first');
        $em = $this->getDoctrine()->getManager();
        $artWorks = $em->getRepository('ArtworkBundle:Artwork')->showMoreArtworks($first, 24);
        return new JsonResponse($artWorks);
    }

    /**
     * @Route("/{_locale}/show-more-artworks-search", name="show_more_artworks_search")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMoreArtworksSearchAction(Request $request) {
        $first = $request->request->get('first');
        $tag = $request->request->get('tag');
        $category = $request->request->get('category');
        $sort = $request->request->get('sort');
        $subcategory = $request->request->get('subcategory');
        $scope = $request->request->get('scope');

        $friends = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFriends() as $friend) {
                array_push($friends, $friend->getId());
            }
        }
        $followings = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFollowings() as $following) {
                array_push($followings, $following->getId());
            }
        }
        $data_scope = [
            'friends' => $friends,
            'followings' => $followings
        ];

        $em = $this->getDoctrine()->getManager();
        $artWorks = $em->getRepository('ArtworkBundle:Artwork')->showMoreSearchArtworks($first, 24, $tag, $category, $sort, $subcategory, $scope, $data_scope);

        $data = [];
        foreach ($artWorks as $artWork) {
            array_push($data, $em->getRepository('ArtworkBundle:Artwork')->findToSearchArtworks($artWork[0]->getId()));
        }
        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @return Text $privacyPolicy the Privacy Policy to display
     * @return Text $disclaimer the Disclaimer to display
     * @Route("/{_locale}/legals", name="general_legal_agreement")
     */
    public function LegalAgreementAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $locale = $request->getLocale();

        $legals = $em->getRepository('AdminBundle:LegalAgreement')->findBy(array(), null, 1);
        if (empty($legals)) {
            $legal = new LegalAgreement();
            $legal->setPrivacyPolicy("");
            $legal->setPrivacyPolicyEn("");
            $legal->setDisclaimer("");
            $legal->setDisclaimerEN("");
            $em->persist($legal);
            $em->flush();
            $legals[] = $legal;
        }
        if (strtolower($locale) == 'fr') {
            $legal = $legals[0];
            $privacyPolicy = $legal->getPrivacyPolicy();
            $disclaimer = $legal->getDisclaimer();
        } else {
            $legal = $legals[0];
            $privacyPolicy = $legal->getPrivacyPolicyEn();
            $disclaimer = $legal->getDisclaimerEN();
        }
        return $this->render('default/legals.html.twig', array(
                    'privacyPolicy' => $privacyPolicy,
                    'disclaimer' => $disclaimer
        ));
    }

    /**
     * We want to print the Page "About"
     * @param Request $request
     * @return Page $page The page we want to display
     * @Route("/{_locale}/about", name="au_about")
     */
    public function aboutAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AdminBundle:Page')->findOneBy(array('name' => 'A propos'));
        return $this->render('default/page.html.twig', array('page' => $page));
    }

    /**
     * We want to print the Page "Help"
     * @param Request $request
     * @return Page $page The page we want to display
     * @Route("/{_locale}/help", name="au_help")
     */
    public function helpAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AdminBundle:Page')->findOneBy(array('name' => 'Aide'));
        return $this->render('default/page.html.twig', array('page' => $page));
    }

    /**
     * We want to print the Page "FAQ"
     * @param Request $request
     * @return Page $page The page we want to display
     * @Route("/{_locale}/faq", name="au_faq")
     */
    public function faqAction(Request $request) {
        return $this->render('faq/show.html.twig');
    }

    /**
     * We want to make a Validation Chart
     * @param Request $request
     * @return Page $page The page we want to display
     * @Route("/{_locale}/validation-chart", name="general_validation chart")
     */
    public function ValidationChartAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $locale = $request->getLocale();

        $charts = $em->getRepository('AdminBundle:ValidationChart')->findBy(array(), null, 1);
        if (empty($charts)) {
            $chart = new ValidationChart();
            $chart->setChart("");
            $chart->setChartEn("");
            $em->persist($chart);
            $em->flush();
            $charts[] = $chart;
        }
        if (strtolower($locale) == 'fr') {
            $chart = $charts[0];
            $privacyPolicy = $chart->getChart();
        } else {
            $chart = $charts[0];
            $privacyPolicy = $chart->getChartEn();
        }
        return $this->render('default/legals.html.twig', array(
                    'privacyPolicy' => $privacyPolicy,
        ));
    }

    /**
     * Agree Policy
     * @Route("/{_locale}/agree-policy", name="agree_policy")
     * @Method({"GET","POST"})
     */
    public function agreePolicyAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $legals = $em->getRepository('AdminBundle:LegalAgreement')->findAll();
        $legals = $legals[0];

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('agree_policy'))
                ->setMethod('POST')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid()) {
            $user->setAgreePrivacyPolicy(false);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('agree_use_success'));
            return $response;
        } else {
            $content = $this->renderView('AppBundle:Agree:Policy.html.twig', array(
                'legals' => $legals,
                'form' => $form->createView(),
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.privacy_policy'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                'error' => true
                    ])
            );
            return $response;
        }
    }
    
    /**
     * Agree Disclaimer
     * @Route("/{_locale}/agree-disclaimer", name="agree_disclaimer")
     * @Method({"GET","POST"})
     */
    public function agreeDisclaimerAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $legals = $em->getRepository('AdminBundle:LegalAgreement')->findAll();
        $legals = $legals[0];

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('agree_disclaimer'))
                ->setMethod('POST')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid()) {
            $user->setAgreeDisclaimer(false);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('agree_use_success'));
            return $response;
        } else {
            $content = $this->renderView('AppBundle:Agree:Disclaimer.html.twig', array(
                'legals' => $legals,
                'form' => $form->createView(),
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.disclaimer'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                'error' => true
                    ])
            );
            return $response;
        }
    }

}
