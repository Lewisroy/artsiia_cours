<?php

namespace AppBundle\Services;

use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Comment;
use UserBundle\Entity\User;
use UserBundle\Entity\Notification;
use ArtworkBundle\Entity\Artwork;
use UserBundle\Entity\Activity;

class NotificationServices {

    private $em;

    public function __construct($entityManager) {
        $this->em = $entityManager;
    }

    public function sendNotificationArtwork(Comment $comment, Artwork $artwork) {

        foreach ($artwork->getAuthor() as $author) {
            $notification = new Notification();
            $notification->setDateCreation(new \Datetime());
            $notification->setIsView(false);
            $notification->setAskingFriend(false);
            $notification->setFromUser($comment->getUser());
            $notification->setToUser($author);
            $notification->setArtworkComment($artwork);
            if ($comment->getUser()->getId() != $author->getId()) {
                $this->em->persist($notification);
                $this->em->flush($notification);
            }
        }
    }

    public function removeNotificationArtwork(User $user, Artwork $artwork) {

        $artworks = $this->em->getRepository('UserBundle:Notification')->findNotificationArtwork($user->getId(), $artwork->getId());
        foreach ($artworks as $art) {
            $art->setIsView(true);
        }
        $this->em->flush();
    }

    public function sendNotificationLecture(Comment $comment, Lecture $lecture) {
        foreach ($lecture->getAuthor() as $author) {
            $notification = new Notification();
            $notification->setDateCreation(new \Datetime());
            $notification->setIsView(false);
            $notification->setAskingFriend(false);
            $notification->setFromUser($comment->getUser());
            $notification->setToUser($author);
            $notification->setLectureComment($lecture);
            if ($comment->getUser()->getId() != $author->getId()) {
                $this->em->persist($notification);
                $this->em->flush($notification);
            }
        }
    }

    public function sendNotificationActivity(Comment $comment, Activity $activity) {
        $notification = new Notification();
        $notification->setDateCreation(new \Datetime());
        $notification->setIsView(false);
        $notification->setAskingFriend(false);
        $notification->setFromUser($comment->getUser());
        $notification->setToUser($activity->getAuthor()[0]);
        $notification->setActivityComment($activity);
        if ($comment->getUser()->getId() != $activity->getAuthor()[0]->getId()) {
            $this->em->persist($notification);
            $this->em->flush($notification);
        }
    }

    public function removeNotificationActivity(User $user) {

        $activitys = $this->em->getRepository('UserBundle:Notification')->findNotificationActivity($user->getId());
        foreach ($activitys as $activity) {
            $activity->setIsView(true);
        }
        $this->em->flush();
    }

    public function SendNotificationSubscribers(User $from, User $to) {

        if ($from->getId() == $to->getId())
            return false;

        $notifs = $this->em->getRepository('UserBundle:Notification')->getNotifSubscribersExist($to->getId());

        if (count($notifs) == 0) {

            $notification = new Notification();
            $notification->setDateCreation(new \Datetime());
            $notification->setIsView(false);
            $notification->setAskingFriend(false);
            $notification->setFromUser($from);
            $notification->setToUser($to);
            $notification->setNewSubscribers(1);
            $this->em->persist($notification);
            $this->em->flush($notification);
        } else {

            $notif = $notifs[0];
            $notif->setNewSubscribers($notif->getNewSubscribers() + 1);
            $notif->setDateCreation(new \Datetime());
            $this->em->persist($notif);
            $this->em->flush($notif);
        }
    }

    public function SendNotificationFriendRequest(User $from, User $to) {

        if ($from->getId() == $to->getId())
            return false;

        $notifs = $this->em->getRepository('UserBundle:Notification')->getNotifFriendRequestExist($to->getId());

        if (count($notifs) == 0) {

            $notification = new Notification();
            $notification->setDateCreation(new \Datetime());
            $notification->setIsView(false);
            $notification->setAskingFriend(false);
            $notification->setFromUser($from);
            $notification->setToUser($to);
            $notification->setNewFriendRequest(1);
            $this->em->persist($notification);
            $this->em->flush($notification);
        } else {

            $notif = $notifs[0];
            $notif->setNewFriendRequest($notif->getNewFriendRequest() + 1);
            $notif->setDateCreation(new \Datetime());
            $this->em->persist($notif);
            $this->em->flush($notif);
        }
    }

    public function removeNotificationSubscribers(User $from, User $to) {

        if ($from->getId() == $to->getId())
            return false;

        $notifs = $this->em->getRepository('UserBundle:Notification')->getNotifSubscribersExist($to->getId());

        if (count($notifs) != 0) {

            $notif = $notifs[0];
            if (($notif->getNewSubscribers() - 1) > 0) {
                $notif->setNewSubscribers($notif->getNewSubscribers() - 1);
                $this->em->persist($notif);
            } else {
                $this->em->remove($notif);
            }
            $this->em->flush();
        }
    }

    public function removeNotificationFriendRequest(User $from, User $to) {

        if ($from->getId() == $to->getId())
            return false;

        $notifs = $this->em->getRepository('UserBundle:Notification')->getNotifFriendRequestExist($to->getId());

        if (count($notifs) != 0) {

            $notif = $notifs[0];
            if (($notif->getNewFriendRequest() - 1) > 0) {
                $notif->setNewFriendRequest($notif->getNewFriendRequest() - 1);
                $this->em->persist($notif);
            } else {
                $this->em->remove($notif);
            }
            $this->em->flush();
        }
    }

    public function viewNotificationFriendRequest(User $user) {

        $notifs = $this->em->getRepository('UserBundle:Notification')->getNotifFriendRequestExist($user->getId());

        if (count($notifs) != 0) {

            $notif = $notifs[0];
            $notif->setNewFriendRequest(0);
            $notif->setIsView(true);
            $this->em->persist($notif);
            $this->em->flush();
        }
    }

    public function viewNotificationSubscribers(User $user) {

        $notifs = $this->em->getRepository('UserBundle:Notification')->getNotifSubscribersExist($user->getId());

        if (count($notifs) != 0) {

            $notif = $notifs[0];
            $notif->setNewSubscribers(0);
            $notif->setIsView(true);
            $this->em->persist($notif);
            $this->em->flush();
        }
    }

    public function sendNotificationAskingFriend(User $from_user, User $to_user) {
        $notification = new Notification();
        $notification->setDateCreation(new \Datetime());
        $notification->setIsView(false);
        $notification->setAskingFriend(true);
        $notification->setFromUser($from_user);
        $notification->setToUser($to_user);
        $this->em->persist($notification);
        $this->em->flush();
    }

    public function checkNotifAskingFriend(User $to_user, User $from_user) {

        $notifs = $this->em->getRepository('UserBundle:Notification')->findNotifAskingFriend($to_user->getId(), $from_user->getId());
        foreach ($notifs as $notif) {
            $notif->setIsView(true);
        }
        $this->em->flush();
    }

}
