<?php

namespace AppBundle\Services;

use MangoPay;

class MangoPayServices
{

	private $mangoPayApi;

	public function __construct()
	{
		$this->mangoPayApi = new MangoPay\MangoPayApi();
		$this->mangoPayApi->Config->ClientId = 'artsiia';
		$this->mangoPayApi->Config->ClientPassword = 'aDYU40tyF9FSb7ZTXDzbp3938BQY3skJSBfx82U693f7geQ512';
		if (!file_exists('../var/cache/dev/mangopay/')){
            mkdir("../var/cache/dev/mangopay/", 0700);
        }
		$this->mangoPayApi->Config->TemporaryFolder = '../var/cache/dev/mangopay/';
	}

    /**
     * Get mangopay user
     * @param $userId
     * @return MangoPay\UserLegal|MangoPay\UserNatural|string
     */
    public function getMangoUser($userId){
        try {

            $user = $this->mangoPayApi->Users->Get($userId);

            return $user;

        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Erreur : ".$e->getMessage();
        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    /**
     * Create Mangopay User
     * @param $firstName
     * @param $lastName
     * @param $birthday
     * @param $nationality
     * @param $country
     * @param $email
     * @return MangoPay\UserLegal|MangoPay\UserNatural|string
     */
    public function createMangoUser($firstName, $lastName, $birthday, $nationality, $country, $email)
    {

        try {
            $userNatural = new MangoPay\UserNatural();
            $userNatural->FirstName = $firstName;
            $userNatural->LastName = $lastName;
            $userNatural->Birthday = $birthday;
            $userNatural->Nationality = $nationality;
            $userNatural->CountryOfResidence = $country;
            $userNatural->Email = $email;

            $result = $this->mangoPayApi->Users->Create($userNatural);

            return $result;
        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error response : ".$e->getMessage();

        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    /**
     * Get mangopay wallet user
     * @param $walletId
     * @return MangoPay\Wallet|string
     */
    public function getWalletUser($walletId){
        try {
            $user = $this->mangoPayApi->Wallets->Get($walletId);

            return $user;

        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error : ".$e->getMessage()."\n".
                "Details : ".$e->getErrorDetails();
        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    /**
     * Create Wallet User
     * @param $ownerId
     * @param $description
     * @return MangoPay\Wallet|string
     */
    public function createWalletUser($ownerId, $description){
        try {
            $Wallet = new MangoPay\Wallet();
            $Wallet->Owners = array($ownerId);
            $Wallet->Description = "Wallet of " . $description;
            $Wallet->Currency = "EUR";

            $result = $this->mangoPayApi->Wallets->Create($Wallet);

            return $result;

        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error response : ".$e->getMessage();

        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    /**
     * Get all mangopay users
     * @return array|string
     */
    public function getAllUsers(){
        try {

            $result = $this->mangoPayApi->Users->GetAll();

            return $result;

        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error response : ".$e->getMessage();

        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    /**
     * Do a transfer between 2 wallets
     * @param $authorId
     * @param $amount
     * @param $idWalletDebited
     * @param $idWalletCredited
     * @return MangoPay\Transfer|string
     */
    public function doTransfer($authorId, $amount, $idWalletDebited, $idWalletCredited){
        try {
            $transfer = new MangoPay\Transfer();
            $transfer->AuthorId = $authorId;
            $transfer->DebitedFunds = new MangoPay\Money();
            $transfer->DebitedFunds->Currency = "EUR";
            $transfer->DebitedFunds->Amount = $amount;
            $transfer->Fees = new MangoPay\Money();
            $transfer->Fees->Currency = "EUR";
            $transfer->Fees->Amount = 0;
            $transfer->DebitedWalletId = $idWalletDebited;
            $transfer->CreditedWalletId = $idWalletCredited;

            $result = $this->mangoPayApi->Transfers->Create($transfer);

            return $result;

        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error : ".$e->getMessage()."\n".
                "Details : ".$e->getErrorDetails();
        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    public function createPayInCard($mangoUserId, $mangoWalletId, $amount, $userId){
        try {

            $payIn = new MangoPay\PayIn();
            $payIn->CreditedWalletId = $mangoWalletId;
            $payIn->AuthorId = $mangoUserId;
            $payIn->PaymentType = "CARD";
            $payIn->PaymentDetails = new MangoPay\PayInPaymentDetailsCard();
            $payIn->PaymentDetails->CardType = "CB_VISA_MASTERCARD";
            $payIn->DebitedFunds = new MangoPay\Money();
            $payIn->DebitedFunds->Currency = "EUR";
            $payIn->DebitedFunds->Amount = $amount;
            $payIn->Fees = new MangoPay\Money();
            $payIn->Fees->Currency = "EUR";
            $payIn->Fees->Amount = 0;
            $payIn->ExecutionType = "WEB";
            $payIn->ExecutionDetails = new MangoPay\PayInExecutionDetailsWeb();
            $payIn->ExecutionDetails->ReturnURL = "http".(isset($_SERVER['HTTPS']) ? "s" : null)."://".$_SERVER["HTTP_HOST"]."/fr/user/".$userId."/profile";
            $payIn->ExecutionDetails->Culture = "FR";

            $result = $this->mangoPayApi->PayIns->Create($payIn);

            return $result;
        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error : ".$e->getMessage()."\n".
                "Details : ".$e->getErrorDetails();
        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

    public function createPayOut($mangoUserId, $mangoWalletId, $amount){
        try {

            $payOut = new MangoPay\PayOut();
            $payOut->AuthorId = $mangoUserId;
//            $payOut->DebitedWalletID = $mangoWalletId;
            $payOut->DebitedFunds = new MangoPay\Money();
            $payOut->DebitedFunds->Currency = "EUR";
            $payOut->DebitedFunds->Amount = $amount;
            $payOut->Fees = new MangoPay\Money();
            $payOut->Fees->Currency = "EUR";
            $payOut->Fees->Amount = 0;
            $payOut->PaymentType = "BANK_WIRE";
            $payOut->MeanOfPaymentDetails = new MangoPay\PayOutPaymentDetailsBankWire();
            $payOut->MeanOfPaymentDetails->BankAccountId = $_SESSION["MangoPayDemo"]["BankAccount"];
            $result = $this->mangoPayApi->PayOuts->Create($payOut);

            return $result;
        } catch(MangoPay\Libraries\ResponseException $e) {
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
            return "Error : ".$e->getMessage()."\n".
                "Details : ".$e->getErrorDetails();
        } catch(MangoPay\Libraries\Exception $e) {
            // handle/log the exception $e->GetMessage()
            return "Exception : ".$e->getMessage();
        }
    }

}