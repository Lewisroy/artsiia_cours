<?php 

namespace AppBundle\Services;

use UserBundle\Entity\User;


class UserServices
{
    private $em;

    public function __construct ($entityManager) {
        $this->em = $entityManager;
    }

    public function getLinkUser(User $me, User $userLink)
    {
        if($me == null)
            return 'Public';
        
        if($me->getId() == $userLink->getId())
            return 'Me';

        if($this->em->getRepository('UserBundle:User')->isFriend($me, $userLink) )
            return 'Friends';

        if($this->em->getRepository('UserBundle:User')->isFollower($me, $userLink) )
            return 'Subscribers';

        return 'Public';
    }
}