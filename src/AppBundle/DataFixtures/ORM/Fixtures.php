<?php

namespace AppBundle\DataFixtures\ORM;

use ReportBundle\Entity\Reporting;
use ArtworkBundle\Entity\Artwork;
use ReportBundle\Entity\Subjects;
use AdminBundle\Entity\Article;
use LectureBundle\Entity\Category;
use LectureBundle\Entity\SubCategory;
use UserBundle\Entity\Activity;
use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Chapter;
use LectureBundle\Entity\Part;
use LectureBundle\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Output\ConsoleOutput;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Fixtures implements FixtureInterface, ContainerAwareInterface {

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function load(ObjectManager $manager) {
        $faker = \Faker\Factory::create();
        /*         * ****************************************************** */
        /*         * ***************** Global Fixtures ******************** */
        /*         * ****************************************************** */

        /*         * ************* Categories & SubCategories ****************** */
        $listCategories = array('Musique', 'Art visuel', 'Architecture');
        for ($i = 0; $i < 3; $i++) {
            $category = new Category();
            $category->setName($listCategories[$i]);
            $category->setDescription($faker->text());
            // $category->setCover($faker->image($this->container->get('kernel')->getRootDir() . '/../web/' . 'uploads/category/', 640, 480, false, false));
            $category->setCover("".time().uniqid());

            $manager->persist($category);

            $subcategory = new SubCategory();
            $subcategory->setCategory($category);
            $subcategory->setName(substr($faker->text(), 0, 25));
            $subcategory->setDescription($faker->text());
            // $subcategory->setCover($faker->image($this->container->get('kernel')->getRootDir() . '/../web/' . 'uploads/subcategory/', 640, 480, false, false));
            $subcategory->setCover("".time().uniqid());

            $manager->persist($subcategory);
        }

        /*         * ***************************************** */
        $manager->flush();
        $categories = $manager->getRepository('LectureBundle:Category')->findAll();
        $subCategories = $manager->getRepository('LectureBundle:SubCategory')->findAll();

        /*         * ************* Users ****************** */
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setUsername('admin');
        $user->setUsernameCanonical($user->getUsername());
        $user->setEmail($faker->email());
        $user->setEmailCanonical($user->getEmail());
        $user->setPlainPassword('admin123');
        $user->setFirstname($faker->firstName());
        $user->setLastname($faker->lastName());
        $user->setRoles(array('ROLE_SUPER_ADMIN'));
        $user->addCategory($categories[mt_rand(1, count($categories)) - 1]);
        $user->setIsBanni(false);
        $userManager->updateUser($user, true);

        for ($i = 0; $i < 20; $i++) {
            $user = $userManager->createUser();
            $user->setEnabled(true);
            $user->setUsername('test_' . $faker->userName());
            $user->setUsernameCanonical($user->getUsername());
            $user->setEmail($faker->email());
            $user->setEmailCanonical($user->getEmail());
            $user->setPlainPassword('test123');
            $user->setFirstname($faker->firstName());
            $user->setLastname($faker->lastName());
            $user->setRoles(array('ROLE_USER'));
            $user->addCategory($categories[mt_rand(1, count($categories)) - 1]);
            $user->setIsBanni(false);

            $userManager->updateUser($user, true);
        }

        /*         * ************* Articles ****************** */
        for ($i = 0; $i < 10; $i++) {
            $article = new Article();
            $article->setName(substr($faker->text(), 0, 30));
            $article->setNameEn(substr($faker->text(), 0, 30));
            $article->setText($faker->text());
            $article->setTextEn($faker->text());
            $article->setDateOpening($faker->dateTimeBetween('-60 days', $timezone = date_default_timezone_get()));

            $manager->persist($article);
        }

        /*         * ***************************************** */
        $manager->flush();
        $users = $manager->getRepository('UserBundle:User')->findAll();

        /*         * ************* Artworks ****************** */
        for ($i = 0; $i < 100; $i++) {
            $artwork = new Artwork();
            $artwork->setTitle(substr($faker->text(), 0, 25));
            $artwork->setText($faker->text());
            $artwork->setDescription($faker->text());
            $artwork->setDateCreation($faker->dateTimeBetween('-30 days', $timezone = date_default_timezone_get()));
            $artwork->setConfidential($faker->randomElement($array = array('Public', 'Subscribers', 'Friends'), $count = 1));
            // $artwork->setCover($faker->image($this->container->get('kernel')->getRootDir() . '/../web/' . $this->container->getParameter('uploads_artwork_cover'), 640, 480, false, false));
            $artwork->setCover("".time().uniqid());
            // $artwork->setFile($faker->image($this->container->get('kernel')->getRootDir() . '/../web/' . $this->container->getParameter('uploads_artwork_file'), 640, 480, false, false));
            $artwork->setFile("".time().uniqid());
            $artwork->setCategory($categories[mt_rand(1, count($categories)) - 1]);
            $artwork->setSubCategory($subCategories[mt_rand(1, count($subCategories)) - 1]);
            $user = $users[mt_rand(1, count($users)) - 1];
            $artwork->addAuthor($user);

            $manager->persist($artwork);
            $user->addArtwork($artwork);
        }

        /*         * ************* Activity ****************** */
        for ($i = 0; $i < 100; $i++) {
            $activity = new Activity();
            $activity->setDescription($faker->text());
            $date = $faker->dateTimeBetween('-30 days', $timezone = date_default_timezone_get());
            $activity->setDateCreation($date);
            $activity->setUpdated($date);
            $activity->addAuthor($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($activity);
        }

        /*         * ************* Lectures ****************** */
        for ($i = 0; $i < 10; $i++) {
            $lecture = new Lecture();
            $lecture->setName(substr($faker->text(), 0, 25));
            $lecture->setDescription($faker->text());
            // $lecture->setCover($faker->image($this->container->get('kernel')->getRootDir() . '/../web/' . 'uploads/lecture/cover/', 640, 480, false, false));
            $lecture->setCover("".time().uniqid());
            $lecture->setStatut('Validate');
            $lecture->setLevel($faker->randomElement($array = array('beginner', 'intermediate', 'advanced', 'hard'), $count = 1));
            $lecture->setLanguage($faker->randomElement($array = array('FR', 'EN'), $count = 1));
            $lecture->setDate($faker->dateTimeBetween('-30 days', $timezone = date_default_timezone_get()));
            $lecture->setViews(0);
            $lecture->setNumberDownload(0);
            $lecture->setTags([]);
            $lecture->addCategory($categories[mt_rand(1, count($categories)) - 1]);
            $lecture->addAuthor($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($lecture);

            for ($j = 0; $j < 5; $j++) {
                $part = new Part();
                $part->setName($faker->name());
                $manager->persist($part);
                $lecture->addPart($part);
                for ($k = 0; $k < 10; $k++) {
                    $chapter = new Chapter();
                    $chapter->setName($faker->name());
                    $chapter->setChapterText($faker->text());
                    $manager->persist($chapter);
                    $part->addChapter($chapter);
                }
            }
        }

        /*         * ************* Friends & Followers ****************** */

        for ($i = 0; $i < count($users); $i++) {
            $current = $users[$i];
            $user = $users[$this->getRandomNumber($users, $i)];

            if ($current->getFriends() != null) {
                if (!in_array($user, $current->getFriends()))
                    $current->addFriend($user);
            }else {
                $current->addFriend($user);
            }

            if ($current->getFollowers() != null) {
                if (!in_array($user, $current->getFollowers()))
                    $current->addFollower($user);
            }else {
                $current->addFollower($user);
            }

            if ($current->getFollowings() != null) {
                if (!in_array($user, $current->getFollowings()))
                    $current->addFollowing($user);
            }else {
                $current->addFollowing($user);
            }

            if ($user->getFriends() != null) {
                if (!in_array($current, $user->getFriends()))
                    $user->addFriend($current);
            }else {
                $user->addFriend($current);
            }

            if ($user->getFollowers() != null) {
                if (!in_array($current, $user->getFollowers()))
                    $user->addFollower($current);
            }else {
                $user->addFollower($current);
            }

            if ($user->getFollowings() != null) {
                if (!in_array($current, $user->getFollowings()))
                    $user->addFollowing($current);
            }else {
                $user->addFollowing($current);
            }


            $manager->persist($current);
            $manager->persist($user);
        }

        /*         * ****************************************************** */
        /*         * ****************************************************** */

        /*         * ************* Subjects For Reporting ****************** */
        for ($i = 0; $i < 8; $i++) {
            $subject = new Subjects();
            $subject->setSubject(substr($faker->text(), 0, 25));
            $subject->setSeverity(mt_rand(1, 6));
            $manager->persist($subject);
        }
        $manager->flush();

        /*         * ************* Comments ****************** */
        $artworks = $manager->getRepository('ArtworkBundle:Artwork')->findAll();
        for ($i = 0; $i < count($artworks); $i++) {
            $artwork = $artworks[$i];
            $comment = new Comment();
            $comment->setContent($faker->text());
            $comment->setDate($faker->dateTimeBetween('-30 days', 'now', $timezone = date_default_timezone_get()));
            $comment->setArtwork($artwork);
            $comment->setUser($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($comment);
        }
        $lectures = $manager->getRepository('LectureBundle:Lecture')->findAll();
        for ($i = 0; $i < count($lectures); $i++) {
            $lecture = $lectures[$i];
            $comment = new Comment();
            $comment->setContent($faker->text());
            $comment->setDate($faker->dateTimeBetween('-30 days', 'now', $timezone = date_default_timezone_get()));
            $comment->setLecture($lecture);
            $comment->setUser($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($comment);
        }

        $manager->flush();

        /*         * ****************************************************** */
        /*         * ************** Fixtures For Reports ****************** */
        /*         * ****************************************************** */

        $output = new ConsoleOutput();
        $output->writeln([
            'Data Information',
            '============',
            '',
        ]);

        $subjects = $manager->getRepository('ReportBundle:Subjects')->findAll();
        $artworks = $manager->getRepository('ArtworkBundle:Artwork')->findAll();
        $comments = $manager->getRepository('LectureBundle:Comment')->findAll();
        $activitys = $manager->getRepository('UserBundle:Activity')->findAll();

        $output->writeln(sprintf('Total users: <comment>%s</comment>', count($users)));
        $output->writeln(sprintf('Total Subjects: <comment>%s</comment>', count($subjects)));
        $output->writeln(sprintf('Total ArtWorks: <comment>%s</comment>', count($artworks)));
        $output->writeln(sprintf('Total Publications: <comment>%s</comment>', count($activitys)));
        $output->writeln(sprintf('Total Comments: <comment>%s</comment>', count($comments)));

        $output->writeln([
            '',
            '============',
            '',
        ]);

        if (count($users) == 0 || count($subjects) == 0 || count($artworks) == 0 || count($activitys) == 0 || count($comments) == 0) {
            if (count($users) == 0)
                $output->writeln(sprintf('<comment>Add at least one <error>user</error> to continue</comment>'));

            if (count($subjects) == 0)
                $output->writeln(sprintf('<comment>Add at least one <error>subject</error> to continue</comment>'));

            if (count($artworks) == 0)
                $output->writeln(sprintf('<comment>Add at least one <error>artwork</error> to continue</comment>'));

            if (count($activitys) == 0)
                $output->writeln(sprintf('<comment>Add at least one <error>publication</error> to continue</comment>'));

            if (count($comments) == 0)
                $output->writeln(sprintf('<comment>Add at least one <error>comment</error> to continue</comment>'));
            $output->writeln('');
            $output->writeln(sprintf('<error>Solve the error to continue</error>'));
            exit();
        }
        // Report Artwork
        for ($i = 0; $i < 25; $i++) {
            $reporting = new Reporting();
            $mark = mt_rand(0, 1);
            $reporting->setMark($mark);
            $reporting->setType(($mark == 1) ? Reporting::TYPE_INPROGRESS : Reporting::TYPE_UNTREATED);
            $reporting->setCreated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setUpdated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setDescription($faker->text());
            $reporting->setSubject($subjects[mt_rand(1, count($subjects) - 1)]);
            $index = mt_rand(1, count($artworks) - 1);
            $reporting->setArtwork($artworks[$index]);
            $reporting->setReporteduser($artworks[$index]->getAuthor()[0]);
            $reporting->setReporter($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($reporting);
        }

        // Report Comment
        for ($i = 0; $i < 25; $i++) {
            $reporting = new Reporting();
            $mark = mt_rand(0, 1);
            $reporting->setMark($mark);
            $reporting->setType(($mark == 1) ? Reporting::TYPE_INPROGRESS : Reporting::TYPE_UNTREATED);
            $reporting->setCreated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setUpdated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setDescription($faker->text());
            $reporting->setSubject($subjects[mt_rand(1, count($subjects) - 1)]);
            $index = mt_rand(1, count($comments) - 1);
            $reporting->setComment($comments[$index]);
            $reporting->setReporteduser($comments[$index]->getUser());
            $reporting->setReporter($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($reporting);
        }

        // Report activitys
        for ($i = 0; $i < 25; $i++) {
            $reporting = new Reporting();
            $mark = mt_rand(0, 1);
            $reporting->setMark($mark);
            $reporting->setType(($mark == 1) ? Reporting::TYPE_INPROGRESS : Reporting::TYPE_UNTREATED);
            $reporting->setCreated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setUpdated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setDescription($faker->text());
            $reporting->setSubject($subjects[mt_rand(1, count($subjects) - 1)]);
            $index = mt_rand(1, count($activitys) - 1);
            $reporting->setActivity($activitys[$index]);
            $reporting->setReporteduser($activitys[$index]->getAuthor()[0]);
            $reporting->setReporter($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($reporting);
        }

        // Report Profil
        for ($i = 0; $i < 3; $i++) {
            $reporting = new Reporting();
            $mark = mt_rand(0, 1);
            $reporting->setMark($mark);
            $reporting->setType(($mark == 1) ? Reporting::TYPE_INPROGRESS : Reporting::TYPE_UNTREATED);
            $reporting->setCreated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setUpdated($faker->dateTimeBetween('-3 days', 'now', $timezone = date_default_timezone_get()));
            $reporting->setDescription($faker->text());
            $reporting->setSubject($subjects[mt_rand(0, count($subjects) - 1)]);
            $index = mt_rand(1, count($users) - 1);
            $user = $users[mt_rand(1, count($users)) - 1];
            $reporting->setProfile($user);
            $reporting->setReporteduser($user);
            $reporting->setReporter($users[mt_rand(1, count($users)) - 1]);

            $manager->persist($reporting);
        }

        $manager->flush();

        $output->writeln(sprintf('<question>The data is added successfully</question>'));
    }

    private function getRandomNumber($array, $index) {
        while (in_array(($n = mt_rand(1, count($array)) - 1), array($index)));

        return $n;
    }

}
