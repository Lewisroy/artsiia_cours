<?php

namespace AppBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use UserBundle\Entity\User;

class ProfilEditType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildUserForm($builder, $options);

        $constraintsOptions = array(
            'message' => 'fos_user.current_password.invalid',
        );

        if (!empty($options['validation_groups'])) {
            $constraintsOptions['groups'] = array(reset($options['validation_groups']));
        }

        $builder->add('current_password', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'), array(
            'label' => 'form.current_password',
            'translation_domain' => 'FOSUserBundle',
            'mapped' => false,
            'constraints' => new UserPassword($constraintsOptions),
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User',
        ));
    }
   

    public function getBlockPrefix()
    {
        return 'fos_user_profile';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

     /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            
            ->add('username', TextType::class, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('firstname', TextType::class, array('label' => 'Prénom', 'required' => false))
            ->add('lastname', TextType::class, array('label' => 'Nom','required' => false))
            ->add('gender', ChoiceType::class, array(
                'choices'  => array(
                    'Homme' =>'Homme',
                    'Femme' => 'Femme',
                    'Agenré(e)' => 'Agenré(e)',
                    'Bigenré(e)' =>  'Bigenré(e)',
                    'Non binaire' => 'Non binaire',
                    'Genderfluid' => 'Genderfluid',
                    'Pas ton souci' => 'Pas ton souci'
                ), 'label'=> 'Genre'))
            ->add('language', TextType::class, array('label' => 'Langue','required' => false))
            ->add('location', TextType::class, array('label' => 'Ville','required' => false))
            ->add('styles', TextareaType::class, array('label' => 'Mes styles','required' => false))
            ->add('tools', TextareaType::class, array('label' => 'Mes outils','required' => false))
            ->add('equipments', TextareaType::class, array('label' => 'Mes équipements','required' => false))
            ->add('website', TextType::class, array('label' => 'website','required' => false))
            ->add('bio', TextareaType::class, array('label' => 'Biographie','required' => false))
            ->add('file', FileType::class, array(
                'required' => false,
                'label' => "Photo de profil",
                'attr' => array(
                    'accept' => 'image/*'
                ),
                'data_class' => null
            ))
            ->add('socialmedia', TextType::class, array('label' => 'Réseaux sociaux','required' => false))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
        ;
    }
}
