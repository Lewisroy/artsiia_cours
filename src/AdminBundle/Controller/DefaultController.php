<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use Datetime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Page controller.
 *
 * @Route("/{_locale}/admin_manager/")
 */
class DefaultController extends Controller {

    /**
     * Lists all stats
     * @return DateTime $start
     * @return DateTime $end
     * @return Array $response,
     * @return Array $response_graph_two,
     * @return DateTime $date_label,
     * @return Array $categPerDate,
     * @return Category $categories,
     * @return Array $userPerCateg,
     * @return Array $genderPerCateg
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("home/", name="home_admin")
     * @Method("GET")
     */
    public function indexAction() {
    
        $em = $this->getDoctrine()->getManager();

        $date = new DateTime();
        $date->setTime(0, 0, 0);
        $date_end = new DateTime();
        $date_end->setTime(23, 59, 59);
        $date_label = array();
        $response = array();
        $datePerCategory = array();
        $categPerDate = array();
        $categories = $em->getRepository('LectureBundle:Category')->findAll();
        $end = $date->format('Y-m-d');
        for ($i = 0; $i < 7; $i++) {
            $result = $em->getRepository('UserBundle:User')->countUserBetweenTwoDate($date, $date_end);
            array_push($response, array($date->format('Y-m-d'), $result));


            array_push($date_label, $date->format('Y-m-d'));


            $date->modify("-1 day");
            $date_end->modify("-1 day");
        }

        foreach ($categories as $key => $category) {
            $date = new DateTime();
            $date->setTime(0, 0, 0);
            $date_end = new DateTime();
            $date_end->setTime(23, 59, 59);
            $datePerCategory = array();
            for ($i = 0; $i < 7; $i++) {
                $resultArtwork = $em->getRepository('ArtworkBundle:Artwork')->countArtworkBetweenTwoDateWithCateg($date, $date_end, $category->getId());
                array_push($datePerCategory, $resultArtwork);

                $date->modify("-1 day");
                $date_end->modify("-1 day");
            }
            array_push($categPerDate, array($category->getName(), $datePerCategory));
            $datePerCategory = array();
        }

        $start = new DateTime();
        $end = new DateTime();
        $start->modify("-6 days");
        $end->modify("+1 day");

        $response_graph_two = array();

        foreach ($categories as $category) {
            $results = $em->getRepository('LectureBundle:Lecture')->getLectureByCateg($category->getId());
            $name_categ = $category->getName();
            $views_lecture = 0; // Nombre de vues de tous les cours
            $likers_lecture = 0; // Somme des likes de tous les cours
            $numbers_lecture = 0; // nombre de cours
            foreach ($results as $result) {
                $numbers_lecture += 1;
                $views_lecture += $result->getViews();
                $likers_lecture += count($result->getLikers());
            }
            array_push($response_graph_two, array($name_categ, $views_lecture, $likers_lecture, $numbers_lecture));
        }

        $userPerCateg = array();
        $genderPerCateg = array();
        foreach ($categories as $key => $category) {
            $result = $em->getRepository('UserBundle:User')->countUserByCategory($category->getId());
            $menByCateg = $em->getRepository('UserBundle:User')->countUserByCategoryAndGender($category->getId(), 'Homme');
            $womenByCateg = $em->getRepository('UserBundle:User')->countUserByCategoryAndGender($category->getId(), 'Femme');
            $genderByCateg = $em->getRepository('UserBundle:User')->countgenderByCateg($category->getId());

            array_push($genderPerCateg, array($menByCateg, $womenByCateg, $genderByCateg));
            array_push($userPerCateg, array($category->getName(), $result));
        }


        return $this->render('AdminBundle:Default:index.html.twig', array(
                    'start' => $start->format("Y-m-d"),
                    'end' => $end->format("Y-m-d"),
                    'last_co' => $response,
                    'graph_two' => $response_graph_two,
                    'date_label' => $date_label,
                    'categ_per_date' => $categPerDate,
                    'categories' => $categories,
                    'userPerCateg' => $userPerCateg,
                    'genderPerCateg' => $genderPerCateg
        ));
    }
}
