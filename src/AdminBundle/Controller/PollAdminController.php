<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

use AdminBundle\Entity\PollAdmin;
use AdminBundle\Entity\PollOptions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Polladmin controller.
 *
 * @Route("/{_locale}/admin/poll")
 */
class PollAdminController extends Controller
{
    /**
     * Lists all pollAdmin entities.
     *
     * @Route("/", name="admin_poll_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $pollAdmins = $em->getRepository('AdminBundle:PollAdmin')->findAll();

        return $this->render('polladmin/index.html.twig', array(
            'pollAdmins' => $pollAdmins,
        ));
    }

    /**
     * Creates a new pollAdmin entity.
     *
     * @Route("/new", name="admin_poll_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $pollAdmin = new Polladmin();
        $form = $this->createForm('AdminBundle\Form\PollAdminType', $pollAdmin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pollAdmin);
            $em->flush($pollAdmin);

            return $this->redirectToRoute('admin_poll_show', array('id' => $pollAdmin->getId()));
        }

        return $this->render('polladmin/new.html.twig', array(
            'title' => 'Créé un nouveau sondage',
            'pollAdmin' => $pollAdmin,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pollAdmin entity.
     *
     * @Route("/{id}", name="admin_poll_show")
     * @Method("GET")
     */
    public function showAction(PollAdmin $pollAdmin)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($pollAdmin);

        return $this->render('polladmin/show.html.twig', array(
            'pollAdmin' => $pollAdmin,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pollAdmin entity.
     *
     * @Route("/{id}/edit", name="admin_poll_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, PollAdmin $pollAdmin)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($pollAdmin);
        $editForm = $this->createForm('AdminBundle\Form\PollAdminType', $pollAdmin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $data = $request->request->all();
            $datas = array();
            if(isset($data['au_adminbundle_polladmin']['links'])) {
                foreach ($data['au_adminbundle_polladmin']['links'] as $key => $vote) {
                    if(!empty($vote['name'])) {
                        $option = new PollOptions();
                        $option->setName($vote['name']);
                        $pollAdmin->addPollOptions($option);
                    }
                }
            }
                $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_poll_show', array('id' => $pollAdmin->getId()));
        }

        return $this->render('polladmin/new.html.twig', array(
            'title' => 'Modifier le sondage',
            'pollAdmin' => $pollAdmin,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pollAdmin entity.
     *
     * @Route("/{id}", name="admin_poll_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, PollAdmin $pollAdmin)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createDeleteForm($pollAdmin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pollAdmin);
            $em->flush($pollAdmin);
        }

        return $this->redirectToRoute('admin_poll_index');
    }

    /**
     * Creates a form to delete a pollAdmin entity.
     *
     * @param PollAdmin $pollAdmin The pollAdmin entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PollAdmin $pollAdmin)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_poll_delete', array('id' => $pollAdmin->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Add vote to poll
     *
     * @Route("/vote/poll/{id_poll}/{id_option}", name="add_vote_poll")
     * @Method({"GET", "POST"})
     */
    public function pollVoteAction($id_poll, $id_option)
    {
        $response = array();
        $response['poll'] = $id_poll;
        $response['option'] = $id_option;
        $has_vote = false;
        $em = $this->getDoctrine()->getManager();

        $pollAdmin = $em->getRepository('AdminBundle:PollAdmin')->findOneBy(array('id' => $id_poll));
        // On récupére l'utilisateur
        $user = $this->getUser();
        
        
        // on vérifie que l'utilisateur n'existe pas.
        foreach ($pollAdmin->getPollOptions() as $key => $option) { // Pour toutes les options
            if($option->getId() == $id_option ) {
                foreach ($option->getUser() as $key2 => $voter) {// Pour tous les
                    // Si l'utilisateur a voté alors on message de refus
                    if($user->getUsername() == $voter->getUsername()) 
                        $has_vote=true;
                }
                if ($has_vote) {
                    $response['vote'] = true;
                    $option->removeUser($user); 
                }
                else {
                    $response['vote'] = false;
                    $option->addUser($user);    
                } 
            }
              
        }
        $em->persist($pollAdmin);
        $em->flush();
       
        return new JsonResponse($response);
    }

    /**
     * Add option to poll
     *
     * @Route("/option/poll/{id}/{id_option}", name="add_option_poll")
     * @Method({"GET", "POST"})
     */
    public function pollAddOptionAction($id, $id_option)
    {
    
        $em = $this->getDoctrine()->getManager();
        $pollAdmin = $em->getRepository('AdminBundle:PollAdmin')->findOneBy(array('id' => $id));
                
        $option = new PollOptions();
        $option->setName($id_option);
        $pollAdmin->addPollOptions($option);

        $em->persist($pollAdmin);
        $this->getDoctrine()->getManager()->flush();

        $response = array();
        $response['add'] = true;
        return new JsonResponse($response);
    }
}
