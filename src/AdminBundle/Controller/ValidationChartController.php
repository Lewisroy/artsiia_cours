<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\LegalAgreement;
use AdminBundle\Entity\Page;
use AdminBundle\Entity\ValidationChart;
use AdminBundle\Form\LegalAgreementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * Page controller.
 *
 * @Route("/{_locale}/admin")
 */
class ValidationChartController extends Controller
{
    /**
     * Lists all page entities.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/chart/edit", name="admin_chart_edit")
     * @Method({"GET", "POST"})
     */
    public function PrivacyPolicyEditAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $charts = $em->getRepository('AdminBundle:ValidationChart')->findBy(array(),null,1);
        if (empty($charts)){
            $chart = new ValidationChart();
            $chart->setChart("");
            $chart->setChartEn("");
            $em->persist($chart);
            $em->flush();
            $charts[] = $chart;
        }else{
            $chart = $charts[0];
        }

        $form = $this->createForm(LegalAgreementType::class);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $text = $form->get('text')->getData();
            $textEN = $form->get('textEn')->getData();

            $chart->setChart($text);
            $chart->setChartEn($textEN);
            $em->persist($chart);
            $em->flush();


            return $this->redirectToRoute('admin_chart_edit');
        }
        $form->get('text')->setData($chart->getChart());
        $form->get('textEn')->setData($chart->getChartEn());

        return $this->render('agreement/new.html.twig', array(
            'title' => 'Validation Chart',
            'form' => $form->createView()
        ));
    }


}
