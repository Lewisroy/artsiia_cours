<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DefaultPage
 *
 * @ORM\Entity
 * @ORM\Table(name="default_page")
 */
class DefaultPage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="about", type="text")
     */
    private $about;


    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="aboutEn", type="text")
     */
    private $aboutEn;

    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="help", type="text")
     */
    private $help;


    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="helpEn", type="text")
     */
    private $helpEn;



    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="faq", type="text")
     */
    private $faq;

       /**
     * @var string
     *@Assert\NotBlank()
        *
     * @ORM\Column(name="faqEn", type="text")
     */
    private $faqEn;


    /**
     * Set about
     *
     * @param string $about
     *
     * @return DefaultPage
     */
    public function setAbout($about)
    {
        $this->about = $about;
    
        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set aboutEn
     *
     * @param string $aboutEn
     *
     * @return DefaultPage
     */
    public function setAboutEn($aboutEn)
    {
        $this->aboutEn = $aboutEn;
    
        return $this;
    }

    /**
     * Get aboutEn
     *
     * @return string
     */
    public function getAboutEn()
    {
        return $this->aboutEn;
    }

    /**
     * Set help
     *
     * @param string $help
     *
     * @return DefaultPage
     */
    public function setHelp($help)
    {
        $this->help = $help;
    
        return $this;
    }

    /**
     * Get help
     *
     * @return string
     */
    public function getHelp()
    {
        return $this->help;
    }

    /**
     * Set helpEn
     *
     * @param string $helpEn
     *
     * @return DefaultPage
     */
    public function setHelpEn($helpEn)
    {
        $this->helpEn = $helpEn;
    
        return $this;
    }

    /**
     * Get helpEn
     *
     * @return string
     */
    public function getHelpEn()
    {
        return $this->helpEn;
    }

    /**
     * Set faq
     *
     * @param string $faq
     *
     * @return DefaultPage
     */
    public function setFaq($faq)
    {
        $this->faq = $faq;
    
        return $this;
    }

    /**
     * Get faq
     *
     * @return string
     */
    public function getFaq()
    {
        return $this->faq;
    }

    /**
     * Set faqEn
     *
     * @param string $faqEn
     *
     * @return DefaultPage
     */
    public function setFaqEn($faqEn)
    {
        $this->faqEn = $faqEn;
    
        return $this;
    }

    /**
     * Get faqEn
     *
     * @return string
     */
    public function getFaqEn()
    {
        return $this->faqEn;
    }

    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }
}
