<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 *
 * @ORM\Table(name="article_admin")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "article.name.minMessage",
     *      maxMessage = "article.name.maxMessage"
     * )
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "article.name.minMessage",
     *      maxMessage = "article.name.maxMessage"
     * )
     *
     * @ORM\Column(name="nameEn", type="string", length=255)
     */
    private $nameEn;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 5,
     *      max = 50000,
     *      minMessage = "article.name.minMessage",
     *      maxMessage = "article.name.maxMessage"
     * )
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="textEn", type="text")
     */
    private $textEn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOpening", type="datetimetz")
     */
    private $dateOpening;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="likers_article")
     */
    private $likers;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="viewers_article")
     */
    private $viewers;

     /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Comment",cascade={"persist"})
     * @ORM\JoinTable(name="article_comments")
     */
    private $comments;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return Article
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;
    
        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Article
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set textEn
     *
     * @param string $textEn
     *
     * @return Article
     */
    public function setTextEn($textEn)
    {
        $this->textEn = $textEn;
    
        return $this;
    }

    /**
     * Get textEn
     *
     * @return string
     */
    public function getTextEn()
    {
        return $this->textEn;
    }

    /**
     * Set dateOpening
     *
     * @param \DateTime $dateOpening
     *
     * @return Article
     */
    public function setDateOpening($dateOpening)
    {
        $this->dateOpening = $dateOpening;
    
        return $this;
    }

    /**
     * Get dateOpening
     *
     * @return \DateTime
     */
    public function getDateOpening()
    {
        return $this->dateOpening;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->likers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add liker
     *
     * @param \UserBundle\Entity\User $liker
     *
     * @return Article
     */
    public function addLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers[] = $liker;
    
        return $this;
    }

    /**
     * Remove liker
     *
     * @param \UserBundle\Entity\User $liker
     */
    public function removeLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Get likers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * Add viewer
     *
     * @param \UserBundle\Entity\User $viewer
     *
     * @return Article
     */
    public function addViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers[] = $viewer;
    
        return $this;
    }

    /**
     * Remove viewer
     *
     * @param \UserBundle\Entity\User $viewer
     */
    public function removeViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers->removeElement($viewer);
    }

    /**
     * Get viewers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewers()
    {
        return $this->viewers;
    }

    /**
     * Add comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     *
     * @return Article
     */
    public function addComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;
    
        return $this;
    }

    /**
     * Remove comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     */
    public function removeComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}
