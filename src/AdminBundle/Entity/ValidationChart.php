<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ValidationChart
 *
 * @ORM\Entity
 * @ORM\Table(name="validation_chart")
 */
class ValidationChart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     *
     * @ORM\Column(name="chart", type="text")
     */
    private $chart;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     *
     * @ORM\Column(name="chart_en", type="text")
     */
    private $chartEn;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chart
     *
     * @param string $chart
     *
     * @return ValidationChart
     */
    public function setChart($chart)
    {
        $this->chart = $chart;

        return $this;
    }

    /**
     * Get chart
     *
     * @return string
     */
    public function getChart()
    {
        return $this->chart;
    }

    /**
     * Set chartEn
     *
     * @param string $chartEn
     *
     * @return ValidationChart
     */
    public function setChartEn($chartEn)
    {
        $this->chartEn = $chartEn;

        return $this;
    }

    /**
     * Get chartEn
     *
     * @return string
     */
    public function getChartEn()
    {
        return $this->chartEn;
    }
}
