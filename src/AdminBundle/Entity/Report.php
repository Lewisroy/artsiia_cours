<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\ReportRepository")
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="criticity", type="string", length=255)
     */
    private $criticity;

    /**
     * @var string
     * // boutique/people etc...
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "name.minMessage",
     *      maxMessage = "name.maxMessage"
     * )
     *
     * @ORM\Column(name="objectReport", type="string", length=255)
     */
    private $objectReport;

    /**
     * @var string
     *
     * @ORM\Column(name="statusReportingAdmin", type="string", length=255)
     */
    private $statusReportingAdmin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetimetz")
     */
    private $dateCreation;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *      min = 5,
     *      max = 1000,
     *      minMessage = "name.minMessage",
     *      maxMessage = "name.maxMessage"
     * )
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="report_user")
     */
    private $reporter;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="report_people")
     */
    private $people;

    /**
     * @ORM\ManyToMany(targetEntity="ArtworkBundle\Entity\Artwork",cascade={"persist"})
     * @ORM\JoinTable(name="report_artwork")
     */
    private $artwork;

    //
    // * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Post",cascade={"persist"})
    // * @ORM\JoinTable(name="report_post")
    // */
    //private $post;

    //
    // array des screens de l'utilisateur
    // */
    //private $screen;

    /**
     * @ORM\ManyToMany(targetEntity="ShopBundle\Entity\Article",cascade={"persist"})
     * @ORM\JoinTable(name="report_article")
     */
    private $article;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reporter = new \Doctrine\Common\Collections\ArrayCollection();
        $this->people = new \Doctrine\Common\Collections\ArrayCollection();
        $this->artwork = new \Doctrine\Common\Collections\ArrayCollection();
        $this->article = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set criticity
     *
     * @param string $criticity
     *
     * @return Report
     */
    public function setCriticity($criticity)
    {
        $this->criticity = $criticity;
    
        return $this;
    }

    /**
     * Get criticity
     *
     * @return string
     */
    public function getCriticity()
    {
        return $this->criticity;
    }

    /**
     * Set objectReport
     *
     * @param string $objectReport
     *
     * @return Report
     */
    public function setObjectReport($objectReport)
    {
        $this->objectReport = $objectReport;
    
        return $this;
    }

    /**
     * Get objectReport
     *
     * @return string
     */
    public function getObjectReport()
    {
        return $this->objectReport;
    }

    /**
     * Set statusReportingAdmin
     *
     * @param string $statusReportingAdmin
     *
     * @return Report
     */
    public function setStatusReportingAdmin($statusReportingAdmin)
    {
        $this->statusReportingAdmin = $statusReportingAdmin;
    
        return $this;
    }

    /**
     * Get statusReportingAdmin
     *
     * @return string
     */
    public function getStatusReportingAdmin()
    {
        return $this->statusReportingAdmin;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Report
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Report
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Add reporter
     *
     * @param \UserBundle\Entity\User $reporter
     *
     * @return Report
     */
    public function addReporter(\UserBundle\Entity\User $reporter)
    {
        $this->reporter[] = $reporter;
    
        return $this;
    }

    /**
     * Remove reporter
     *
     * @param \UserBundle\Entity\User $reporter
     */
    public function removeReporter(\UserBundle\Entity\User $reporter)
    {
        $this->reporter->removeElement($reporter);
    }

    /**
     * Get reporter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Add person
     *
     * @param \UserBundle\Entity\User $person
     *
     * @return Report
     */
    public function addPerson(\UserBundle\Entity\User $person)
    {
        $this->people[] = $person;
    
        return $this;
    }

    /**
     * Remove person
     *
     * @param \UserBundle\Entity\User $person
     */
    public function removePerson(\UserBundle\Entity\User $person)
    {
        $this->people->removeElement($person);
    }

    /**
     * Get people
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeople()
    {
        return $this->people;
    }

    /**
     * Add artwork
     *
     * @param \ArtworkBundle\Entity\Artwork $artwork
     *
     * @return Report
     */
    public function addArtwork(\ArtworkBundle\Entity\Artwork $artwork)
    {
        $this->artwork[] = $artwork;
    
        return $this;
    }

    /**
     * Remove artwork
     *
     * @param \ArtworkBundle\Entity\Artwork $artwork
     */
    public function removeArtwork(\ArtworkBundle\Entity\Artwork $artwork)
    {
        $this->artwork->removeElement($artwork);
    }

    /**
     * Get artwork
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArtwork()
    {
        return $this->artwork;
    }

    /**
     * Add article
     *
     * @param \ShopBundle\Entity\Article $article
     *
     * @return Report
     */
    public function addArticle(\ShopBundle\Entity\Article $article)
    {
        $this->article[] = $article;
    
        return $this;
    }

    /**
     * Remove article
     *
     * @param \ShopBundle\Entity\Article $article
     */
    public function removeArticle(\ShopBundle\Entity\Article $article)
    {
        $this->article->removeElement($article);
    }

    /**
     * Get article
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticle()
    {
        return $this->article;
    }
}
