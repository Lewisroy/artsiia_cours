<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Post
 *
 * @ORM\Table(name="poll")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\PostRepository")
 */
class PollAdmin
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "name.minMessage",
     *      maxMessage = "name.maxMessage"
     * )
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *      min = 5,
     *      max = 50000,
     *      minMessage = "name.minMessage",
     *      maxMessage = "name.maxMessage"
     * )
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="ongoing", type="boolean", nullable=true)
     */
    private $ongoing;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="datetimetz")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="datetimetz")
     */
    private $dateEnd;

     /**
     * @ORM\ManyToMany(targetEntity="AdminBundle\Entity\PollOptions",cascade={"persist"})
     * @ORM\JoinTable(name="poll_options_users")
     */
    private $pollOptions;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Post
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Post
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ongoing
     *
     * @param boolean $ongoing
     *
     * @return Post
     */
    public function setOngoing($ongoing)
    {
        $this->ongoing = $ongoing;
    
        return $this;
    }

    /**
     * Get ongoing
     *
     * @return boolean
     */
    public function getOngoing()
    {
        return $this->ongoing;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Post
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Post
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pollOptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add polloptions
     *
     * @param \AdminBundle\Entity\PollOptions $polloptions
     *
     * @return PollAdmin
     */
    public function addPollOptions(\AdminBundle\Entity\PollOptions $polloptions)
    {
        $this->pollOptions[] = $polloptions;
    
        return $this;
    }

    /**
     * Remove polloptions
     *
     * @param \AdminBundle\Entity\PollOptions $polloptions
     */
    public function removePollOptions(\AdminBundle\Entity\PollOptions $polloptions)
    {
        $this->pollOptions->removeElement($polloptions);
    }

    /**
     * Get polloptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPollOptions()
    {
        return $this->pollOptions;
    }

    /**
     * Add pollOption
     *
     * @param \AdminBundle\Entity\PollOptions $pollOption
     *
     * @return PollAdmin
     */
    public function addPollOption(\AdminBundle\Entity\PollOptions $pollOption)
    {
        $this->pollOptions[] = $pollOption;

        return $this;
    }

    /**
     * Remove pollOption
     *
     * @param \AdminBundle\Entity\PollOptions $pollOption
     */
    public function removePollOption(\AdminBundle\Entity\PollOptions $pollOption)
    {
        $this->pollOptions->removeElement($pollOption);
    }
}
