<?php

namespace AdminBundle\Form;

use AdminBundle\Entity\PollAdmin;
use AdminBundle\Entity\PollOptions;
use AdminBundle\Form\PollOptionsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class PollAdminType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('label' => 'Titre', 'required' => true))
        ->add('description',CKEditorType::class, array(
            'label' => 'Description',
            'config' => array(
                'uiColor' => '#ffffff',
                "toolbar" => [["Source","-","Preview","Print"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo"],["Find","Replace","-","SelectAll","-","Scayt"],["Form","Checkbox","Radio","TextField","Textarea","SelectField","Button","ImageButton","HiddenField"],"\/",["Bold","Italic","Underline","Strike","Subscript","Superscript","-","RemoveFormat"],["NumberedList","BulletedList","-","Outdent","Indent","-","Blockquote","CreateDiv","-","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock","-","BidiLtr","BidiRtl"],["Link","Unlink","Anchor"],["Image","Flash","Table","HorizontalRule","SpecialChar","Smiley","PageBreak","Iframe"],"\/",["Styles","Format","Font","FontSize","TextColor","BGColor"],["Maximize","ShowBlocks"],[]],
                )
            )) 
        ->add('ongoing', CheckboxType::class, array('label' => 'En cours', 'required' => false))
        ->add('dateStart', DateTimeType::class, array('label' => 'Date de début', 'required' => true))
        ->add('dateEnd', DateTimeType::class, array('label' => 'Date de fin', 'required' => true))
        ->add('pollOptions', CollectionType::class, array(
            'entry_type' => PollOptionsType::class,'label' => ' ','allow_add'=> true, 'allow_delete' => true, 'prototype' => true
        ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\PollAdmin'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'au_adminbundle_polladmin';
    }


}
