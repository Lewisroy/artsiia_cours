<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ArticleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('label' => 'Titre', 'required' => false))
        ->add('nameEn', TextType::class, array('label' => 'Titre anglais', 'required' => false))
        ->add('text',CKEditorType::class, array(
            'label' => 'Texte',
            'config' => array(
                'uiColor' => '#ffffff',
                "toolbar" => [["Source","-","Preview","Print"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo"],["Find","Replace","-","SelectAll","-","Scayt"],["Form","Checkbox","Radio","TextField","Textarea","SelectField","Button","ImageButton","HiddenField"],"\/",["Bold","Italic","Underline","Strike","Subscript","Superscript","-","RemoveFormat"],["NumberedList","BulletedList","-","Outdent","Indent","-","Blockquote","CreateDiv","-","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock","-","BidiLtr","BidiRtl"],["Link","Unlink","Anchor"],["Image","Flash","Table","HorizontalRule","SpecialChar","Smiley","PageBreak","Iframe"],"\/",["Styles","Format","Font","FontSize","TextColor","BGColor"],["Maximize","ShowBlocks"],[]],
                )
            )) 
        ->add('textEn',CKEditorType::class, array(
            'label' => 'Texte anglais',
            'config' => array(
                'uiColor' => '#ffffff',
                "toolbar" => [["Source","-","Preview","Print"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo"],["Find","Replace","-","SelectAll","-","Scayt"],["Form","Checkbox","Radio","TextField","Textarea","SelectField","Button","ImageButton","HiddenField"],"\/",["Bold","Italic","Underline","Strike","Subscript","Superscript","-","RemoveFormat"],["NumberedList","BulletedList","-","Outdent","Indent","-","Blockquote","CreateDiv","-","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock","-","BidiLtr","BidiRtl"],["Link","Unlink","Anchor"],["Image","Flash","Table","HorizontalRule","SpecialChar","Smiley","PageBreak","Iframe"],"\/",["Styles","Format","Font","FontSize","TextColor","BGColor"],["Maximize","ShowBlocks"],[]],
                )
            ))       
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\Article'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'au_adminbundle_article';
    }


}
