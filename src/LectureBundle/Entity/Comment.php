<?php

namespace LectureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Comment
 *
 * @ORM\Entity
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="LectureBundle\Repository\CommentRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Comment", inversedBy="subComments")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id",nullable=true)
     */
    private $comment;


    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="comment")
     */
    private $subComments;

    /**
     * @ORM\ManyToOne(targetEntity="Lecture", inversedBy="comments")
     * @ORM\JoinColumn(name="lecture_id", referencedColumnName="id")
     */
    private $lecture;

    /**
     * @ORM\ManyToOne(targetEntity="ArtworkBundle\Entity\Artwork", inversedBy="comments")
     * @ORM\JoinColumn(name="artwork_id", referencedColumnName="id")
     */
    private $artwork;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Activity", inversedBy="commentslist")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    private $activity;


    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="comment")
     */
    public $reporting;

    /**
     * Add reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     *
     * @return Reporting
     */
    public function addReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting[] = $reporting;

        return $this;
    }

    /**
     * Remove reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     */
    public function removeReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting->removeElement($reporting);
    }

    /**
     * Get reporting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporting()
    {
        return $this->reporting;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subComments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reporting = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contentfo
     *
     * @param string $content
     *
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Comment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     *
     * @return Comment
     */
    public function setComment(\LectureBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \LectureBundle\Entity\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add subComment
     *
     * @param \LectureBundle\Entity\Comment $subComment
     *
     * @return Comment
     */
    public function addSubComment(\LectureBundle\Entity\Comment $subComment)
    {
        $this->subComments[] = $subComment;

        return $this;
    }

    /**
     * Remove subComment
     *
     * @param \LectureBundle\Entity\Comment $subComment
     */
    public function removeSubComment(\LectureBundle\Entity\Comment $subComment)
    {
        $this->subComments->removeElement($subComment);
    }

    /**
     * Get subComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubComments()
    {
        return $this->subComments;
    }

    /**
     * Set lecture
     *
     * @param \LectureBundle\Entity\Lecture $lecture
     *
     * @return Comment
     */
    public function setLecture(\LectureBundle\Entity\Lecture $lecture = null)
    {
        $this->lecture = $lecture;

        return $this;
    }

    /**
     * Get lecture
     *
     * @return \LectureBundle\Entity\Lecture $lecture
     */
    public function getLecture()
    {
        return $this->lecture;
    }

    /**
     * Set Artwork
     *
     * @param \ArtworkBundle\Entity\Artwork $artwork
     *
     * @return Comment
     */
    public function setArtwork(\ArtworkBundle\Entity\Artwork $artwork = null)
    {
        $this->artwork = $artwork;

        return $this;
    }

    /**
     * Get artwork
     *
     * @return \ArtworkBundle\Entity\Artwork
     */
    public function getArtwork()
    {
        return $this->artwork;
    }
    
    /**
     * Set Activity
     *
     * @param \UserBundle\Entity\Activity $activity
     *
     * @return Comment
     */
    public function setActivity(\UserBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \UserBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }


   
    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Comment
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }
}
