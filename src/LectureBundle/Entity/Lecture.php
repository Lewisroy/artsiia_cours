<?php

namespace LectureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Lecture
 *
 * @ORM\Table(name="lecture")
 * @ORM\Entity(repositoryClass="LectureBundle\Repository\LectureRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 */
class Lecture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="author_lecture",
     *      joinColumns={@ORM\JoinColumn(name="lecture_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $author;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Category",cascade={"persist"})
     * @ORM\JoinTable(name="category_lecture")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Part",cascade={"persist"})
     * @ORM\JoinTable(name="part_lecture")
     */
    private $part;

    /**
     * @var string
     *
     * @ORM\Column(name="statut", type="string", length=255)
     */
    private $statut;


    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @var int
     *
     * @ORM\Column(name="views", type="integer", length=8)
     */
    private $views;

    /**
     * @var int
     *
     * @ORM\Column(name="number_download", type="integer", length=8)
     */
    private $number_download;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255)
     */
    private $language;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="likers")
     */
    private $likers;

    /**
     * @var array
     *
     * @ORM\Column(name="tags", type="array", nullable=true)
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="Comment",mappedBy="lecture")
     */
    private $comments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->author = new \Doctrine\Common\Collections\ArrayCollection();
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->part = new \Doctrine\Common\Collections\ArrayCollection();
        $this->likers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Lecture
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Lecture
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Lecture
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return Lecture
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return Lecture
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set numberDownload
     *
     * @param integer $numberDownload
     *
     * @return Lecture
     */
    public function setNumberDownload($numberDownload)
    {
        $this->number_download = $numberDownload;

        return $this;
    }

    /**
     * Get numberDownload
     *
     * @return integer
     */
    public function getNumberDownload()
    {
        return $this->number_download;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Lecture
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Lecture
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set tags
     *
     * @param array $tags
     *
     * @return Lecture
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add author
     *
     * @param \UserBundle\Entity\User $author
     *
     * @return Lecture
     */
    public function addAuthor(\UserBundle\Entity\User $author)
    {
        $this->author[] = $author;

        return $this;
    }

    /**
     * Remove author
     *
     * @param \UserBundle\Entity\User $author
     */
    public function removeAuthor(\UserBundle\Entity\User $author)
    {
        $this->author->removeElement($author);
    }

    /**
     * Get author
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add category
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return Lecture
     */
    public function addCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \LectureBundle\Entity\Category $category
     */
    public function removeCategory(\LectureBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add part
     *
     * @param \LectureBundle\Entity\Part $part
     *
     * @return Lecture
     */
    public function addPart(\LectureBundle\Entity\Part $part)
    {
        $this->part[] = $part;

        return $this;
    }

    /**
     * Remove part
     *
     * @param \LectureBundle\Entity\Part $part
     */
    public function removePart(\LectureBundle\Entity\Part $part)
    {
        $this->part->removeElement($part);
    }

    /**
     * Get part
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPart()
    {
        return $this->part;
    }

    /**
     * Add liker
     *
     * @param \UserBundle\Entity\User $liker
     *
     * @return Lecture
     */
    public function addLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers[] = $liker;

        return $this;
    }

    /**
     * Remove liker
     *
     * @param \UserBundle\Entity\User $liker
     */
    public function removeLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Get likers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Lecture
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Add comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     *
     * @return Lecture
     */
    public function addComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     */
    public function removeComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
    
    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }
}
