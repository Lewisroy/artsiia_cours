<?php

namespace LectureBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, array('label' => 'global.title', 'required' => false))
            ->add('cover', FileType::class, array(
                'label' => 'global.array.cover',
                'required' => false,
                'data_class' => null,
                'attr' => array(
                    'accept' => 'image/*'
                )
            ))
            ->add('description',CKEditorType::class, array(
                'label' => 'Description',
                'config' => array(
                    'uiColor' => '#ffffff',
                    "toolbar" => [["Source","-","Preview","Print"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo"],["Find","Replace","-","SelectAll","-","Scayt"],["Form","Checkbox","Radio","TextField","Textarea","SelectField","Button","ImageButton","HiddenField"],"\/",["Bold","Italic","Underline","Strike","Subscript","Superscript","-","RemoveFormat"],["NumberedList","BulletedList","-","Outdent","Indent","-","Blockquote","CreateDiv","-","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock","-","BidiLtr","BidiRtl"],["Link","Unlink","Anchor"],["Image","Flash","Table","HorizontalRule","SpecialChar","Smiley","PageBreak","Iframe"],"\/",["Styles","Format","Font","FontSize","TextColor","BGColor"],["Maximize","ShowBlocks"],[]],
                    'filebrowserBrowseRoute' => 'elfinder',
                    'filebrowserBrowseRouteParameters' => array(
                        'instance' => 'default',
                        'homeFolder' => ''
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LectureBundle\Entity\Category'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lecturebundle_category';
    }
    
}
