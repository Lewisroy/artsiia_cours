<?php

namespace LectureBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubCategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('cover', FileType::class, array(
                'label' => 'global.array.cover',
                'required' => false,
                'data_class' => null,
                'attr' => array(
                    'accept' => 'image/*'
                )
            ))
            ->add('description')
            ->add('category', EntityType::class, array(
                'class' => 'LectureBundle\Entity\Category',
                'choice_label' => 'name',
                'placeholder'  => '--choisir une catégorie--',
            )) ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LectureBundle\Entity\SubCategory'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lecturebundle_subcategory';
    }


}
