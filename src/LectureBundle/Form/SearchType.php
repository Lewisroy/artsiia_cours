<?php

namespace LectureBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use \Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SearchType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, array('label' => 'Name', 'required' => false))
                ->add('language', ChoiceType::class, array(
                    'choices' => array(
                        '' => '',
                        'FR' => 'FR',
                        'EN' => 'EN'
                    ), 'label' => 'language'))
                ->add('category', EntityType::class, array('label' => 'category.title', 'required' => false,
                    'class' => 'LectureBundle:Category', 'choice_label' => 'name'))
                ->add('sort', ChoiceType::class, array('label' => 'sort', 'required' => false,
                    'choices' => array(
                        'number_like' => 'number_like',
                        'older' => 'older',
                        'newer' => 'newer',
            )))
                ->add('tags', TextType::class, array('label' => 'Tags', 'required' => false))
                ->add('level', ChoiceType::class, array(
                    'required' => false,
                    'multiple' => true,
                    'choices' => array(
                        'lecture.levels.beginner' => 'beginner',
                        'lecture.levels.intermediate' => 'intermediate',
                        'lecture.levels.advanced' => 'advanced',
                        'lecture.levels.hard' => 'hard',
                    ), 'choice_translation_domain' => 'messages'
                    , 'label' => 'Level'
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults([
            'data_class' => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'searchForm';
    }

}
