<?php

namespace LectureBundle\Form;

use LectureBundle\Entity\Lecture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class LectureType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var Lecture
         */
        $lecture = $builder->getData();
        $choices = array();
        {
            if ($lecture->getTags() != null)
                foreach ($lecture->getTags() as $key => $tag){
                    $choices[$tag] = $tag;
                }
        }

        $builder->add('name', TextType::class, array('label' => 'global.array.title', 'required' => false))
            ->add('cover', FileType::class, array(
                'label' => 'global.array.cover',
                'required' => false,
                'attr' => array(
                    'accept' => 'image/*'
                ),
                'data_class' => null
            ))
            ->add('description', CKEditorType::class, array(
                'label' => 'global.array.description',
                'config' => array(
                    'uiColor' => '#ffffff',
                    "toolbar" => [["Source","-","Preview","Print"],["Cut","Copy","Paste","PasteText","PasteFromWord","-","Undo","Redo"],["Find","Replace","-","SelectAll","-","Scayt"],["Form","Checkbox","Radio","TextField","Textarea","SelectField","Button","ImageButton","HiddenField"],"\/",["Bold","Italic","Underline","Strike","Subscript","Superscript","-","RemoveFormat"],["NumberedList","BulletedList","-","Outdent","Indent","-","Blockquote","CreateDiv","-","JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock","-","BidiLtr","BidiRtl"],["Link","Unlink","Anchor"],["Image","Flash","Table","HorizontalRule","SpecialChar","Smiley","PageBreak","Iframe"],"\/",["Styles","Format","Font","FontSize","TextColor","BGColor"],["Maximize","ShowBlocks"],[]],
                )
            ))
            ->add('language', ChoiceType::class, array(
                'choices'  => array(
                    'FR' =>'FR',
                    'EN' => 'EN'
                ), 'label' => 'language'))


            ->add('level', ChoiceType::class, array(
                'choices'  => array(
                    'lecture.levels.beginner' =>'beginner',
                    'lecture.levels.intermediate' =>'intermediate',
                    'lecture.levels.advanced' =>'advanced',
                    'lecture.levels.hard' =>'hard',
                ),'choice_translation_domain' => 'messages'
                ,'label' => 'Level'
            ))

            ->add('category', EntityType::class, array('label' => 'category.title',
                'required' => false,
                'multiple' => true,
                'expanded' => false,
                'mapped' => false,
                'class' => 'LectureBundle:Category',
                'choice_label' => 'name'
            ))

            ->add('tags',ChoiceType::class,array(
                'required' => false,
                'multiple' => true,
                'expanded' => false,
                'by_reference' => false,
                'choices' => $choices
                ))

        ;


        $builder->get('tags')->resetViewTransformers();

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LectureBundle\Entity\Lecture'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'lecturebundle_lecture';
    }


}
