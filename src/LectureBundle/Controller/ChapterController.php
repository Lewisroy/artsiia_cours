<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Chapter;
use LectureBundle\Entity\Part;
use LectureBundle\Entity\Lecture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Chapter controller.
 *
 * @Route("/{_locale}/chapter")
 */
class ChapterController extends Controller {

    /**
     * Creates a new chapter entity.
     *
     * @Security("is_granted('ROLE_USER')")
     * @param Request $request
     * @return \Symfony\Component\Form\Form $form The form of new chapter
     * @return  Chapter chapter the new Chapter
     * @Route("/new", name="chapter_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $chapter = new Chapter();
        $form = $this->createForm('LectureBundle\Form\ChapterType', $chapter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $uploaded_file = $editForm['video']->getData();
            if (!empty($chapter->getVideo()) && $uploaded_file) {
                $file = $chapter->getVideo();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move('uploads/chapter/video/', $fileName);
                $chapter->setVideo($filename);
            }
            $em->persist($chapter);
            $em->flush($chapter);

            return $this->redirectToRoute('chapter_show', array('id' => $chapter->getId()));
        }

        return $this->render('chapter/new.html.twig', array(
                    'chapter' => $chapter,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a chapter entity.
     * 
     * @param Chapter $chapter 
     * 
     * @Route("/{id}", name="chapter_show")
     * @Method("GET")
     */
    public function showAction(Chapter $chapter) {


        $deleteForm = $this->createDeleteForm($chapter);

        $em = $this->getDoctrine()->getManager();

        $part = $em->getRepository('LectureBundle:Part')->getPartByChapter($chapter->getId());
        $lecture = $em->getRepository('LectureBundle:Lecture')->getLectureByPart($part[0]->getId());
        foreach ($lecture[0]->getAuthor() as $user) {
            if($user->getIsBanni()){
               throw $this->createNotFoundException();
            }
        }

        if ($lecture[0]->getPart()[0]->getChapter()[0] == $chapter)
            $lecture[0]->setViews($lecture[0]->getViews() + 1);
        $em->flush();
        return $this->render('chapter/show.html.twig', array(
                    'lecture' => $lecture[0],
                    'part' => $part[0],
                    'chapter' => $chapter,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing chapter entity.
     * @Security("is_granted('ROLE_USER')")
     * @Route("/{id}/edit", name="chapter_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Chapter $chapter) {
        $deleteForm = $this->createDeleteForm($chapter);
        $editForm = $this->createForm('LectureBundle\Form\ChapterType', $chapter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $uploaded_file = $editForm['video']->getData();
            if (!empty($chapter->getVideo()) && $uploaded_file) {
                $file = $chapter->getVideo();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move('uploads/chapter/video/', $fileName
                );
                $chapter->setVideo($filename);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chapter_edit', array('id' => $chapter->getId()));
        }

        return $this->render('chapter/edit.html.twig', array(
                    'chapter' => $chapter,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a chapter entity.
     * @Security("is_granted('ROLE_USER')")
     * @Route("/{id}/delete", name="chapter_delete")
     */
    public function deleteAction(Request $request, Chapter $chapter) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($chapter);
        $em->flush();
        $lecture = $request->get('lecture');

        return $this->redirectToRoute('lecture_show', array('id' => $lecture));
    }

    /**
     * Creates a form to delete a chapter entity.
     *
     * @param Chapter $chapter The chapter entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Chapter $chapter) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('chapter_delete', array('id' => $chapter->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
