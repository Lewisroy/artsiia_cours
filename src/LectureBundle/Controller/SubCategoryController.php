<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\SubCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * SubCategoryController controller.
 *
 * @Route("/{_locale}/admin/subcategory")
 */
class SubCategoryController extends Controller
{
    /**
     * Lists all sub category entities.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/", name="subcategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $subCategories = $this->getDoctrine()->getRepository('LectureBundle:SubCategory')->findAll();

        return $this->render('LectureBundle:SubCategories:index.html.twig', array(
            'subCategories' => $subCategories,
        ));
    }


    /**
     * Creates a new sub category entity.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/new", name="subcategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $subCategory = new SubCategory();
        $form = $this->createForm('LectureBundle\Form\SubCategoryType', $subCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            if(!empty($subCategory->getCover()))
            {
                $file = $subCategory->getCover();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move('uploads/subcategory/',
                    $fileName
                );
                $subCategory->setCover($fileName);
            }
            $em->persist($subCategory);
            $em->flush($subCategory);

            return $this->redirectToRoute('subcategory_index');
        }

        $count = $this->getDoctrine()->getRepository('LectureBundle:Category')->getNbQueryBuilder();

        return $this->render('LectureBundle:SubCategories:new.html.twig', array(
            'title' => $this->container->get('translator')->trans('subCategory.new'),
            'subCategory' => $subCategory,
            'form' => $form->createView(),
            'count' => $count,
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}/edit", name="subcategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SubCategory $subCategory)
    {
        $deleteForm = $this->createDeleteForm($subCategory);
        $editForm = $this->createForm('LectureBundle\Form\SubCategoryType', $subCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if(empty($editForm->get('cover')->getData()))
            {
                $em->clear();
                $category_tmp = $em->getRepository('LectureBundle:SubCategory')->findOneBy(array('id' => $subCategory->getId()));

                $subCategory->setCover($category_tmp->getCover());
            }
            else
            {
                /** @var UploadedFile $file */

                $file = $editForm->get('cover')->getData();
                $fileName = md5(uniqid()).'.'.$file->getClientOriginalExtension();
                $file->move(
                    $this->getParameter('subcategory_directory'),
                    $fileName
                );
                $subCategory->setCover($fileName);
            }

            $em->flush();
            // return $this->redirectToRoute('category_indexgit comm', array('id' => $subCategory->getId()));
            return $this->redirectToRoute('subcategory_index');
        }

        return $this->render('LectureBundle:SubCategories:edit.html.twig', array(
            'title' => $this->container->get('translator')->trans('subCategory.edition'),
            'category' => $subCategory,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush($category);
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param SubCategory $category The sub category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SubCategory $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
