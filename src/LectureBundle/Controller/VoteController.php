<?php

namespace LectureBundle\Controller;

use AdminBundle\Entity\ValidationChart;
use UserBundle\Entity\User;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use LectureBundle\Entity\Comment;
use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Category;
use LectureBundle\Entity\Part;
use LectureBundle\Entity\Chapter;
use LectureBundle\Entity\Vote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Lecture controller.
 *
 * @Route("/{_locale}/vote",name="vote")
 */
class VoteController extends Controller
{
   

    /**
     * Add new Vote entities.
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/new", name="vote_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request){
        
        extract($_POST);
        $vote = new Vote();
        $form = $this->createForm('LectureBundle\Form\VoteType', $vote);
        $form->handleRequest($request);

        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $lecture = $em->getRepository('LectureBundle:Lecture')->findOneBy(array('id' => $lectureId) );
            $user=$em->getRepository('UserBundle:User')->findOneBy(array('id' => $userId) );
            if($userVote=='true' && $userVoteId!=-1){
               $vote=$em->getRepository('LectureBundle:Vote')->findOneBy(array('id' => $userVoteId) );
            }
            $vote->setLecture($lecture);
            $vote->setUser($user);    
            $vote->setNote($note);
            
            $em->persist($vote);
            $em->flush($vote);

            $data=array(
            'status' => 'success'
            );
            $serializer = $this->container->get('serializer');
            $reports = $serializer->serialize($data, 'json');
            return new Response($reports);
        }
         $data=array(
            'status' => 'error'
            );
         $serializer = $this->container->get('serializer');
            $reports = $serializer->serialize($data, 'json');
            return new Response($reports);
    }
        
}
