<?php

namespace LectureBundle\Controller;

use AdminBundle\Entity\ValidationChart;
use UserBundle\Entity\User;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use LectureBundle\Entity\Comment;
use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Category;
use LectureBundle\Entity\Part;
use LectureBundle\Entity\Chapter;
use LectureBundle\Entity\Vote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use LectureBundle\Form\SearchType;
use LectureBundle\Form\CommentType;
use LectureBundle\Form\AddAuthorType;

/**
 * Lecture controller.
 *
 * @Route("/{_locale}/lecture",name="lecture_general")
 */
class LectureController extends Controller {

    /**
     * Lists all lecture entities.
     *
     * @Route("/", name="lecture_index")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $lectures = $em->getRepository('LectureBundle:Lecture')->getLectures();


        return $this->render('lecture/index.html.twig', array(
                    'lectures' => $lectures,
        ));
    }

    /**
     * @Route("/{id}/courses", name="general_profile_cours")
     */
    public function coursAction($id) {
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository('LectureBundle:Lecture')->getUserLectures($id, false);


        return $this->render(
                        'default/cours.html.twig', array(
                    'courses' => $courses
                        )
        );
    }

    /**
     * Lists all lecture entities.
     *
     * @Security("is_granted('ROLE_USER')")
     * @Route("/all", name="lecture_all")
     * @Method({"GET", "POST"})
     */
    public function showAllAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $lectures = null;

        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        if (!isset($maxResult)) {
            $maxResult = 24;
            $lastResult = 0;
        }


        if ($form->isSubmitted() && $form->isValid()) {

            $lectures = $em->getRepository('LectureBundle:Lecture')->searchLecture($form->getData(), $maxResult, $lastResult);
            $categories = $em->getRepository('LectureBundle:Category')->findAll();

            return $this->render('lecture/indexAll.html.twig', array(
                        'lectures' => $lectures,
                        'categories' => $categories,
                        'form' => $form->createView(),
                        'search' => 'true'
            ));
        }

        extract($_POST);
        if (isset($formSearch)) {
            $lectures = $em->getRepository('LectureBundle:Lecture')->searchLecture($formSearch, $maxResult, $lastResult);
        } else {
            $lectures = $em->getRepository('LectureBundle:Lecture')->findBy(array(), null, $maxResult, $lastResult);
            $lectures = $em->getRepository('LectureBundle:Lecture')->getLecturesWithLimit($maxResult, $lastResult);
        }

        if ($request->isXMLHttpRequest()) {

            $data = array(
                'lectures' => $lectures
            );
            $serializer = $this->container->get('serializer');
            $reports = $serializer->serialize($data, 'json');
            return new Response($reports);
        }

        $categories = $em->getRepository('LectureBundle:Category')->findAll();


        return $this->render('lecture/indexAll.html.twig', array(
                    'lectures' => $lectures,
                    'categories' => $categories,
                    'form' => $form->createView(),
        ));
    }

    public function returnPDFResponseFromHTML($html) {
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor(" ");
        $pdf->SetTitle(" ");
        $pdf->SetSubject(" ");
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 11, '', true);
        //$pdf->SetMargins(20,20,40, true);
        $pdf->AddPage();

        $filename = " ";

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename . ".pdf", 'I'); // This will output the PDF as a response directly
    }

    /**
     * Lists all category entities.
     *
     * @Route("/category", name="lecture_category_index")
     * @Method("GET")
     */
    public function indexUserAction() {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('LectureBundle:Category')->findAll();

        return $this->render('lecture/indexUser.html.twig', array(
                    'categories' => $categories,
        ));
    }

    /**
     * @Route("/ask_validation/{id}", name="lecture_ask_validation")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Method("GET")
     */
    public function validationAction(Request $request, Lecture $lecture) {
        $em = $this->getDoctrine()->getManager();

        $lecture->setStatut('OnGoingValidate');
        $em->flush();

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @Route("/download/{id}", name="lecture_download")
     * @Security("is_granted('ROLE_USER')")
     * @Method("GET")
     */
    public function downloadAction(Request $request, Lecture $lecture) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $html = $this->renderView('lecture/pdf.html.twig', array(
            'lecture' => $lecture,
        ));

        $lecture->setNumberDownload($lecture->getNumberDownload() + 1);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $delimiter = '<saut>';
        $chunks = explode($delimiter, $html);
        $cnt = count($chunks);



        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor($lecture->getAuthor()[0]);
        $pdf->SetTitle($lecture->getName());
        $pdf->SetSubject($lecture->getName());
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 11, '', true);
        //$pdf->SetMargins(20,20,40, true);
        $pdf->AddPage();
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $filename = $lecture->getName();

        // $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        for ($i = 0; $i < $cnt; $i++) {
            $pdf->writeHTML($delimiter . $chunks[$i], true, 0, true, 0);

            if ($i < $cnt - 1) {
                $pdf->AddPage();
            }
        }
        $pdf->Output($filename . ".pdf", 'I'); // This will output the PDF as a response directly

        return $pdf;
    }

    /**
     * @Route("/validate/{id}", name="lecture_validation")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Method("GET")
     */
    public function askValidationAction(Request $request, Lecture $lecture) {
        $em = $this->getDoctrine()->getManager();

        $lecture->setStatut('Validate');
        $em->flush();

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/cancel_validation/{id}", name="lecture_cancel_validation")
     * @Method("GET")
     */
    public function cancelValidationAction(Request $request, Lecture $lecture) {

        $em = $this->getDoctrine()->getManager();

        $lecture->setStatut('Draft');
        $em->flush();

        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }

    /**
     * Creates a new lecture entity.
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/new", name="lecture_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {

        $lecture = new Lecture();
        $form = $this->createForm('LectureBundle\Form\LectureType', $lecture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $lecture->addAuthor($this->getUser());
            $lecture->setDate(new \Datetime());
            $lecture->setStatut('Draft');
            $lecture->setViews(0);
            $lecture->setNumberDownload(0);
            foreach ($form->get('category')->getData() as $category) {
                $lecture->addCategory($category);
            }
            if (!empty($lecture->getCover())) {
                $file = $lecture->getCover();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move('uploads/lecture/cover/', $fileName
                );
                $lecture->setCover($fileName);
            }
            $em->persist($lecture);
            $em->flush($lecture);

            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/new.html.twig', array(
                    'title' => $this->get('translator')->trans('lecture.new'),
                    'lecture' => $lecture,
                    'form' => $form->createView(),
                    'message' => 'new',
        ));
    }

    /**
     * Finds and displays all lecture entity.
     * 
     * @Route("/{id}", name="lecture_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, Lecture $lecture) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
        foreach ($lecture->getAuthor() as $user) {
            if ($user->getIsBanni()) {
                throw $this->createNotFoundException();
            }
        }


        $deleteForm = $this->createDeleteForm($lecture);

        $locale = $request->getLocale();
        $em = $this->getDoctrine()->getManager();
        $charts = $em->getRepository('AdminBundle:ValidationChart')->findBy(array(), null, 1);
        if (empty($charts)) {
            $chart = new ValidationChart();
            $chart->setChart("");
            $chart->setChartEn("");
            $em->persist($chart);
            $em->flush();
            $charts[] = $chart;
        }
        $vote = new Vote();
        $voteForm = $this->createForm('LectureBundle\Form\VoteType', $vote);
        $voteForm->handleRequest($request);
        $userNote = null;
        $voteCount = $em->getRepository('LectureBundle:Vote')->findBy(array('lecture' => $lecture->getId()));
        $userVote = 'false';
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();
            $voteCheck = $em->getRepository('LectureBundle:Vote')->findBy(array(
                'lecture' => $lecture->getId(),
                'user' => $user->getId()));
            if (!empty($voteCheck)) {
                $userVote = 'true';
                $userNote = $voteCheck[0];
            }
        }

        $moy = 'null';
        if (!empty($voteCount)) {
            $count = 0;
            $i = 0;
            foreach ($voteCount as $rowVote) {
                $count += $rowVote->getNote();
                $i++;
            }
            $moy = round($count / $i, 2);
        }

        $form = $this->createFormBuilder(null, array('method' => "POST", 'attr' => array('id' => 'authors')))
                ->add('author', EntityType::class, array(
                    'class' => 'UserBundle:User',
                    'multiple' => true,
                    'expanded' => false,
                    'mapped' => false,
                ))
                ->getForm();

        $form->get('author')->setData($lecture->getAuthor());

        $form->handleRequest($request);

        $commentsForm = $this->createForm(CommentType::class);

        if ($form->isSubmitted()) {
            $authors = $form->get('author')->getData();

            foreach ($authors as $author) {
                if (!$lecture->getAuthor()->contains($author)) {
                    $lecture->addAuthor($author);
                    $em->merge($lecture);
                }
            }
            $em->flush();
            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }
        $commentsForm->handleRequest($request);

        if ($commentsForm->isSubmitted()) {
            $commentData = $commentsForm->get('comment')->getData();
            if ($commentData != null or $commentData != "") {
                $comment = new Comment();
                $comment->setContent($commentData);
                $comment->setDate(new \DateTime());
                $comment->setLecture($lecture);
                $comment->setUser($this->getUser());
                $em->persist($comment);
                $em->flush();
                $notification = $this->get('app_notification_services');
                $notification->sendNotificationLecture($comment, $lecture);
            }
            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }



        if (strtolower($locale) == 'fr') {
            $chart = $charts[0];
            $validationChart = $chart->getChart();
        } else {
            $chart = $charts[0];
            $validationChart = $chart->getChartEn();
        }


        if (!($this->getUser() instanceof User) AND ! $lecture->getAuthor()->contains($this->getUser())) {
            $lecture->setViews(intval($lecture->getViews()) + 1);
            $em->merge($lecture);
            $em->flush();
        }

        return $this->render('lecture/essai.html.twig', array(
                    'lecture' => $lecture,
                    'validationChart' => $validationChart,
                    'delete_form' => $deleteForm->createView(),
                    'form' => $form->createView(),
                    'commentsForm' => $commentsForm->createView(),
                    'voteForm' => $voteForm->createView(),
                    'moyenneVote' => $moy,
                    'userVote' => $userVote,
                    'userNote' => $userNote
        ));
    }

    /**
     * Finds and displays all lecture entity.
     * @Route("/{id}/show", name="lecture_show_user")
     * @Method({"GET","POST"})
     */
    public function showUserAction(Request $request, Lecture $lecture) {
        foreach ($lecture->getAuthor() as $user) {
            if ($user->getIsBanni()) {
                throw $this->createNotFoundException();
            }
        }

        $deleteForm = $this->createDeleteForm($lecture);

        $locale = $request->getLocale();
        $em = $this->getDoctrine()->getManager();
        $charts = $em->getRepository('AdminBundle:ValidationChart')->findBy(array(), null, 1);
        if (empty($charts)) {
            $chart = new ValidationChart();
            $chart->setChart("");
            $chart->setChartEn("");
            $em->persist($chart);
            $em->flush();
            $charts[] = $chart;
        }
        $form = $this->createFormBuilder(null, array('method' => "POST", 'attr' => array('id' => 'authors')))
                ->add('author', EntityType::class, array(
                    'class' => 'UserBundle:User',
                    'multiple' => true,
                    'expanded' => false,
                    'mapped' => false,
                ))
                ->getForm();

        $form->get('author')->setData($lecture->getAuthor());

        $form->handleRequest($request);

        $commentsForm = $this->createForm(CommentType::class);

        $form_reply = $this->createForm(CommentType::class);

        if ($form->isSubmitted()) {
            $authors = $form->get('author')->getData();

            foreach ($authors as $author) {
                if (!$lecture->getAuthor()->contains($author)) {
                    $lecture->addAuthor($author);
                    $em->merge($lecture);
                }
            }
            $em->flush();
            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }
        $commentsForm->handleRequest($request);

        if ($commentsForm->isSubmitted()) {
            $commentData = $commentsForm->get('comment')->getData();
            if ($commentData != null or $commentData != "") {
                $comment = new Comment();
                $comment->setContent($commentData);
                $comment->setDate(new \DateTime());
                $comment->setLecture($lecture);
                $comment->setUser($this->getUser());
                $em->persist($comment);
                $em->flush();
            }
            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }


        $form_reply->handleRequest($request);

        if ($form_reply->isSubmitted()) {
            $replayData = $form_reply->get('comment')->getData();
            if ($replayData != null or $replayData != "") {

                $replay = new Comment();
                $comment = $em->getRepository('LectureBundle:Comment')->findOneBy(array('id' => intval($request->get('comment'))));
                $replay->setContent($replayData);
                $replay->setComment($comment);
                $replay->setDate(new \DateTime());
                $replay->setLecture($lecture);
                $replay->setUser($this->getUser());
                $em->persist($replay);
                $em->flush();
            }
            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }
        if (strtolower($locale) == 'fr') {
            $chart = $charts[0];
            $validationChart = $chart->getChart();
        } else {
            $chart = $charts[0];
            $validationChart = $chart->getChartEn();
        }
        return $this->render('lecture/show.html.twig', array(
                    'lecture' => $lecture,
                    'validationChart' => $validationChart,
                    'delete_form' => $deleteForm->createView(),
                    'form' => $form->createView(),
                    'commentsForm' => $commentsForm->createView(),
                    'replyForm' => $form_reply->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing lecture entity.
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/{id}/edit", name="lecture_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Lecture $lecture) {
        $isAuthor = false;
        foreach ($lecture->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($lecture);
        $editForm = $this->createForm('LectureBundle\Form\LectureType', $lecture);
        $editForm->get('category')->setData($lecture->getCategory());
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $uploaded_file = $editForm['cover']->getData();
            foreach ($editForm->get('category')->getData() as $category) {
                $lecture->addCategory($category);
            }
            if ($uploaded_file != null) {
                $file = $lecture->getCover();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move('uploads/lecture/cover/', $fileName
                );
                $lecture->setCover($fileName);
            } else {
                $em->clear();
                $lecture_tmp = $em->getRepository('LectureBundle:Lecture')->findOneBy(array('id' => $lecture->getId()));
                $lecture->setCover($lecture_tmp->getCover());
            }
            $em->merge($lecture);
            $em->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/new.html.twig', array(
                    'title' => $this->get('translator')->trans('lecture.edit'),
                    'lecture' => $lecture,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'message' => 'edit',
        ));
    }

    /**
     * Add a new part
     *
     * @Route("/{id}/new_part", name="lecture_add_part")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Method({"GET", "POST"})
     */
    public function addPartAction(Request $request, Lecture $lecture) {

        $isAuthor = false;
        foreach ($lecture->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }
        $part = new Part();
        $editForm = $this->createForm('LectureBundle\Form\PartType', $part);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $lecture->addPart($part);
            $em->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/part.html.twig', array(
                    'title' => $this->get('translator')->trans('lecture.new_part'),
                    'lecture' => $lecture,
                    'part' => $part,
                    'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a lecture entity.
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/{id}", name="lecture_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Lecture $lecture) {
        $isAuthor = false;
        foreach ($lecture->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createDeleteForm($lecture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($lecture);
            $em->flush($lecture);
        }

        return $this->redirectToRoute('lecture_index');
    }

    /**
     * Creates a form to delete a lecture entity.
     *
     * @param Lecture $lecture The lecture entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Lecture $lecture) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('lecture_delete', array('id' => $lecture->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * search lecture
     * $data['category']
     * @Route("/search/", name="lecture_search")
     * @Method({"GET", "POST"})
     */
    public function searchAction(Request $request) {
        extract($_POST);

        if (!isset($maxResult)) {
            $maxResult = 24;
            $lastResult = 0;
        }
        $em = $this->getDoctrine()->getManager();
        $lectures = null;

        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $lectures = $em->getRepository('LectureBundle:Lecture')->searchLecture($form->getData(), $maxResult, $lastResult);
            $categories = $em->getRepository('LectureBundle:Category')->findAll();

            if ($request->isXMLHttpRequest()) {
                $data = array(
                    'lectures' => $lectures
                );
                $serializer = $this->container->get('serializer');
                $reports = $serializer->serialize($data, 'json');
                return new Response($reports);
            }

            return $this->render('lecture/indexAll.html.twig', array(
                        'lectures' => $lectures,
                        'categories' => $categories,
                        'form' => $form->createView(),
                        'search' => 'true'
            ));
        }

        return $this->render('lecture/search.html.twig', array(
                    'lectures' => $lectures,
                    'title' => 'search',
                    'form' => $form->createView(),
        ));
    }

    /**
     * search lecture
     * @Route("/search/category/{id}", name="lecture_search_category")
     * @Method({"GET", "POST"})
     */
    public function searchCategoryAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $lectures = null;
        $maxResult = 24;
        $lastResult = 0;
        $form = $this->createForm(SearchType::class);

        $form->handleRequest($request);
        if ($id != 'all') {
            $lectures = $em->getRepository('LectureBundle:Lecture')->searchLectureByCategory($id, $maxResult, $lastResult);
        } else {
            $lectures = $em->getRepository('LectureBundle:Lecture')->searchLectureByCategory(null, $maxResult, $lastResult);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $lectures = $em->getRepository('LectureBundle:Lecture')->searchLecture($form->getData(), $maxResult, $lastResult);
        }
        $categories = $em->getRepository('LectureBundle:Category')->findAll();

        return $this->render('lecture/indexAll.html.twig', array(
                    'lectures' => $lectures,
                    'categories' => $categories,
                    'title' => 'search',
                    'form' => $form->createView(),
                    'search' => 'true'
        ));
    }

    /**
     * My lectures
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/mylectures/", name="lecture_my_lectures")
     * @Method({"GET"})
     */
    public function myLecturesAction() {

        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $lectures = $em->getRepository('LectureBundle:Lecture')->getLectureByAuthor($user->getId());

        $OnGoingValidateLectures = array();
        $ValidateLectures = array();
        $DraftLectures = array();

        foreach ($lectures as $lecture) {
            if ($lecture->getStatut() == 'OnGoingValidate') {
                $OnGoingValidateLectures[] = $lecture;
            } elseif ($lecture->getStatut() == 'Validate') {
                $ValidateLectures[] = $lecture;
            } elseif ($lecture->getStatut() == 'Draft') {
                $DraftLectures[] = $lecture;
            }
        }

        return $this->render('lecture/mylectures.html.twig', array(
                    'OnGoingValidateLectures' => $OnGoingValidateLectures,
                    'ValidateLectures' => $ValidateLectures,
                    'DraftLectures' => $DraftLectures,
                    'user' => $user,
        ));
    }

    /**
     * search lecture
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/likemanager/{id}", name="lecture_like")
     * @Method({"GET", "POST"})
     */
    public function manageLikeAction(Request $request, Lecture $lecture) {

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $isLiking = false;
        foreach ($lecture->getLikers() as $key => $liker) {
            if ($liker->getId() == $user->getId())
                $isLiking = true;
        }

        if (!$isLiking)
            $lecture->addLiker($user);
        else
            $lecture->removeLiker($user);

        $em->flush();
        $route = "lecture_show";
        return $this->redirect($this->generateUrl($route, array('id' => $lecture->getId())));
    }

    /**
     * remove my lecture
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/{id}/remove-my-lecture", name="remove_my_lecture")
     * @Method({"GET", "DELETE"})
     */
    public function removeMyLectureAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LectureBundle:Lecture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lecture entity.');
        }

        $isAuthor = false;
        foreach ($lecture->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('remove_my_lecture', array('id' => $id)))
                ->setMethod('DELETE')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($form->isValid()) {
            $em->remove($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('administration.reports.text.remove_lecture_success'));
            return $response;
        } else {
            $content = $this->renderView('lecture/delete.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.remove_lecture'),
                'button' => $this->get('translator')->trans('global.delete'),
                'content' => $content
                    ])
            );
            return $response;
        }
    }

}
