<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Category;
use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Category controller.
 *
 * @Route("/{_locale}/comment")
 */
class CommentController extends Controller
{
    /**
     * Finds and displays all lecture entity.
     *
     * @Security("is_granted('ROLE_USER')")
     * @Route("/newsubcomment", name="subcomment_new")
     * @Method({"GET","POST"})
     */
    public function newSubcommentAction(Request $request){

        extract($_POST);
         $em = $this->getDoctrine()->getManager();
        $replayData=$_POST['subComment'.$numID];
        if ($replayData != null or $replayData != "") {

            $replay = new Comment();
            $lecture = $em->getRepository('LectureBundle:Lecture')->findOneBy(array('id' => $idLecture));
            $comment = $em->getRepository('LectureBundle:Comment')->findOneBy(array('id' => $idComment));
            $replay->setContent($replayData);
            $replay->setComment($comment);
            $replay->setDate(new \DateTime());
            $replay->setLecture($lecture);
            $replay->setUser($this->getUser());
            $em->persist($replay);
            $em->flush();
        }

         return $this->redirectToRoute('lecture_show', array('id' => $idLecture));
           
    }

    /**
     * Finds and displays all lecture entity.
     *
     * @Security("is_granted('ROLE_USER')")
     * @Route("/newsubcommentartwork", name="artwork_new_sub")
     * @Method({"GET","POST"})
     */
    public function newSubcommentartworkAction(Request $request){

        extract($_POST);
         $em = $this->getDoctrine()->getManager();
        $replayData=$_POST['subComment'.$numID];
        if ($replayData != null or $replayData != "") {

            $replay = new Comment();
            $artwork = $em->getRepository('ArtworkBundle:Artwork')->findOneBy(array('id' => $idArtwork));
            $comment = $em->getRepository('LectureBundle:Comment')->findOneBy(array('id' => $idComment));
            $replay->setContent($replayData);
            $replay->setComment($comment);
            $replay->setDate(new \DateTime());
            $replay->setArtwork($artwork);
            $replay->setUser($this->getUser());
            $em->persist($replay);
            $em->flush();
        }

         return $this->redirectToRoute('artwork_show', array('id' => $idArtwork));
           
    }

}
