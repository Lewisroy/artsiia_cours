<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Part;
use LectureBundle\Entity\Lecture;
use LectureBundle\Entity\Chapter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Part controller.
 *
 * @Route("/{_locale}/part")
 */
class PartController extends Controller {


    /**
     * Creates a new part entity.
     *
     * @Security("is_granted('ROLE_USER')")
     * @Route("/new", name="part_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $part = new Part();
        $form = $this->createForm('LectureBundle\Form\PartType', $part);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($part);
            $em->flush($part);

            return $this->redirectToRoute('part_show', array('id' => $part->getId()));
        }

        return $this->render('part/new.html.twig', array(
                    'part' => $part,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a part entity.
     *
     * @Route("/{id}", name="part_show")
     * @Method("GET")
     */
    public function showAction(Part $part) {
        $em = $this->getDoctrine()->getManager();
        $lecture = $em->getRepository('LectureBundle:Lecture')->getLectureByPart($part->getId());
        $isAuthor = false;
        foreach ($lecture[0]->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }
        foreach ($lecture[0]->getAuthor() as $user) {
            if($user->getIsBanni()){
               throw $this->createNotFoundException();
            }
        }

        $deleteForm = $this->createDeleteForm($part);

        return $this->render('part/show.html.twig', array(
                    'part' => $part,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing part entity.
     * @Security("is_granted('ROLE_USER')")
     * @Route("/{part}/{lecture}/edit", name="part_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Part $part, Lecture $lecture) {
        $editForm = $this->createForm('LectureBundle\Form\PartType', $part);
        $editForm->handleRequest($request);
        $isAuthor = false;
        foreach ($lecture->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture->getId()));
        }

        return $this->render('lecture/part.html.twig', array(
                    'part' => $part,
                    'lecture' => $lecture,
                    'form' => $editForm->createView(),
        ));
    }

    /**
     * Add a new chapter
     * @Security("is_granted('ROLE_USER')")
     * @Route("/{id}/new_chapter", name="part_add_chapter")
     * @Method({"GET", "POST"})
     */
    public function addChapterAction(Request $request, Part $part) {
        $chapter = new Chapter();
        $editForm = $this->createForm('LectureBundle\Form\ChapterType', $chapter);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $lecture = $em->getRepository('LectureBundle:Lecture')->getLectureByPart($part->getId());
        $isAuthor = false;
        foreach ($lecture[0]->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }
        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $part->addChapter($chapter);
            $em->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture[0]->getId()));
        }

        return $this->render('part/chapter.html.twig', array(
                    'title' => $this->get('translator')->trans('lecture.new_chapter'),
                    'lecture' => $lecture[0],
                    'part' => $part,
                    'chapter' => $chapter,
                    'form' => $editForm->createView(),
        ));
    }

    /**
     * edit a chapter
     * @Security("is_granted('ROLE_USER')")
     * @Route("/{part}/{chapter}/edit/chapter", name="part_edit_chapter")
     * @Method({"GET", "POST"})
     */
    public function editChapterAction(Request $request, Part $part, Chapter $chapter) {
        $editForm = $this->createForm('LectureBundle\Form\ChapterType', $chapter);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        
        $lecture = $em->getRepository('LectureBundle:Lecture')->getLectureByPart($part->getId());
        $isAuthor = false;
        foreach ($lecture[0]->getAuthor() as $author) {
            if ($author->getId())
                $isAuthor = true;
        }
        if (!$isAuthor) {
            throw $this->createAccessDeniedException();
        }


        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->flush();

            return $this->redirectToRoute('lecture_show', array('id' => $lecture[0]->getId()));
        }

        return $this->render('part/chapter.html.twig', array(
                    'title' => $this->get('translator')->trans('lecture.new_chapter'),
                    'lecture' => $lecture[0],
                    'part' => $part,
                    'chapter' => $chapter,
                    'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a part entity.
     * @Security("is_granted('ROLE_USER')")
     * @Route("/{id}/delete", name="part_delete")
     */
    public function deleteAction(Request $request, Part $part) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($part);
        $em->flush();
        $lecture = $request->get('lecture');

        return $this->redirectToRoute('lecture_show', array('id' => $lecture));
    }

    /**
     * Creates a form to delete a part entity.
     *
     * @param Part $part The part entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Part $part) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('part_delete', array('id' => $part->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
