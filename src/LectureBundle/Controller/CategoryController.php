<?php

namespace LectureBundle\Controller;

use LectureBundle\Entity\Category;
use LectureBundle\Entity\Lecture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Category controller.
 *
 * @Route("/{_locale}/category")
 */
class CategoryController extends Controller {

    /**
     * Lists all category entities.
     * 
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @return Category $categories 
     * @Route("/", name="category_index")
     * @Method("GET")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('LectureBundle:Category')->findAll();

        return $this->render('category/index.html.twig', array(
                    'categories' => $categories,
        ));
    }

    /**
     * Creates a new category entity.
     * 
     * @param Request $request
     * @return String title Title of the page
     * @return Category $category Category to create or create
     * @return \Symfony\Component\Form\Form $form Form of new Category
     * @return Integer $count Number of category
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/new", name="category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        
        $category = new Category();
        $form = $this->createForm('LectureBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if (!empty($category->getCover())) {
                $file = $category->getCover();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move('uploads/category/', $fileName);
                $category->setCover($fileName);
            }
            $em->persist($category);
            $em->flush($category);

            return $this->redirectToRoute('category_index');
        }

        $count = $this->getDoctrine()->getRepository('LectureBundle:Category')->getNbQueryBuilder();
        $title = null;
        $category = null;


        return $this->render('category/new.html.twig', array(
                    'title' => $this->container->get('translator')->trans('category.new'),
                    'category' => $category,
                    'form' => $form->createView(),
                    'count' => $count,
        ));
    }

    /**
     * Lists all category entities.
     *
     * @Route("/{id}", name="category_courses")
     * @Method("GET")
     */
    public function coursesAction(Category $category) {

        $em = $this->getDoctrine()->getManager();

        $lectures = $em->getRepository('LectureBundle:Lecture')->getLectureByCategory($category->getId());

        return $this->render('category/list.html.twig', array(
                    'lectures' => $lectures,
                    'category' => $category
        ));
    }

    /**
     * Finds and displays a category entity.
     * 
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param Category $category 
     * @return Category Category
     * @Route("/{id}", name="category_show")
     * @Method("GET")
     */
    public function showAction(Category $category) {

        return $this->render('category/show.html.twig', array(
                    'category' => $category,
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     * 
     * @param Request request
     * @param Category $category
     * @return String title Title of the page
     * @return \Symfony\Component\Form\Form $form The form for comment
     * @return Integer count Number of Categories
     * 
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}/edit", name="category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Category $category) {

        $editForm = $this->createForm('LectureBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (empty($editForm->get('cover')->getData())) {
                $em->clear();
                $category_tmp = $em->getRepository('LectureBundle:Category')->findOneBy(array('id' => $category->getId()));

                $category->setCover($category_tmp->getCover());
            } else {
                /** @var UploadedFile $file */
                $file = $editForm->get('cover')->getData();
                $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();
                $file->move(
                        $this->getParameter('category_directory'), $fileName
                );
                $category->setCover($fileName);
            }
            $em->flush();
            return $this->redirectToRoute('category_index');
        }

        $count = $this->getDoctrine()->getRepository('LectureBundle:Category')->getNbQueryBuilder();

        return $this->render('category/new.html.twig', array(
                    'title' => 'Edition catégorie',
                    'category' => $category,
                    'form' => $editForm->createView(),
                    'count' => $count,
        ));
    }

    /**
     * Deletes a category entity.
     * 
     * @param Request $request 
     * @param  Category $category
     * @return  render Index category
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category) {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush($category);
        }

        return $this->redirectToRoute('category_index');
    }

    /**
     * Return list of Subcategory of a category.
     * 
     * @param Request $request
     * @return Response $response Subcategories of Catgories
     *
     * @Route("/ajax-list/sub", name="category_subcategories")
     * @return JsonResponse
     */
    public function listSubCategories(Request $request) {
        $idCategory = $request->get('categories');
        $subCateg = $this->getDoctrine()->getManager()->getRepository('LectureBundle:SubCategory')->listByCategorie($idCategory);
        $serializer = $this->container->get('serializer');
        $response = $serializer->serialize($subCateg, 'json');

        return new Response($response);
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Category $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('category_delete', array('id' => $category->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     *
     * @Route("/list-sub-category/{id}", name="list_sub_category")
     * @Method("GET")
     */
    public function listSubCategory(Category $categ) {

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('LectureBundle:SubCategory')->getSubCategoriesByCateg($categ->getId());

        return new JsonResponse($categories);
    }

}
