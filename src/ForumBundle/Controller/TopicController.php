<?php

namespace ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ForumBundle\Form\TopicType;
use ForumBundle\Form\AnswerType;
use ForumBundle\Entity\Topic;
use ForumBundle\Entity\Answer;
use ForumBundle\Entity\Room;


/**
 * @Route("/{_locale}/forum/", name="forum")
 */
class TopicController extends Controller
{
	/**
     * New topic.
     *
     * @Route("topic/{slug}/new", name="topic_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $slug)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $topic = new Topic();
        $room = $em->getRepository('ForumBundle:Room')->findOneBy(array('slug' => $slug));


        $form = $this->createForm('ForumBundle\Form\TopicType', $topic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $topic->setRoom($room);
            $room->addTopic($topic);
            $topic->setDateOpening(new \DateTime());
            $topic->addWriter($this->getUser());
            $em->flush();

	        $locale = $request->getLocale();
	        if(strtolower($locale) == 'fr')
	            return $this->redirectToRoute('topic_show_fr', array('slug' => $topic->getSlug(), 'page' => 1));
	        if(strtolower($locale) == 'en')
	            return $this->redirectToRoute('topic_show_en', array('slug' => $topic->getSlug(), 'page' => 1));
	    }
        return $this->render('ForumBundle:Front:topic/new.html.twig', array('form' => $form->createView(), 'room' => $room));
    }

    /**
     * Show Topic.
     * @Route("sujet/{slug}/page_{page}", name="topic_show_fr")
     * @Route("topic/{slug}/page_{page}", name="topic_show_en")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request, $slug, $page = 1)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $answer = new Answer();

        $locale = $request->getLocale();
        if(strtolower($locale) == 'fr')
        	$topic = $em->getRepository('ForumBundle:Topic')->findOneBy(array('slug' => $slug));
        if(strtolower($locale) == 'en')
        	$topic = $em->getRepository('ForumBundle:Topic')->findOneBy(array('slug' => $slug));


        if($topic->getRemoved() || $topic->getHidden() || $topic->getRemovedByAdmin()) {
        		dump('Removed By User');
        		exit();

        }
        $answers = $em->getRepository('ForumBundle:Answer')->findAnswers($topic->getSlug(), ($page - 1)*10, 10);

        $form = $this->createForm('ForumBundle\Form\AnswerType', $answer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $answer->setTopic($topic);
            $topic->addAnswer($answer);
            $answer->setDateOpening(new \DateTime());
            $answer->setWriter($this->getUser());
            $em->flush();

            $locale = $request->getLocale();
	        if(strtolower($locale) == 'fr')
	            return $this->redirectToRoute('topic_show_fr', array('slug' => $topic->getSlug()));
	        if(strtolower($locale) == 'en')
	            return $this->redirectToRoute('topic_show_fr', array('slug' => $topic->getSluggit()));
        }

        return $this->render('ForumBundle:Front:topic/show.html.twig', 
        	array(
        		'form' 				=> $form->createView(), 
        		'answers' 			=> $answers,
        		'topic'				=> $topic,
        		'page'				=> $page,
        		'total_page'    	=> $em->getRepository('ForumBundle:Answer')->countAnswers($slug,10)
        	)
        );
    }
}