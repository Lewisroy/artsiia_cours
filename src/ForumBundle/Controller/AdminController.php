<?php

namespace ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use ForumBundle\Form\ForumType;
use ForumBundle\Form\RoomType;
use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Room;

/**
 * @Route("/{_locale}/admin/forum/", name="forum")
 */
class AdminController extends Controller
{

    /**
     * Forum index.
     *
     * @Route("/", name="admin_forum_index")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render('ForumBundle:Admin:index.html.twig');
    }

    /**
     * Forums and Rooms Management.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("manage", name="admin_forum_manage_forums")
     * @Method("GET")
     */
    public function manageForumAction()
    {

        $em = $this->getDoctrine()->getManager() ;
        
        return $this->render('ForumBundle:Admin:manage_forums.html.twig', 
            array(
                'forums' => $em->getRepository('ForumBundle:Forum')->findDefaults(),
                'lastPosition' => $em->getRepository('ForumBundle:Forum')->getLastPosition()
            )
        );
    }

    /**
     * New Forum.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("new/forum", name="admin_forum_new_forum")
     * @Method({"GET", "POST"})
     */
    public function newForumAction(Request $request)
    {
        $forum = new Forum();
        $editForm = $this->createForm('ForumBundle\Form\ForumType', $forum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($forum);
            $em->flush();

            return $this->redirectToRoute('admin_forum_manage_forums');
        }
        return $this->render('ForumBundle:Admin:new_forum.html.twig', array('form' => $editForm->createView()));
    }

    /**
     * Edit Forum.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("{id}/edit/forum", name="admin_forum_edit_forum")
     * @Security
     * @Method({"GET", "POST"})
     */
    public function editForumAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager() ;
        $forum = $em->getRepository('ForumBundle:Forum')->find($id);
        $editForm = $this->createForm('ForumBundle\Form\ForumType', $forum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($forum);
            $em->flush();

            return $this->redirectToRoute('admin_forum_manage_forums');
        }
        return $this->render('ForumBundle:Admin:new_forum.html.twig', array('form' => $editForm->createView()));
    }

    /**
     * New Room.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("new/{slug}/room", name="admin_forum_add_room")
     * @Method({"GET", "POST"})
     */
    public function newRoomAction(Request $request, Forum $forum)
    {
        
        $room = new Room();
        $editForm = $this->createForm('ForumBundle\Form\RoomType', $room);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $file = $room->getPicture();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                    $this->getParameter('uploads_room_cover'),
                $fileName
            );
            $room->setPicture($fileName);
            $forum->addRoom($room);
            $room->setForum($forum);
            $em->flush();

            return $this->redirectToRoute('admin_forum_manage_forums');
        }
        return $this->render('ForumBundle:Admin:new_room.html.twig', array('form' => $editForm->createView()));
    }

    /**
     * Edit Room.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("edit/{id}/room", name="admin_forum_edit_room")
     * @Method({"GET", "POST"})
     */
    public function editRoomAction(Request $request, Room $room)
    {
        

        $editForm = $this->createForm('ForumBundle\Form\RoomType', $room);
        $editForm->handleRequest($request);
        $picture = $room->getPicture();
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($editForm->get('picture')->getData() != null) {
                $file = $room->getPicture();
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                        $this->getParameter('uploads_room_cover'),
                    $fileName
                );
                $room->setPicture($fileName);
            }
            else {
                $room->setPicture($picture);
            }
            $em->flush();

            return $this->redirectToRoute('admin_forum_manage_forums');
        }
        return $this->render('ForumBundle:Admin:new_room.html.twig', array('form' => $editForm->createView()));
    }


}
