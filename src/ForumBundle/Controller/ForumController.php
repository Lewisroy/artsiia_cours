<?php

namespace ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ForumBundle\Form\ForumType;
use ForumBundle\Entity\Forum;
use ForumBundle\Entity\Room;


/**
 * @Route("/{_locale}/forum/", name="forum")
 */
class ForumController extends Controller
{

    /**
     * Forum index.
     *
     * @Route("", name="forum_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager() ;

        $forums = array();
        $tmp = $em->getRepository('ForumBundle:Forum')->findDefaults();

        foreach ($tmp as $forum) {
            $tmpForums = array( 
                'title'     => $forum->getTitle(),
                'titleEn'   => $forum->getTitleEn(),
                'slug'      => $forum->getSlug(),
                'slugEn'      => $forum->getSlugEn(),
            );
            $tmpRooms = $em->getRepository('ForumBundle:Room')->findForumRooms($forum->getSlug(), 3);
            $rooms = array();
            foreach ($tmpRooms as $room) {
                $tmpRoom = array(
                    'title'         => $room->getTitle(),
                    'titleEn'       => $room->getTitleEn(),
                    'picture'       => $room->getPicture(),
                    'slug'          => $room->getSlug(),
                    'slugEn'        => $room->getSlugEn(),
                    'description'   => $room->getDescription(),
                    'descriptionEn' => $room->getDescriptionEn(),
                );
                array_push($rooms, $tmpRoom);
            }
            $tmpForums['rooms'] = $rooms;

            array_push($forums, $tmpForums);

        }
        return $this->render('ForumBundle:Front:index.html.twig', 
            array(
                'forums' => $forums,
            )
        );
    }


    /**
     * Forum show.
     *
     * @Route("forum-{slug}/", name="forum_show")
     * @Method("GET")
     */
    public function showForumAction(Request $request, $slug = "")
    {
        $em = $this->getDoctrine()->getManager() ;
        $forum = null;
        $locale = $request->getLocale();
        if(strtolower($locale) == 'fr')
            $forum = $em->getRepository('ForumBundle:Forum')->findOneBy(array('slug' => $slug));
        if(strtolower($locale) == 'en')
            $forum = $em->getRepository('ForumBundle:Forum')->findOneBy(array('slugEn' => $slug));

        if($forum == null)
            dump("Mettre la page de forum non trouvé");

        $rooms = $em->getRepository('ForumBundle:Room')->findForumRooms($forum->getSlug(), 3);
        return $this->render('ForumBundle:Front:show.html.twig', 
            array(
                'forum' => $forum,
            )
        );
    }

    /**
     * Room show.
     *
     * @Route("salon-{slug}/page-{page}/", name="room_show")
     * @Method("GET")
     */
    public function showRoomAction(Request $request, $slug = "", $page = 1)
    {
        $em = $this->getDoctrine()->getManager() ;
        if($page < 1)
            dump('Cette page n existe pas');
        $room = $em->getRepository('ForumBundle:Room')->findOneBy(array('slug' => $slug));
        $topics = $em->getRepository('ForumBundle:Topic')->findRoomTopics($room->getSlug(), ($page - 1)*10, 10);
        return $this->render('ForumBundle:Front:show_room.html.twig', 
            array(
                'topics'        => $topics,
                'room'          => $room,
                'page'          => $page,
                'total_page'    => $em->getRepository('ForumBundle:Topic')->countTopicsInRoom($slug,10)
            )
        );
    }

    /**
     * Room show En.
     *
     * @Route("room-{slug}/page-{page}/", name="room_show_en")
     * @Method("GET")
     */
    public function showRoomEnAction(Request $request, $slug = "", $page = 1)
    {
        $em = $this->getDoctrine()->getManager() ;
        if($page < 1)
            dump('Cette page n existe pas');
        $room = $em->getRepository('ForumBundle:Room')->findOneBy(array('slugEn' => $slug));
        $topics = $em->getRepository('ForumBundle:Topic')->findRoomTopics($room->getSlug(), ($page - 1)*10, 10);
        return $this->render('ForumBundle:Front:show_room.html.twig', 
            array(
                'topics'        => $topics,
                'room'          => $room,
                'page'          => $page,
                'total_page'    => $em->getRepository('ForumBundle:Topic')->countTopicsInRoom($slug,10)
            )
        );
    }
}
