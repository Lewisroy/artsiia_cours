<?php

namespace ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class RoomType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array('label' => 'global.array.titleFr', 'required' => true))
                ->add('titleEn', TextType::class, array('label' => 'global.array.titleEn', 'required' => true))
                ->add('description', TextType::class, array('label' => 'Description', 'required' => true))
                ->add('descriptionEn', TextType::class, array('label' => 'Description(En)', 'required' => true))
                ->add('position', TextType::class, array('label' => 'forum.position', 'required' => true))
                ->add('picture', FileType::class, array('required' => false, 'label' => "Cover", 'data_class' => null));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ForumBundle\Entity\Room'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'forumbundle_room';
    }


}
