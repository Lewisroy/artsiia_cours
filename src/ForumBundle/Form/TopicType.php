<?php

namespace ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class TopicType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, array('label' => 'global.array.title', 'required' => true))
                ->add('language', ChoiceType::class, array(
                    'choices'  => array(
                        'Français' =>'fr',
                        'English' =>  'en',
                    ), 'label'=> 'language','required' => true))
                ->add('text',  CKEditorType::class, array(
                'label' => 'contact.message',
                'required'=> true,
                'config' => array(
                    'uiColor' => '#ffffff',
                    'toolbar' => array(
                    array(
                        'name'  => 'basicstyles',
                        'items' => array('Cut','Copy','Paste','PasteText','PasteFromWord','Undo','Redo','Bold','Italic','Underline','Strike','RemoveFormat', 'NumberedList','BulletedList', 'Outdent','Indent','Blockquote','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','HorizontalRule','Smiley','SpecialChar','Styles','Format','Font','FontSize','TextColor','BGColor','Maximise')
                    )),
                    'height'  => '280px',
                    'removeButtons' => 'elementspath',
                )
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ForumBundle\Entity\Topic'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'forumbundle_topic';
    }


}
