<?php

namespace ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class AnswerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text',  CKEditorType::class, array(
                'label' => 'contact.message',
                'required'=> true,
                'config' => array(
                    'uiColor' => '#1c262f',
                    'toolbar' => array(
                    array(
                        'name'  => 'basicstyles',
                        'items' => array('Cut','Copy','Paste','PasteText','PasteFromWord','Undo','Redo','Bold','Italic','Underline','Strike','RemoveFormat', 'NumberedList','BulletedList', 'Outdent','Indent','Blockquote','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Table','HorizontalRule','Smiley','SpecialChar','Styles','Format','Font','FontSize','TextColor','BGColor','Maximise')
                    )),
                    'height'  => '140px',
                    'colorButton_enableMore' => false,
                    'colorButton_backStyle' => array(
                        'element'   => 'span',
                        'styles'    => [ 'color' => '#1c262f' ]
                    ),
                    'dialog_backgroundCoverColor' =>  '#1c262f',
                    'removeButtons' => 'elementspath',
                )
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ForumBundle\Entity\Answer'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'forumbundle_answer';
    }


}
