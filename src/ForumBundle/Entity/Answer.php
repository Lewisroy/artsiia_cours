<?php

namespace ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Answer
 *
 * @ORM\Table(name="answer")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\AnswerRepository")
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOpening", type="datetimetz")
     */
    private $dateOpening;

    /**
     * @ORM\ManyToOne(targetEntity="ForumBundle\Entity\Topic", inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $topic;

    /**
     * @var boolean
     *
     * @ORM\Column(name="removed", type="boolean")
     */
    private $removed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="removedByAdmin", type="boolean")
     */
    private $removedByAdmin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean")
     */
    private $hidden;

    /**
     * writer of the message
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @var User
     */
    private $writer;


    
    function __construct() {
        $this->removed = false;
        $this->removedByAdmin = false;
        $this->hidden = false;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Answer
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set dateOpening
     *
     * @param \DateTime $dateOpening
     *
     * @return Answer
     */
    public function setDateOpening($dateOpening)
    {
        $this->dateOpening = $dateOpening;

        return $this;
    }

    /**
     * Get dateOpening
     *
     * @return \DateTime
     */
    public function getDateOpening()
    {
        return $this->dateOpening;
    }


    /**
     * Set removed
     *
     * @param boolean $removed
     *
     * @return Answer
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;

        return $this;
    }

    /**
     * Get removed
     *
     * @return boolean
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Set removedByAdmin
     *
     * @param boolean $removedByAdmin
     *
     * @return Answer
     */
    public function setRemovedByAdmin($removedByAdmin)
    {
        $this->removedByAdmin = $removedByAdmin;

        return $this;
    }

    /**
     * Get removedByAdmin
     *
     * @return boolean
     */
    public function getRemovedByAdmin()
    {
        return $this->removedByAdmin;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return Answer
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set topic
     *
     * @param \ForumBundle\Entity\Topic $topic
     *
     * @return Answer
     */
    public function setTopic(\ForumBundle\Entity\Topic $topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return \ForumBundle\Entity\Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set writer
     *
     * @param \UserBundle\Entity\User $writer
     *
     * @return Answer
     */
    public function setWriter(\UserBundle\Entity\User $writer = null)
    {
        $this->writer = $writer;

        return $this;
    }

    /**
     * Get writer
     *
     * @return \UserBundle\Entity\User
     */
    public function getWriter()
    {
        return $this->writer;
    }
}
