<?php

namespace ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Topic
 *
 * @ORM\Table(name="topic")
 * @ORM\Entity(repositoryClass="ForumBundle\Repository\TopicRepository")
 */
class Topic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *      min = 5,
     *      max = 255,
     *      minMessage = "name.minMessage",
     *      maxMessage = "name.maxMessage"
     * )
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

     /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255)
     */
    private $language;

    /**
     * @var string
     * @Assert\NotBlank(message="not_blank")
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateOpening", type="datetimetz")
     */
    private $dateOpening;

    /**
     * @Gedmo\Slug(fields={"title"}, updatable=false)
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;


    /**
     * @var boolean
     *
     * @ORM\Column(name="removed", type="boolean")
     */
    private $removed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="removedByAdmin", type="boolean")
     */
    private $removedByAdmin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="boolean")
     */
    private $hidden;

    /**
     * @ORM\ManyToOne(targetEntity="ForumBundle\Entity\Room", inversedBy="topics")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="viewers_topic")
     */
    private $viewers;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="authors_topic")
     */
    private $writers;


    /**
     * @ORM\OneToMany(targetEntity="ForumBundle\Entity\Answer", mappedBy="answer", cascade ={"persist"})
     */
    private $answer;




    function __construct() {
        $this->removed = false;
        $this->removedByAdmin = false;
        $this->hidden = false;
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Topic
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set removed
     *
     * @param boolean $removed
     *
     * @return Topic
     */
    public function setRemoved($removed)
    {
        $this->removed = $removed;

        return $this;
    }

    /**
     * Get removed
     *
     * @return boolean
     */
    public function getRemoved()
    {
        return $this->removed;
    }

    /**
     * Set removedByAdmin
     *
     * @param boolean $removedByAdmin
     *
     * @return Topic
     */
    public function setRemovedByAdmin($removedByAdmin)
    {
        $this->removedByAdmin = $removedByAdmin;

        return $this;
    }

    /**
     * Get removedByAdmin
     *
     * @return boolean
     */
    public function getRemovedByAdmin()
    {
        return $this->removedByAdmin;
    }

    /**
     * Set hidden
     *
     * @param boolean $hidden
     *
     * @return Topic
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return boolean
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set room
     *
     * @param \ForumBundle\Entity\Room $room
     *
     * @return Topic
     */
    public function setRoom(\ForumBundle\Entity\Room $room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \ForumBundle\Entity\Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Topic
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set dateOpening
     *
     * @param \DateTime $dateOpening
     *
     * @return Topic
     */
    public function setDateOpening($dateOpening)
    {
        $this->dateOpening = $dateOpening;

        return $this;
    }

    /**
     * Get dateOpening
     *
     * @return \DateTime
     */
    public function getDateOpening()
    {
        return $this->dateOpening;
    }

    /**
     * Add viewer
     *
     * @param \UserBundle\Entity\User $viewer
     *
     * @return Topic
     */
    public function addViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers[] = $viewer;

        return $this;
    }

    /**
     * Remove viewer
     *
     * @param \UserBundle\Entity\User $viewer
     */
    public function removeViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers->removeElement($viewer);
    }

    /**
     * Get viewers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewers()
    {
        return $this->viewers;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Topic
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Add writer
     *
     * @param \UserBundle\Entity\User $writer
     *
     * @return Topic
     */
    public function addWriter(\UserBundle\Entity\User $writer)
    {
        $this->writers[] = $writer;

        return $this;
    }

    /**
     * Remove writer
     *
     * @param \UserBundle\Entity\User $writer
     */
    public function removeWriter(\UserBundle\Entity\User $writer)
    {
        $this->writers->removeElement($writer);
    }

    /**
     * Get writers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWriters()
    {
        return $this->writers;
    }

    /**
     * Add answer
     *
     * @param \ForumBundle\Entity\Answer $answer
     *
     * @return Topic
     */
    public function addAnswer(\ForumBundle\Entity\Answer $answer)
    {
        $this->answer[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \ForumBundle\Entity\Answer $answer
     */
    public function removeAnswer(\ForumBundle\Entity\Answer $answer)
    {
        $this->answer->removeElement($answer);
    }

    /**
     * Get answer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
