<?php

namespace ReportBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserBundle\Entity\User;

class BanishUser {

    private $_container;
    private $check;
    private $_route;
    private $_controller;

    public function __construct(Container $container) {
        $this->_container = $container;
        $this->check = 0;
    }

    public function onKernelRequest(GetResponseEvent $event) {

        $token = $this->_container->get('security.token_storage')->getToken();
        $em = $this->_container->get('doctrine')->getManager();
        $twig = $this->_container->get('twig');

        $request = $event->getRequest();
        if ($this->check == 0) {
            $this->_route = $request->attributes->get('_route');
            $this->_controller = $request->attributes->get('_controller');
            $this->check++;
        }

        $allow_routes = [
            'banishment_routes_check_user',
        ];
        $allow_controllers = [
            'ReportBundle\Controller\BanishmentController::checkAction',
        ];

        if ($token) {
            $user = $token->getUser();
            if ($user instanceof User) {
                $reports = $em->getRepository('ReportBundle:Reporting')->getReportForUser($user->getId());
                foreach ($reports as $r) {
                    if ($r->getBanishment()) {
                        if ($r->getBanishment()->getEndbanish()) {
                            $now = date('Y-m-d H:i:s');
                            $now_date = new \DateTime($now);
                            if ($now_date < $r->getBanishment()->getEndbanish()) {
                                $response = new RedirectResponse($this->_container->get('router')->generate('banishment_routes_check_user', ['id' => $r->getId()]));
                                if (!in_array($this->_route, $allow_routes) && !in_array($this->_controller, $allow_controllers)) {
                                    $event->setResponse($response);
                                }
                            }
                        }
                    }
                }
            }
            
            $AgreePrivacyPolicy = false;
            $AgreeDisclaimer = false;
            if ($user instanceof User) {
                if ($user->getAgreePrivacyPolicy()) {
                    $AgreePrivacyPolicy = true;
                }
                if ($user->getAgreeDisclaimer()) {
                    $AgreeDisclaimer = true;
                }
            }
            $twig->addGlobal('agree_use', [
                'AgreePrivacyPolicy' => $AgreePrivacyPolicy,
                'AgreeDisclaimer' => $AgreeDisclaimer
            ]);
        }
    }

}
