<?php

namespace ReportBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReportingType extends AbstractType
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('type')
//            ->add('mark')
            ->add('description', null, [
                'attr' => [
                    'class' => 'form-control textarea-color',
                    'rows' => '3',
                    'placeholder' => $this->translator->trans('administration.reports.textarea'),
                ]
            ])
            ->add('subject', EntityType::class, [
                'constraints' => [
                    new NotBlank(array(
                                     'message' => $this->translator->trans('administration.reports.report_subjects.required_subject'),
                                 ))
                ],
                'class'         => 'ReportBundle\Entity\Subjects',
                'query_builder' => function (\ReportBundle\Repository\SubjectsRepository $b) {
                    return $b->createQueryBuilder('s')
                             ->orderBy('s.severity', 'asc');
                },
                'choice_label'  => 'subject',
                'label'         => $this->translator->trans('administration.reports.subject_list') . ' :',
                'attr'          => [
                    'class' => 'form-control'
                ]
            ])
//            ->add('reporter')
//            ->add('artwork')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                                   'data_class' => 'ReportBundle\Entity\Reporting'
                               ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reportbundle_reporting';
    }


}
