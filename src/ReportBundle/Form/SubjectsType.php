<?php

namespace ReportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraints\NotBlank;

class SubjectsType extends AbstractType
{
    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject', TextType::class, [
                'constraints' => [
                    new NotBlank(array(
                                     'message' => $this->translator->trans('administration.reports.report_subjects.required_subject'),
                                 ))
                ],
                'attr'        => [
                    'class'       => 'form-input',
                    'placeholder' => $this->translator->trans('administration.reports.report_subjects.new_subject')
                ],
                'required'    => false
            ])
            ->add('severity', ChoiceType::class, [
                'attr'    => [
                    'class' => 'form-input select-style'
                ],
                'choices' => [
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                                   'data_class' => 'ReportBundle\Entity\Subjects'
                               ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'reportbundle_subjects';
    }


}
