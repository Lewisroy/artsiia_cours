<?php

namespace ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\NotBlank;
use ReportBundle\Entity\ReportMessage;
use ReportBundle\Entity\Banishment;
use ReportBundle\Entity\Reporting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class BanishmentController extends Controller {

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * 
     * @return render
     */
    public function homepageAction() {

        return $this->render('ReportBundle:Banishment:index.html.twig');
    }

    /**
     * 
     * @return render
     * @return Reporting banis Differents bannishement on going
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function inprogressAction() {

        $em = $this->getDoctrine()->getManager();
        $now = date('Y-m-d H:i:s');
        $banis = $em->getRepository('ReportBundle:Reporting')->getBanishInProgress($now);

        return $this->render('ReportBundle:Banishment:inprogress.html.twig', [
                    'banis' => $banis,
        ]);
    }

    /**
     * bannishement history
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @return render 
     */
    public function historyAction() {

        $em = $this->getDoctrine()->getManager();
        $now = date('Y-m-d H:i:s');
        $banis = $em->getRepository('ReportBundle:Reporting')->getBanishInHistory($now);

        return $this->render('ReportBundle:Banishment:history.html.twig', [
                    'banis' => $banis,
        ]);
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function checkAction(Request $req, $id) {
        $em = $this->getDoctrine()->getManager();
        $report = $em->getRepository('ReportBundle:Reporting')->find($id);

        if (!$report) {
            throw $this->createNotFoundException('The report does not exist');
        }
        if ($report->getBanishment()) {
            $now = date('Y-m-d H:i:s');
            $now_date = new \DateTime($now);
            if ($now_date >= $report->getBanishment()->getEndbanish()) {
                return $this->redirectToRoute('au_homepage');
            }
        } else {
            return $this->redirectToRoute('au_homepage');
        }

        $messages = $em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id);

        foreach ($messages as $msg) {
            $msg->setIsread(true);
            $em->persist($msg);
        }
        $em->flush();

        $form = $this->createFormBuilder()
                ->add('message', TextareaType::class, [
                    'constraints' => [
                        new NotBlank(array(
                            'message' => $this->get('translator')->trans('administration.reports.report_subjects.required_message'),
                                ))
                    ],
                    'attr' => [
                        'class' => 'form-control',
                        'rows' => 3,
                        'placeholder' => $this->get('translator')->trans('administration.reports.text.placeholder_message')
                    ],
                    'required' => false
                ])
                ->getForm();

        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()) {
            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id));
            $message = new ReportMessage();

            $message->setContent($form->getData()['message']);
            $message->setSender($this->getUser());
            $message->setReceiver($em->getRepository('UserBundle:User')->find($report->getBanishment()->getAdmin()));
            $message->setReport($report);
            $message->setIsread(true);
            $message->setIsreadadmin(false);
            $message->setIsparent(($check) ? false : true);
            $em->persist($message);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('administration.reports.text.add_message_success'));
            return $this->redirectToRoute('banishment_routes_check_user', ['id' => $id]);
        }
        return $this->render('ReportBundle:Banishment:check.html.twig', [
                    'report' => $report,
                    'messagesUsers' => $messages,
                    'form' => $form->createView()
        ]);
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @param type $days
     * @return Response|JsonResponse
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function banishReportAction(Request $request, $id, $days) {

        if ($days < 1 || $days > 90) {
            $out = [
                'error' => false,
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.add_banishy_error')
            ];
            return new JsonResponse($out);
        }

        $em = $this->getDoctrine()->getManager();
        $report = $em->getRepository('ReportBundle:Reporting')->find($id);

        if ($report->getBanishment()) {
            $out = [
                'error' => false,
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.add_banish_success_exist')
            ];
            return new JsonResponse($out);
        }

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('banishment_routes_add_banish_report', ['id' => $id, 'days' => $days]))
                ->setMethod('DELETE')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($form->isValid()) {

            $now = date('Y-m-d H:i:s');
            $start = new \DateTime($now);
            $end_date = $start->add(new \DateInterval('P' . $days . 'D'));

            $banishment = new Banishment();

            $banishment->setEndbanish($end_date);
            $banishment->setAdmin($this->getUser());
            $banishment->setDays($days);
            $em->persist($banishment);
            $report->setBanishment($banishment);
            $report->setType(Reporting::TYPE_BANISHED_INPROGRESS);
            $em->flush();

            $response->setContent(
                    json_encode([
                'error' => false,
                'status' => true,
                'message' => $this->get('translator')->trans('administration.reports.text.add_banish_success')
                    ])
            );
            return $response;
        } else {
            $content = $this->renderView('ReportBundle:Banishment:banishProfile.html.twig', array(
                'report' => $report,
                'id' => $id,
                'days' => $days,
                'form' => $form->createView(),
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.button.ban_artist_profile'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                'error' => true,
                'status' => true,
                    ])
            );
            return $response;
        }
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @return Response
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function stopBanishReportAction(Request $request, $id) {
        
        $em = $this->getDoctrine()->getManager();
        $report = $em->getRepository('ReportBundle:Reporting')->find($id);
        if (!$report) {
            throw $this->createNotFoundException('The report does not exist');
        }
        $banishment = $em->getRepository('ReportBundle:Banishment')->find($report->getBanishment()->getId());

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('banishment_routes_stop_banish_report', ['id' => $id]))
                ->setMethod('POST')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($form->isValid()) {
            $now = date('Y-m-d H:i:s');
            $now = new \DateTime($now);
            $banishment->setEndbanish($now);
            $report->setType(Reporting::TYPE_BANISHED_HISTORY);
            $em->flush();

            $response->setContent(
                    json_encode([
                'error' => false,
                'status' => true,
                'message' => $this->get('translator')->trans('administration.reports.text.stop_banish_success')
                    ])
            );
            return $response;
        } else {
            $content = $this->renderView('ReportBundle:Banishment:stopBanish.html.twig', array(
                'report' => $report,
                'id' => $id,
                'form' => $form->createView(),
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.button.stop_ban_artist_profile'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                'error' => true,
                'status' => true,
                    ])
            );
            return $response;
        }
    }

}
