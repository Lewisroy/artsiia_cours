<?php

namespace ReportBundle\Controller;

use ReportBundle\Entity\Subjects;
use ReportBundle\Form\SubjectsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DefaultController extends Controller
{
    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * 
     * @param Request $req
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $req)
    {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN'))
        {
            throw $this->createAccessDeniedException();
        }

        $em       = $this->getDoctrine()->getManager();
        $subjects = $em->getRepository('ReportBundle:Subjects')->findAllOrderSeverity();
        $subject  = new Subjects();
        $form     = $this->createForm(SubjectsType::class, $subject, [
            'action' => $this->generateUrl('report_routes_homepage'),
            'method' => 'POST'
        ]);
        $form->handleRequest($req);
        if ($form->isValid())
        {
            $em->persist($subject);
            $em->flush();
            return $this->redirectToRoute('report_routes_homepage');
        }
        return $this->render('ReportBundle:Default:index.html.twig', [
            'form'     => $form->createView(),
            'subjects' => $subjects
        ]);
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param Request $req
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeSubjectsAction(Request $req)
    {
        if (count($req->request->get('checksubjects')) != 0)
        {
            $em = $this->getDoctrine()->getManager();
            foreach ($req->request->get('checksubjects') as $id)
            {
                $check   = count($em->getRepository('ReportBundle:Reporting')->findWithSubject($id));
                $subject = $em->getRepository('ReportBundle:Subjects')->find($id);
                if ($check && $subject)
                {
                    $this->addFlash('warning', $this->get('translator')->trans('administration.reports.text.unable_delete_subject', ['%subject%' => $subject->getSubject()]));
                }
                if (!$check && $subject)
                {
                    $em->remove($subject);
                }

            }
            $em->flush();
        }
        return $this->redirectToRoute('report_routes_homepage');
    }
}
