<?php

namespace ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ReportingUntreatedController extends Controller {

    /**
     * @return Response
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();

        $now = date('Y-m-d H:i:s');
        $start = new \DateTime($now);
        $start_1d = $start->sub(new \DateInterval('P1D'))->format('Y-m-d H:i:s');
        $start = new \DateTime($now);
        $start_2d = $start->sub(new \DateInterval('P2D'))->format('Y-m-d H:i:s');
        $start = new \DateTime($now);
        $start_3d = $start->sub(new \DateInterval('P3D'))->format('Y-m-d H:i:s');

        $artworks24 = $em->getRepository('ReportBundle:Reporting')->getArtworksUntreated($start_1d, $now);
        $artworks48 = $em->getRepository('ReportBundle:Reporting')->getArtworksUntreated($start_2d, $start_1d);
        $artworks72 = $em->getRepository('ReportBundle:Reporting')->getArtworksUntreated($start_3d, $start_2d);

        $profile24 = $em->getRepository('ReportBundle:Reporting')->getProfileUntreated($start_1d, $now);
        $profile48 = $em->getRepository('ReportBundle:Reporting')->getProfileUntreated($start_2d, $start_1d);
        $profile72 = $em->getRepository('ReportBundle:Reporting')->getProfileUntreated($start_3d, $start_2d);

        $comments24 = $em->getRepository('ReportBundle:Reporting')->getCommentsUntreated($start_1d, $now);
        $comments48 = $em->getRepository('ReportBundle:Reporting')->getCommentsUntreated($start_2d, $start_1d);
        $comments72 = $em->getRepository('ReportBundle:Reporting')->getCommentsUntreated($start_3d, $start_2d);
        $activitys24 = $em->getRepository('ReportBundle:Reporting')->getActivitysUntreated($start_1d, $now);
        $activitys48 = $em->getRepository('ReportBundle:Reporting')->getActivitysUntreated($start_2d, $start_1d);
        $activitys72 = $em->getRepository('ReportBundle:Reporting')->getActivitysUntreated($start_3d, $start_2d);

        return $this->render('ReportBundle:ReportingUntreated:index.html.twig', [
                    'artworks24' => $artworks24,
                    'artworks48' => $artworks48,
                    'artworks72' => $artworks72,
                    'profile24' => $profile24,
                    'profile48' => $profile48,
                    'profile72' => $profile72,
                    'comments24' => $comments24,
                    'comments48' => $comments48,
                    'comments72' => $comments72,
                    'activitys24' => $activitys24,
                    'activitys48' => $activitys48,
                    'activitys72' => $activitys72,
        ]);
    }

}
