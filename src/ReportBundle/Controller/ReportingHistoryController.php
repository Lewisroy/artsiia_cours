<?php

namespace ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ReportingHistoryController extends Controller
{
    /**
     * @return Response
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $artworks = $em->getRepository('ReportBundle:Reporting')->getArtworksHistory();
        $profiles = $em->getRepository('ReportBundle:Reporting')->getProfilesHistory();
        $comments = $em->getRepository('ReportBundle:Reporting')->getCommentsHistory();
        $activitys = $em->getRepository('ReportBundle:Reporting')->getActivitysHistory();

        $now = date('Y-m-d H:i:s');
        $banishedUsers = $em->getRepository('ReportBundle:Reporting')->getListUserBannished($now);

        return $this->render('ReportBundle:ReportingHistory:index.html.twig', [
            'artworks' => $artworks,
            'profiles' => $profiles,
            'comments' => $comments,
            'activitys' => $activitys,
            'banishedUsers' => $banishedUsers,
        ]);
    }
}
