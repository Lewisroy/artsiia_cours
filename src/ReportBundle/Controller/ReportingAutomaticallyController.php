<?php

namespace ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ReportingAutomaticallyController extends Controller
{
    /**
     * @return Response
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $artworks = $em->getRepository('ReportBundle:Reporting')->getArtworksAutomatically();
        $profiles = $em->getRepository('ReportBundle:Reporting')->getProfilesAutomatically();
        $comments = $em->getRepository('ReportBundle:Reporting')->getCommentsAutomatically();
        $activitys = $em->getRepository('ReportBundle:Reporting')->getActivitysAutomatically();

        $now = date('Y-m-d H:i:s');
        $banishedUsers = $em->getRepository('ReportBundle:Reporting')->getListUserBannished($now);

        return $this->render('ReportBundle:ReportingAutomatically:index.html.twig', [
            'artworks' => $artworks,
            'profiles' => $profiles,
            'comments' => $comments,
            'activitys' => $activitys,
            'banishedUsers' => $banishedUsers,
        ]);
    }
}
