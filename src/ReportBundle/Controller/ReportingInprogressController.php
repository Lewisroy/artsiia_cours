<?php

namespace ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ReportingInprogressController extends Controller {

    /**
     * @return Response
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function indexAction() {
    
        $em = $this->getDoctrine()->getManager();

        $artworks = $em->getRepository('ReportBundle:Reporting')->getArtworksInprogress();
        $profiles = $em->getRepository('ReportBundle:Reporting')->getProfilesInprogress();
        $comments = $em->getRepository('ReportBundle:Reporting')->getCommentsInprogress();
        $activitys = $em->getRepository('ReportBundle:Reporting')->getActivitysInprogress();

        $now = date('Y-m-d H:i:s');
        $banishedUsers = $em->getRepository('ReportBundle:Reporting')->getListUserBannished($now);

        return $this->render('ReportBundle:ReportingInprogress:index.html.twig', [
                    'artworks' => $artworks,
                    'profiles' => $profiles,
                    'comments' => $comments,
                    'activitys' => $activitys,
                    'banishedUsers' => $banishedUsers,
        ]);
    }

}
