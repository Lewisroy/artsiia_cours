<?php

namespace ReportBundle\Controller;

use ReportBundle\Entity\Reporting;
use ReportBundle\Entity\ReportMessage;
use ReportBundle\Form\ReportingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class ReportingController extends Controller {

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function reportArtworkAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $artwork = $em->getRepository('ArtworkBundle:Artwork')->find($id);
        $check = false;
        foreach ($artwork->getReporting() as $report) {
            if ($report->getReporter()->getId() == $this->getUser()->getId()) {
                $check = true;
            }
        }

        $entity = new Reporting();
        $entity->setReporteduser($artwork->getAuthor()[0]);
        $entity->setType(Reporting::TYPE_UNTREATED);
        $entity->setMark(false);
        $entity->setReporter($this->getUser());
        $entity->setArtwork($artwork);
        $form = $this->createForm(ReportingType::class, $entity, array(
            'action' => $this->generateUrl('report_routes_add_reporting_artwork', array('id' => $id)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('administration.reports.report_notif'));
            return $response;
        } else {
            $content = $this->renderView('ReportBundle:Reporting:reportArtwork.html.twig', array(
                'entity' => $entity,
                'artwork' => $artwork,
                'id' => $id,
                'form' => $form->createView(),
                'check' => $check
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.report_artwork'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.signaler'),
                'error' => true
                    ])
            );
            return $response;
        }
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @return Response
     */
    public function reportProfilAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $profil = $em->getRepository('UserBundle:User')->find($id);
        $check = false;
        foreach ($profil->getReportingprofile() as $report) {
            if ($report->getReporter()->getId() == $this->getUser()->getId()) {
                $check = true;
            }
        }

        $entity = new Reporting();
        $entity->setReporteduser($profil);
        $entity->setType(Reporting::TYPE_UNTREATED);
        $entity->setMark(false);
        $entity->setReporter($this->getUser());
        $entity->setProfile($profil);
        $form = $this->createForm(ReportingType::class, $entity, array(
            'action' => $this->generateUrl('report_routes_add_reporting_profil', array('id' => $id)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('administration.reports.report_notif'));
            return $response;
        } else {
            $content = $this->renderView('ReportBundle:Reporting:reportProfil.html.twig', array(
                'entity' => $entity,
                'id' => $id,
                'form' => $form->createView(),
                'check' => $check
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.report_profil'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.signaler'),
                'error' => true
                    ])
            );
            return $response;
        }
    }

    /**
     * 
     * @param Request $request
     * @param type $id
     * @return Response
     */
    public function reportCommentAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('LectureBundle:Comment')->find($id);
        $check = false;
        foreach ($comment->getReporting() as $report) {
            if ($report->getReporter()->getId() == $this->getUser()->getId()) {
                $check = true;
            }
        }
        $entity = new Reporting();
        $entity->setReporteduser($comment->getUser());
        $entity->setType(Reporting::TYPE_UNTREATED);
        $entity->setMark(false);
        $entity->setReporter($this->getUser());
        $entity->setComment($comment);
        $form = $this->createForm(ReportingType::class, $entity, array(
            'action' => $this->generateUrl('report_routes_add_reporting_comment', array('id' => $id)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('administration.reports.report_notif'));
            return $response;
        } else {
            $content = $this->renderView('ReportBundle:Reporting:reportComment.html.twig', array(
                'entity' => $entity,
                'id' => $id,
                'form' => $form->createView(),
                'check' => $check
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.button.report_comment'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.signaler'),
                'error' => true
                    ])
            );
            return $response;
        }
    }

    public function reportActivityAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $activity = $em->getRepository('UserBundle:Activity')->find($id);
        $check = false;
        foreach ($activity->getReporting() as $report) {
            if ($report->getReporter()->getId() == $this->getUser()->getId()) {
                $check = true;
            }
        }
        $entity = new Reporting();
        $entity->setReporteduser($activity->getAuthor()[0]);
        $entity->setType(Reporting::TYPE_UNTREATED);
        $entity->setMark(false);
        $entity->setReporter($this->getUser());
        $entity->setActivity($activity);
        $form = $this->createForm(ReportingType::class, $entity, array(
            'action' => $this->generateUrl('report_routes_add_reporting_activity', array('id' => $id)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                    ])
            );
            $this->addFlash('success', $this->get('translator')->trans('administration.reports.report_notif'));
            return $response;
        } else {
            $content = $this->renderView('ReportBundle:Reporting:reportActivity.html.twig', array(
                'entity' => $entity,
                'id' => $id,
                'form' => $form->createView(),
                'check' => $check
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('administration.reports.button.report_activity'),
                'content' => $content,
                'button' => $this->get('translator')->trans('administration.reports.button.signaler'),
                'error' => true
                    ])
            );
            return $response;
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function showMessagesAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $messages = $em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id);

        foreach ($messages as $msg) {
            $msg->setIsreadadmin(true);
            $em->persist($msg);
        }
        $em->flush();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        $content = $this->renderView('ReportBundle:Reporting:reportMessages.html.twig', array(
            'messages' => $messages,
        ));
        $response->setContent(
                json_encode([
            'title' => $this->get('translator')->trans('administration.reports.text.list_messages_exchanged'),
            'content' => $content,
            'button' => $this->get('translator')->trans('administration.reports.button.close'),
            'error' => true
                ])
        );
        return $response;
    }

    /**
     * @param Request $req
     * @return JsonResponse
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function sendReportMessageAction(Request $req) {

        try {

            $em = $this->getDoctrine()->getManager();

            $report = $em->getRepository('ReportBundle:Reporting')->find($req->get('id'));
            $sender = $em->getRepository('UserBundle:User')->find($req->get('sender'));
            $receiver = $em->getRepository('UserBundle:User')->find($req->get('receiver'));
            $msg = $req->get('message');

            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($req->get('id')));

            $message = new ReportMessage();

            if ($report->getType() != Reporting::TYPE_FENCEDAUTOMATICALLY) {
                $report->setType(Reporting::TYPE_INPROGRESS);
                $em->persist($report);
            }

            $message->setContent($msg);
            $message->setSender($sender);
            $message->setReceiver($receiver);
            $message->setReport($report);
            $message->setIsread(false);
            $message->setIsreadadmin(false);
            $message->setIsparent(($check) ? false : true);
            $em->persist($message);
            $em->flush();
            $out = [
                'status' => true,
                'message' => $this->get('translator')->trans('administration.reports.text.add_message_success')
            ];
        } catch (\Exception $e) {
            $out = [
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.add_message_error')
            ];
        }

        return new JsonResponse($out);
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param Request $req
     * @return JsonResponse
     */
    public function closeReportAction(Request $req) {

        try {

            $em = $this->getDoctrine()->getManager();

            $report = $em->getRepository('ReportBundle:Reporting')->find($req->get('id'));

            if ($report->getArtwork()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithArtwork($report->getArtwork()->getId());
            } elseif ($report->getActivity()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithActivity($report->getActivity()->getId());
            } elseif ($report->getComment()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithComment($report->getComment()->getId());
            } elseif ($report->getProfile()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithProfile($report->getProfile()->getId());
            } else {
                $reporting = [];
            }

            foreach ($reporting as $r) {
                $r->setType(Reporting::TYPE_HISTORY);
                $em->persist($r);
            }

            $report->setType(Reporting::TYPE_HISTORY);
            $em->persist($report);

            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($req->get('id')));

            $message = new ReportMessage();
            $message->setContent($this->get('translator')->trans('administration.reports.text.report_was_closed'));
            $message->setSender($this->getUser());
            $message->setReceiver($report->getReporter());
            $message->setReport($report);
            $message->setIsread(false);
            $message->setIsreadadmin(true);
            $message->setIsparent(($check) ? false : true);
            $em->persist($message);


            $em->flush();
            $out = [
                'status' => true,
                'message' => $this->get('translator')->trans('administration.reports.text.close_report_success')
            ];
        } catch (\Exception $e) {
            $out = [
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.close_report_error')
            ];
        }

        return new JsonResponse($out);
    }

    /**
     * @param Request $req
     * @return JsonResponse
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function markReportAction(Request $req) {

        try {

            $em = $this->getDoctrine()->getManager();

            $report = $em->getRepository('ReportBundle:Reporting')->find($req->get('id'));

            if ($report->getArtwork()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithArtwork($report->getArtwork()->getId());
            } elseif ($report->getActivity()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithActivity($report->getActivity()->getId());
            } elseif ($report->getComment()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithComment($report->getComment()->getId());
            } elseif ($report->getProfile()) {
                $reporting = $em->getRepository('ReportBundle:Reporting')->getReportWithProfile($report->getProfile()->getId());
            } else {
                $reporting = [];
            }

            foreach ($reporting as $r) {
                $r->setType(Reporting::TYPE_INPROGRESS);
                $em->persist($r);
            }
            $report->setMark(true);
            $report->setType(Reporting::TYPE_INPROGRESS);
            $em->persist($report);
            $em->flush();
            $out = [
                'status' => true,
                'message' => $this->get('translator')->trans('administration.reports.text.mark_report_success')
            ];
        } catch (\Exception $e) {
            $out = [
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.mark_report_error')
            ];
        }

        return new JsonResponse($out);
    }

    /**
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @param  Request $req 
     * @param  int  $id  id of the artwork
     * @return render    
     */
    public function removeArtworkAction(Request $req, $id) {
        try {

            $em = $this->getDoctrine()->getManager();
            $report = $em->getRepository('ReportBundle:Reporting')->find($id);
            $artwork = $em->getRepository('ArtworkBundle:Artwork')->findOneOrNull($report->getArtwork());

            if (!$artwork) {
                $out = [
                    'error' => false,
                    'status' => false,
                    'message' => $this->get('translator')->trans('administration.reports.text.remove_artwork_error_not_exist')
                ];
                return new JsonResponse($out);
            }

            $form = $this->createFormBuilder()
                    ->setAction($this->generateUrl('report_routes_remove_reporting_artwork', ['id' => $id]))
                    ->setMethod('DELETE')
                    ->getForm();
            $form->handleRequest($req);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');

            if ($form->isValid()) {
                //Todo Change the type of all reports related to this artwork
                $reports = $em->getRepository('ReportBundle:Reporting')->getReportWithArtwork($artwork->getId());
                foreach ($reports as $r) {
                    $r->setType(Reporting::TYPE_DELETED);
                    $em->persist($r);
                    $em->flush($r);
                }

                $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id));

                $message = new ReportMessage();
                $message->setContent($this->get('translator')->trans('administration.reports.text.oeuvre_deleted'));
                $message->setSender($this->getUser());
                $message->setReceiver($report->getReporteduser());
                $message->setReport($report);
                $message->setIsread(false);
                $message->setIsreadadmin(true);
                $message->setIsparent(($check) ? false : true);
                $em->persist($message);

                $em->flush($message);

                $em->remove($artwork);
                $em->flush();
                $response->setContent(
                        json_encode([
                    'error' => false,
                    'status' => true,
                    'message' => $this->get('translator')->trans('administration.reports.text.remove_artwork_success')
                        ])
                );
                return $response;
            } else {
                $content = $this->renderView('ReportBundle:Reporting:removeArtwork.html.twig', array(
                    'artwork' => $artwork,
                    'id' => $id,
                    'form' => $form->createView(),
                ));
                $response->setContent(
                        json_encode([
                    'title' => $this->get('translator')->trans('administration.reports.text.remove_artwork'),
                    'content' => $content,
                    'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                    'error' => true,
                    'status' => true,
                        ])
                );
                return $response;
            }
        } catch (\Exception $e) {
            $out = [
                'error' => false,
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.remove_artwork_error')
            ];
            return new JsonResponse($out);
        }
    }

    public function removeCommentAction(Request $req, $id) {
        try {
            if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
                throw $this->createAccessDeniedException();
            }

            $em = $this->getDoctrine()->getManager();
            $report = $em->getRepository('ReportBundle:Reporting')->find($id);
            $comment = $em->getRepository('LectureBundle:Comment')->findOneOrNull($report->getComment());

            if (!$comment) {
                $out = [
                    'error' => false,
                    'status' => false,
                    'message' => $this->get('translator')->trans('administration.reports.text.remove_comment_error_not_exist')
                ];
                return new JsonResponse($out);
            }

            $form = $this->createFormBuilder()
                    ->setAction($this->generateUrl('report_routes_remove_reporting_comment', ['id' => $id]))
                    ->setMethod('DELETE')
                    ->getForm();
            $form->handleRequest($req);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');

            if ($form->isValid()) {
                //Todo Change the type of all reports related to this comment
                $reports = $em->getRepository('ReportBundle:Reporting')->getReportWithComment($comment->getId());
                foreach ($reports as $r) {
                    $r->setType(Reporting::TYPE_DELETED);
                    $em->persist($r);
                    $em->flush($r);
                }

                $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id));

                $message = new ReportMessage();
                $message->setContent($this->get('translator')->trans('administration.reports.text.comment_deleted'));
                $message->setSender($this->getUser());
                $message->setReceiver($report->getReporteduser());
                $message->setReport($report);
                $message->setIsread(false);
                $message->setIsreadadmin(true);
                $message->setIsparent(($check) ? false : true);
                $em->persist($message);

                $em->flush($message);

                $em->remove($comment);
                $em->flush();
                $response->setContent(
                        json_encode([
                    'error' => false,
                    'status' => true,
                    'message' => $this->get('translator')->trans('administration.reports.text.remove_comment_success')
                        ])
                );
                return $response;
            } else {
                $content = $this->renderView('ReportBundle:Reporting:removeComment.html.twig', array(
                    'comment' => $comment,
                    'id' => $id,
                    'form' => $form->createView(),
                ));
                $response->setContent(
                        json_encode([
                    'title' => $this->get('translator')->trans('administration.reports.text.remove_comment'),
                    'content' => $content,
                    'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                    'error' => true,
                    'status' => true,
                        ])
                );
                return $response;
            }
        } catch (\Exception $e) {
            $out = [
                'error' => false,
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.remove_comment_error')
            ];
            return new JsonResponse($out);
        }
    }

    public function removeActivityAction(Request $req, $id) {
        try {
            if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
                throw $this->createAccessDeniedException();
            }

            $em = $this->getDoctrine()->getManager();
            $report = $em->getRepository('ReportBundle:Reporting')->find($id);
            $activity = $em->getRepository('UserBundle:Activity')->findOneOrNull($report->getActivity());

            if (!$activity) {
                $out = [
                    'error' => false,
                    'status' => false,
                    'message' => $this->get('translator')->trans('administration.reports.text.remove_activity_error_not_exist')
                ];
                return new JsonResponse($out);
            }

            $form = $this->createFormBuilder()
                    ->setAction($this->generateUrl('report_routes_remove_reporting_activity', ['id' => $id]))
                    ->setMethod('DELETE')
                    ->getForm();
            $form->handleRequest($req);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');

            if ($form->isValid()) {
                //Todo Change the type of all reports related to this activity
                $reports = $em->getRepository('ReportBundle:Reporting')->getReportWithActivity($activity->getId());
                foreach ($reports as $r) {
                    $r->setType(Reporting::TYPE_DELETED);
                    $em->persist($r);
                    $em->flush($r);
                }

                $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id));

                $message = new ReportMessage();
                $message->setContent($this->get('translator')->trans('administration.reports.text.activity_deleted'));
                $message->setSender($this->getUser());
                $message->setReceiver($report->getReporteduser());
                $message->setReport($report);
                $message->setIsread(false);
                $message->setIsreadadmin(true);
                $message->setIsparent(($check) ? false : true);
                $em->persist($message);

                $em->flush($message);

                $em->remove($activity);
                $em->flush();
                $response->setContent(
                        json_encode([
                    'error' => false,
                    'status' => true,
                    'message' => $this->get('translator')->trans('administration.reports.text.remove_activity_success')
                        ])
                );
                return $response;
            } else {
                $content = $this->renderView('ReportBundle:Reporting:removeActivity.html.twig', array(
                    'activity' => $activity,
                    'id' => $id,
                    'form' => $form->createView(),
                ));
                $response->setContent(
                        json_encode([
                    'title' => $this->get('translator')->trans('administration.reports.text.remove_publication'),
                    'content' => $content,
                    'button' => $this->get('translator')->trans('administration.reports.button.confirm'),
                    'error' => true,
                    'status' => true,
                        ])
                );
                return $response;
            }
        } catch (\Exception $e) {
            $out = [
                'error' => false,
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.remove_activity_error')
            ];
            return new JsonResponse($out);
        }
    }

}
