<?php

namespace ReportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ReportBundle\Entity\ReportMessage;
use ReportBundle\Entity\Reporting;

class CronController extends Controller {

    public function reportAction() {

        try {

            $em = $this->getDoctrine()->getManager();

            $now = date('Y-m-d H:i:s');
            $start = new \DateTime($now);
            $start_3d = $start->sub(new \DateInterval('P3D'))->format('Y-m-d H:i:s');

            // List of Artworks > 72H => deleted automatically
            $reports_artworks = $em->getRepository('ReportBundle:Reporting')->getArtworksUntreatedPlus72($start_3d);
            foreach ($reports_artworks as $report) {
                $report->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                $em->persist($report);
                // An automatic message is sent
                $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($report->getId()));
                $message = new ReportMessage();
                $message->setContent($this->get('translator')->trans('administration.reports.text.oeuvre_deleted'));
                $message->setSender($this->getUser());
                $message->setReceiver($report->getReporteduser());
                $message->setReport($report);
                $message->setIsread(false);
                $message->setIsreadadmin(true);
                $message->setIsparent(($check) ? false : true);
                $em->persist($message);

                if ($report->getArtwork()) {
                    $em->remove($report->getArtwork());
                }
            }
            $em->flush();

            $now = date('Y-m-d H:i:s');
            $start = new \DateTime($now);
            $start_3d = $start->sub(new \DateInterval('P3D'))->format('Y-m-d H:i:s');

            // List of Artworks > 20 reports => deleted automatically
            $reports_artworks = $em->getRepository('ReportBundle:Reporting')->getArtworksUntreatedLess72($start_3d);
            foreach ($reports_artworks as $report) {
                if ($report->getArtwork()) {
                    $reports = $em->getRepository('ReportBundle:Reporting')->getUntreatedReportWithArtwork($report->getArtwork()->getId());
                    if (count($reports) >= 20) {
                        foreach ($reports as $r) {
                            $r->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                            $em->persist($r);
                            // An automatic message is sent
                            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($r->getId()));
                            $message = new ReportMessage();
                            $message->setContent($this->get('translator')->trans('administration.reports.text.oeuvre_deleted'));
                            $message->setSender($this->getUser());
                            $message->setReceiver($r->getReporteduser());
                            $message->setReport($r);
                            $message->setIsread(false);
                            $message->setIsreadadmin(true);
                            $message->setIsparent(($check) ? false : true);
                            $em->persist($message);
                        }
                    }
                    $em->remove($report->getArtwork());
                }
            }
            $em->flush();

            /*             * ******************************************************************************** */

            $now = date('Y-m-d H:i:s');
            $start = new \DateTime($now);
            $start_7d = $start->sub(new \DateInterval('P7D'))->format('Y-m-d H:i:s');

            // List of Comments & Acitivitys > 7D => deleted automatically
            $reports_comments = $em->getRepository('ReportBundle:Reporting')->getCommentsUntreatedPlus7D($start_7d);
            $reports_activitys = $em->getRepository('ReportBundle:Reporting')->getActivitysUntreatedPlus7D($start_7d);

            foreach ($reports_comments as $report) {
                $report->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                $em->persist($report);
                // An automatic message is sent
                $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($report->getId()));
                $message = new ReportMessage();
                $message->setContent($this->get('translator')->trans('administration.reports.text.comment_deleted'));
                $message->setSender($this->getUser());
                $message->setReceiver($report->getReporteduser());
                $message->setReport($report);
                $message->setIsread(false);
                $message->setIsreadadmin(true);
                $message->setIsparent(($check) ? false : true);
                $em->persist($message);

                if ($report->getComment()) {
                    $em->remove($report->getComment());
                }
            }

            foreach ($reports_activitys as $report) {
                $report->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                $em->persist($report);
                // An automatic message is sent
                $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($report->getId()));
                $message = new ReportMessage();
                $message->setContent($this->get('translator')->trans('administration.reports.text.activity_deleted'));
                $message->setSender($this->getUser());
                $message->setReceiver($report->getReporteduser());
                $message->setReport($report);
                $message->setIsread(false);
                $message->setIsreadadmin(true);
                $message->setIsparent(($check) ? false : true);
                $em->persist($message);

                if ($report->getActivity()) {
                    $em->remove($report->getActivity());
                }
            }

            $em->flush();

            $now = date('Y-m-d H:i:s');
            $start = new \DateTime($now);
            $start_2d = $start->sub(new \DateInterval('P2D'))->format('Y-m-d H:i:s');

            // List of Comments & Activitys > 48H & >=10  reports => deleted automatically
            $reports_comments = $em->getRepository('ReportBundle:Reporting')->getCommentsUntreatedPlus48($start_2d);
            $reports_activitys = $em->getRepository('ReportBundle:Reporting')->getActivitysUntreatedPlus48($start_2d);

            foreach ($reports_comments as $report) {
                if ($report->getComment()) {
                    $reports = $em->getRepository('ReportBundle:Reporting')->getUntreatedReportWithComment($report->getComment()->getId());
                    if (count($reports) >= 10) {
                        foreach ($reports as $r) {
                            $r->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                            $em->persist($r);
                            // An automatic message is sent
                            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($r->getId()));
                            $message = new ReportMessage();
                            $message->setContent($this->get('translator')->trans('administration.reports.text.comment_deleted'));
                            $message->setSender($this->getUser());
                            $message->setReceiver($r->getReporteduser());
                            $message->setReport($r);
                            $message->setIsread(false);
                            $message->setIsreadadmin(true);
                            $message->setIsparent(($check) ? false : true);
                            $em->persist($message);
                        }
                    }
                    $em->remove($report->getComment());
                }
            }
            foreach ($reports_activitys as $report) {
                if ($report->getActivity()) {
                    $reports = $em->getRepository('ReportBundle:Reporting')->getUntreatedReportWithActivity($report->getActivity()->getId());
                    if (count($reports) >= 10) {
                        foreach ($reports as $r) {
                            $r->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                            $em->persist($r);
                            // An automatic message is sent
                            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($r->getId()));
                            $message = new ReportMessage();
                            $message->setContent($this->get('translator')->trans('administration.reports.text.activity_deleted'));
                            $message->setSender($this->getUser());
                            $message->setReceiver($r->getReporteduser());
                            $message->setReport($r);
                            $message->setIsread(false);
                            $message->setIsreadadmin(true);
                            $message->setIsparent(($check) ? false : true);
                            $em->persist($message);
                        }
                    }
                    $em->remove($report->getActivity());
                }
            }
            $em->flush();

            $now = date('Y-m-d H:i:s');
            $start = new \DateTime($now);
            $start_7d = $start->sub(new \DateInterval('P7D'))->format('Y-m-d H:i:s');

            // List of Comments & Acitivitys < 7D & >=50 reports => deleted automatically
            $reports_comments = $em->getRepository('ReportBundle:Reporting')->getCommentsUntreatedLess7D($start_7d);
            $reports_activitys = $em->getRepository('ReportBundle:Reporting')->getActivitysUntreatedLess7D($start_7d);

            foreach ($reports_comments as $report) {
                if ($report->getComment()) {
                    $reports = $em->getRepository('ReportBundle:Reporting')->getUntreatedReportWithComment($report->getComment()->getId());
                    if (count($reports) >= 50) {
                        foreach ($reports as $r) {
                            $r->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                            $em->persist($r);
                            // An automatic message is sent
                            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($r->getId()));
                            $message = new ReportMessage();
                            $message->setContent($this->get('translator')->trans('administration.reports.text.comment_deleted'));
                            $message->setSender($this->getUser());
                            $message->setReceiver($r->getReporteduser());
                            $message->setReport($r);
                            $message->setIsread(false);
                            $message->setIsreadadmin(true);
                            $message->setIsparent(($check) ? false : true);
                            $em->persist($message);
                        }
                    }
                    $em->remove($report->getComment());
                }
            }

            foreach ($reports_activitys as $report) {
                if ($report->getActivity()) {
                    $reports = $em->getRepository('ReportBundle:Reporting')->getUntreatedReportWithActivity($report->getActivity()->getId());
                    if (count($reports) >= 10) {
                        foreach ($reports as $r) {
                            $r->setType(Reporting::TYPE_FENCEDAUTOMATICALLY);
                            $em->persist($r);
                            // An automatic message is sent
                            $check = count($em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($r->getId()));
                            $message = new ReportMessage();
                            $message->setContent($this->get('translator')->trans('administration.reports.text.activity_deleted'));
                            $message->setSender($this->getUser());
                            $message->setReceiver($r->getReporteduser());
                            $message->setReport($r);
                            $message->setIsread(false);
                            $message->setIsreadadmin(true);
                            $message->setIsparent(($check) ? false : true);
                            $em->persist($message);
                        }
                    }
                    $em->remove($report->getActivity());
                }
            }
            $em->flush();

            // Change Type for banis report
            $now = date('Y-m-d H:i:s');
            $reports = $em->getRepository('ReportBundle:Reporting')->getReportForCron($now);

            foreach ($reports as $report) {
                $report->setType(Reporting::TYPE_BANISHED_HISTORY);
            }
            $em->flush();

            return new JsonResponse(['status' => true]);
        } catch (\Exception $e) {
//            return new JsonResponse(['status' => $e->getMessage()]);
            return new RedirectResponse($this->generateUrl('cron_routes_report'));
        }
    }

    public function banishedAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();
        foreach ($users as $user) {
            $reports = $em->getRepository('ReportBundle:Reporting')->getReportForUser($user->getId());
            $check = false;
            $i = 0;
            while ($i < count($reports) && $check == false) {
                $r = $reports[$i];
                if ($r->getBanishment()) {
                    if ($r->getBanishment()->getEndbanish()) {
                        $now = date('Y-m-d H:i:s');
                        $now_date = new \DateTime($now);
                        if ($now_date < $r->getBanishment()->getEndbanish()) {
                            $check = true;
                            $user->setIsBanni($check);
                        }
                    }
                }
                $i++;
            }
            $user->setIsBanni($check);
            $em->persist($user);
        }
        $em->flush();
        return new JsonResponse(['status' => true]);
    }

}
