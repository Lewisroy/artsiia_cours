<?php

namespace ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ReportMessage
 *
 * @ORM\Table(name="reporting_message")
 * @ORM\Entity(repositoryClass="ReportBundle\Repository\ReportMessageRepository")
 */
class ReportMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var bool
     *
     * @ORM\Column(name="isread", type="boolean")
     */
    private $isread;

    /**
     * @var bool
     *
     * @ORM\Column(name="isreadadmin", type="boolean")
     */
    private $isreadadmin;

    /**
     * @var bool
     *
     * @ORM\Column(name="isparent", type="boolean")
     */
    private $isparent;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="messagesender")
     */
    private $sender;

    /**
     * Set sender
     *
     * @param \UserBundle\Entity\User
     *
     * @return Sender
     */
    public function setSender(\UserBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \UserBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="messagereceiver")
     */
    private $receiver;

    /**
     * Set receiver
     *
     * @param \UserBundle\Entity\User
     *
     * @return Receiver
     */
    public function setReceiver(\UserBundle\Entity\User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \UserBundle\Entity\User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ReportBundle\Entity\Reporting", inversedBy="reportmessage")
     */
    private $report;

    /**
     * Set report
     *
     * @param \ReportBundle\Entity\Reporting
     *
     * @return Report
     */
    public function setReport(\ReportBundle\Entity\Reporting $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \ReportBundle\Entity\Reporting
     */
    public function getReport()
    {
        return $this->report;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return ReportMessage
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set isread
     *
     * @param boolean $isread
     *
     * @return ReportMessage
     */
    public function setIsread($isread)
    {
        $this->isread = $isread;

        return $this;
    }

    /**
     * Get isread
     *
     * @return bool
     */
    public function getIsread()
    {
        return $this->isread;
    }

    /**
     * Set isreadadmin
     *
     * @param boolean $isreadadmin
     *
     * @return ReportMessage
     */
    public function setIsreadadmin($isreadadmin)
    {
        $this->isreadadmin = $isreadadmin;

        return $this;
    }

    /**
     * Get isreadadmin
     *
     * @return bool
     */
    public function getIsreadadmin()
    {
        return $this->isreadadmin;
    }

    /**
     * Set isparent
     *
     * @param boolean $isparent
     *
     * @return ReportMessage
     */
    public function setIsparent($isparent)
    {
        $this->isparent = $isparent;

        return $this;
    }

    /**
     * Get isparent
     *
     * @return bool
     */
    public function getIsparent()
    {
        return $this->isparent;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
}
