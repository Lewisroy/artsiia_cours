<?php

namespace ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Subjects
 *
 * @ORM\Table(name="subjects")
 * @ORM\Entity(repositoryClass="ReportBundle\Repository\SubjectsRepository")
 */
class Subjects
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var int
     *
     * @ORM\Column(name="severity", type="integer")
     */
    private $severity;

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="subject")
     */
    public $reporting;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reporting = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     *
     * @return Reporting
     */
    public function addReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting[] = $reporting;

        return $this;
    }

    /**
     * Remove reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     */
    public function removeReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting->removeElement($reporting);
    }

    /**
     * Get reporting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporting()
    {
        return $this->reporting;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Subjects
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set severity
     *
     * @param integer $severity
     *
     * @return Subjects
     */
    public function setSeverity($severity)
    {
        $this->severity = $severity;

        return $this;
    }

    /**
     * Get severity
     *
     * @return int
     */
    public function getSeverity()
    {
        return $this->severity;
    }
}
