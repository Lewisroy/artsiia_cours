<?php

namespace ReportBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Reporting
 *
 * @ORM\Table(name="reporting")
 * @ORM\Entity(repositoryClass="ReportBundle\Repository\ReportingRepository")
 */
class Reporting
 {
    const TYPE_UNTREATED           = 1; # Non traité
    const TYPE_INPROGRESS          = 2; # En cours
    const TYPE_HISTORY             = 3; # Historique
    const TYPE_FENCEDAUTOMATICALLY = 4; # Clôturé automatiquement
    const TYPE_DELETED             = 5; # Supprimé
    const TYPE_BANISHED_INPROGRESS = 6; # bani En cours
    const TYPE_BANISHED_HISTORY    = 7; # bani Historique

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="mark", type="boolean")
     */
    private $mark;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="ReportBundle\Entity\Subjects", inversedBy="reporting")
     */
    private $subject;

    /**
     * Set subject
     *
     * @param \ReportBundle\Entity\Subjects
     *
     * @return Subjects
     */
    public function setSubject(\ReportBundle\Entity\Subjects $subject = null)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return \ReportBundle\Entity\Subjects
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="reporting")
     */
    private $reporter;

    /**
     * Set reporter
     *
     * @param \UserBundle\Entity\User
     *
     * @return Reporter
     */
    public function setReporter(\UserBundle\Entity\User $reporter = null)
    {
        $this->reporter = $reporter;

        return $this;
    }

    /**
     * Get reporter
     *
     * @return \UserBundle\Entity\User
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="reportinguser")
     */
    private $reporteduser;

    /**
     * Set reporteduser
     *
     * @param \UserBundle\Entity\User
     *
     * @return Reporteduser
     */
    public function setReporteduser(\UserBundle\Entity\User $reporteduser = null)
    {
        $this->reporteduser = $reporteduser;

        return $this;
    }

    /**
     * Get reporteduser
     *
     * @return \UserBundle\Entity\User
     */
    public function getReporteduser()
    {
        return $this->reporteduser;
    }

    /**
     * @ORM\ManyToOne(targetEntity="ArtworkBundle\Entity\Artwork", inversedBy="reporting")
     */
    private $artwork;

    /**
     * Set artwork
     *
     * @param \ArtworkBundle\Entity\Artwork
     *
     * @return Artwork
     */
    public function setArtwork(\ArtworkBundle\Entity\Artwork $artwork = null)
    {
        $this->artwork = $artwork;

        return $this;
    }

    /**
     * Get artwork
     *
     * @return \ArtworkBundle\Entity\Artwork
     */
    public function getArtwork()
    {
        return $this->artwork;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Activity", inversedBy="reporting")
     */
    private $activity;

    /**
     * Set activity
     *
     * @param \UserBundle\Entity\Activity
     *
     * @return Activity
     */
    public function setActivity(\UserBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \UserBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="reportingprofile")
     */
    private $profile;
    
    /**
     * Set profile
     *
     * @param \UserBundle\Entity\User
     *
     * @return Profile
     */
    public function setProfile(\UserBundle\Entity\User $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \UserBundle\Entity\User
     */
    public function getProfile()
    {
        return $this->profile;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="LectureBundle\Entity\Comment", inversedBy="reporting")
     */
    private $comment;
    
    /**
     * Set comment
     *
     * @param \LectureBundle\Entity\Comment
     *
     * @return Comment
     */
    public function setComment(\LectureBundle\Entity\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \LectureBundle\Entity\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * @ORM\ManyToOne(targetEntity="ReportBundle\Entity\Banishment", inversedBy="reporting")
     */
    private $banishment;
    
    /**
     * Set banishment
     *
     * @param \ReportBundle\Entity\Banishment
     *
     * @return Banishment
     */
    public function setBanishment(\ReportBundle\Entity\Banishment $banishment = null)
    {
        $this->banishment = $banishment;

        return $this;
    }

    /**
     * Get banishment
     *
     * @return \ReportBundle\Entity\Banishment
     */
    public function getBanishment()
    {
        return $this->banishment;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reportmessage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Reporting
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Set mark
     *
     * @param boolean $mark
     *
     * @return Reporting
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return bool
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Test
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\ReportMessage", mappedBy="report")
     */
    public $reportmessage;

    /**
     * Add reportmessage
     *
     * @param \ReportBundle\Entity\ReportMessage $reportmessage
     *
     * @return Reportmessage
     */
    public function addReportmessage(\ReportBundle\Entity\ReportMessage $reportmessage)
    {
        $this->reportmessage[] = $reportmessage;

        return $this;
    }

    /**
     * Remove reportmessage
     *
     * @param \ReportBundle\Entity\ReportMessage $reportmessage
     */
    public function removeReportmessage(\ReportBundle\Entity\ReportMessage $reportmessage)
    {
        $this->reportmessage->removeElement($reportmessage);
    }

    /**
     * Get reportmessage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportmessage()
    {
        return $this->reportmessage;
    }
}
