<?php

namespace ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Banishment
 *
 * @ORM\Table(name="banishments")
 * @ORM\Entity(repositoryClass="ReportBundle\Repository\BanishmentRepository")
 */
class Banishment {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var \DateTime $created
     * @ORM\Column(type="datetime")
     */
    private $endbanish;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var int
     *
     * @ORM\Column(name="days", type="integer")
     */
    private $days;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="banish")
     */
    private $admin;

    /**
     * Set admin
     *
     * @param \UserBundle\Entity\User
     *
     * @return Admin
     */
    public function setAdmin(\UserBundle\Entity\User $admin = null) {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return \UserBundle\Entity\User
     */
    public function getAdmin() {
        return $this->admin;
    }

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="banishment")
     */
    public $reporting;

    /**
     * Add reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     *
     * @return Reporting
     */
    public function addReporting(\ReportBundle\Entity\Reporting $reporting) {
        $this->reporting[] = $reporting;

        return $this;
    }

    /**
     * Remove reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     */
    public function removeReporting(\ReportBundle\Entity\Reporting $reporting) {
        $this->reporting->removeElement($reporting);
    }

    /**
     * Get reporting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporting() {
        return $this->reporting;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->reporting = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * @return \DateTime
     */
    public function getEndbanish() {
        return $this->endbanish;
    }

    /**
     * @param \DateTime $endbanish
     */
    public function setEndbanish($endbanish) {
        $this->endbanish = $endbanish;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set days
     *
     * @param integer $days
     *
     * @return Subjects
     */
    public function setDays($days) {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return int
     */
    public function getDays() {
        return $this->days;
    }

}
