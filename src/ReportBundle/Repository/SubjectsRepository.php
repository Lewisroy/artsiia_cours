<?php

namespace ReportBundle\Repository;

/**
 * SubjectsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SubjectsRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllOrderSeverity()
    {
        $qb = $this->createQueryBuilder('s')
                   ->orderBy('s.severity', 'asc')
                   ->getQuery();

        return $qb->getResult();
    }
}
