<?php

namespace FaqBundle\Controller;

use FaqBundle\Entity\Faq;
use LectureBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Faq controller.
 * @Route("/{_locale}/faq")
 */
class FaqController extends Controller
{
    /**
     * Lists all contact entities.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/admin/", name="faq_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository('FaqBundle:Faq')->findAll();

        return $this->render('faq/admin.html.twig', array(
            'faqs' => $faqs,
        ));
    }

    /**
     * Creates a new faq entity.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/new", name="faq_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    

        $faq = new Faq();
        $form = $this->createForm('FaqBundle\Form\FaqType', $faq);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
          
            $em->persist($faq);
            $em->flush($faq);
           return $this->redirectToRoute('faq_show', array('id' => $faq->getId()));
           
        }
           
         return $this->render('faq/new.html.twig', array(
            'faq' => $faq,
            'form' => $form->createView()
        ));
    }

    /**
     * Finds and displays a contact entity.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="faq_show")
     * @Method("GET")
     */
    public function showAction(Faq $faq)
    {
        $deleteForm = $this->createDeleteForm($faq);

        return $this->render('faq/show.html.twig', array(
            'faq' => $faq,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing contact entity.
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}/edit", name="faq_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Faq $faq)
    {
        $deleteForm = $this->createDeleteForm($faq);
        $editForm = $this->createForm('FaqBundle\Form\FaqType', $faq);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('faq_edit', array('id' => $faq->getId()));
        }

        return $this->render('faq/edit.html.twig', array(
            'faq' => $faq,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contact entity.
     *
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @Route("/{id}", name="faq_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Faq $faq)
    {
        $form = $this->createDeleteForm($faq);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($faq);
            $em->flush();
        }
            return $this->redirectToRoute('faq_index');
    }

    /**
     * Show all faq entities.
     *
     * @Route("/all/", name="faq_all")
     * @Method("GET")
     */
    public function showAllAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        $faqs = $em->getRepository('FaqBundle:Faq')->findAll();
        
        return $this->render('faq/index.html.twig', array(

            'faqs' => $faqs,
        ));
    }

    /**
     * show all categories.
     *
     * @Route("/search/", name="faq_search")
     * @Method({"GET", "POST"})
     */
    public function faqSearchAction()
    {

        $em = $this->getDoctrine()->getManager();
        
        $faqs = $em->getRepository('FaqBundle:Faq')->findAll();
        $i=0;
       
        $data=array(
            'faq' => $faqs,
            );
        $serializer = $this->container->get('serializer');
        $reports = $serializer->serialize($data, 'json');
        return new Response($reports);
    }

    /**
     * Show all faq categories.
     *
     * @Route("/category/{id}", name="faq_category")
     * @Method("GET")
     */
    public function showFaqCategoryAction(Request $request)
    {
       

        $em = $this->getDoctrine()->getManager();
        $faqs = $em->getRepository('FaqBundle:Faq')->findBy(array('category' => $request->attributes->get('id')));
        
        return $this->render('faq/index.html.twig', array(

            'faqs' => $faqs,
        ));
    }


    /**
     * Creates a form to delete a contact entity.
     *
     * @param Contact $contact The contact entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Faq $faq)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('faq_delete', array('id' => $faq->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
