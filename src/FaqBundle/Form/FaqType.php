<?php

namespace FaqBundle\Form;

use LectureBundle\Entity\Lecture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class FaqType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('question', TextType::class, array('label' => 'faq.question', 'required' => true))
                ->add('response', TextType::class, array('label' => 'faq.response', 'required' => true))
                /*->add('category', EntityType::class, array('label' => 'category.title',
                'required' => false,
                'expanded' => false,
                'mapped' => false,
                'class' => 'LectureBundle:Category',
                'choice_label' => 'name'
                    ))*/
                ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FaqBundle\Entity\Faq'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'faqbundle_faq';
    }


}
