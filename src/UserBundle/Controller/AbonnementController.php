<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Abonnement controller.
 *
 * @Route("{_locale}/followers")
 */
class AbonnementController extends Controller {

    /**
     * Abonnement utilisateurs.
     *
     * @Route("/", options={ "expose" = true }, name="user_followers")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function followersAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $followersId = $request->request->get('followers');
            $userFollowersId = $request->request->get('userFollowers');

            $followers = $this->getDoctrine()->getRepository('UserBundle:User')->find($followersId);
            $userFollowers = $this->getDoctrine()->getRepository('UserBundle:User')->find($userFollowersId);

            $followers->addFollower($userFollowers);
            $userFollowers->addFollowing($followers);
            $em = $this->getDoctrine()->getManager();
            $translator = $this->container->get('translator');

            try {
                $em->persist($followers);
                $em->persist($userFollowers);
                $em->flush();
                $notification = $this->get('app_notification_services');
                $notification->SendNotificationSubscribers($user, $userToFollow);

                $zMessage = $translator->trans('toast.subscribe.success', array('%user%' => $userFollowers->getEmail()));
                $status = 'OK';
                $http_status = 200;
            } catch (\Exception $e) {
                $zMessage = $translator->trans('toast.subscribe.error');
                $status = 'KO';
                $http_status = 500;
            }

            return new JsonResponse(
                    array(
                'status' => $status,
                'message' => $zMessage
                    ), $http_status, array('Content-Type' => 'application/json'));
        }
    }

    /**
     * Abonnement utilisateur
     *
     * @Route("/subscribe", name="user_subscribing")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function subscribeAction(Request $request) {
        $return = array();
        $userIdToFollow = $request->request->get('user_id');
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $userToFollow = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdToFollow);
        try {
            $user->addFollowing($userToFollow);
            $userToFollow->addFollower($user);
            $em->persist($user);
            $em->persist($userToFollow);
            $em->flush();
            $notification = $this->get('app_notification_services');
            $notification->SendNotificationSubscribers($user, $userToFollow);
        } catch (\Exception $e) {
            $return['status'] = 'Error';
            return new JsonResponse($return);
        }

        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

    /**
     * Desbonnement utilisateur
     *
     * @Route("/unsubscribe", name="user_unsubscribing")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function unsubscribeAction(Request $request) {
        $return = array();
        $userIdToUnfollow = $request->request->get('user_id');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $userToUnfollow = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdToUnfollow);
        try {
            $user->removeFollowing($userToUnfollow);
            $userToUnfollow->removeFollower($user);
            $em->persist($user);
            $em->persist($userToUnfollow);
            $em->flush();
            $notification = $this->get('app_notification_services');
            $notification->removeNotificationSubscribers($user, $userToUnfollow);
        } catch (\Exception $e) {
            $return['status'] = 'Error';
            return new JsonResponse($return);
        }
        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

    /**
     * Demande d'ami
     *
     * @Route("/asking_friend", name="user_asking_friend")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function askingFriendAction(Request $request) {
        $return = array();
        $userIdAskFriend = $request->request->get('user_id');
        $already_refuse = false;
        $user = $this->getUser();

        try {
            $em = $this->getDoctrine()->getManager();
            $userToAdd = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdAskFriend);
            foreach ($userToAdd->getRefuseFriends() as $refuse) {
                if ($refuse->getId() == $user->getId()) {
                    $already_refuse = true;
                }
            }
            if ($already_refuse == false) {
                $userToAdd->addAskingFriend($user);
                $em->persist($userToAdd);
                $em->flush();
                $notification = $this->get('app_notification_services');
            $notification->SendNotificationFriendRequest($user, $userToAdd);
            }
        } catch (\Exception $e) {
            $return['status'] = 'Error';
            return new JsonResponse($return);
        }

        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

    /**
     * Accepter un ami
     *
     * @Route("/accept_friend", name="user_accept_friend")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function acceptFriendAction(Request $request) {
        $return = array();
        $userIdToAccept = $request->request->get('user_id');
        $user = $this->getUser();

        try {
            $em = $this->getDoctrine()->getManager();
            $userToAdd = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdToAccept);
            $user->removeAskingFriend($userToAdd);
            $user->addFriend($userToAdd);
            $userToAdd->addFriend($user);
            $em->persist($userToAdd);
            $em->persist($user);
            $em->flush();
            $notification = $this->get('app_notification_services');
            $notification->sendNotificationAskingFriend($user, $userToAdd);
        } catch (\Exception $e) {
            $return['status'] = 'Error';
            return new JsonResponse($return);
        }

        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

    /**
     * Refuser un ami
     *
     * @Route("/refuse_friend", name="user_refuse_friend")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function refuseFriendAction(Request $request) {
        $return = array();
        $userIdToAccept = $request->request->get('user_id');
        $user = $this->getUser();

        try {
            $em = $this->getDoctrine()->getManager();
            $userToAdd = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdToAccept);
            $user->removeAskingFriend($userToAdd);
            $user->addRefuseFriend($userToAdd);
            $em->persist($userToAdd);
            $em->persist($user);
            $em->flush();
        } catch (\Exception $e) {
            $return['status'] = 'Error';
            return new JsonResponse($return);
        }

        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

    /**
     * Retirer une demande ami
     *
     * @Route("/retrieve_asking_friend", name="user_retrieve_asking_friend")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function retrieveAskingFriendAction(Request $request) {
        $return = array();
        $userIdToRetrieve = $request->request->get('user_id');
        $user = $this->getUser();

        try {
            $em = $this->getDoctrine()->getManager();
            $userToAdd = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdToRetrieve);
            $userToAdd->removeAskingFriend($user);
            $em->persist($userToAdd);
            $em->flush();
            $notification = $this->get('app_notification_services');
            $notification->removeNotificationFriendRequest($user, $userToAdd);
        } catch (\Exception $e) {
            $return['status'] = 'Error';
            return new JsonResponse($return);
        }

        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

    /**
     * Retirer un ami
     *
     * @Route("/retrieve_friend", name="user_retrieve_friend")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function retrieveFriendAction(Request $request) {
        $return = array();
        $userIdToRetrieve = $request->request->get('user_id');
        $user = $this->getUser();


        $em = $this->getDoctrine()->getManager();
        $userToRetrieve = $this->getDoctrine()->getRepository('UserBundle:User')->find($userIdToRetrieve);
        $user->removeFriend($userToRetrieve);
        $userToRetrieve->removeFriend($user);
        $em->persist($userToRetrieve);
        $em->persist($user);
        $em->flush();


        $return['status'] = "Ok";
        return new JsonResponse($return);
    }

}
