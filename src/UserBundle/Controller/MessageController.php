<?php

namespace UserBundle\Controller;

use UserBundle\Entity\Message;
use UserBundle\Entity\User;
use UserBundle\Entity\TextMessage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use UserBundle\Form\MessageType;

/**
 * Message controller.
 *
 * @Route("/{_locale}/message")
 */
class MessageController extends Controller {

    /**
     * Lists all message entities.
     *
     * @Route("/", name="message_index")
     * @Method("GET")
     */
    public function indexAction() {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $messages = $em->getRepository('UserBundle:Message')->getMessagesForMember($this->getUser());

        return $this->render('message/index.html.twig', array(
                    'messages' => $messages,
        ));
    }

    /**
     * Add message
     *
     * @Route("/ajaxadd", name="ajax_add_message")
     * @Method({"GET", "POST"})
     */
    public function addMessageAction(Request $request) {

        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $response = array();

        $message = $request->request->get('message');
        $id_message = $request->request->get('id_message');


        $em = $this->getDoctrine()->getManager();
        $dbMessage = $em->getRepository('UserBundle:Message')->findOneBy(array('id' => $id_message));


        $txtmessage = new TextMessage();
        $txtmessage->setText($message);
        $txtmessage->addSender($this->getUser());


        $dbMessage->addMessage($txtmessage);
        $dbMessage->setRead("Non Lu");

        $em->persist($dbMessage);
        $em->flush();


        return new JsonResponse($response);
    }

    /**
     * get last message
     *
     * @Route("/ajax/last_message", name="ajax_sidebar_message")
     * @Method({"GET", "POST"})
     */
    public function sidebarAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $messages = $em->getRepository('UserBundle:Message')->getMessageToSidebar($this->getUser(), 10);

        $response = array();
        $user = $this->getUser();
        foreach ($messages as $key => $message) {
            $tmp = array();
            $tmp['id'] = $message->getId();
            foreach ($message->getSender() as $sender) {
                if ($sender->getUsername() != $user->getUsername())
                    $tmp['username'] = $sender->getUsername();
            }

            foreach ($message->getReceiver() as $sender) {
                if ($sender->getUsername() != $user->getUsername())
                    $tmp['username'] = $sender->getUsername();
            }

            $last_message = $message->getMessage()->last();
            $tmp['message'] = substr($last_message->getText(), 0, 20);
            if (strlen($last_message->getText()) > 20)
                $tmp['message'] .= '...';

            array_push($response, $tmp);
        }


        return new JsonResponse($response);
    }

    /**
     * Creates a new message entity.
     *
     * @Route("/new", name="message_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $message = new Message();
        $error = array();

        $form = $this->createForm(MessageType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            // rechercher si une correspondance Sender / receiver existe
            $sender = $this->getUser();
            $receiver = $em->getRepository('UserBundle:User')->findOneBy(array('username' => $data['receiver']));
            // Si le receiver n'existe pas, on retourne un message d'erreur et le formulaire pré-rempli
            if ($receiver == NULL) {
                array_push($error, $this->get('translator')->trans('inbox.no_existing_user'));
                $defaultData = array('message' => $data['message']);

                $form = $this->createForm(MessageType::class, $defaultData);
                return $this->render('message/new.html.twig', array(
                            'message' => $message,
                            'error' => $error,
                            'form' => $form->createView(),
                ));
            }

            $conversation = $em->getRepository('UserBundle:Message')->getConversation($sender, $receiver);
            // On crée le message avec sa date
            $message = new TextMessage();
            $message->setText($data['message']);
            $message->setRead(false);
            $message->addSender($this->getUser());

            // Si oui
            // On ajoute le message dans la conversation contenant receiver et sender
            // Si non
            // on crée le message
            if (!empty($conversation)) {
                $conversation[0]->addMessage($message);
                $conversation[0]->setRead("Non Lu");
                $em->persist($conversation[0]);
                $em->flush();
                return $this->redirectToRoute('message_show', array('id' => $conversation[0]->getId()));
            } else {
                $conversation = new Message();
                $conversation->setRead("Non Lu");
                $conversation->addMessage($message);
                $conversation->addSender($sender);
                $conversation->addReceiver($receiver);
                $em->persist($conversation);
                $em->flush();
                return $this->redirectToRoute('message_show', array('id' => $conversation->getId()));
            }
        }

        return $this->render('message/new.html.twig', array(
                    'message' => $message,
                    'error' => $error,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a message entity.
     *
     * @Route("/{id}", name="message_show")
     * @Method("GET")
     */
    public function showAction(Message $message) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($message);
        $em = $this->getDoctrine()->getManager();

        $last_message = $message->getMessage()->last();
        $last_message_sender = $last_message->getSender();
        if ($last_message_sender->first() != $this->getUser())
            $message->setRead("Lu");

        $em->persist($message);
        $em->flush();
        $with = array();
        $user = $this->getUser();
        foreach ($message->getSender() as $sender) {
            if ($sender->getUsername() != $user->getUsername())
                array_push($with, $sender->getUsername());
        }

        foreach ($message->getReceiver() as $sender) {
            if ($sender->getUsername() != $user->getUsername())
                array_push($with, $sender->getUsername());
        }


        return $this->render('message/show.html.twig', array(
                    'message' => $message,
                    'with' => $with,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing message entity.
     *
     * @Route("/{id}/edit", name="message_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Message $message) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($message);
        $editForm = $this->createForm('UserBundle\Form\MessageType', $message);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('message_edit', array('id' => $message->getId()));
        }

        return $this->render('message/edit.html.twig', array(
                    'message' => $message,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a message entity.
     *
     * @Route("/{id}", name="message_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Message $message) {
        $form = $this->createDeleteForm($message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($message);
            $em->flush($message);
        }

        return $this->redirectToRoute('message_index');
    }

    /**
     * Creates a form to delete a message entity.
     *
     * @param Message $message The message entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Message $message) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('message_delete', array('id' => $message->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function messageSidebarAction() {
        return $this->render('message/sidebar.html.twig');
    }

}
