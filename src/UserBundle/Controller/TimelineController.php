<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserBundle\Entity\User;
use UserBundle\Entity\Timeline;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use LectureBundle\Entity\Comment;

/**
 * Timeline controller.
 *
 * @Route("/{_locale}/timeline")
 */
class TimelineController extends Controller {

    /**
     * Index.
     *
     * @Route("/", name="timeline_index")
     * @Method("GET")
     */
    public function indexAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
        if($this->getUser()){
            $notification = $this->get('app_notification_services');
            $notification->removeNotificationActivity($this->getUser());
        }
        return $this->render('timeline/me.html.twig');
    }

    /**
     * Add post to timeline
     *
     * @Route("/post/", name="add_post_timeline")
     * @Method({"GET", "POST"})
     */
    public function addPostToMyWall() {
        $em = $this->getDoctrine()->getManager();
        $message = $request->request->get('message');
        $post = New Post();
        $post->setMessage($message);
        $em->persist($post);
        $em->flush();
    }

    /**
     * Like an activity
     *
     * @Route("/like", name="post_like")
     * @Method("POST")
     */
    public function likeAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $return = array();
        $return['minus_one'] = false;
        $return['plus_one'] = false;
        $return['id'] = $id;
        $user = $this->getUser();
        $post = $em->getRepository('UserBundle:Activity')->findOneBy(array('id' => $id));
        $inLiker = false;
        foreach ($post->getLikers() as $liker) {
            if ($liker->getId() == $user->getId()) {
                $inLiker = true;
                break;
            }
        }
        if (!$inLiker) {
            $post->addLiker($user);
            $return['plus_one'] = true;
            $em->persist($post);
            $em->flush();
        } else {
            $post->removeLiker($user);
            $return['minus_one'] = true;
            $em->persist($post);
            $em->flush();
        }
        return new JsonResponse($return);
    }

    /**
     * Lists all activity entities.
     *
     * @Route("/ajax", name="get_activity_general_ajax")
     * @Method("POST")
     */
    public function ajaxGetTimelineAction(Request $request) {

        $activitiesToReturn = array();

        $em = $this->getDoctrine()->getManager();
        $data['user'] = $this->getUser();
        $data['firstresult'] = $request->request->get('start');
        $data['maxresult'] = $request->request->get('number');

        $data['id'] = $request->request->get('id');
        if ($data['id'] <= 0)
            $activities = $em->getRepository('UserBundle:Activity')->findActivityByUser($data);
        else
            $activities = $em->getRepository('UserBundle:Activity')->findActivityOfUser($data);

        $locale = $request->getLocale();

        foreach ($activities as $activity) {
            $tmp = array();
            $user_liker = false;
            $user_comment = false;
            $user_id = $this->getUser()->getId();
            foreach ($activity->getLikers() as $liker) {
                if ($user_id == $liker->getId()) {
                    $user_liker = true;
                    break;
                }
            }

            foreach ($activity->getCommentslist() as $comment) {
                if ($user_id == $comment->getUser()->getId()) {
                    $user_comment = true;
                    break;
                }
            }

            $comments = $em->getRepository('LectureBundle:Comment')->getCommentByActivity($activity->getId(), 0, 3);

            $tmp['username'] = $activity->getAuthor()[0]->getUsername();
            $tmp['user_id'] = $activity->getAuthor()[0]->getId();
            $tmp['description'] = substr($activity->getDescription(), 0, 250);
            $tmp['id'] = $activity->getId();
            $tmp['like_text'] = '';
            $tmp['comments'] = $comments;

            if ($activity->getUpdated() > $activity->getDateCreation()) {
                $tmp['created'] = $activity->getUpdated();
                $tmp['updated'] = true;
            } else {
                $tmp['created'] = $activity->getDateCreation();
                $tmp['updated'] = false;
            }

            if ($user_liker)
                $tmp['like_text'] .= '<i class="fa fa-thumbs-up color-icon-2"></i>';
            else
                $tmp['like_text'] .= '<i class="fa fa-thumbs-o-up"></i>';
            $tmp['like_text'] .= '<span id="liker_' . $activity->getId() . '"> ' . count($activity->getLikers()) . '</span>';

            $tmp['comment_text'] = '';
            if ($user_comment)
                $tmp['comment_text'] .= '<i class="fa fa-comment color-icon-2"></i>';
            else
                $tmp['comment_text'] .= '<i id="fa-' . $activity->getId() . '" class="fa fa-comment-o"></i>';
            $tmp['comment_text'] .= '<span id="count-comments-' . $activity->getId() . '"><span data-comments="' . count($activity->getCommentslist()) . '" id="comment_' . $activity->getId() . '"> ' . count($activity->getCommentslist()) . '</span></span>';


            if ($activity->getRedirectToArtwork()[0] == null)
                $tmp['artwork'] = false;
            array_push($activitiesToReturn, $tmp);
        }


        return new JsonResponse($activitiesToReturn);
    }

    /**
     * remove my activity
     * @Route("/{id}/remove-my-activity", name="remove_my_activity")
     * @Method({"GET", "DELETE"})
     */
    public function removeMyActivityAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('remove_my_activity', array('id' => $id)))
                ->setMethod('DELETE')
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($form->isValid()) {
            $em->remove($entity);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                'id' => $id,
                'message' => $this->get('translator')->trans('administration.reports.text.remove_activity_success'),
                'status' => 'success',
                    ])
            );
            return $response;
        } else {
            $content = $this->renderView('timeline/delete.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.remove_activity'),
                'button' => $this->get('translator')->trans('global.delete'),
                'content' => $content
                    ])
            );
            return $response;
        }
    }

    /**
     * edit my activity
     * @Route("/{id}/edit-my-activity", name="edit_my_activity")
     * @Method({"POST"})
     */
    public function editMyActivityAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('UserBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('edit_my_activity', array('id' => $id)))
                ->setMethod('POST')
                ->add('description', TextareaType::class, [
                    'data' => $entity->getDescription()
                ])
                ->getForm();
        $form->handleRequest($request);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        if ($form->isValid()) {
            $entity->setDescription($form->getData()['description']);
            $em->flush();
            $response->setContent(
                    json_encode([
                'error' => false,
                'id' => $id,
                'message' => $this->get('translator')->trans('administration.reports.text.edit_activity_success'),
                'status' => 'success',
                'content' => $entity->getDescription()
                    ])
            );
            return $response;
        } else {
            $content = $this->renderView('timeline/edit.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView()
            ));
            $response->setContent(
                    json_encode([
                'title' => $this->get('translator')->trans('global.edit_activity'),
                'button' => $this->get('translator')->trans('global.edit'),
                'content' => $content
                    ])
            );
            return $response;
        }
    }

    /**
     * add comment to activity
     * @Route("/add-comment-activity", name="add_comment_activity")
     * @Method({"POST"})
     */
    public function addCommentActivity(Request $req) {
        try {

            $em = $this->getDoctrine()->getManager();

            $activity = $em->getRepository('UserBundle:Activity')->find($req->request->get('id'));
            $msg = $req->request->get('message');


            $comment = new Comment();
            $comment->setActivity($activity);
            $comment->setUser($this->getUser());
            $comment->setContent($msg);
            $comment->setDate(new \DateTime());

            $em->persist($comment);
            $em->flush();

            $notification = $this->get('app_notification_services');
            $notification->sendNotificationActivity($comment, $activity);

            $monthNamesEN = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
            $monthNamesFR = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];

            $d = $comment->getDate()->format('d');
            $m = $comment->getDate()->format('m');
            $y = $comment->getDate()->format('Y');
            $date = $d . ' ' . (($req->getLocale() == "en") ? $monthNamesEN[$m - 1] : $monthNamesFR[$m - 1]) . ' ' . $y . ', ' . $comment->getDate()->format('H:i');

            $out = [
                'status' => true,
                'message' => $this->get('translator')->trans('global.add_comment_success'),
                'username' => $this->getUser()->getUsername(),
                'userid' => $this->getUser()->getId(),
                'comment' => $comment->getContent(),
                'date' => $date,
                'url_user' => $this->generateUrl('show_me', ['id' => $this->getUser()->getId()])
            ];
        } catch (\Exception $e) {
            $out = [
                'status' => false,
                'message' => $this->get('translator')->trans('global.add_comment_error')
            ];
        }

        return new JsonResponse($out);
    }

    /**
     * @Route("/show-more-comments", name="show_more_comments")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMoreCommentsAction(Request $request) {
        $first = $request->request->get('first');
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $activity = $em->getRepository('UserBundle:Activity')->find($id);
        $comments = $em->getRepository('LectureBundle:Comment')->getCommentByActivity($activity->getId(), $first, 24);
        return new JsonResponse($comments);
    }

}
