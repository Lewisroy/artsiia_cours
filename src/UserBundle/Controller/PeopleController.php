<?php

namespace UserBundle\Controller;

use UserBundle\Entity\User;
use LectureBundle\Entity\Category;
use LectureBundle\Entity\SubCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * People controller.
 *
 * @Route("/{_locale}/people")
 */
class PeopleController extends Controller {

    /**
     * Lists all people entities.
     *
     * @Route("/", name="people_index")
     * @Method("GET")
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('UserBundle:User')->showMoreUsers(0, 24);
        $categories = $em->getRepository('LectureBundle:Category')->findAll();
        $subcategories = $em->getRepository('LectureBundle:SubCategory')->findAll();

        $followings = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFollowings() as $f) {
                array_push($followings, $f->getId());
            }
        }

        $data = [];
        foreach ($users as $user) {
            array_push($data, $em->getRepository('UserBundle:User')->findToSearchUsers($user->getId()));
        }

        return $this->render('people/index.html.twig', array(
                    'users' => $data,
                    'categories' => $categories,
                    'subcategories' => $subcategories,
                    'followings' => json_encode($followings),
        ));
    }

    /**
     * search
     * @Route("/search", name="search_peoples")
     * @Method({"GET", "POST"})
     */
    public function searchAction(Request $request) {

        if ($request->getMethod() != "POST") {
            return $this->redirectToRoute('people_index');
        }
        $tag = $request->request->get('tag');
        $category = $request->request->get('category');
        $subcategory = $request->request->get('subcategory');
        $ranking = $request->request->get('ranking');

//        dump($request);
//        exit();

        $em = $this->getDoctrine()->getManager();
        // utilisateur connecté.
        $user = $this->getUser();
        $messages = array();

        if (null !== $user) {
            // recupération de tous les messages non lu.
            $messages = $em->getRepository('UserBundle:Message')->messagesNotRead($user->getId());
        }

        $followings = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFollowings() as $following) {
                array_push($followings, $following->getId());
            }
        }
        $data_ranking = [
            'followings' => $followings,
        ];
        $users = $em->getRepository('UserBundle:User')->showMoreSearchUsers(0, 24, $tag, $category, $ranking, $subcategory, $data_ranking);

        $data = [];
        foreach ($users as $user) {
            array_push($data, $em->getRepository('UserBundle:User')->findToSearchUsers($user[0]->getId()));
        }

        $aCategory = $em->getRepository('LectureBundle:Category')->findAll();
        $aSubCategory = [];
        if ($category != null) {
            $aSubCategory = $em->getRepository('LectureBundle:SubCategory')->getSubCategoriesByCateg($category);
        }
        return $this->render('people/search.html.twig', array(
                    'users' => $users,
                    'categories' => $aCategory,
                    'subCategories' => $aSubCategory,
                    'messages' => $messages,
                    'tag' => $tag,
                    'category' => $category,
                    'ranking' => $ranking,
                    'subcategory' => $subcategory,
                    'followings' => json_encode($followings),
        ));
    }

    /**
     * @Route("/show-more-people", name="show_more_people")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMorePeopleAction(Request $request) {
        $first = $request->request->get('first');
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->showMoreUsers($first, 24);
        $data = [];
        foreach ($users as $user) {
            array_push($data, $em->getRepository('UserBundle:User')->findToSearchUsers($user->getId()));
        }
        return new JsonResponse($data);
    }

    /**
     * @Route("/show-more-peoples-search", name="show_more_peoples_search")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMorePeoplesSearchAction(Request $request) {
        $first = $request->request->get('first');
        $tag = $request->request->get('tag');
        $category = $request->request->get('category');
        $ranking = $request->request->get('ranking');
        $subcategory = $request->request->get('subcategory');

        $followings = [];
        if ($this->getUser()) {
            foreach ($this->getUser()->getFollowings() as $following) {
                array_push($followings, $following->getId());
            }
        }
        $data_ranking = [
            'followings' => $followings,
        ];

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->showMoreSearchUsers($first, 24, $tag, $category, $ranking, $subcategory, $data_ranking);

        $data = [];
        foreach ($users as $user) {
            array_push($data, $em->getRepository('UserBundle:User')->findToSearchUsers($user[0]->getId()));
        }
        return new JsonResponse($data);
    }

    /**
     * Lists all people entities.
     *
     * @Route("/peopleMoreResult", name="people_more_results")
     * @Method({"GET", "POST"})
     */
    public function MoreResultAction() {
        $em = $this->getDoctrine()->getManager();
        extract($_POST);
        $users = $em->getRepository('UserBundle:User')->findBy(array('enabled' => 1, 'isBanni' => false), null, $maxResult, $lastResult);
        $nbs = array();
        $project = null;
        $artwork = null;

        foreach ($users as $key => $row) {
            $id = $row->getId();
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT count(*) as nbArtwork FROM author_artwork WHERE user_id = :id");
            $statement->bindValue('id', $id);
            $statement->execute();
            $results = $statement->fetchAll();
            if ($results != null) {
                $nbArtwork = $results['0']['nbArtwork'];
            } else {
                $nbArtwork = 0;
            }
            $artwork[$id] = $nbArtwork;
        }

        $data = array(
            'users' => $users,
            'project' => $project,
            'artwork' => $artwork
        );
        $serializer = $this->container->get('serializer');
        $reports = $serializer->serialize($data, 'json');
        return new Response($reports);
    }

    /**
     * Lists all people entities with followers.
     *
     * @Route("/peopleFollow", name="people_followers")
     * @Method({"GET", "POST"})
     */
    public function peopleFollowersAction() {
        extract($_POST);
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        if ($categoryFilter != "") {
            $statement = $connection->prepare("SELECT * FROM category_users WHERE category_id = :id");
            $statement->bindValue('id', $categoryFilter);
            $statement->execute();
            $results = $statement->fetchAll();
            $n = 0;
            foreach ($results as $key => $row) {
                $tabId[$n] = $row['user_id'];
                $n++;
            }
            $users = $em->getRepository('UserBundle:User')->findBy(array('enabled' => 1, 'id' => $tabId), null, $maxResult, 0);
        } else {
            $users = $em->getRepository('UserBundle:User')->findBy(array('enabled' => 1), null, $maxResult, 0);
        }
        $nbs = array();
        $artwork = null;
        $project = null;

        foreach ($users as $key => $row) {
            $id = $row->getId();
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT count(*) as nbArtwork FROM author_artwork WHERE user_id = :id");
            $statement->bindValue('id', $id);
            $statement->execute();
            $results = $statement->fetchAll();
            if ($results != null) {
                $nbArtwork = $results['0']['nbArtwork'];
            } else {
                $nbArtwork = 0;
            }
            $artwork[$id] = $nbArtwork;


            $nbs[$key] = count($row->getFollowers());
        }
        array_multisort($nbs, SORT_DESC, $users);

        $data = array(
            'users' => $users,
            'project' => $project,
            'artwork' => $artwork
        );
        $serializer = $this->container->get('serializer');
        $reports = $serializer->serialize($data, 'json');
        return new Response($reports);
    }

    /**
     * Lists all people entities with followers.
     *
     * @Route("/peopleArtwork", name="people_artwork")
     * @Method({"GET", "POST"})
     */
    public function peopleArtworkAction() {
        $em = $this->getDoctrine()->getManager();
        $connection = $em->getConnection();

        extract($_POST);

        if ($categoryFilter != "") {
            $statement = $connection->prepare("SELECT * FROM category_users WHERE category_id = :id");
            $statement->bindValue('id', $categoryFilter);
            $statement->execute();
            $results = $statement->fetchAll();
            $n = 0;
            foreach ($results as $key => $row) {
                $tabId[$n] = $row['user_id'];
                $n++;
            }
            $users = $em->getRepository('UserBundle:User')->findBy(array('enabled' => 1, 'id' => $tabId), null, $maxResult, 0);
        } else {
            $users = $em->getRepository('UserBundle:User')->findBy(array('enabled' => 1), null, $maxResult, 0);
        }

        $nbs = array();
        $artwork = null;
        $project = null;

        foreach ($users as $key => $row) {
            $id = $row->getId();
            $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $statement = $connection->prepare("SELECT count(*) as nbArtwork FROM author_artwork WHERE user_id = :id");
            $statement->bindValue('id', $id);
            $statement->execute();
            $results = $statement->fetchAll();
            if ($results != null) {
                $nbArtwork = $results['0']['nbArtwork'];
            } else {
                $nbArtwork = 0;
            }
            $artwork[$id] = $nbArtwork;

            $nbs[$key] = $nbArtwork;
        }

        array_multisort($nbs, SORT_DESC, $users);
        $data = array(
            'users' => $users,
            'artwork' => $artwork
        );
        $serializer = $this->container->get('serializer');
        $reports = $serializer->serialize($data, 'json');
        return new Response($reports);
    }

    /**
     * Show user followers.
     *
     * @Route("/{id}/followers", name="show_followers")
     * @Method({"GET"})
     */
    public function showFollowersAction($id) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($id);
        $followers = $this->getDoctrine()->getRepository('UserBundle:User')->getFollowers($id, 0, 25);
        $data = [];
        foreach ($followers as $f) {
            $d = $this->getDoctrine()->getRepository('UserBundle:User')->find($f);
            array_push($data, $d);
        }

        if ($this->getUser()) {
            $notification = $this->get('app_notification_services');
            $notification->viewNotificationSubscribers($this->getUser());
        }

        return $this->render('UserBundle:Followers:followers.html.twig', array(
                    'user' => $user,
                    'users' => $data,
                    'title' => 'Subscribers'
        ));
    }

    /**
     * @Route("/show-more-followers", name="show_more_followers")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMoreFollowers(Request $request) {
        $first = $request->request->get('first');
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $friends = $em->getRepository('UserBundle:User')->getFollowers($id, $first - 1, 25);
        $data = [];
        foreach ($friends as $f) {
            $d = $this->getDoctrine()->getRepository('UserBundle:User')->getUserArrayResult($f);
            array_push($data, $d[0]);
        }
        return new JsonResponse($data);
    }

    /**
     * Show user followings.
     *
     * @Route("/{id}/followings", name="show_followings")
     * @Method({"GET"})
     */
    public function showFollowingsAction($id) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($id);
        $followings = $this->getDoctrine()->getRepository('UserBundle:User')->getFollowings($id, 0, 25);
        $data = [];
        foreach ($followings as $f) {
            $d = $this->getDoctrine()->getRepository('UserBundle:User')->getUserArrayResult($f);
            array_push($data, $d[0]);
        }
        return $this->render('UserBundle:Followers:followings.html.twig', array(
                    'user' => $user,
                    'users' => $data,
                    'title' => 'Subscribs'
        ));
    }

    /**
     * @Route("/show-more-followings", name="show_more_followings")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMoreFollowings(Request $request) {
        $first = $request->request->get('first');
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $followings = $em->getRepository('UserBundle:User')->getFollowings($id, $first - 1, 25);
        $data = [];
        foreach ($followings as $f) {
            $d = $this->getDoctrine()->getRepository('UserBundle:User')->getUserArrayResult($f);
            array_push($data, $d[0]);
        }
        return new JsonResponse($data);
    }

    /**
     * Show user followers.
     *
     * @Route("/{id}/friends", name="show_friends")
     * @Method({"GET"})
     */
    public function showFriendsAction($id) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($id);

        $friends = $this->getDoctrine()->getRepository('UserBundle:User')->getFriends($id, 0, 25);
        $data = [];
        foreach ($friends as $f) {
            $d = $this->getDoctrine()->getRepository('UserBundle:User')->find($f);
            array_push($data, $d);
        }
        if ($this->getUser()) {
            $notification = $this->get('app_notification_services');
            $notification->viewNotificationFriendRequest($this->getUser());
        }
        return $this->render('UserBundle:Followers:index.html.twig', array(
                    'users' => $data,
                    'user' => $user,
                    'title' => 'Friends',
                    'askingFriends' => $user->getAskingFriends()
        ));
    }

    /**
     * @Route("/show-more-friends", name="show_more_friends")
     * @Method({"POST"})
     * @return JsonResponse
     */
    public function showMoreFriends(Request $request) {
        $first = $request->request->get('first');
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $friends = $em->getRepository('UserBundle:User')->getFriends($id, $first - 1, 25);
        $data = [];
        foreach ($friends as $f) {
            $d = $this->getDoctrine()->getRepository('UserBundle:User')->getUserArrayResult($f);
            array_push($data, $d[0]);
        }
        return new JsonResponse($data);
    }

}
