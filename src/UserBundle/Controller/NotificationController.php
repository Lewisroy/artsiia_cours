<?php

namespace UserBundle\Controller;

use UserBundle\Entity\Notification;
use UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Notification controller.
 *
 * @Route("{_locale}/notification")
 */
class NotificationController extends Controller {

    /**
     * get the ten last notifications unviews from user.
     *
     * @Route("/{id}/user", name="get_notifications_user")
     * @Method("GET")
     */
    public function getNotificationsUserAction(User $user) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER') || $this->getUser()->getId() != $user->getId()) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $notifications = $em->getRepository('UserBundle:Notification')->findNotificationsByUser($user, 10);
    }

    /**
     * put notification view.
     *
     * @Route("/{id}/view", name="get_notifications_user")
     * @Method("POST")
     */
    public function putNotificationViewAction(Notification $notification) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER') || $this->getUser()->getId() != $user->getId()) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $notification->setIsView(true);
        $em->persist($notification);
        $em->flush($notification);
    }

    /**
     * get notification.
     *
     * @Route("/get-notifications", name="get_notifications")
     * @Method({"POST"})
     */
    public function getNotificationAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $notif_artwork = $em->getRepository('UserBundle:Notification')->getNotificationArtworks($this->getUser()->getId());
        $notif_activity = $em->getRepository('UserBundle:Notification')->getNotificationActivitys($this->getUser()->getId());
        $notif_AskingFriend = $em->getRepository('UserBundle:Notification')->getNotificationAcceptFriend($this->getUser()->getId());
        $notif_Subscribers = $em->getRepository('UserBundle:Notification')->getNotificationSubscribers($this->getUser()->getId());
        $notif_FriendRequest = $em->getRepository('UserBundle:Notification')->getNotificationFriendRequest($this->getUser()->getId());

        $data = [];
        foreach ($notif_artwork as $notif) {
            $notif['type'] = 'notifArtwork';
            array_push($data, $notif);
        }

        foreach ($notif_activity as $notif) {
            $notif['type'] = 'notifActivity';
            array_push($data, $notif);
        }

        foreach ($notif_AskingFriend as $notif) {
            $notif['type'] = 'notifAskingFriend';
            array_push($data, $notif);
        }

        foreach ($notif_Subscribers as $notif) {
            $notif['type'] = 'notifSubscribers';
            array_push($data, $notif);
        }

        foreach ($notif_FriendRequest as $notif) {
            $notif['type'] = 'notifFriendRequest';
            array_push($data, $notif);
        }

        $data = $this->insertion($data);

        return new JsonResponse([
            'data' => $data,
            'time' => new \DateTime()
        ]);
    }

    /**
     * remove notification.
     *
     * @Route("/remove-notification/{id}", name="remove_notification")
     * @Method({"POST"})
     */
    public function removeNotificationAction(Notification $notification) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();
        $notification->setIsView(true);
        $em->flush();
        return new JsonResponse([
            'status' => true,
        ]);
    }

    private function insertion($array) {

        for ($i = 0; $i < count($array); $i++) {
            $x = $array[$i];
            $j = $i;
            while ($j > 0 && $array[$j - 1]['dateCreation'] < $x['dateCreation']) {
                $array[$j] = $array[$j - 1];
                $j = $j - 1;
            }
            $array[$j] = $x;
        }

        return $array;
    }

}
