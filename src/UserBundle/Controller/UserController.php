<?php

namespace UserBundle\Controller;

use AppBundle\Services\MangoPayServices;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * User controller.
 *
 * @Route("/{_locale}/user")
 */
class UserController extends Controller {

    /**
     * Lists all User entities.
     *
     * @Route("/", name="user_index")
     * @Method("GET")
     */
    public function indexAction() {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('UserBundle:User')->findAll();

        return $this->render('user/index.html.twig', array(
                    'users' => $users,
        ));
    }

    /**
     * Creates a new User entity.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $user = new User();
        $form = $this->createForm('UserBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $file = $user->getPicture();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                    'uploads/users/' . $user->getId(), $fileName
            );
            $user->setPicture($fileName);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('user/new.html.twig', array(
                    'user' => $user,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a User entity.
     *
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/{id}/profile", name="show_me")
     * @Method("GET")
     */
    public function showMeAction(User $user) {
        $mangoPayApi = new MangoPayServices();

        if ($user->getMangopayUserId() == null){
            $mangoUser = $mangoPayApi->createMangoUser($user->getFirstname(),$user->getLastname(),($user->getBirthday() != null)?$user->getBirthday()->getTimestamp():time(),"FR","FR",$user->getEmail());
            $walletUser = $mangoPayApi->createWalletUser($mangoUser->Id, $user->getFirstname()." ".$user->getLastname());

            $user->setMangopayUserId($mangoUser->Id);
            $user->setMangopayWalletId($walletUser->Id);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        } else {
            $mangoUser = $mangoPayApi->getMangoUser($user->getMangopayUserId());
            $walletUser = $mangoPayApi->getWalletUser($user->getMangopayWalletId());
        }

        dump($mangoUser);
        dump($walletUser);

        $payIn = $mangoPayApi->createPayInCard($user->getMangopayUserId(),$user->getMangopayWalletId(),4900,$user->getId());
        dump($payIn->ExecutionDetails->RedirectURL);

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getIsBanni()) {
            throw $this->createNotFoundException();
        }

        $currentUser = $this->getUser();
        $followers = $this->getDoctrine()->getRepository('UserBundle:User')->isFollowers($currentUser, $user);

        $deleteForm = $this->createDeleteForm($user);

        if ($this->getUser()) {
            $notification = $this->get('app_notification_services');
            if ($user == $this->getUser()) {
                $notification->removeNotificationActivity($this->getUser());
            }
            $notification->checkNotifAskingFriend($this->getUser(), $user);
        }

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
            'isFollowers' => $followers['nb'],
            'payinLink' => $payIn->ExecutionDetails->RedirectURL,
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method({"GET", "POST", "HEAD"})
     */
    public function editAction(Request $request, User $user) {

        $em = $this->getDoctrine()->getManager();

        $picture = $user->getPicture();
        $user = $em->getRepository('UserBundle:User')->findOneBy(array('id' => $user->getId()));

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getId() != $this->getUser()->getId() && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('UserBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($editForm->get('picture')->getData() != null) {
                $file = $editForm->get('picture')->getData();
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                        'uploads/users/' . $user->getId(), $fileName
                );
                $user->setPicture($fileName);
            } else {
                $user->setPicture($picture);
            }

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('show_me', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
                    'user' => $user,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to change parameters an existing User entity.
     *
     * @Route("/{id}/parameters", name="user_parameters")
     * @Method({"GET", "POST"})
     */
    public function parametersAction(Request $request, User $user) {

        $em = $this->getDoctrine()->getManager();

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getId() != $this->getUser()->getId() && !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }


        return $this->render('user/parameters.html.twig', array(
                    'user' => $user,
        ));
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/follow/{id}", name="user_follow")
     * @Method({"GET", "POST", "HEAD"})
     */
    public function followUserAction(Request $request) {
        extract($_POST);
        $em = $this->getDoctrine()->getManager();
        $id = $request->attributes->get('id');
        $user = $this->getUser();
        $userF = $em->getRepository('UserBundle:User')->findOneBy(array('id' => $id));
        $userF->addFollower($user);
        $user->addFollowing($userF);
        $em->merge($userF);
        $em->merge($user);
        $em->flush();
        $notification = $this->get('app_notification_services');
        $notification->SendNotificationSubscribers($user, $userF);
        return $this->redirectToRoute('people_index');
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user) {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        if ($user->getId() != $this->getUser()->getId() || !$this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('au_homepage');
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * @Route("/{id}/statistics", name="general_profile_statistics")
     */
    public function statisticsAction($id) {
        $em = $this->getDoctrine()->getManager();

        $likes = 0;
        $comments = 0;
        $views = 0;

        $artworks = $em->getRepository('ArtworkBundle:Artwork')->getUserArtworks($id);
        $user = $this->getDoctrine()->getRepository('UserBundle:User')->find($id);

        foreach ($artworks as $artwork) {
            $likes = $likes + count($artwork->getLikers());
            $comments = $comments + count($artwork->getComments());
            $views = $views + count($artwork->getViewers());
        }
        return $this->render(
                        'default/statistics.html.twig', array(
                    'likes' => $likes,
                    'comments' => $comments,
                    'views' => $views,
                    'user' => $user,
                        )
        );
    }

    /**
	 * Delete article on Shop user
	 * 
	 * @Route("/", name="delete_article_user")
	 * @Method("POST")
	 */
	public function delete_articleAction(Request $request){
		$em = $this->getDoctrine()->getManager();

		$article = $em->getRepository('ShopBundle:Article')->find($request->get('id'));

		$em->remove($article);
		$em->flush($article);
		
		return new JsonResponse(array("Article supprimé avec succes."));
	}

}
