<?php

namespace UserBundle\Controller;

use UserBundle\Entity\Activity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Activity controller.
 *
 * @Route("{_locale}/activity")
 */
class ActivityController extends Controller
{


    /**
     * Creates a new activity entity.
     *
     * @Route("/new", name="activity_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $activity = new Activity();
        $form = $this->createForm('UserBundle\Form\ActivityType', $activity, array(
        'action' => $this->generateUrl('activity_new'),
        'method' => 'POST',
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $activity->setDateCreation(new \Datetime());
            $activity->addAuthor($this->getUser());
            $em->persist($activity);
            $this->getUser()->addActivity($activity);
            $em->persist($this->getUser());
            $em->flush($activity);

            return $this->redirectToRoute('show_me', array('id' => $this->getUser()->getId()));
        }

        return $this->render('activity/new.html.twig', array(
            'activity' => $activity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays activities of an user.
     *
     * @Route("/myactivity", name="print_my_activity")
     * @Method({"GET", "POST"})
     */
    public function printMyActivityAction(Request $request)
    {
        if($this->getUser() == null)
            return JsonResponse(array('error' => " Non connecté"));
        
        $em = $this->getDoctrine()->getManager();
        $start = $request->request->get('start');
        $number = $request->request->get('number');
        $user = $this->getUser();
        $response = array();

        $activities = $em->getRepository('UserBundle:Activity')->getActivityToUser($user, $start, $number);

        foreach ($activities as $activity) {
            $tmp = array();
            $tmp['description'] = $activity->getDescription();
            $tmp['date'] = $activity->getDateCreation()->format('d-m-y h:i');
            array_push($response, $tmp);
        }
        
        return new JsonResponse($response);
    }

    /**
     * Finds and displays activities of an user's friends and subscriers.
     *
     * @Route("/mytimeline", name="print_my_timeline")
     * @Method({"GET", "POST"})
     */
    public function printMyTimelineAction(Request $request)
    {
        if($this->getUser() == null)
            return JsonResponse(array('error' => " Non connect�"));
        
        $em = $this->getDoctrine()->getManager();
        $start = $request->request->get('start');
        $number = $request->request->get('number');
        $user = $this->getUser();
        $response = array();

        
        $activities = $em->getRepository('UserBundle:Activity')->printMyTimeline($user, $start, $number);

        foreach ($activities as $activity) {
            $tmp = array();
            $tmp['description'] = $activity->getDescription();
            $tmp['date'] = $activity->getDateCreation()->format('d-m-y h:i');
            array_push($response, $tmp);
        }
        
        return new JsonResponse($response);
    }

    /**
     * Displays a form to edit an existing activity entity.
     *
     * @Route("/{id}/edit", name="activity_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Activity $activity)
    {
        $deleteForm = $this->createDeleteForm($activity);
        $editForm = $this->createForm('UserBundle\Form\ActivityType', $activity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('activity_edit', array('id' => $activity->getId()));
        }

        return $this->render('activity/edit.html.twig', array(
            'activity' => $activity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a activity entity.
     *
     * @Route("/{id}/delete", name="activity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Activity $activity)
    {

        // vérifier que $this->getUser est dans $activity->getAuthors
            $em = $this->getDoctrine()->getManager();
            $em->remove($activity);
            $em->flush($activity);


         return $this->redirectToRoute('show_me', array('id' => $this->getUser()->getId()));
    }

}
