<?php

namespace UserBundle\Controller;

use ArtworkBundle\Utils\ArtsiiaUtil;
use AppBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Entity\Subject;
use UserBundle\Entity\Message;
use UserBundle\Entity\User;
use ReportBundle\Entity\ReportMessage;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Tchat message controller.
 *
 * @Route("/{_locale}/tchat")
 */
class TchatController extends Controller {

    /**
     * Creates a new notification.
     *
     * @Route("/new", name="tchat_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $message = new Message();
        $form = $this->createForm('UserBundle\Form\MessageType', $message);
        $form->handleRequest($request);
        if ($request->getMethod() == "POST") {
            $zMessage = $request->get("message_text");
            $zDestinataire = $request->get("destinataires");
            if ($zDestinataire != null && $zMessage != null) {
                $tDestinataires = explode(",", $zDestinataire);
                $lastReceiverId = null;
                foreach ($tDestinataires as $name) {
                    $receiver = $em->getRepository('UserBundle:User')->findOneBy(array('username' => $name));
                    if ($receiver instanceof User) {
                        // Création des messages vers les destinataires
                        $leMessage = new Message();
                        $leMessage->setSender($user);
                        $leMessage->setReceiver($receiver);
                        $leMessage->setMessage($zMessage);
                        $leMessage->setReadState(0);
                        $leMessage->setOpenDate(new \DateTime('now'));
                        $em->persist($leMessage);
                        $em->flush();
                        $lastReceiverId = $receiver->getId();
                    }
                }
                if ($lastReceiverId !== null) {
                    $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('contact.add_success'));

                    return $this->redirectToRoute('tchat_edit', array('id' => $lastReceiverId));
                }
            }
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('contact.add_error'));
        }
        $conversations = $em->getRepository('UserBundle:Message')->getMyMessageGroupByUserId($user->getId(), true);
        $data = [];

        foreach ($conversations as $conversation) {
            $check = $em->getRepository('UserBundle:Message')->checkNotReadConversation($conversation['sender_id'], $user->getId());
            $conversation['check'] = $check['total'] ? true : false;
            array_push($data, $conversation);
        }

        $reports = $em->getRepository('ReportBundle:Reporting')->getReportForUserMessagerie($this->getUser()->getId());
        $total_notRead = 0;

        foreach ($reports as $report) {
            list($count, $notRead, $items) = $em->getRepository('ReportBundle:ReportMessage')->getMessageWithReportForMessagerie($report->getId());
            if ($count) {
                $total_notRead += ($notRead) ? 1 : 0;
            }
        }
        $users = $em->getRepository('UserBundle:User')->findBy(array('enabled' => 1));

        return $this->render('tchat/new.html.twig', array(
                    'user' => $user,
                    'users' => $users,
                    'conversations' => $data,
                    'message' => $message,
                    'total_notRead' => $total_notRead,
                    'form' => $form->createView(),
        ));
    }

    /**
     * List of reports notification.
     *
     * @Route("/conversation/reprts-messages/", name="tchat_reports_messages")
     */
    public function reprtsMessagesAction() {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $conversations = [];
        $ids = [];
        $reports = $em->getRepository('ReportBundle:Reporting')->getReportForUserMessagerie($this->getUser()->getId());
        $total_notRead = 0;

        foreach ($reports as $report) {
            list($count, $notRead, $items) = $em->getRepository('ReportBundle:ReportMessage')->getMessageWithReportForMessagerie($report->getId());
            if ($count) {
                $total_notRead += ($notRead) ? 1 : 0;
                $data = [
                    'report' => $report,
                    'date' => $items[0]['created'],
                    'content' => $items[0]['content'],
                    'isread' => $items[count($items) - 1]['isread'],
                    'total' => $count,
                    'notRead' => $notRead,
                    'items' => $items,
                ];
                array_push($conversations, $data);
                array_push($ids, $report->getId());
            }
        }
        return $this->render('tchat/reprts_messages.html.twig', array(
                    'conversations' => $conversations,
                    'total_notRead' => $total_notRead,
                    'ids' => json_encode($ids),
        ));
    }

    /**
     * Get list messages for report.
     *
     * @Route("/get-list-messages/report/{id}", name="tchat_get_list_report_messages")
     * @Method({"GET", "POST"})
     */
    public function getListMessagesReportAction($id) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $report = $em->getRepository('ReportBundle:Reporting')->find($id);
        $messages = $em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($id);

        foreach ($messages as $message) {
            $message->setIsread(true);
        }
        $em->flush();

        return $this->render('tchat/listMessagesReport.html.twig', array(
                    'report' => $report,
                    'messages' => $messages
        ));
    }

    /**
     * Get list messages for report.
     *
     * @Route("/reply-report-message", name="tchat_reply_report_message")
     * @Method({"POST"})
     */
    public function replyReportMessageAction(Request $req) {

        try {
            if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
                throw $this->createAccessDeniedException();
            }

            $em = $this->getDoctrine()->getManager();

            $report = $em->getRepository('ReportBundle:Reporting')->find($req->get('id'));
            $msg = $req->get('message');

            $check = $em->getRepository('ReportBundle:ReportMessage')->getMessageForReport($req->get('id'));

            $message = new ReportMessage();

            $message->setContent($msg);
            $message->setSender($this->getUser());
            $message->setReceiver($check[0]->getSender());
            $message->setReport($report);
            $message->setIsread(true);
            $message->setIsreadadmin(false);
            $message->setIsparent(false);
            $em->persist($message);
            $em->flush();
            $out = [
                'status' => true,
                'message' => $this->get('translator')->trans('administration.reports.text.add_message_success')
            ];
        } catch (\Exception $e) {
            $out = [
                'status' => false,
                'message' => $this->get('translator')->trans('administration.reports.text.add_message_error')
            ];
        }

        return new JsonResponse($out);
    }

    /**
     * Edit a new notification.
     *
     * @Route("/conversation/{id}/{filter}", name="tchat_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(User $receiver, Request $request, $filter = '') {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if ($filter == 'all')
            $conversations = $em->getRepository('UserBundle:Message')->getMyMessageGroupByUserId($user->getId());
        else
            $conversations = $em->getRepository('UserBundle:Message')->getMyMessageGroupByUserId($user->getId(), true);
        $data = [];

        foreach ($conversations as $conversation) {
            $check = $em->getRepository('UserBundle:Message')->checkNotReadConversation($conversation['sender_id'], $user->getId());
            $conversation['check'] = $check['total'] ? true : false;
            array_push($data, $conversation);
        }

        $reports = $em->getRepository('ReportBundle:Reporting')->getReportForUserMessagerie($this->getUser()->getId());
        $total_notRead = 0;

        foreach ($reports as $report) {
            list($count, $notRead, $items) = $em->getRepository('ReportBundle:ReportMessage')->getMessageWithReportForMessagerie($report->getId());
            if ($count) {
                $total_notRead += ($notRead) ? 1 : 0;
            }
        }


        return $this->render('tchat/edit.html.twig', array(
                    'user' => $user,
                    'conversations' => $data,
                    'total_notRead' => $total_notRead,
                    'destinataire' => $receiver,
                    'filter' => ($filter == 'all') ? false : true
        ));
    }

    /**
     * list all notification.
     *
     * @Route("/list/{filter}", name="tchat_list")
     * @Method({"GET", "POST"})
     */
    public function listAction(Request $request, $filter = '') {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        if ($filter == 'all')
            $conversations = $em->getRepository('UserBundle:Message')->getMyMessageGroupByUserId($user->getId());
        else
            $conversations = $em->getRepository('UserBundle:Message')->getMyMessageGroupByUserId($user->getId(), true);
        $data = [];

        foreach ($conversations as $conversation) {
            $check = $em->getRepository('UserBundle:Message')->checkNotReadConversation($conversation['sender_id'], $user->getId());
            $conversation['check'] = $check['total'] ? true : false;
            array_push($data, $conversation);
        }

        $reports = $em->getRepository('ReportBundle:Reporting')->getReportForUserMessagerie($this->getUser()->getId());
        $total_notRead = 0;

        foreach ($reports as $report) {
            list($count, $notRead, $items) = $em->getRepository('ReportBundle:ReportMessage')->getMessageWithReportForMessagerie($report->getId());
            if ($count) {
                $total_notRead += ($notRead) ? 1 : 0;
            }
        }

        return $this->render('tchat/index.html.twig', array(
                    'user' => $user,
                    'conversations' => $data,
                    'total_notRead' => $total_notRead,
                    'filter' => ($filter == 'all') ? false : true)
        );
    }

    /**
     * Creates a new notification.
     *
     * @Route("/send/{id}", name="send_tchat_message")
     * @Method({"GET", "POST"})
     */
    public function sendAction(User $receiver, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $message = $request->request->get("message", null);
        $newMessage = new Message();
        $newMessage->setReceiver($receiver);
        $newMessage->setSender($user);
        $newMessage->setMessage($message);
        $newMessage->setReadState(0);
        $newMessage->setOpenDate(new \DateTime('now'));
        $em->persist($newMessage);
        $em->flush();
        $messages = $em->getRepository('UserBundle:Message')->getOneToOneMessage($user->getId(), $receiver->getId());

        return $this->render('tchat/pull.html.twig', array(
                    'user' => $user,
                    'messages' => $messages
        ));
    }

    /**
     * Pull a new notification.
     *
     * @Route("/pulls/{id}/{page}", name="pull_tchat_messages", defaults={"page" = 1})
     * @Method({"GET", "POST"})
     */
    public function pullAction(User $receiver, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $page = $request->request->get('page') ? $request->request->get('page') : ArtsiiaUtil::NB_PAGE_DEFAULT_MSG;
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $lesMessages = array_reverse($em->getRepository('UserBundle:Message')->getOneToOneMessage($user->getId(), $receiver->getId(), $page));
        $criteria = array('userId' => $user->getId(), 'receiverId' => $receiver->getId() );
        $count = $em->getRepository('UserBundle:Message')->count($criteria);
        $nbPage = intval(($page * ArtsiiaUtil::NB_MAX_MESSAGE) / ArtsiiaUtil::NB_MAX_MESSAGE);

        return $this->render('tchat/pull.html.twig', array(
                    'user' => $user,
                    'receiver' => $receiver,
                    'messages' => $lesMessages,
                    'count' => $count['total'],
                    'page' => $nbPage,
        ));
    }

    /**
     * Récuperer nombre messages non lu.
     *
     * @Route("/notification/{id}", name="notif_messages")
     * @Method({"POST"})
     */
    public function checkoutAction(User $user, Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $doctrine = $this->getDoctrine();
        $count = $doctrine->getRepository('UserBundle:Message')->checkoutMessageNotRead($user->getId());

        return new JsonResponse(
                array(
            'status' => 'OK',
            'message' => $count[0]['nbMessage']
                ), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Récuperer tous les messages non lu.
     *
     * @Route("/notread/{id}", name="notread_messages")
     * @Method({"POST"})
     */
    public function messagesNotReadAction(User $user) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $manager = $this->get('artwork.artsiiamanager');
        $messages = $manager->getMessageNotRead($user);

        return $this->render('tchat/allMessageNotRead.html.twig', array(
                    'messages' => $messages
        ));
    }

    /**
     * Creates a new subject entity.
     *
     * @Route("/new-object", name="subject_create_form")
     * @Method({"GET", "POST"})
     */
    public function createSubjectAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $user = $this->getUser();
        $subject = new Subject();
        $form = $this->createForm('UserBundle\Form\SubjectType', $subject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subject);
            $em->flush($subject);
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('subject.add_subject'));
        } else {
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('subject.add_error'));
        }

        return $this->redirectToRoute('tchat_new');
    }

    /**
     * Update a notification.
     *
     * @Route("/update-activity/{id}", name="update_notification_activity")
     * @Method({"GET", "POST"})
     */
    public function updateActivityAction(User $sender) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $user = $this->getUser();
        $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:Message')
                ->updateDateActivityByUser($user->getId(), $sender->getId());

        return new JsonResponse(array("success" => 1));
    }

    /**
     * Notification GET.
     *
     * @Route("/get-notification", name="get_notification_activity")
     * @Method({"GET", "POST"})
     */
    public function getActivityAction() {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $user = $this->getUser();
        $messageRepository = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:Message');
        $datas = $messageRepository->getMessageByUser($user->getId());
        $htmlResponse = "";
        if ($datas && count($datas) > 0) {
            foreach ($datas as $data) {
                $htmlResponse .= $this->render('tchat/notification.html.twig', array(
                            'user' => $user,
                            'dernierMessage' => $data
                        ))->getContent();
            }
        } else {
            $htmlResponse = $this->render('tchat/notification.html.twig', array(
                        'nomessage' => true
                    ))->getContent();
        }

        return new JsonResponse(array("unreadCount" => $messageRepository->countUreadMessageByUser($user->getId()), 'html' => $htmlResponse));
    }

    /**
     * Autocomplete users by term.
     *
     * @Route("/autocomplete", name="get_autocomplete_user")
     * @Method({"GET", "POST"})
     */
    public function getAutocompleteAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $term = $request->query->get("q");
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository('UserBundle:User')->getFriendsAutocomplete($user->getId(), $term);

        return new JsonResponse($datas);
    }

    /**
     * Delete conversations.
     *
     * @Route("/delete-conversations", name="delete_conversations")
     * @Method({"GET", "POST"})
     */
    public function deleteMessageAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:Message');
        $ids = $request->get("ids");
        $user = $this->getUser();
        if ($ids && count($ids) > 0) {
            foreach ($ids AS $senderId) {
                $repository->updateDeleteByReceiver($user->getId(), $senderId);
                $repository->updateDeleteBySender($user->getId(), $senderId);
            }
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('message.add_success'));
        } else {
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('message.add_error'));
        }

        return new JsonResponse(true);
    }

    /**
     * Marquer comme non lu conversations.
     *
     * @Route("/unread-conversations", name="unread_conversations")
     * @Method({"GET", "POST"})
     */
    public function markNonLuMessageAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:Message');
        $ids = $request->get("ids");
        $user = $this->getUser();
        if ($ids && count($ids) > 0) {
            foreach ($ids AS $senderId) {
                $repository->updateNonLuMessageByUser($user->getId(), $senderId);
            }
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('message.market_success'));
        } else {
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('message.market_error'));
        }

        return new JsonResponse(true);
    }

    /**
     * Marquer comme lu conversations.
     *
     * @Route("/read-conversations", name="read_conversations")
     * @Method({"GET", "POST"})
     */
    public function markLuMessageAction(Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }
        $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository('UserBundle:Message');
        $ids = $request->get("ids");
        $user = $this->getUser();
        if ($ids && count($ids) > 0) {
            foreach ($ids AS $senderId) {
                $repository->updateLuMessageByUser($user->getId(), $senderId);
            }
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('message.market_read_success'));
        } else {
            $request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('message.market_error'));
        }

        return new JsonResponse(true);
    }

}
