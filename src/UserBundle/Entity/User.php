<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", nullable = true)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname_status", type="string", nullable = true)
     */
    protected $firstnameStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", nullable = true)
     */
    protected $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname_status", type="string", nullable = true)
     */
    protected $lastnameStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="email_status", type="string", nullable = true)
     */
    protected $emailStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable = true)
     */
    protected $birthday;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday_status", type="string", nullable = true)
     */
    protected $birthday_status;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable = true)
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="gender_status", type="string", nullable = true)
     */
    protected $genderStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable = true)
     */
    protected $language;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", nullable = true)
     */
    protected $street;

    /**
     * @var string
     *
     * @ORM\Column(name="street_status", type="string", nullable = true)
     */
    protected $streetStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="postal_code", type="integer", nullable = true)
     */
    protected $postalCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="postal_code_status", type="string", nullable = true)
     */
    protected $postalCodeStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", nullable = true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="city_status", type="string", nullable = true)
     */
    protected $cityStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", nullable = true)
     */
    protected $country;

    /**
     * @var string
     *
     * @ORM\Column(name="country_status", type="string", nullable = true)
     */
    protected $countryStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", nullable = true)
     */
    protected $location;

    /**
     * @var text
     *
     * @ORM\Column(name="styles", type="text", nullable = true)
     */
    protected $styles;

    /**
     * @var text
     *
     * @ORM\Column(name="styles_status", type="text", nullable = true)
     */
    protected $stylesStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="tools", type="text", nullable = true)
     */
    protected $tools;

    /**
     * @var text
     *
     * @ORM\Column(name="tools_status", type="text", nullable = true)
     */
    protected $toolsStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="equipments", type="text", nullable = true)
     */
    protected $equipments;

    /**
     * @var text
     *
     * @ORM\Column(name="equipments_status", type="text", nullable = true)
     */
    protected $equipmentsStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="hobbies", type="text", nullable = true)
     */
    protected $hobbies;

    /**
     * @var text
     *
     * @ORM\Column(name="hobbies_status", type="text", nullable = true)
     */
    protected $hobbiesStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", nullable = true)
     */
    protected $website;

    /**
     * @var text
     *
     * @ORM\Column(name="bio", type="text", nullable = true)
     */
    protected $bio;

    /**
     * @var string
     *
     * @ORM\Column(name="socialmedia", type="string", nullable = true)
     */
    protected $socialmedia;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", nullable = true)
     */
    protected $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", nullable = true)
     */
    protected $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", nullable = true)
     */
    protected $instagram;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin", type="string", nullable = true)
     */
    protected $linkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="pinterest", type="string", nullable = true)
     */
    protected $pinterest;

    /**
     * @var string
     *
     * @ORM\Column(name="google_plus", type="string", nullable = true)
     */
    protected $googlePlus;

    /**
     * @var text
     *
     * @ORM\Column(name="studies", type="text", nullable = true)
     */
    protected $studies;

    /**
     * @var text
     *
     * @ORM\Column(name="studies_status", type="text", nullable = true)
     */
    protected $studiesStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="skills", type="text", nullable = true)
     */
    protected $skills;

    /**
     * @var text
     *
     * @ORM\Column(name="skills_status", type="text", nullable = true)
     */
    protected $skillsStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="experience", type="text", nullable = true)
     */
    protected $experiences;

    /**
     * @var text
     *
     * @ORM\Column(name="experience_status", type="text", nullable = true)
     */
    protected $experiencesStatus;

    /**
     * @var text
     *
     * @ORM\Column(name="my_projects", type="text", nullable = true)
     */
    protected $myProjects;

    /**
     * @var text
     *
     * @ORM\Column(name="visited_country", type="text", nullable = true)
     */
    protected $visiteCountry;

    /**
     * @var text
     *
     * @ORM\Column(name="talking_travels", type="text", nullable = true)
     */
    protected $talkingTravels;

    /**
     * @var text
     *
     * @ORM\Column(name="my_work", type="text", nullable = true)
     */
    protected $my_work;

    /**
     * @var boolean
     *
     * @ORM\Column(name="profile_type", type="boolean", nullable = true)
     */
    protected $profileType;

    /**
     * @var boolean
     *
     * @ORM\Column(name="defy_me", type="boolean", nullable = true)
     */
    protected $defyMe;

    /**
     * @Assert\File(maxSize="2048k")
     * @Assert\Image(mimeTypesMessage="Please upload a valid image.")
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $picture;
    public $path;
    public $t;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Category",cascade={"persist"})
     * @ORM\JoinTable(name="category_users")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="ArtworkBundle\Entity\Artwork",cascade={"persist"})
     * @ORM\JoinTable(name="artwork_users")
     */
    private $artworks;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Activity",cascade={"persist"})
     * @ORM\JoinTable(name="activity_users")
     */
    private $activities;

    /**
     * @ORM\ManyToMany(targetEntity="ShopBundle\Entity\Article",cascade={"persist"})
     * @ORM\JoinTable(name="article_users")
     */
    private $articles;

    /**
     * @ORM\Column(name="type_artsien", type="array", length=255, nullable=true)
     */
    public $typeArtsien;

    /**
     * @ORM\Column(name="active_boutique", type="boolean", nullable = true)
     */
    public $activeBoutique;

    /**
     * @ORM\Column(name="accept_donation", type="boolean", nullable = true)
     */
    public $acceptDonation;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="user_followers")
     */
    private $followers;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="user_following")
     */
    private $followings;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="user_friends")
     */
    private $friends;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="user_asking_friends")
     */
    private $askingFriends;

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="reporter")
     */
    public $reporting;

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Banishment", mappedBy="admin")
     */
    public $banish;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isBanni", type="boolean", options={"default" : false})
     */
    private $isBanni;
    
    /**
     * @var bool
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $agreePrivacyPolicy;
    
    /**
     * @var bool
     * 
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $agreeDisclaimer;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="mangopayUserId", type="string", nullable=true)
     */
    private $mangopayUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="mangopayWalletId", type="string", nullable=true)
     */
    private $mangopayWalletId;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->reporting = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reportinguser = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reportingprofile = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messagesender = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messagereceiver = new \Doctrine\Common\Collections\ArrayCollection();
        $this->banish = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isBanni = false;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    /**
     * Add reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     *
     * @return Reporting
     */
    public function addReporting(\ReportBundle\Entity\Reporting $reporting) {
        $this->reporting[] = $reporting;

        return $this;
    }

    /**
     * Remove reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     */
    public function removeReporting(\ReportBundle\Entity\Reporting $reporting) {
        $this->reporting->removeElement($reporting);
    }

    /**
     * Get reporting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporting() {
        return $this->reporting;
    }

    /**
     * Add banish
     *
     * @param \ReportBundle\Entity\Banishment $banish
     *
     * @return Banishment
     */
    public function addBanish(\ReportBundle\Entity\Banishment $banish) {
        $this->banish[] = $banish;

        return $this;
    }

    /**
     * Remove banish
     *
     * @param \ReportBundle\Entity\Banishment $banish
     */
    public function removeBanish(\ReportBundle\Entity\Banishment $banish) {
        $this->banish->removeElement($banish);
    }

    /**
     * Get banish
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBanish() {
        return $this->banish;
    }

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="reporteduser")
     */
    public $reportinguser;

    /**
     * Add reportinguser
     *
     * @param \ReportBundle\Entity\Reporting $reportinguser
     *
     * @return Reportinguser
     */
    public function addReportinguser(\ReportBundle\Entity\Reporting $reportinguser) {
        $this->reportinguser[] = $reportinguser;

        return $this;
    }

    /**
     * Remove reportinguser
     *
     * @param \ReportBundle\Entity\Reporting $reportinguser
     */
    public function removeReportinguser(\ReportBundle\Entity\Reporting $reportinguser) {
        $this->reportinguser->removeElement($reportinguser);
    }

    /**
     * Get reportinguser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportinguser() {
        return $this->reportinguser;
    }

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="profile")
     */
    public $reportingprofile;

    /**
     * Add reportingprofile
     *
     * @param \ReportBundle\Entity\Reporting $reportingprofile
     *
     * @return Reportingprofile
     */
    public function addReportingprofile(\ReportBundle\Entity\Reporting $reportingprofile) {
        $this->reportingprofile[] = $reportingprofile;

        return $this;
    }

    /**
     * Remove reportingprofile
     *
     * @param \ReportBundle\Entity\Reporting $reportingprofile
     */
    public function removeReportingprofile(\ReportBundle\Entity\Reporting $reportingprofile) {
        $this->reportingprofile->removeElement($reportingprofile);
    }

    /**
     * Get reportingprofile
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReportingprofile() {
        return $this->reportingprofile;
    }

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\ReportMessage", mappedBy="sender")
     */
    public $messagesender;

    /**
     * Add messagesender
     *
     * @param \ReportBundle\Entity\ReportMessage $messagesender
     *
     * @return Messagesender
     */
    public function addMessagesender(\ReportBundle\Entity\ReportMessage $messagesender) {
        $this->messagesender[] = $messagesender;

        return $this;
    }

    /**
     * Remove messagesender
     *
     * @param \ReportBundle\Entity\ReportMessage $messagesender
     */
    public function removeMessagesender(\ReportBundle\Entity\ReportMessage $messagesender) {
        $this->messagesender->removeElement($messagesender);
    }

    /**
     * Get messagesender
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessagesender() {
        return $this->messagesender;
    }

    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\ReportMessage", mappedBy="receiver")
     */
    public $messagereceiver;

    /**
     * Add messagereceiver
     *
     * @param \ReportBundle\Entity\ReportMessage $messagereceiver
     *
     * @return Messagereceiver
     */
    public function addMessagereceiver(\ReportBundle\Entity\ReportMessage $messagereceiver) {
        $this->messagereceiver[] = $messagereceiver;

        return $this;
    }

    /**
     * Remove messagereceiver
     *
     * @param \ReportBundle\Entity\ReportMessage $messagereceiver
     */
    public function removeMessagereceiver(\ReportBundle\Entity\ReportMessage $messagereceiver) {
        $this->messagereceiver->removeElement($messagereceiver);
    }

    /**
     * Get messagereceiver
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessagereceiver() {
        return $this->messagereceiver;
    }

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="user_refuse_friends")
     */
    private $refuseFriends;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="user_blocked")
     */
    private $userBlocked;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return User
     */
    public function setBirthday($birthday) {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday() {
        return $this->birthday;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return User
     */
    public function setGender($gender) {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender() {
        return $this->gender;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return User
     */
    public function setLanguage($language) {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return User
     */
    public function setLocation($location) {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set styles
     *
     * @param $styles
     *
     * @return User
     */
    public function setStyles($styles) {
        $this->styles = $styles;

        return $this;
    }

    /**
     * Get styles
     *
     * @return string
     */
    public function getStyles() {
        return $this->styles;
    }

    /**
     * Set tools
     *
     * @param string $tools
     *
     * @return User
     */
    public function setTools($tools) {
        $this->tools = $tools;

        return $this;
    }

    /**
     * Get tools
     *
     * @return string
     */
    public function getTools() {
        return $this->tools;
    }

    /**
     * Set equipments
     *
     * @param string $equipments
     *
     * @return User
     */
    public function setEquipments($equipments) {
        $this->equipments = $equipments;

        return $this;
    }

    /**
     * Get equipments
     *
     * @return string
     */
    public function getEquipments() {
        return $this->equipments;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return User
     */
    public function setWebsite($website) {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite() {
        return $this->website;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return User
     */
    public function setBio($bio) {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio() {
        return $this->bio;
    }

    /**
     * Set socialmedia
     *
     * @param string $socialmedia
     *
     * @return User
     */
    public function setSocialmedia($socialmedia) {
        $this->socialmedia = $socialmedia;

        return $this;
    }

    /**
     * Get socialmedia
     *
     * @return string
     */
    public function getSocialmedia() {
        return $this->socialmedia;
    }

    /**
     * Set profileType
     *
     * @param boolean $profileType
     *
     * @return User
     */
    public function setProfileType($profileType) {
        $this->profileType = $profileType;

        return $this;
    }

    /**
     * Get profileType
     *
     * @return boolean
     */
    public function getProfileType() {
        return $this->profileType;
    }

    /**
     * Set defyMe
     *
     * @param boolean $defyMe
     *
     * @return User
     */
    public function setDefyMe($defyMe) {
        $this->defyMe = $defyMe;

        return $this;
    }

    /**
     * Get defyMe
     *
     * @return boolean
     */
    public function getDefyMe() {
        return $this->defyMe;
    }

    /**
     * @param UploadedFile $file
     * @return object
     */
    public function setFile(UploadedFile $file = null) {
        // set the value of the holder
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->path = 'initial';
            $this->t = $this->profilePicturePath;
            $this->tempPath = null;
        } else {
            $this->path = 'initial';
        }

        return $this;
    }

    /**
     * Get the file used for profile picture uploads
     *
     * @return UploadedFile
     */
    public function getFile() {

        return $this->file;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            // a file was uploaded
            // generate a unique filename
            $filename = $this->generateRandomProfilePictureFilename();
            $this->setPath($filename);
        }
    }

    /**
     * Generates a 32 char long random filename
     *
     * @return string
     */
    public function generateRandomProfilePictureFilename() {

        $random = md5(uniqid()) . "date('Y-m-d h:m:s')." . $this->getFile()->guessExtension();
        return $random;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->file) {
            return;
        }
        $this->getFile()->move($this->getUploadRootDir(), $this->getPath());

        if (isset($this->tempPath) && file_exists($this->getUploadRootDir() . '/' . $this->tempPath)) {
            unlink($this->getUploadRootDir() . '/' . $this->tempPath);
            $this->tempPath = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file == $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath() {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath() {
        return null === $this->path ? null : $this->getUploadDir() . '/' .
                $this->id . '/' . $this->path;
    }

    protected function getUploadRootDir() {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir() . '/' . $this->id;
    }

    protected function getUploadDir() {
        return 'uploads/users';
    }

    /**
     * Set path
     *
     * @param string $path
     * @return User
     */
    public function setPath($path) {
        $this->picture = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath() {
        return $this->picture;
    }

    /**
     * Set cover
     *
     * @param string $picture
     *
     * @return User
     */
    public function setPicturer($picture) {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getPicture() {
        return $this->picture;
    }

    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return User
     */
    public function setPicture($picture) {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Add category
     *
     * @param \LectureBundle\Entity\Category $category
     *
     * @return User
     */
    public function addCategory(\LectureBundle\Entity\Category $category) {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \LectureBundle\Entity\Category $category
     */
    public function removeCategory(\LectureBundle\Entity\Category $category) {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street) {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }

    /**
     * Set postalCode
     *
     * @param integer $postalCode
     *
     * @return User
     */
    public function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    /**
     * Set typeArtsien
     *
     * @param array $typeArtsien
     *
     * @return User
     */
    public function setTypeArtsien($typeArtsien) {
        $this->typeArtsien = $typeArtsien;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return integer
     */
    public function getPostalCode() {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return User
     */
    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }

    /**
     * Get typeArtsien
     *
     * @return array
     */
    public function getTypeArtsien() {
        return $this->typeArtsien;
    }

    /**
     * Set activeBoutique
     *
     * @param boolean $activeBoutique
     *
     * @return User
     */
    public function setActiveBoutique($activeBoutique) {
        $this->activeBoutique = $activeBoutique;

        return $this;
    }

    /**
     * Get isBanni
     *
     * @return boolean
     */
    public function getIsBanni() {
        return $this->isBanni;
    }

    /**
     * Set isBanni
     *
     * @param boolean $isBanni
     *
     * @return User
     */
    public function setIsBanni($isBanni) {
        $this->isBanni = $isBanni;

        return $this;
    }
    
    /**
     * Get agreePrivacyPolicy
     *
     * @return boolean
     */
    public function getAgreePrivacyPolicy() {
        return $this->agreePrivacyPolicy;
    }

    /**
     * Set agreePrivacyPolicy
     *
     * @param boolean $agreePrivacyPolicy
     *
     * @return User
     */
    public function setAgreePrivacyPolicy($agreePrivacyPolicy) {
        $this->agreePrivacyPolicy = $agreePrivacyPolicy;

        return $this;
    }
    
    /**
     * Get agreeDisclaimer
     *
     * @return boolean
     */
    public function getAgreeDisclaimer() {
        return $this->agreeDisclaimer;
    }

    /**
     * Set agreeDisclaimer
     *
     * @param boolean $agreeDisclaimer
     *
     * @return User
     */
    public function setAgreeDisclaimer($agreeDisclaimer) {
        $this->agreeDisclaimer = $agreeDisclaimer;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set hobbies
     *
     * @param string $hobbies
     *
     * @return User
     */
    public function setHobbies($hobbies) {
        $this->hobbies = $hobbies;

        return $this;
    }

    /**
     * Get hobbies
     *
     * @return string
     */
    public function getHobbies() {
        return $this->hobbies;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return User
     */
    public function setFacebook($facebook) {
        $this->facebook = $facebook;
    }

    /**
     * Get activeBoutique
     *
     * @return boolean
     */
    public function getActiveBoutique() {
        return $this->activeBoutique;
    }

    /**
     * Set acceptDonation
     *
     * @param boolean $acceptDonation
     *
     * @return User
     */
    public function setAcceptDonation($acceptDonation) {
        $this->acceptDonation = $acceptDonation;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook() {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return User
     */
    public function setTwitter($twitter) {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter() {
        return $this->twitter;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return User
     */
    public function setInstagram($instagram) {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram() {
        return $this->instagram;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     *
     * @return User
     */
    public function setLinkedin($linkedin) {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin() {
        return $this->linkedin;
    }

    /**
     * Set googlePlus
     *
     * @param string $googlePlus
     *
     * @return User
     */
    public function setGooglePlus($googlePlus) {
        $this->googlePlus = $googlePlus;

        return $this;
    }

    /**
     * Get googlePlus
     *
     * @return string
     */
    public function getGooglePlus() {
        return $this->googlePlus;
    }

    /**
     * Set studies
     *
     * @param string $studies
     *
     * @return User
     */
    public function setStudies($studies) {
        $this->studies = $studies;

        return $this;
    }

    /**
     * Get studies
     *
     * @return string
     */
    public function getStudies() {
        return $this->studies;
    }

    /**
     * Set skills
     *
     * @param string $skills
     *
     * @return User
     */
    public function setSkills($skills) {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return string
     */
    public function getSkills() {
        return $this->skills;
    }

    /**
     * Set experiences
     *
     * @param string $experiences
     *
     * @return User
     */
    public function setExperiences($experiences) {
        $this->experiences = $experiences;

        return $this;
    }

    /**
     * Get experiences
     *
     * @return string
     */
    public function getExperiences() {
        return $this->experiences;
    }

    /**
     * Set myProjects
     *
     * @param string $myProjects
     *
     * @return User
     */
    public function setMyProjects($myProjects) {
        $this->myProjects = $myProjects;

        return $this;
    }

    /**
     * Get myProjects
     *
     * @return string
     */
    public function getMyProjects() {
        return $this->myProjects;
    }

    /**
     * Set visiteCountry
     *
     * @param string $visiteCountry
     *
     * @return User
     */
    public function setVisiteCountry($visiteCountry) {
        $this->visiteCountry = $visiteCountry;

        return $this;
    }

    /**
     * Get visiteCountry
     *
     * @return string
     */
    public function getVisiteCountry() {
        return $this->visiteCountry;
    }

    /**
     * Set talkingTravels
     *
     * @param string $talkingTravels
     *
     * @return User
     */
    public function setTalkingTravels($talkingTravels) {
        $this->talkingTravels = $talkingTravels;

        return $this;
    }

    /**
     * Get talkingTravels
     *
     * @return string
     */
    public function getTalkingTravels() {
        return $this->talkingTravels;
    }

    /**
     * Set myWork
     *
     * @param string $myWork
     *
     * @return User
     */
    public function setMyWork($myWork) {
        $this->my_work = $myWork;

        return $this;
    }

    /**
     * Get myWork
     *
     * @return string
     */
    public function getMyWork() {
        return $this->my_work;
    }

    /**
     * Add artwork
     *
     * @param \ArtworkBundle\Entity\Artwork $artwork
     *
     * @return User
     */
    public function addArtwork(\ArtworkBundle\Entity\Artwork $artwork) {
        $this->artworks[] = $artwork;

        return $this;
    }

    /**
     * Remove artwork
     *
     * @param \ArtworkBundle\Entity\Artwork $artwork
     */
    public function removeArtwork(\ArtworkBundle\Entity\Artwork $artwork) {
        $this->artworks->removeElement($artwork);
    }

    /**
     * Get artworks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArtworks() {
        return $this->artworks;
    }

    /**
     * Add article
     *
     * @param \ShopBundle\Entity\Article $article
     *
     * @return User
     */
    public function addArticle(\ShopBundle\Entity\Article $article) {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \ShopBundle\Entity\Article $article
     */
    public function removeArticle(\ShopBundle\Entity\Article $article) {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles() {
        return $this->articles;
    }

    /**
     * Get acceptDonation
     * @return boolean
     */
    public function getAcceptDonation() {
        return $this->acceptDonation;
    }

    /**
     * Add follower
     *
     * @param \UserBundle\Entity\User $follower
     *
     * @return User
     */
    public function addFollower(\UserBundle\Entity\User $follower) {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \UserBundle\Entity\User $follower
     */
    public function removeFollower(\UserBundle\Entity\User $follower) {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers() {
        return $this->followers;
    }

    /**
     * Add activity
     *
     * @param \UserBundle\Entity\Activity $activity
     *
     * @return User
     */
    public function addActivity(\UserBundle\Entity\Activity $activity) {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \UserBundle\Entity\Activity $activity
     */
    public function removeActivity(\UserBundle\Entity\Activity $activity) {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities() {
        return $this->activities;
    }

    /**
     * Add following
     *
     * @param \UserBundle\Entity\User $following
     *
     * @return User
     */
    public function addFollowing(\UserBundle\Entity\User $following) {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param \UserBundle\Entity\User $following
     */
    public function removeFollowing(\UserBundle\Entity\User $following) {
        $this->followings->removeElement($following);
    }

    /**
     * Get followings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowings() {
        return $this->followings;
    }

    /**
     * Add friend
     *
     * @param \UserBundle\Entity\User $friend
     *
     * @return User
     */
    public function addFriend(\UserBundle\Entity\User $friend) {
        $this->friends[] = $friend;

        return $this;
    }

    /**
     * Remove friend
     *
     * @param \UserBundle\Entity\User $friend
     */
    public function removeFriend(\UserBundle\Entity\User $friend) {
        $this->friends->removeElement($friend);
    }

    /**
     * Get friends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriends() {
        return $this->friends;
    }

    /**
     * Add askingFriend
     *
     * @param \UserBundle\Entity\User $askingFriend
     *
     * @return User
     */
    public function addAskingFriend(\UserBundle\Entity\User $askingFriend) {
        $this->askingFriends[] = $askingFriend;

        return $this;
    }

    /**
     * Remove askingFriend
     *
     * @param \UserBundle\Entity\User $askingFriend
     */
    public function removeAskingFriend(\UserBundle\Entity\User $askingFriend) {
        $this->askingFriends->removeElement($askingFriend);
    }

    /**
     * Get askingFriends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAskingFriends() {
        return $this->askingFriends;
    }

    /**
     * Add refuseFriend
     *
     * @param \UserBundle\Entity\User $refuseFriend
     *
     * @return User
     */
    public function addRefuseFriend(\UserBundle\Entity\User $refuseFriend) {
        $this->refuseFriends[] = $refuseFriend;

        return $this;
    }

    /**
     * Remove refuseFriend
     *
     * @param \UserBundle\Entity\User $refuseFriend
     */
    public function removeRefuseFriend(\UserBundle\Entity\User $refuseFriend) {
        $this->refuseFriends->removeElement($refuseFriend);
    }

    /**
     * Get refuseFriends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRefuseFriends() {
        return $this->refuseFriends;
    }

    /**
     * Add userBlocked
     *
     * @param \UserBundle\Entity\User $userBlocked
     *
     * @return User
     */
    public function addUserBlocked(\UserBundle\Entity\User $userBlocked) {
        $this->userBlocked[] = $userBlocked;

        return $this;
    }

    /**
     * Remove userBlocked
     *
     * @param \UserBundle\Entity\User $userBlocked
     */
    public function removeUserBlocked(\UserBundle\Entity\User $userBlocked) {
        $this->userBlocked->removeElement($userBlocked);
    }

    /**
     * Get userBlocked
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserBlocked() {
        return $this->userBlocked;
    }

    /**
     * Set firstnameStatus
     *
     * @param string $firstnameStatus
     *
     * @return User
     */
    public function setFirstnameStatus($firstnameStatus) {
        $this->firstnameStatus = $firstnameStatus;

        return $this;
    }

    /**
     * Get firstnameStatus
     *
     * @return string
     */
    public function getFirstnameStatus() {
        return $this->firstnameStatus;
    }

    /**
     * Set lastnameStatus
     *
     * @param string $lastnameStatus
     *
     * @return User
     */
    public function setLastnameStatus($lastnameStatus) {
        $this->lastnameStatus = $lastnameStatus;

        return $this;
    }

    /**
     * Get lastnameStatus
     *
     * @return string
     */
    public function getLastnameStatus() {
        return $this->lastnameStatus;
    }

    /**
     * Set birthdayStatus
     *
     * @param \DateTime $birthdayStatus
     *
     * @return User
     */
    public function setBirthdayStatus($birthdayStatus) {
        $this->birthday_status = $birthdayStatus;

        return $this;
    }

    /**
     * Get birthdayStatus
     *
     * @return \DateTime
     */
    public function getBirthdayStatus() {
        return $this->birthday_status;
    }

    /**
     * Set genderStatus
     *
     * @param string $genderStatus
     *
     * @return User
     */
    public function setGenderStatus($genderStatus) {
        $this->genderStatus = $genderStatus;

        return $this;
    }

    /**
     * Get genderStatus
     *
     * @return string
     */
    public function getGenderStatus() {
        return $this->genderStatus;
    }

    /**
     * Set streetStatus
     *
     * @param string $streetStatus
     *
     * @return User
     */
    public function setStreetStatus($streetStatus) {
        $this->streetStatus = $streetStatus;

        return $this;
    }

    /**
     * Get streetStatus
     *
     * @return string
     */
    public function getStreetStatus() {
        return $this->streetStatus;
    }

    /**
     * Set postalCodeStatus
     *
     * @param integer $postalCodeStatus
     *
     * @return User
     */
    public function setPostalCodeStatus($postalCodeStatus) {
        $this->postalCodeStatus = $postalCodeStatus;

        return $this;
    }

    /**
     * Get postalCodeStatus
     *
     * @return integer
     */
    public function getPostalCodeStatus() {
        return $this->postalCodeStatus;
    }

    /**
     * Set cityStatus
     *
     * @param string $cityStatus
     *
     * @return User
     */
    public function setCityStatus($cityStatus) {
        $this->cityStatus = $cityStatus;

        return $this;
    }

    /**
     * Get cityStatus
     *
     * @return string
     */
    public function getCityStatus() {
        return $this->cityStatus;
    }

    /**
     * Set countryStatus
     *
     * @param string $countryStatus
     *
     * @return User
     */
    public function setCountryStatus($countryStatus) {
        $this->countryStatus = $countryStatus;

        return $this;
    }

    /**
     * Get countryStatus
     *
     * @return string
     */
    public function getCountryStatus() {
        return $this->countryStatus;
    }

    /**
     * Set stylesStatus
     *
     * @param string $stylesStatus
     *
     * @return User
     */
    public function setStylesStatus($stylesStatus) {
        $this->stylesStatus = $stylesStatus;

        return $this;
    }

    /**
     * Get stylesStatus
     *
     * @return string
     */
    public function getStylesStatus() {
        return $this->stylesStatus;
    }

    /**
     * Set toolsStatus
     *
     * @param string $toolsStatus
     *
     * @return User
     */
    public function setToolsStatus($toolsStatus) {
        $this->toolsStatus = $toolsStatus;

        return $this;
    }

    /**
     * Get toolsStatus
     *
     * @return string
     */
    public function getToolsStatus() {
        return $this->toolsStatus;
    }

    /**
     * Set equipmentsStatus
     *
     * @param string $equipmentsStatus
     *
     * @return User
     */
    public function setEquipmentsStatus($equipmentsStatus) {
        $this->equipmentsStatus = $equipmentsStatus;

        return $this;
    }

    /**
     * Get equipmentsStatus
     *
     * @return string
     */
    public function getEquipmentsStatus() {
        return $this->equipmentsStatus;
    }

    /**
     * Set hobbiesStatus
     *
     * @param string $hobbiesStatus
     *
     * @return User
     */
    public function setHobbiesStatus($hobbiesStatus) {
        $this->hobbiesStatus = $hobbiesStatus;

        return $this;
    }

    /**
     * Get hobbiesStatus
     *
     * @return string
     */
    public function getHobbiesStatus() {
        return $this->hobbiesStatus;
    }

    /**
     * Set studiesStatus
     *
     * @param string $studiesStatus
     *
     * @return User
     */
    public function setStudiesStatus($studiesStatus) {
        $this->studiesStatus = $studiesStatus;

        return $this;
    }

    /**
     * Get studiesStatus
     *
     * @return string
     */
    public function getStudiesStatus() {
        return $this->studiesStatus;
    }

    /**
     * Set skillsStatus
     *
     * @param string $skillsStatus
     *
     * @return User
     */
    public function setSkillsStatus($skillsStatus) {
        $this->skillsStatus = $skillsStatus;

        return $this;
    }

    /**
     * Get skillsStatus
     *
     * @return string
     */
    public function getSkillsStatus() {
        return $this->skillsStatus;
    }

    /**
     * Set experiencesStatus
     *
     * @param string $experiencesStatus
     *
     * @return User
     */
    public function setExperiencesStatus($experiencesStatus) {
        $this->experiencesStatus = $experiencesStatus;

        return $this;
    }

    /**
     * Get experiencesStatus
     *
     * @return string
     */
    public function getExperiencesStatus() {
        return $this->experiencesStatus;
    }

    /**
     * Set pinterest
     *
     * @param string $pinterest
     *
     * @return User
     */
    public function setPinterest($pinterest) {
        $this->pinterest = $pinterest;

        return $this;
    }

    /**
     * Get pinterest
     *
     * @return string
     */
    public function getPinterest() {
        return $this->pinterest;
    }

    /**
     * Set emailStatus
     *
     * @param string $emailStatus
     *
     * @return User
     */
    public function setEmailStatus($emailStatus) {
        $this->emailStatus = $emailStatus;

        return $this;
    }

    /**
     * Get emailStatus
     *
     * @return string
     */
    public function getEmailStatus() {
        return $this->emailStatus;
    }

    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted() {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted) {
        $this->deleted = $deleted;
    }


    /**
     * Set mangopayUserId.
     *
     * @param string|null $mangopayUserId
     *
     * @return User
     */
    public function setMangopayUserId($mangopayUserId = null)
    {
        $this->mangopayUserId = $mangopayUserId;

        return $this;
    }

    /**
     * Get mangopayUserId.
     *
     * @return string|null
     */
    public function getMangopayUserId()
    {
        return $this->mangopayUserId;
    }

    /**
     * Set mangopayWalletId.
     *
     * @param string|null $mangopayWalletId
     *
     * @return User
     */
    public function setMangopayWalletId($mangopayWalletId = null)
    {
        $this->mangopayWalletId = $mangopayWalletId;

        return $this;
    }

    /**
     * Get mangopayWalletId.
     *
     * @return string|null
     */
    public function getMangopayWalletId()
    {
        return $this->mangopayWalletId;
    }
}
