<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\ActivityRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 */
class Activity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetimetz")
     */
    private $dateCreation;
    
    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="activity_viewers")
     */
    private $viewers;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="activity_likers")
     */
    private $likers;

    /**
     * @ORM\ManyToMany(targetEntity="LectureBundle\Entity\Comment",cascade={"persist"})
     * @ORM\JoinTable(name="activity_comments")
     */
    private $comments;
    
    /**
     * @ORM\OneToMany(targetEntity="LectureBundle\Entity\Comment",mappedBy="activity")
     */
    private $commentslist;

    /**
     * @ORM\ManyToMany(targetEntity="ArtworkBundle\Entity\Artwork",cascade={"persist"})
     * @ORM\JoinTable(name="activity_artwork")
     */
    private $redirectToArtwork;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

     /**
     * @var cover
     *
     * @ORM\Column(name="cover", type="string", nullable = true)
     */
    private $cover;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="activity_author")
     */
    private $author;
    
    /**
     * @ORM\OneToMany(targetEntity="ReportBundle\Entity\Reporting", mappedBy="activity")
     */
    public $reporting;

    /**
     * Add reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     *
     * @return Reporting
     */
    public function addReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting[] = $reporting;

        return $this;
    }

    /**
     * Remove reporting
     *
     * @param \ReportBundle\Entity\Reporting $reporting
     */
    public function removeReporting(\ReportBundle\Entity\Reporting $reporting)
    {
        $this->reporting->removeElement($reporting);
    }

    /**
     * Get reporting
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReporting()
    {
        return $this->reporting;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Activity
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateViewByUser
     *
     * @param string $dateViewByUser
     *
     * @return Activity
     */
    public function setDateViewByUser($dateViewByUser)
    {
        $this->dateViewByUser = $dateViewByUser;
    
        return $this;
    }

    /**
     * Get dateViewByUser
     *
     * @return string
     */
    public function getDateViewByUser()
    {
        return $this->dateViewByUser;
    }

    /**
     * Set redirectToArtwork
     *
     * @param string $redirectToArtwork
     *
     * @return Activity
     */
    public function setRedirectToArtwork($redirectToArtwork)
    {
        $this->redirectToArtwork = $redirectToArtwork;
    
        return $this;
    }

    /**
     * Get redirectToArtwork
     *
     * @return string
     */
    public function getRedirectToArtwork()
    {
        return $this->redirectToArtwork;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Activity
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->viewers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->redirectToArtwork = new \Doctrine\Common\Collections\ArrayCollection();
        $this->author = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reporting = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Activity
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cover
     *
     * @param string $cover
     *
     * @return Activity
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    
        return $this;
    }

    /**
     * Get cover
     *
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Add viewer
     *
     * @param \UserBundle\Entity\User $viewer
     *
     * @return Activity
     */
    public function addViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers[] = $viewer;
    
        return $this;
    }

    /**
     * Remove viewer
     *
     * @param \UserBundle\Entity\User $viewer
     */
    public function removeViewer(\UserBundle\Entity\User $viewer)
    {
        $this->viewers->removeElement($viewer);
    }

    /**
     * Get viewers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getViewers()
    {
        return $this->viewers;
    }

    /**
     * Add redirectToArtwork
     *
     * @param \ArtworkBundle\Entity\Artwork $redirectToArtwork
     *
     * @return Activity
     */
    public function addRedirectToArtwork(\ArtworkBundle\Entity\Artwork $redirectToArtwork)
    {
        $this->redirectToArtwork[] = $redirectToArtwork;
    
        return $this;
    }

    /**
     * Remove redirectToArtwork
     *
     * @param \ArtworkBundle\Entity\Artwork $redirectToArtwork
     */
    public function removeRedirectToArtwork(\ArtworkBundle\Entity\Artwork $redirectToArtwork)
    {
        $this->redirectToArtwork->removeElement($redirectToArtwork);
    }

    /**
     * Add author
     *
     * @param \UserBundle\Entity\User $author
     *
     * @return Activity
     */
    public function addAuthor(\UserBundle\Entity\User $author)
    {
        $this->author[] = $author;
    
        return $this;
    }

    /**
     * Remove author
     *
     * @param \UserBundle\Entity\User $author
     */
    public function removeAuthor(\UserBundle\Entity\User $author)
    {
        $this->author->removeElement($author);
    }

    /**
     * Get author
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add liker
     *
     * @param \UserBundle\Entity\User $liker
     *
     * @return Activity
     */
    public function addLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers[] = $liker;
    
        return $this;
    }

    /**
     * Remove liker
     *
     * @param \UserBundle\Entity\User $liker
     */
    public function removeLiker(\UserBundle\Entity\User $liker)
    {
        $this->likers->removeElement($liker);
    }

    /**
     * Get likers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLikers()
    {
        return $this->likers;
    }

    /**
     * Add comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     *
     * @return Activity
     */
    public function addComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;
    
        return $this;
    }

    /**
     * Remove comment
     *
     * @param \LectureBundle\Entity\Comment $comment
     */
    public function removeComment(\LectureBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
    
    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }
    
    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }
    
    /**
     * Add commentlist
     *
     * @param \LectureBundle\Entity\Comment $commentlist
     *
     * @return Activity
     */
    public function addCommentlist(\LectureBundle\Entity\Comment $commentlist)
    {
        $this->commentslist[] = $commentlist;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \LectureBundle\Entity\Comment $commentlist
     */
    public function removeCommentlist(\LectureBundle\Entity\Comment $commentlist)
    {
        $this->commentslist->removeElement($commentlist);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommentslist()
    {
        return $this->commentslist;
    }
}
