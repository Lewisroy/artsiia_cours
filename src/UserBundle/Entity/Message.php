<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\MessageRepository")
 */
class Message {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="openDate", type="datetimetz")
     */
    private $openDate;

    /**
     * Sender of the message
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @var User
     */
    protected $sender;
    
    /**
     * Destinator of the message
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @var User
     */
    protected $receiver;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateMaj", type="datetimetz",nullable=true)
     */
    private $dateMaj;

    /**
     * @var integer
     *
     * @ORM\Column(name="readByReceiver", type="integer", nullable=true)
     */
    private $readState;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="deletedBySender", type="integer", nullable=true)
     */
    private $deletedBySender;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="deletedByReceiver", type="integer", nullable=true)
     */
    private $deletedByReceiver;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateMaj = new \DateTime('now');
        $this->openDate = new \DateTime('now');
        $this->deletedBySender = 0;
        $this->deletedByReceiver = 0;
    }

    /**
     * @return the string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param  $message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return the DateTime
     */
    public function getOpenDate()
    {
        return $this->openDate;
    }

    /**
     * @param \DateTime $openDate
     */
    public function setOpenDate(\DateTime $openDate)
    {
        $this->openDate = $openDate;

        return $this;
    }

    /**
     * @return the User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $sender
     */
    public function setSender(User $sender)
    {
        $this->sender = $sender;

        return $this;
    }
    
    /**
     * @return the User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
    
    /**
     * @param User $receiver
     */
    public function setReceiver(User $receiver)
    {
        $this->receiver = $receiver;
    
        return $this;
    }

    /**
     * @return the DateTime
     */
    public function getDateMaj()
    {
        return $this->dateMaj;
    }

    /**
     * @param \DateTime $dateMaj
     */
    public function setDateMaj(\DateTime $dateMaj)
    {
        $this->dateMaj = $dateMaj;

        return $this;
    }

    /**
     * @return the integer
     */
    public function getReadState()
    {
        return $this->readState;
    }

    /**
     * @param  $readState
     */
    public function setReadState($readState)
    {
        $this->readState = $readState;

        return $this;
    }
    

    /**
     * @return the integer
     */
    public function getDeletedBySender()
    {
        return $this->deletedBySender;
    }
    
    /**
     * @param  $deletedBySender
     */
    public function setDeletedBySender($deletedBySender)
    {
        $this->deletedBySender = $deletedBySender;
    
        return $this;
    }
    

    /**
     * @return the integer
     */
    public function getDeletedByReceiver()
    {
        return $this->deletedByReceiver;
    }
    
    /**
     * @param  $deletedByReceiver
     */
    public function setDeletedByReceiver($deletedByReceiver)
    {
        $this->deletedByReceiver = $deletedByReceiver;
    
        return $this;
    }

}
