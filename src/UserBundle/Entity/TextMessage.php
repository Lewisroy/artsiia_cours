<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TextMessage
 *
 * @ORM\Table(name="text_message")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\TextMessageRepository")
 */
class TextMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="openDate", type="datetimetz")
     */
    private $openDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="report_message", type="boolean")
     */
    private $report;

    /**
     * @var bool
     *
     * @ORM\Column(name="read_by_receiver", type="boolean")
     */
    private $read;

     /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinTable(name="sender_message")
     */
    private $sender;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        
        $this->openDate = new \DateTime('now');
        $this->report = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return TextMessage
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set openDate
     *
     * @param \DateTime $openDate
     *
     * @return TextMessage
     */
    public function setOpenDate($openDate)
    {
        $this->openDate = $openDate;
    
        return $this;
    }

    /**
     * Get openDate
     *
     * @return \DateTime
     */
    public function getOpenDate()
    {
        return $this->openDate;
    }

    /**
     * Set report
     *
     * @param boolean $report
     *
     * @return TextMessage
     */
    public function setReport($report)
    {
        $this->report = $report;
    
        return $this;
    }

    /**
     * Get report
     *
     * @return boolean
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Add sender
     *
     * @param \UserBundle\Entity\User $sender
     *
     * @return TextMessage
     */
    public function addSender(\UserBundle\Entity\User $sender)
    {
        $this->sender[] = $sender;
    
        return $this;
    }

    /**
     * Remove sender
     *
     * @param \UserBundle\Entity\User $sender
     */
    public function removeSender(\UserBundle\Entity\User $sender)
    {
        $this->sender->removeElement($sender);
    }

    /**
     * Get sender
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set read
     *
     * @param boolean $read
     *
     * @return TextMessage
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return boolean
     */
    public function getRead()
    {
        return $this->read;
    }
}
