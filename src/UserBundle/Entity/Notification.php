<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;
use UserBundle\Entity\Activity;
use ArtworkBundle\Entity\Artwork;
use LectureBundle\Entity\Lecture;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\NotificationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deleted", timeAware=false)
 */
class Notification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetimetz")
     */
    private $dateCreation;

    /**
     * @var integer
     *
     * @ORM\Column(name="isView", type="boolean")
     */
    private $isView;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="askingFriend", type="boolean")
     */
    private $askingFriend;

    /**
     * Sender of the notification
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @var User
     */
    protected $fromUser;

    /**
     * Receiver of the message
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @var User
     */
    protected $toUser;

    /**
     * Artwork if sender post a comment in receiver Artwork
     *
     * @ORM\ManyToOne(targetEntity="ArtworkBundle\Entity\Artwork")
     * @ORM\JoinColumn(name="notification_artwork_comment", referencedColumnName = "id", nullable=true)
     * 
     * @var Artwork
     * 
     */
    protected $artworkComment;

    /**
     * Lecture if if sender post a comment in receiver Lecture
     *
     * @ORM\ManyToOne(targetEntity="LectureBundle\Entity\Lecture")
     * @ORM\JoinColumn(name="notification_lecture_comment", referencedColumnName = "id", nullable=true)
     * 
     * @var Lecture
     * 
     */
    protected $lectureComment;

    /**
     * Activity if if sender post a comment in receiver Activity
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Activity")
     * @ORM\JoinColumn(name="notification_activity_comment", referencedColumnName = "id", nullable=true)
     * 
     * @var Activity
     * 
     */
    protected $activityComment;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_new_subscribers", type="integer", nullable=true)
     */
    protected $newSubscribers;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="number_new_friend_request", type="integer", nullable=true)
     */
    protected $newFriendRequest;
    
    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param string $dateCreation
     *
     * @return Notification
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    
        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    

    /**
     * Set isView
     *
     * @param integer $isView
     *
     * @return Notification
     */
    public function setIsView($isView)
    {
        $this->isView = $isView;

        return $this;
    }

    /**
     * Get isView
     *
     * @return integer
     */
    public function getIsView()
    {
        return $this->isView;
    }
    
    /**
     * Set askingFriend
     *
     * @param integer $askingFriend
     *
     * @return Notification
     */
    public function setAskingFriend($askingFriend)
    {
        $this->askingFriend = $askingFriend;

        return $this;
    }

    /**
     * Get askingFriend
     *
     * @return integer
     */
    public function getAskingFriend()
    {
        return $this->askingFriend;
    }

    /**
     * Set artworkComment
     *
     * @param Artwork $artworkComment
     *
     * @return Notification
     */
    public function setArtworkComment(Artwork $artworkComment)
    {
        $this->artworkComment = $artworkComment;

        return $this;
    }

    /**
     * Get artworkComment
     *
     * @return string
     */
    public function getArtworkComment()
    {
        return $this->artworkComment;
    }

    /**
     * Set lectureComment
     *
     * @param string $lectureComment
     *
     * @return Notification
     */
    public function setLectureComment($lectureComment)
    {
        $this->lectureComment = $lectureComment;

        return $this;
    }

    /**
     * Get lectureComment
     *
     * @return string
     */
    public function getLectureComment()
    {
        return $this->lectureComment;
    }

    /**
     * Set newSubscribers
     *
     * @param integer $newSubscribers
     *
     * @return Notification
     */
    public function setNewSubscribers($newSubscribers)
    {
        $this->newSubscribers = $newSubscribers;

        return $this;
    }

    /**
     * Get newSubscribers
     *
     * @return integer
     */
    public function getNewSubscribers()
    {
        return $this->newSubscribers;
    }
    
    /**
     * Set newFriendRequest
     *
     * @param integer $newFriendRequest
     *
     * @return Notification
     */
    public function setNewFriendRequest($newFriendRequest)
    {
        $this->newFriendRequest = $newFriendRequest;

        return $this;
    }

    /**
     * Get newFriendRequest
     *
     * @return integer
     */
    public function getNewFriendRequest()
    {
        return $this->newFriendRequest;
    }

    /**
     * Set fromUser
     *
     * @param \UserBundle\Entity\User $fromUser
     *
     * @return Notification
     */
    public function setFromUser(\UserBundle\Entity\User $fromUser = null)
    {
        $this->fromUser = $fromUser;

        return $this;
    }

    /**
     * Get fromUser
     *
     * @return \UserBundle\Entity\User
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     * Set toUser
     *
     * @param \UserBundle\Entity\User $toUser
     *
     * @return Notification
     */
    public function setToUser(\UserBundle\Entity\User $toUser = null)
    {
        $this->toUser = $toUser;

        return $this;
    }

    /**
     * Get toUser
     *
     * @return \UserBundle\Entity\User
     */
    public function getToUser()
    {
        return $this->toUser;
    }

 

    /**
     * Set activityComment
     *
     * @param \UserBundle\Entity\Activity $activityComment
     *
     * @return Notification
     */
    public function setActivityComment(\UserBundle\Entity\Activity $activityComment = null)
    {
        $this->activityComment = $activityComment;

        return $this;
    }

    /**
     * Get activityComment
     *
     * @return \UserBundle\Entity\Activity
     */
    public function getActivityComment()
    {
        return $this->activityComment;
    }
    
    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=true)
     */
    private $deleted;

    /**
     * Get deleted
     *
     * @return datetime
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set deleted
     *
     * @param datetime $deleted
     *
     * @return Artwork
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }
    
    /**
     * @return \DateTime
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated) {
        $this->updated = $updated;
    }
}
