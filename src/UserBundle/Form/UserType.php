<?php

namespace UserBundle\Form;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use UserBundle\Entity\User;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, array('label' => 'profile.firstname', 'required' => false))
            ->add('firstnameStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'global.private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false))
            ->add('emailStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'global.private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false))
            ->add('lastname', TextType::class, array('label' => 'profile.lastname','required' => false))
            ->add('lastnameStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'global.private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false))
            ->add('gender', ChoiceType::class, array(
                'choices'  => array(
                    'profile.men' =>'profile.men',
                    'profile.woman' => 'profile.woman',
                    'profile.no_genre' => 'profile.no_genre',
                    'profile.bigender' => 'profile.bigender',
                    'profile.not_binary' => 'profile.not_binary',
                    'profile.genderfluid' => 'profile.genderfluid',
                    'profile.not_your_business' => 'profile.not_your_business'
                ), 'label'=> 'users.gender','required' => false))
            ->add('genderStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'global.private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false))
            ->add('birthday', DateTimeType::class, array('label' => 'users.birthday','required' => false, 'widget' => 'single_text','html5' => false,'format' => 'dd-MM-yyyy','attr' => ['class' => 'js-datepicker form-input']))
            ->add('birthday_status', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'Private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false,'attr' => ['class' => 'form-input']))
            ->add('language', TextType::class, array('label' => 'language','required' => false,'attr' => ['class' => 'form-input']))
            ->add('postalCode', TextType::class, array('label' => 'profile.postalCode','required' => false,'attr' => ['class' => 'form-input']))
            ->add('postalCodeStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'Private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false,'attr' => ['class' => 'form-input']))
            ->add('street', TextType::class, array('label' => 'profile.street','required' => false,'attr' => ['class' => 'form-input']))
            ->add('streetStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'Private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false,'attr' => ['class' => 'form-input']))
            ->add('city', TextType::class, array('label' => 'profile.city','required' => false,'attr' => ['class' => 'form-input']))
            ->add('cityStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'Private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false,'attr' => ['class' => 'form-input']))
            ->add('country', TextType::class, array('label' => 'profile.country','required' => false,'attr' => ['class' => 'form-input']))
            ->add('countryStatus', ChoiceType::class, array(
                'choices'  => array(
                    'global.private' =>'Private',
                    'Friends' => 'Friends',
                    'Subscribers' => 'Subscribers',
                   'users.Public' =>  'Public'
                ), 'label'=> 'profile.status','required' => false,'attr' => ['class' => 'form-input']))
            ->add('styles', TextareaType::class, array('label' => 'users.styles','required' => false,'attr' => ['class' => 'form-input']))
            ->add('facebook', TextType::class, array('label' => '','required' => false,'attr' => ['class' => 'form-input']))
            ->add('twitter', TextType::class, array('label' => '','required' => false,'attr' => ['class' => 'form-input']))
            ->add('googlePlus', TextType::class, array('label' => '','required' => false,'attr' => ['class' => 'form-input']))
            ->add('pinterest', TextType::class, array('label' => '','required' => false,'attr' => ['class' => 'form-input']))
            ->add('instagram', TextType::class, array('label' => '','required' => false,'attr' => ['class' => 'form-input']))
            ->add('tools', TextareaType::class, array('label' => 'users.tools','required' => false,'attr' => ['class' => 'form-input']))
            ->add('equipments', TextareaType::class, array('label' => 'users.equipments','required' => false,'attr' => ['class' => 'form-input']))
            ->add('website', TextType::class, array('label' => 'users.website','required' => false,'attr' => ['class' => 'form-input']))
            ->add('bio', TextareaType::class, array('label' => 'users.biography','required' => false,'attr' => ['class' => 'form-input no-change-focus']))
            ->add('picture', FileType::class, array('required' => false, 'label' => "users.picture", 'data_class' => null))

            ->add('acceptDonation', ChoiceType::class, array(
                'required' => false,
                'choices'  => array(
                    'Oui' => true,
                    'Non' => false,
                ), 'label'=> 'profile.acceptDonation','attr' => ['class' => 'form-input']))
            ->add('category',null,
                array('label' => 'category.title', 
                    'required' => false, 
                    'multiple' => true, 
                    'expanded' => false,
            
                    'choice_label' => 'name'
                )
            )
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
        ;
        
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }
}
