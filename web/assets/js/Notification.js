Notification = function () {

    $.ajax({
        url: Routing.generate('get_notifications', {_locale: locale}),
        method: 'POST',
        dataType: 'JSON',
        success: function (rep) {
            var count = rep.data.length;
            $(".notification-block").empty();

            for (var i = 0; i < rep.data.length; i++) {

                if (rep.data[i].type == 'notifArtwork') {
                    var notif = rep.data[i];
                    var url_show_me = Routing.generate('show_me', {_locale: locale, id: notif.fromUser.id});
                    var url_show_artwork = Routing.generate('artwork_show', {_locale: locale, id: notif.artworkComment.id});
                    if (notif.artworkComment.cover != "") {
                        var img_artwork = '<img src="' + imagine_filter_artwork + notif.artworkComment.cover + '" alt="" class="be-ava-comment-notif">';
                    } else {
                        var img_artwork = '<img src="' + default_img_artworks + '" alt="" class="be-ava-comment-notif">';
                    }

                    if (notif.fromUser.picture != null) {
                        var img_user = '<img src="' + imagine_filter_user + notif.fromUser.id + '/' + notif.fromUser.picture + '" alt="" class="be-ava-comment">';
                    } else {
                        var img_user = '<img src="' + default_img_user + '" alt="" class="be-ava-comment">';
                    }
                    var html = '<div class="noto-entry" id="block-notif-' + notif.id + '">' +
                            '<span class="close-notif" onClick="closeNotif(\'' + notif.id + '\')"><a><i class="fa fa-times" aria-hidden="true"></i></a></span>' +
                            '<div class="noto-content clearfix">' +
                            '<span class="noto-img-notif-left">' +
                            '<a href="' + url_show_artwork + '">' +
                            img_artwork +
                            '</a>' +
                            '</span>' +
                            '<div class="noto-message-notif notif-artwork">' +
                            '<a href="' + url_show_me + '">' + notif.fromUser.username + '</a> ' + comment_your_artwork + ' <a href="' + url_show_artwork + '">' + notif.artworkComment.title + '.</a>' +
                            '<span class="noto-date-notif"><i class="fa fa-clock-o"></i> ' + getDate(rep.time.date, notif.dateCreation.date) + '</span>' +
                            '</div>' +
                            '<span class="noto-img-notif-right">' +
                            '<a href="' + url_show_me + '">' +
                            img_user +
                            '</a>' +
                            '</span>' +
                            '</div>' +
                            '</div>';
                    $(".notification-block").append(html);
                } else if (rep.data[i].type == 'notifActivity') {
                    var notif = rep.data[i];
                    var url_show_me = Routing.generate('show_me', {_locale: locale, id: notif.fromUser.id});
                    var url_show_current = Routing.generate('show_me', {_locale: locale, id: current});
                    if (notif.fromUser.picture != null) {
                        var img_user = '<img src="' + imagine_filter_user + notif.fromUser.id + '/' + notif.fromUser.picture + '" alt="" class="be-ava-comment">';
                    } else {
                        var img_user = '<img src="' + default_img_user + '" alt="" class="be-ava-comment">';
                    }
                    var html = '<div class="noto-entry" id="block-notif-' + notif.id + '">' +
                            '<span class="close-notif" onClick="closeNotif(\'' + notif.id + '\')"><a><i class="fa fa-times" aria-hidden="true"></i></a></span>' +
                            '<div class="noto-content clearfix">' +
                            '<div class="noto-message-notif notif-activity">' +
                            '<a href="' + url_show_me + '">' + notif.fromUser.username + '</a> ' + comment_your + ' <a href="' + url_show_current + '">publication.</a>' +
                            '<span class="noto-date-notif"><i class="fa fa-clock-o"></i> ' + getDate(rep.time.date, notif.dateCreation.date) + '</span>' +
                            '</div>' +
                            '<span class="noto-img-notif-right">' +
                            '<a href="' + url_show_me + '">' +
                            img_user +
                            '</a>' +
                            '</span>' +
                            '</div>' +
                            '</div>';
                    $(".notification-block").append(html);
                } else if (rep.data[i].type == 'notifAskingFriend') {
                    var notif = rep.data[i];
                    var url_show_me = Routing.generate('show_me', {_locale: locale, id: notif.fromUser.id});
                    if (notif.fromUser.picture != null) {
                        var img_user = '<img src="' + imagine_filter_user + notif.fromUser.id + '/' + notif.fromUser.picture + '" alt="" class="be-ava-comment">';
                    } else {
                        var img_user = '<img src="' + default_img_user + '" alt="" class="be-ava-comment">';
                    }
                    var html = '<div class="noto-entry" id="block-notif-' + notif.id + '">' +
                            '<span class="close-notif" onClick="closeNotif(\'' + notif.id + '\')"><a><i class="fa fa-times" aria-hidden="true"></i></a></span>' +
                            '<div class="noto-content clearfix">' +
                            '<div class="noto-message-notif notif-activity">' +
                            '<a href="' + url_show_me + '">' + notif.fromUser.username + '</a> ' + accepted_friend_request +
                            '.<span class="noto-date-notif"><i class="fa fa-clock-o"></i> ' + getDate(rep.time.date, notif.dateCreation.date) + '</span>' +
                            '</div>' +
                            '<span class="noto-img-notif-right">' +
                            '<a href="' + url_show_me + '">' +
                            img_user +
                            '</a>' +
                            '</span>' +
                            '</div>' +
                            '</div>';
                    $(".notification-block").append(html);
                } else if (rep.data[i].type == 'notifSubscribers') {
                    var notif = rep.data[i];
                    var show_followers = Routing.generate('show_followers', {_locale: locale, id: current});
                    var subscribers = (notif.newSubscribers == 1) ? new_subscriber : new_subscribers;
                    var html = '<div class="noto-entry" id="block-notif-' + notif.id + '">' +
                            '<span class="close-notif" onClick="closeNotif(\'' + notif.id + '\')"><a><i class="fa fa-times" aria-hidden="true"></i></a></span>' +
                            '<div class="noto-content clearfix">' +
                            '<div class="noto-message-notif notif-followers">' +
                            you_have + ' ' + notif.newSubscribers + ' <a href="' + show_followers + '">' + subscribers + '</a> !' +
                            '<span class="noto-date-notif"><i class="fa fa-clock-o"></i> ' + getDate(rep.time.date, notif.dateCreation.date) + '</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    $(".notification-block").append(html);
                } else if (rep.data[i].type == 'notifFriendRequest') {
                    var notif = rep.data[i];
                    var show_friends = Routing.generate('show_friends', {_locale: locale, id: current});
                    var new_f = (notif.notifFriendRequest == 1) ? new_friend_request : new_friend_requests;
                    var f_request = (notif.notifFriendRequest == 1) ? friend_request : friend_requests;
                    var html = '<div class="noto-entry" id="block-notif-' + notif.id + '">' +
                            '<span class="close-notif" onClick="closeNotif(\'' + notif.id + '\')"><a><i class="fa fa-times" aria-hidden="true"></i></a></span>' +
                            '<div class="noto-content clearfix">' +
                            '<div class="noto-message-notif notif-followers">' +
                            you_have + ' ' + notif.newFriendRequest + ' ' + new_f + ' <a href="' + show_friends + '">' + f_request + '</a>' +
                            '<span class="noto-date-notif"><i class="fa fa-clock-o"></i> ' + getDate(rep.time.date, notif.dateCreation.date) + '</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    $(".notification-block").append(html);
                }

            }

            $(".notification-count").html((count <= 99) ? count : '99+');
        }
    });

};

getDate = function (begin, end) {

    var d1 = new Date(begin.substring(0, 19));
    var d2 = new Date(end.substring(0, 19));

    var timeDiff = Math.abs(d1.getTime() - d2.getTime());
    var diffMinutes = Math.ceil(timeDiff / (1000 * 60));

    if (diffMinutes < 60) {
        return 'il y a ' + diffMinutes + ' minutes';
    } else if (diffMinutes >= 60 && diffMinutes < 119) {
        return 'il y a plus d\'une heure';
    } else if (diffMinutes >= 120 && diffMinutes < 1439) {
        return 'il y a ' + Math.trunc(diffMinutes / 60) + ' heures';
    } else if (diffMinutes >= 1440 && diffMinutes < 2879) {
        return 'il y a 1 jours';
    } else {
        return 'il y a ' + Math.trunc(diffMinutes / 60 / 24) + ' jours';
    }

};

closeNotif = function (id) {

    $.ajax({
        url: Routing.generate('remove_notification', {_locale: locale, id: id}),
        method: 'POST',
        dataType: 'JSON',
        success: function (rep) {
            $('#block-notif-' + id).fadeOut(700, function () {
                $(".notification-count").html(($(".notification-count").html() - 1));
                $('#block-notif-' + id).remove();
            });
        }
    });
};
