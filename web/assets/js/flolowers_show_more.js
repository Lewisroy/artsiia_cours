$('.show-more-followers').on('click', function (e) {
    var length = $('.data-length').data('length');
    $.ajax({
        url: Routing.generate('show_more_followers', {_locale: locale}),
        method: 'POST',
        data: {
            id: user_id,
            first: length + 1
        },
        dataType: 'JSON',
        success: function (rep) {
            var wrapper = $('#container-mix-followers');
            if (rep.length != 0) {
                for (var i = 0; i < rep.length; i++) {
                    var user = rep[i];
                    var category = '';
                    var category_length = user.category.length;
                    for (var j = 0; j < user.category.length; j++) {
                        var cat = user.category[j];
                        var url_category_show = Routing.generate('category_show', {_locale: locale, id: cat.id});
                        if (j < category_length - 1) {
                            var comma = ', ';
                        } else {
                            var comma = '';
                        }
                        var c = '<a href="' + url_category_show + '">' + cat.name + comma + '</a>';
                        category += c;
                    }

                    var url_show_me = Routing.generate('show_me', {_locale: locale, id: user.id});
                    if (user.picture != null) {
                        var img = '<img src="' + uploads_user_picture + user.id + '/' + user.picture + '" alt="">';
                    } else {
                        var img = '<img src="' + default_img + '" alt="" >';
                    }

                    var fieldHTML = '<div class="custom-column-5">' +
                            '<div class="be-user-block style-2" >' +
                            '<a class="be-ava-user style-2" href="' + url_show_me + '">' +
                            img +
                            '</a>' +
                            '<div class="be-user-counter">' +
                            '<div class="c_number">' + user.followers.length + '</div>' +
                            '<div class="c_text">' + subscribers + '</div>' +
                            '</div>' +
                            '<a href="' + url_show_me + '" class="be-use-name">' + user.username + '</a>' +
                            '<div class="be-text-tags">' +
                            category +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    $(wrapper).append(fieldHTML);
                }
                $('#data-length').html('<input type="hidden" class="data-length" data-length="' + (length + rep.length) + '"/>');
            } else {
                $('.show-more-followers').addClass('disabled');
                $('.show-more-followers').html(empty_users);
            }
        }
    });
});