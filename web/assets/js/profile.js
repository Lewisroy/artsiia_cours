//swiper arrows
$(document).on('click', '.ajax_unsubscribe', function(){
    unsubscribeUser();
});

$(document).on('click', '.ajax_subscribe', function(){
    subscribeUser();
});

$(document).on('click', '.ajax_retrieve_friend', function(){
    retrieveFriend();
});

$(document).on('click', '.ajax_retrieve_asking', function(){
    retrieveAskingFriend();
});

$(document).on('click', '.ajax_ask_friend', function(){
    askingFriend();
});





function subscribeUser() {
	var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
	var request = $.ajax({
        data: datas,
        url: url_subscribe,
        type: "POST",          
    });

    request.done(function(msg) {
        $( ".ajax_subscribe" ).replaceWith( '<a class="btn color-1 size-2 hover-1 ajax_unsubscribe" style="margin:2px 2px;">'+unsubscribe_text+'</a>' );
        return true;
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}

function unsubscribeUser() {
    var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url_unsubscribe,
        type: "POST",          
    });

    request.done(function(msg) {
        $( ".ajax_unsubscribe" ).replaceWith( '<a class="btn color-1 size-2 hover-1 ajax_subscribe"style="margin:2px 2px;">'+subscribe_text+'</a>' );
        return true;
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}


function askingFriend() {
    var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url_askFriend,
        type: "POST",          
    });

    request.done(function(msg) {
        $( ".ajax_ask_friend" ).replaceWith( '<a class="btn color-1 size-2 hover-1 ajax_retrieve_asking" style="margin:2px 2px;">'+pending_text+'</a>' );
        return true;
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}

function retrieveAskingFriend() {
    var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url_retrieveAskingFriend,
        type: "POST",          
    });

    request.done(function(msg) {
        $( ".ajax_retrieve_asking" ).replaceWith( '<a class="btn color-1 size-2 hover-1 ajax_ask_friend" style="margin:2px 2px;">'+askFriend_text+'</a>' );
        return true;
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}

function retrieveFriend() {
    var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url_retrieveFriend,
        type: "POST",          
    });

    request.done(function(msg) {
        $( ".ajax_retrieve_friend" ).replaceWith( '<a class="btn color-1 size-2 hover-1 ajax_ask_friend" style="margin:2px 2px;">'+askFriend_text+'</a>' );
        return true;
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}

function acceptFriend(user_id) {
    var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url_acceptFriend,
        type: "POST",          
    });

    request.done(function(msg) {
       $( ".ajax_info_action" ).text( accepted );
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}



function refuseFriend(user_id) {
    var datas = {};
    if (typeof user_id !== 'undefined') {
        datas['user_id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url_refuseFriend,
        type: "POST",          
    });

    request.done(function(msg) {
        $( ".ajax_info_action" ).text( refused );
        
        });

    request.fail(function(jqXHR, textStatus) {
        alert(error);
    });
}