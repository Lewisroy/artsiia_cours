$(".menu-artwork").on("click", function () {
    $(".cover-block").css('display', 'none');
    $(".settings-block").css('display', 'none');
    $(".artwork-block").css("display", "block").fadeIn('fast');
});

$(".menu-cover").on("click", function () {
    $(".cover-block").css("display", "block").fadeIn('fast');
    $(".settings-block").css('display', 'none');
    $(".artwork-block").css('display', 'none');
});

$(".menu-settings").on("click", function () {
    $(".cover-block").css('display', 'none');
    $(".settings-block").css("display", "block").fadeIn('fast');
    $(".artwork-block").css('display', 'none');
});

$("#visual_work_button").on("click", function () {
    $(".literary_work_block").css('display', 'none');
    $(".visual_work_block").css("display", "block").fadeIn('fast');
});

$("#literary_work_button").on("click", function () {
    $(".visual_work_block").css('display', 'none');
    $(".literary_work_block").css("display", "block").fadeIn('fast');
});

$("#artworkbundle_artwork_cover").on("change", function () {
    $("#alert_miniature_add").text(file_download)
});

$("#artworkbundle_artwork_file").on("change", function () {
    $("#alert_artwork_add").text(file_download)
});

$(document).ready(function () {

    $('textarea.texteditor2').ckeditor(
            {
                toolbarGroups: [
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                    {name: 'links'}
                ]
            }
    );
});
$('.followA').on("click", function (event) {
    event.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        type: 'post',

        success: function (response) {
            location.reload(true);

        }
    });
});