function printArticlesUser() {
    var datas = {};
    datas['start'] = positionArticles;
    datas['number'] = numberArticles;

    var request = $.ajax({
        data: datas,
        url: url,
        type: "POST",
    });

    request.done(function (msg) {
        console.log(msg);
        for (var i = 0; i < msg.length; i++) {
            var Articles = '<div class="gallery-box-2 clearfix"><div class="gallery-info"><h3>' + msg[i]["name"] + '</h3><p>' + msg[i]["text"] + ' <br />' + msg[i]["url"] + '</p></div><div class="gallery-btn"><span class="color-icon-1"><i class="fa fa-thumbs-o-up"></i> ' + msg[i]["likers"] + ' </span><br /><span class="color-icon-1"><i class="fa fa-eye"></i> ' + msg[i]["viewers"] + ' </span><br /><span class="color-icon-1"><i class="fa fa-comment-o "></i> ' + msg[i]["comments"] + ' </span><br /></div></div>';
            $(".Articles").append(Articles);
        }
        if (msg.length < 24)
            $(".drm").hide();
        else {
            positionArticles = positionArticles + 24;
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function like(id_article) {
    var datas = {'id': id_article};

    var request = $.ajax({
        url: url_vote,
        data: datas,
        type: "POST",
    });
    console.log('like');
    request.done(function (msg) {
        if (msg.plus_one == true) {
            var value = parseInt($("#likers").text(), 10) + 1;
            $("#likers").text(value);
            $(".button_like").html(' <label class="be-user-info" style=";color:green;font-size:11px;"><i class="fa fa-check" aria-hidden="true"></i>' + already_like + ' </label>');
        }

        if (msg.minus_one == true) {
            var value = parseInt($("#likers").text(), 10) - 1;
            $("#likers").text(value);

        }

    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function ajaxBuyArticle(id_article){
    var datas = {'id': id_article};

    if (confirm("Voulez-vous acheter cet article ?")){
        $.ajax({
            method: "POST",
            url: "buy",
            data: datas,
            success: function(resultat, statut, erreur){
                alert(resultat.transfert.ResultMessage);
            },
            error: function(resultat, statut, erreur){

            }
        });
    }
}