/*
 * Seif v1.0
 * This is an overwrite to the bootbox and blockui js plugins.
 */
/*
 Author     : Seif
 */

(function ($) {
    $.extend({
        alert: function (parameters) {
            bootbox.dialog({
                message: parameters.message,
                title: "Ouups!",
                buttons: {
                    dismiss: {
                        label: "Ok",
                        className: "btn-sm btn-primary"
                    }
                },
                onEscape: function () {
                    $.hideAll();
                }
            });
        },
        serverError: function () {
            $.hideAll();
            $.unblockPage();
            bootbox.dialog({
                message: "Une erreur est survenue. Veuillez contacter votre administrateur système. (Code: #500)",
                title: "Ouups!",
                buttons: {
                    dismiss: {
                        label: "Ok",
                        className: "btn-sm btn-primary"
                    }
                },
                onEscape: function () {
                    $.hideAll();
                }
            });
        },
        confirm: function (parameters, callback) {
            bootbox.dialog({
                message: parameters.message,
                title: parameters.title,
                buttons: {
                    success: {
                        label: "Oui",
                        className: "btn-sm btn-default",
                        callback: function () {
                            callback(true);
                        }
                    },
                    danger: {
                        label: "Non",
                        className: "btn-sm btn-primary",
                        callback: function () {
                            callback(false);
                        }
                    }
                },
                onEscape: function () {
                    $.hideAll();
                }
            });
        },
        prompt: function (parameters, callback, def) {
            var def_form = parameters.label + ': <input id="popup-btn" type="text" />';
            if (typeof (def) == 'undefined') {
                def = true;
            }
            if (def == true) {
                bootbox.dialog({
                    message: def_form,
                    title: parameters.title,
                    buttons: {
                        success: {
                            label: "Ok",
                            className: "btn-sm btn-default",
                            callback: function () {
                                callback($('#popup-btn').val());
                            }
                        }
                    },
                    onEscape: function () {
                        $.hideAll();
                    }
                })
            } else if (def == false) {
                bootbox.dialog({
                    message: parameters.form,
                    title: parameters.title,
                    buttons: parameters.buttons,
                    onEscape: function () {
                        $.hideAll();
                    }
                })
            }
        },
        dialog: function (parameters) {
            bootbox.dialog({
                message: parameters.message,
                title: parameters.title,
                className: parameters.className,
                buttons: parameters.buttons,
                onEscape: function () {
                    $.hideAll();
                }
            });
        },
        hideAll: function () {
            bootbox.hideAll();
        },
        blockPage: function () {
            $.blockUI({message: ' '});
        },
        unblockPage: function () {
            $.unblockUI();
        },
        multiPress: function (keys, handler) {
            'use strict';
            if (keys.length === 0) {
                return;
            }

            var down = {};
            $(document).keydown(function (event) {
                down[event.keyCode] = true;
            }).keyup(function (event) {
                // Copy keys array, build array of pressed keys
                var remaining = keys.slice(0),
                        pressed = Object.keys(down).map(function (num) {
                    return parseInt(num, 10);
                }),
                        indexOfKey;
                // Remove pressedKeys from remainingKeys
                $.each(pressed, function (i, key) {
                    if (down[key] === true) {
                        down[key] = false;
                        indexOfKey = remaining.indexOf(key);
                        if (indexOfKey > -1) {
                            remaining.splice(indexOfKey, 1);
                        }
                    }
                });
                // If we hit all the keys, fire off handler
                if (remaining.length === 0) {
                    handler(event);
                }
            });
        }
    })
})(jQuery);


var Reporting = function (parameters) {
    var _params = parameters;
    var objects = {
        form: {
            _submit_add_report_artwork: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            },
            _submit_remove_report_artwork: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'DELETE',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            bootbox.hideAll();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        }
                    }
                });
            },
            /*********************************************************/
            _submit_add_report_profil: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            },
            /*********************************************************/
            _submit_add_report_comment: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            },
            _submit_remove_report_comment: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'DELETE',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            bootbox.hideAll();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        }
                    }
                });
            },
            /*********************************************************/
            _submit_remove_report_activity: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'DELETE',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            bootbox.hideAll();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        }
                    }
                });
            },
            /*********************************************************/
            _submit_add_banish_profile: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            bootbox.hideAll();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        }
                    }
                });
            },
            _submit_stop_banish_profile: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            bootbox.hideAll();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        }
                    }
                });
            },
            /*********************************************************/
            _submit_remove_my_artwork: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.href = Routing.generate('au_homepage', {_locale: 'fr'});
                        }
                    }
                });
            },
            _submit_remove_my_lecture: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.href = Routing.generate('lecture_all', {_locale: 'fr'});
                        }
                    }
                });
            },
            /*********************************************************/
            _submit_add_role_user: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            },
            _submit_remove_role_user: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            },
            _bind: function () {
                $(document).on('submit', '.add-report-artwork-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_add_report_artwork($(this));
                });
                $(document).on('submit', '.remove-report-artwork-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_remove_report_artwork($(this));
                });
                /*********************************************************/
                $(document).on('submit', '.add-report-profil-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_add_report_profil($(this));
                });
                /*********************************************************/
                $(document).on('submit', '.add-report-comment-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_add_report_comment($(this));
                });
                $(document).on('submit', '.remove-report-comment-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_remove_report_comment($(this));
                });
                /*********************************************************/
                $(document).on('submit', '.remove-report-activity-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_remove_report_activity($(this));
                });
                /*********************************************************/
                $(document).on('submit', '.add-banish-profile-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_add_banish_profile($(this));
                });
                $(document).on('submit', '.stop-banish-profile-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_stop_banish_profile($(this));
                });
                /*********************************************************/
                $(document).on('submit', '.remove-my-artwork-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_remove_my_artwork($(this));
                });
                $(document).on('submit', '.remove-my-lecture-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_remove_my_lecture($(this));
                });
                /*********************************************************/
                $(document).on('submit', '.add-role-user-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_add_role_user($(this));
                });
                $(document).on('submit', '.remove-role-user-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_remove_role_user($(this));
                });
            },
            _init: function () {
                objects.form._bind();
            }
        },
        popup: {
            _add_report_artwork: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_add_reporting_artwork', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.add-report-artwork-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-report-name').focus();
                        }, 500);
                    }
                });
            },
            _remove_report_artwork: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_remove_reporting_artwork', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        if (!response.status) {
                            $('.alert-warning').hide();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        } else {
                            $.dialog({
                                message: response.content,
                                title: response.title,
                                buttons: {
                                    success: {
                                        label: response.button,
                                        className: "btn-input color-1 size-1 hover-1",
                                        callback: function () {
                                            $('.remove-report-artwork-form').submit();
                                            return false;
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            },
            _show_report_messages: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_show_messages', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        var m = document.getElementById('block-message-' + id).classList;
                        m.remove("message-not-read");
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: " color-1 size-1 hover-1",
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-report-name').focus();
                        }, 500);
                    }
                });
            },
            _send_report_message: function (id, locale, sender, receiver, message) {
                $.ajax({
                    url: Routing.generate('reprot_routes_send_report_message', {_locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        'id': id,
                        'locale': locale,
                        'sender': sender,
                        'receiver': receiver,
                        'message': message
                    },
                    success: function (response) {
                        Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                    }
                });
            },
            _close_report: function (id, locale) {
                $.ajax({
                    url: Routing.generate('reprot_routes_close_report', {_locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        'id': id,
                    },
                    success: function (response) {
                        Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                    }
                });
            },
            _mark_report: function (id, locale) {
                $.ajax({
                    url: Routing.generate('reprot_routes_mark_report', {_locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        'id': id,
                    },
                    success: function (response) {
                        Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                    }
                });
            },
            /*********************************************************/
            _add_report_profil: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_add_reporting_profil', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.add-report-profil-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-report-name').focus();
                        }, 500);
                    }
                });
            },
            /*********************************************************/
            _add_report_comment: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_add_reporting_comment', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.add-report-comment-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-report-name').focus();
                        }, 500);
                    }
                });
            },
            _remove_report_comment: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_remove_reporting_comment', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        if (!response.status) {
                            $('.alert-warning').hide();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        } else {
                            $.dialog({
                                message: response.content,
                                title: response.title,
                                buttons: {
                                    success: {
                                        label: response.button,
                                        className: "btn-input color-1 size-1 hover-1",
                                        callback: function () {
                                            $('.remove-report-comment-form').submit();
                                            return false;
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            },
            /*********************************************************/
            _remove_report_activity: function (id, locale) {
                $.ajax({
                    url: Routing.generate('report_routes_remove_reporting_activity', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        if (!response.status) {
                            $('.alert-warning').hide();
                            Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                        } else {
                            $.dialog({
                                message: response.content,
                                title: response.title,
                                buttons: {
                                    success: {
                                        label: response.button,
                                        className: "btn-input color-1 size-1 hover-1",
                                        callback: function () {
                                            $('.remove-report-activity-form').submit();
                                            return false;
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            },
            /*********************************************************/
            _add_banish_profil: function (id, days, locale) {
                $.ajax({
                    url: Routing.generate('banishment_routes_add_banish_report', {id: id, days: days, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.add-banish-profile-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-banish-name').focus();
                        }, 500);
                    }
                });
            },
            _stop_banish_profil: function (id, days, locale) {
                $.ajax({
                    url: Routing.generate('banishment_routes_stop_banish_report', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.stop-banish-profile-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-banish-name').focus();
                        }, 500);
                    }
                });
            },
            /*********************************************************/
            _remove_my_artwork: function (id, locale) {
                $.ajax({
                    url: Routing.generate('remove_my_artwork', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.remove-my-artwork-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-artwork-name').focus();
                        }, 500);
                    }
                });
            },
            _remove_my_lecture: function (id, locale) {
                $.ajax({
                    url: Routing.generate('remove_my_lecture', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.remove-my-lecture-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-artwork-name').focus();
                        }, 500);
                    }
                });
            },
            /*********************************************************/
            _add_role_user: function (id, locale) {
                $.ajax({
                    url: Routing.generate('admin_roles_management_add_role', {id: id, _locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.add-role-user-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-role-name').focus();
                        }, 500);
                    }
                });
            },
            _remove_role_user: function (id, locale) {
                $.ajax({
                    url: Routing.generate('admin_roles_management_remove_role', {id: id, _locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        $.dialog({
                            message: response.content,
                            title: response.title,
                            buttons: {
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.remove-role-user-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-role-name').focus();
                        }, 500);
                    }
                });
            },
            /*********************************************************/
            _artwork_like_manager: function (id, locale) {
                $.ajax({
                    url: Routing.generate('artwork_like', {id: id, _locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        $('.artwork-like-manager').removeClass(response.removeClass);
                        $('.artwork-like-manager').addClass(response.addClass);
                        $('.artwork-like-manager').html(response.follow);
                        $('.like-manager-count').html(response.followCount);
                    }
                });
            },
            _bind: function () {
                $('.add-report-artwork').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._add_report_artwork(id, locale);
                });
                $('.remove-report-artwork').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._remove_report_artwork(id, locale);
                });
                $('.show-report-messages').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._show_report_messages(id, locale);
                });
                $('.send-report-message').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    var sender = $(this).data('sender');
                    var receiver = $(this).data('receiver');
                    var message = $('#message-' + id).val();
                    if (message != '' && message != null) {
                        objects.popup._send_report_message(id, locale, sender, receiver, message);
                        $('#message-' + id).val('');
                    } else {
                        $('.alert-warning').hide();
                        if (locale == 'fr')
                            Notify('Veuillez taper un message d\'abord !', null, null, 'warning');
                        else
                            Notify('Please enter a message first !', null, null, 'warning');
                    }
                });
                $('.close-report').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._close_report(id, locale);
                });
                $('.mark-report').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._mark_report(id, locale);
                });
                /*********************************************************/
                $('.add-report-profil').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._add_report_profil(id, locale);
                });
                /*********************************************************/
                $('.add-report-comment').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._add_report_comment(id, locale);
                });
                $('.remove-report-comment').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._remove_report_comment(id, locale);
                });
                /*********************************************************/
                $('.remove-report-activity').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._remove_report_activity(id, locale);
                });
                /*********************************************************/
                $('.add-banish-profil').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    var days = $('#banish-input-' + id).val();
                    objects.popup._add_banish_profil(id, days, locale);
                });
                $('.stop-banish-profil').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._stop_banish_profil(id, locale);
                });
                /*********************************************************/
                $('.remove-my-artwork').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._remove_my_artwork(id, locale);
                });
                $('.remove-my-lecture').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._remove_my_lecture(id, locale);
                });
                /*********************************************************/
                $('.add-role-user').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._add_role_user(id, locale);
                });
                $('.remove-role-user').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._remove_role_user(id, locale);
                });
                /*********************************************************/
                $('.artwork-like-manager').on('click', function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var locale = $(this).data('locale');
                    objects.popup._artwork_like_manager(id, locale);
                });
            },
            _init: function () {
                objects.popup._bind();
            }
        }
    };
    return {
        init: function () {
            objects.form._init();
            objects.popup._init();
        }
    };
};

function reportActivity(id, locale) {
    $.ajax({
        url: Routing.generate('report_routes_add_reporting_activity', {id: id, _locale: locale}),
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            $.dialog({
                message: response.content,
                title: response.title,
                buttons: {
                    success: {
                        label: response.button,
                        className: "full btn-input color-1 size-1 hover-1",
                        callback: function () {
                            var form = $('.add-report-activity-form');
                            $.ajax({
                                url: form.attr('action'),
                                type: 'POST',
                                data: form.serialize(),
                                success: function (response) {
                                    if (response.error == true) {
                                        $('.bootbox-body').html(response.content);
                                    } else {
                                        window.location.reload();
                                    }
                                }
                            });
                            return false;
                        }
                    }
                }
            });
        },
        complete: function () {
            setTimeout(function () {
                $('.input-report-name').focus();
            }, 500);
        }
    });
}

function removeMyActivity(id, locale) {
    $.ajax({
        url: Routing.generate('remove_my_activity', {id: id, _locale: locale}),
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            $.dialog({
                message: response.content,
                title: response.title,
                buttons: {
                    success: {
                        label: response.button,
                        className: "full btn-input color-1 size-1 hover-1",
                        callback: function () {
                            var form = $('.remove-my-activity-form');
                            $.ajax({
                                url: form.attr('action'),
                                type: 'POST',
                                data: form.serialize(),
                                success: function (response) {
                                    if (response.error == true) {
                                        $('.bootbox-body').html(response.content);
                                    } else {
                                        $("#block-activitie-" + response.id).remove();
                                        bootbox.hideAll();
                                        Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                                    }
                                }
                            });
                            return false;
                        }
                    }
                }
            });
        },
        complete: function () {
            setTimeout(function () {
                $('.input-activity-name').focus();
            }, 500);
        }
    });
}

function editMyActivity(id, locale) {
    $.ajax({
        url: Routing.generate('edit_my_activity', {id: id, _locale: locale}),
        type: 'POST',
        dataType: 'JSON',
        success: function (response) {
            $.dialog({
                message: response.content,
                title: response.title,
                buttons: {
                    success: {
                        label: response.button,
                        className: "full btn-input color-1 size-1 hover-1",
                        callback: function () {
                            updateAllMessageForms();
                            var form = $('.edit-my-activity-form');
                            $.ajax({
                                url: form.attr('action'),
                                type: 'POST',
                                data: form.serialize(),
                                success: function (response) {
                                    if (response.error == false) {
                                        $("#block-content-" + response.id).html(response.content);
                                        bootbox.hideAll();
                                        Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                                    } else {
                                        $('.bootbox-body').html(response.content);
                                    }
                                }
                            });
                            return false;
                        }
                    }
                }
            });
        },
        complete: function () {
            setTimeout(function () {
                $('.input-activity-name').focus();
            }, 500);
        }
    });
}

function addComment(id, locale) {
    var message = $('#comment-to-' + id).val();
    if (message != '' && message != null) {
        $.ajax({
            url: Routing.generate('add_comment_activity', {_locale: locale}),
            type: 'POST',
            dataType: 'JSON',
            data: {
                'id': id,
                'locale': locale,
                'message': message
            },
            success: function (response) {
                $('#comment-to-' + id).val('');
                $('#comment-to-' + id).height('auto');
                var comment = '<div class="row">' +
                        '<div class="block-comment">' +
                        '<div class="username-comment">' +
                        '<a href="' + response.url_user + '">' + response.username + '</a>' +
                        '<span class="timestampContent"> ' + response.date + '</span>' +
                        '</div>' +
                        '<div class="content-comment break-word">' + response.comment + '</div>' +
                        '</div>' +
                        '</div>';
                $(comment).insertBefore('#list-comments-' + id);
                var length = $('#data-comments-' + id).data('comments');
                $('.data-comments-' + id).html('<input id="data-comments-' + id + '" type="hidden" data-comments="' + (length + 1) + '">');
                $('#fa-' + id).removeClass('fa-comment-o');
                $('#fa-' + id).addClass('fa-comment color-icon-2');
                var comments = $('#comment_' + id).data('comments');
                $('#count-comments-' + id).html('<span data-comments="' + (comments + 1) + '" id="comment_' + id + '"> ' + (comments + 1) + '</span>');
                Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
            }
        });
    } else {
        $('.alert-warning').hide();
        if (locale == 'fr')
            Notify('Veuillez taper un commentaire d\'abord !', null, null, 'warning');
        else
            Notify('Please enter a comment first !', null, null, 'warning');
    }
}

function showMoreComment(id, locale) {
    var length = $('#data-comments-' + id).data('comments');
    $.ajax({
        url: Routing.generate('show_more_comments', {_locale: locale}),
        method: 'POST',
        data: {
            first: length,
            id: id
        },
        dataType: 'JSON',
        success: function (rep) {
            var wrapper = $('#list-comments-' + id);
            if (rep.length != 0) {
                var monthNamesEN = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
                var monthNamesFR = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
                for (var i = 0; i < rep.length; i++) {
                    var response = rep[i];
                    var date = (response.comment.date.date).toString();
                    if (locale == 'en') {
                        var month = monthNamesEN[date.substring(5, 7) - 1];
                    } else {
                        var month = monthNamesFR[date.substring(5, 7) - 1];
                    }
                    var parsedDate = parseInt(date.substring(8, 10)) + ' ' + month + ' ' + date.substring(0, 4) + ', ' + date.substring(11, 16);
                    var comment = '<div class="block-comment">' +
                            '<div class="username-comment">' +
                            '<a href="' + response.user_id + '">' + response.user_name + '</a>' +
                            '<span class="timestampContent"> ' + parsedDate + '</span>' +
                            '</div>' +
                            '<div class="content-comment">' + response.comment.content + '</div>' +
                            '</div>';
                    $(wrapper).append(comment);
                }
                $('.data-comments-' + id).html('<input id="data-comments-' + id + '" type="hidden" data-comments="' + (length + rep.length) + '">');
                $('.less-comments-' + id).html('<a onClick="showLessComment(\'' + id + '\')" class="show-less-comment">' + show_less_comment + '</a>');
            } else {
                $('.show-more-comment-' + id).html('');
            }
        }
    });
}

function showLessComment(id) {
    var length = $('#data-comments-' + id).data('comments');
    for (var i = 0; i < ((length >= 3) ? length - 3 : 0); i++) {
        $('#list-comments-' + id + ' .block-comment').last().remove();
        var new_length = $('#data-comments-' + id).data('comments');
        $('.data-comments-' + id).html('<input id="data-comments-' + id + '" type="hidden" data-comments="' + (new_length - 1) + '">');
    }
    $('.less-comments-' + id).html('');
    $('.show-more-comment-' + id).html('<a onClick="showMoreComment(\'' + id + '\',\'' + locale + '\')" class="show-more-comment">' + show_more_comment + '</a>');

}

function updateAllMessageForms() {
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
}