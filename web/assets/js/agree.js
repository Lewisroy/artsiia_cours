var Agree = function (parameters) {
    var _params = parameters;
    var objects = {
        form: {
            _submit_update_agree_policy: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            },
            _submit_update_agree_disclaimer: function (form) {
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    data: form.serialize(),
                    success: function (response) {
                        if (response.error == true) {
                            $('.bootbox-body').html(response.content);
                        } else {
                            window.location.reload();
                        }
                    }
                });
            }, 
            _bind: function () {                
                $(document).on('submit', '.update-agree-policy-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_update_agree_policy($(this));
                });                
                $(document).on('submit', '.update-agree-disclaimer-form', function (e) {
                    e.preventDefault();
                    objects.form._submit_update_agree_disclaimer($(this));
                });                
            },
            _init: function () {
                objects.form._bind();
            }
        },
        popup: {                        
            _update_agree_policy: function (locale) {
                $.ajax({
                    url: Routing.generate('agree_policy', {_locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        bootbox.dialog({
                            message: response.content,
                            title: response.title,
                            closeButton: false,
                            buttons: {                                
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.update-agree-policy-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-agree-name').focus();
                        }, 500);
                    }
                });
            },
            _update_agree_disclaimer: function (locale) {
                $.ajax({
                    url: Routing.generate('agree_disclaimer', {_locale: locale}),
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (response) {
                        bootbox.dialog({
                            message: response.content,
                            title: response.title,
                            closeButton: false,
                            buttons: {                                
                                success: {
                                    label: response.button,
                                    className: "full btn-input color-1 size-1 hover-1",
                                    callback: function () {
                                        $('.update-agree-disclaimer-form').submit();
                                        return false;
                                    }
                                }
                            }
                        });
                    },
                    complete: function () {
                        setTimeout(function () {
                            $('.input-agree-name').focus();
                        }, 500);
                    }
                });
            },
            _bind: function () {                
                jQuery(window).on('load', function (e) {
                    e.preventDefault();
                    var policy = $('.agree-policy').data('policy');
                    var disclaimer = $('.agree-disclaimer').data('disclaimer');
                    var locale = $('.agree-policy').data('locale');
                    if(policy == true && disclaimer == true){
                        disclaimer = false;
                    }
                    if(policy == true){
                        objects.popup._update_agree_policy(locale);
                    }
                    if(disclaimer == true){
                        objects.popup._update_agree_disclaimer(locale);
                    }
                });
            },
            _init: function () {
                objects.popup._bind();
            }
        }
    };
    return {
        init: function () {
            objects.form._init();
            objects.popup._init();
        }
    };
};
