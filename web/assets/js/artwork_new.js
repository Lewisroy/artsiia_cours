$(function () {
    $(".widthPreview").mnFileInput({
        'preview': '.preview'
    });
    $(".widthPreviewCover").mnFileInput({
        'preview': '.previewCover'
    });
});
$(function () {
    $(".widthPreview").change(function () {
        var fileName = $(this).val();
        if (fileName) {
            $('.removeFile').removeClass('hidden-block');
        } else {
            $('.removeFile').addClass('hidden-block');
            $('.nameFileWidthPreview').html(no_file);
            $('.previewContainerWidthPreview').html('<img class="preview" src="" alt="" />');
        }
    });
    $(".removeFile").on('click', function () {
        $('.widthPreview').val('');
        $('.removeFile').addClass('hidden-block');
        $('.nameFileWidthPreview').html(no_file);
        $('.previewContainerWidthPreview').html('<img class="preview" src="" alt="" />');
    });
    $(".widthPreviewCover").change(function () {
        var fileName = $(this).val();
        if (fileName) {
            $('.removeCover').removeClass('hidden-block');
        } else {
            $('.removeCover').addClass('hidden-block');
            $('.nameFileWidthPreviewCover').html(no_file);
            $('.previewContainerCover').html('<img class="previewCover" src="" alt="" />');
        }
    });
    $(".removeCover").on('click', function () {
        $('.widthPreviewCover').val('');
        $('.removeCover').addClass('hidden-block');
        $('.nameFileWidthPreviewCover').html(no_file);
        $('.previewContainerCover').html('<img class="previewCover" src="" alt="" />');
    });
});
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
                curStepLevel = curStep.data('step'),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("#artworkbundle_artwork_title, #artworkbundle_artwork_description"),
                isValid = true;

        $(".form-group").removeClass("has-error");
        if (curStepLevel == 1) {
            var visual = $(".widthPreview").val();
            var literary = CKEDITOR.instances.artworkbundle_artwork_text.getData();
            if (!visual && !literary) {
                if ($('.data_block_type').val() == 'visual_work_block') {
                    $('.alert-warning').hide();
                    Notify(select_file, null, null, 'warning');
                } else {
                    $('.alert-warning').hide();
                    Notify(write_text, null, null, 'warning');
                }
                isValid = false;
            }
        }
        if (curStepLevel == 2) {
            var cover = $(".widthPreviewCover").val();
            if (!cover) {
                $('.alert-warning').hide();
                Notify(select_file, null, null, 'warning');
                isValid = false;
            }
        }
        if (curStepLevel == 3) {
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');

        if (curStepLevel == 3 && isValid)
            $(".wizard-form").submit();
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
$('.literary_work_button').click(function () {
    var visual = $(".widthPreview").val();
    if (visual) {
        bootbox.confirm({
            title: confirm,
            message: confirm_delete_oeuvre_visual,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ' + cancel,
                    className: 'btn btn-default size-3'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ' + confirm,
                    className: 'btn btn-primary size-3'
                }
            },
            callback: function (result) {
                if (result) {
                    $('.widthPreview').val('');
                    $('.removeFile').addClass('hidden-block');
                    $('.nameFileWidthPreview').html(no_file);
                    $('.previewContainerWidthPreview').html('<img class="preview" src="" alt="" />');
                    $('.visual_work_block').addClass('hidden-block');
                    $('.literary_work_block').removeClass('hidden-block');
                    $('.data_block_type').val('literary_work_block');
                } else {
                    bootbox.hideAll();
                    return false;
                }
            }
        });
    } else {
        $('.visual_work_block').addClass('hidden-block');
        $('.literary_work_block').removeClass('hidden-block');
        $('.data_block_type').val('literary_work_block');
    }
});
$('.visual_work_button').click(function () {
    var literary = CKEDITOR.instances.artworkbundle_artwork_text.getData();
    if (literary) {
        bootbox.confirm({
            title: confirm,
            message: confirm_delete_oeuvre_literary,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ' + cancel,
                    className: 'btn btn-default size-3'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ' + confirm,
                    className: 'btn btn-primary size-3'
                }
            },
            callback: function (result) {
                if (result) {
                    CKEDITOR.instances.artworkbundle_artwork_text.setData();
                    $('.literary_work_block').addClass('hidden-block');
                    $('.visual_work_block').removeClass('hidden-block');
                    $('.data_block_type').val('visual_work_block');
                } else {
                    bootbox.hideAll();
                    return false;
                }
            }
        });
    } else {
        $('.literary_work_block').addClass('hidden-block');
        $('.visual_work_block').removeClass('hidden-block');
        $('.data_block_type').val('visual_work_block');
    }
});