$(document).ready(function () {

    $('textarea.texteditor').ckeditor(
            {toolbarGroups: [
                    {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                    {name: 'links'}
                ]}
    );
});


$('.subCommentSubmit').click(function (event) {
    event.preventDefault();
    data = {};
    id = $(this).data('id');
    /*DOMId=textComment+''+id;
     val=value = CKEDITOR.instances[DOMId].getData();
     */
    data['replayData'] = 'zoirjijo';//$('.subComment'+id).text();
    data['lectureId'] = $('.lectureId').val();

    $.ajax({
        url: url,
        type: 'post',
        data: data,
        success: function (response) {
            location.reload(true);
        }
    });
});


msg = msg;
if ($('.userApp').val() != -1) {
    $('#form_author').select2({
        tags: true,
        tokenSeparators: [','],
        placeholder: msg
    });
}

$('#authors').hide();

$('#add_authors_btn').click(function () {
    $('#authors').show();
});
$('.navC').on("click", function () {
    $('.navC').removeClass('enable');
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 700, function () {
        $('.navC').addClass('enable');
    });
    return true;
});
$('.stars').click(function (e) {
    $('.starActive').each(function () {
        $(this).removeClass('starActive');
    });
    note = $(this).attr('id');

    for (i = 1; i <= note; i++)
        $('.star' + i).addClass('starActive');
});
$('.voteSubmit').click(function (e) {
    e.preventDefault();
    $('.loader').show();
    data = {};
    i = 0;
    $('.starActive').each(function () {
        i++;
    });
    data['note'] = i;
    data['lectureId'] = $('.lectureId').val();
    data['userVote'] = $('.userVote').val();
    data['userVoteId'] = $('.userVoteId').val();
    data['userId'] = $('.userApp').val();
    $.ajax({
        url: url2,
        type: 'post',
        data: data,
        success: function (response) {
            location.reload(true);
        }
    });
});