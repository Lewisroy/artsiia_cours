$(".confirm").confirm({
    content: content,
    title: "Validation required",
    container: 'body',
    columnClass: 'col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-4 col-xs-12',
    boxWidth: '100%',
    buttons: {
        close: {
            text: text_close,
            btnClass: 'btn-warning',
            action: function () {
            }
        },
        confirm: {
            text: text_confirm,
            btnClass: 'btn-success',
            action: function () {
                location.href = this.$target.attr('href');
            }
        }
    },
    post: false,
});


$('#form_author').select2({
    tags: true,
    tokenSeparators: [','],
    placeholder: "Add your tags here"
});
$('#authors').hide();

$('#add_authors_btn').click(function () {
    $('#authors').show();
});