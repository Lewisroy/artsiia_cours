function maxlength(id) {
    var settings = {
        events: [], // Array of events to be triggerd
        maxCharacters: 2000, // Characters limit
        status: true, // True to show status indicator bewlow the element
        statusClass: "status-count", // The class on the status div
        statusText: limit_publication_characters_still, // The status text
        notificationClass: "notification-status-count", // Will be added to the emement when maxlength is reached
        hiddenClass: "hidden-block", // Will be added to the emement when maxlength is reached
        showAlert: true, // True to show a regular alert message
        alertText: limit_publication, // Text in the alert message
        slider: true // Use counter slider
    };


    var item = $("#comment-to-" + id);
    var counter = $("#status-count-" + id);
    var charactersLength = item.val().length;
    checkChars();

    // Update the status text
    function updateStatus() {
        var charactersLeft = settings.maxCharacters - charactersLength;

        charactersLeft = charactersLeft < 0 ? 0 : charactersLeft;

        counter.html(charactersLeft + " " + settings.statusText);
    }

    function checkChars() {
        var valid = true;

        // Too many chars?
        if (charactersLength >= settings.maxCharacters) {
            // Too may chars, set the valid boolean to false
            valid = false;
            // Add the notification class when we have too many chars
            item.addClass(settings.notificationClass);
            // Cut down the string
            item.val(item.val().substr(0, settings.maxCharacters));
            // Show the alert dialog box, if its set to true
            showAlert();
        } else if (item.hasClass(settings.notificationClass)) {
            item.removeClass(settings.notificationClass);
        }

        if (settings.status) {
            updateStatus();
        }
    }

    // Shows an alert msg
    function showAlert() {
        if (settings.showAlert) {
            $('.alert-warning').hide();
            Notify(settings.alertText, null, null, 'warning');
        }
    }

    // Check if the element is valid.
    function validateElement() {
        return item.is('textarea') || item.filter("input[type=text]") || item.filter("input[type=password]");
    }

    // Validate
    if (!validateElement()) {
        return false;
    }

    // Insert the status div
    if (settings.status) {
        updateStatus();
    }

    // Remove the status div
    if (!settings.status) {
        var removeThisDiv = item.next("div." + settings.statusClass);

        if (removeThisDiv) {
            removeThisDiv.remove();
        }
    }

    // Slide counter
    if (settings.slider) {
        item.focus(function () {
            counter.slideDown('fast');
        });

        item.blur(function () {
            counter.slideUp('fast');
        });
    }
}
;

