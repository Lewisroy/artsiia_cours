$(".del_article_user").click(function (e) {
    e.preventDefault();

    if (confirm("Voulez vraiment supprimer cette article ?")) {
        var take_id = $(this).attr("id");

        $.ajax({
            type: 'POST',
            url: Routing.generate('delete_article_user', { _locale: locale }),
            data: { id: take_id },
            success: function (res) {
                $(".del_art").append(res);
                $(".del_art").fadeIn('slow');
                setTimeout(function () {
                    $(".del_art").fadeOut('slow');
                    $(".refreshShop").load(location.pathname + " .refreshShop");
                }, 3000);

            },
            error: function (msg, string) {
                $(".error_del_art").append("Erreur de suppression de l'article.");
                $(".error_del_art").fadeIn('slow');
                setTimeout(function () {
                    $(".error_del_art").fadeOut('slow');
                }, 2000);
            }
        })
    }
});

function ajaxBuyArticle(id_article){
    var datas = {'id': id_article};

    if (confirm("Voulez-vous acheter cet article ?")){
        $.ajax({
            method: "POST",
            url: "buy",
            data: datas,
            success: function(resultat, statut, erreur){
                alert(resultat.transfert.ResultMessage);
            },
            error: function(resultat, statut, erreur){

            }
        });
    }
}