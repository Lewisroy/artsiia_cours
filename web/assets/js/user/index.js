if (locale == 'fr' || locale == 'FR') {
    var url = "//cdn.datatables.net/plug-ins/1.10.15/i18n/French.json";
} else {
    var url = "//cdn.datatables.net/plug-ins/1.10.15/i18n/English.json";
}
$(document).ready(function () {
    var table = $('#paginations').DataTable({
        stateSave: true,
        "bJQueryUI": true,
        "bStateSave": true,
        "language": {
            "url": url
        }
    });
    yadcf.init(table, [{
            column_number: 0,
            filter_type: "multi_select",
            select_type: 'select2'
        }, {
            column_number: 1,
            filter_type: "multi_select",
            select_type: 'select2'}, {
            column_number: 2,
            filter_type: "multi_select",
            select_type: 'select2',
            select_type_options: {
                width: '150px',
                minimumResultsForSearch: -1 // remove search box
            }
        }]);
});