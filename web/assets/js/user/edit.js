$(document).ready(function () {
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
    });

    $('.btn-file :file').on('fileselect', function (event, label) {
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
                $("#img-upload").show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#user_picture").change(function () {
        readURL(this);
        $(".preview_picture").show();
    });

    $('.js-datepicker').datepicker({
        format: 'dd-mm-yyyy',
        language: 'fr',
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#user_category').select2();
});
