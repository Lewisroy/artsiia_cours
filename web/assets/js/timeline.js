function printActivitiesUser() {
    var datas = {};
    datas['start'] = positionActivity;
    datas['number'] = numberActivity;
    if (typeof user_id !== 'undefined') {
        datas['id'] = user_id;
    }
    var request = $.ajax({
        data: datas,
        url: url,
        type: "POST",
    });

    request.done(function (msg) {
        for (var i = 0; i < msg.length; i++) {
            var delete_my_activity = '<div class="dropdown float-right">' +
                    '<a class="edit-my-activity"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>' +
                    '<div class="dropdown-content-remove">' +
                    '<a onClick="removeMyActivity(\'' + msg[i]['id'] + '\',\'' + locale + '\')" class="remove-my-activity" data-id="' + msg[i]['id'] + '" data-locale="' + locale + '">' + delete_activity + '</a>' +
                    '</div>' +
                    '</div>';
            var edit_my_activity = '<div class="dropdown">' +
                    '<a class="edit-my-activity"><i class="fa fa-pencil" aria-hidden="true"></i></a>' +
                    '<div class="dropdown-content-edit">' +
                    '<a onClick="editMyActivity(\'' + msg[i]['id'] + '\',\'' + locale + '\')">' + edit + '</a>' +
                    '</div>' +
                    '</div>';
            if (msg[i]['user_id'] == this_user) {
                check = delete_my_activity;
                check_edit = edit_my_activity;
            } else {
                check = '';
                check_edit = '';
            }
            var showChar = 150;
            var ellipsestext = "...";
            var moretext = show_more;
            if ((msg[i]["description"]).length > showChar) {
                var c = (msg[i]["description"]).substr(0, showChar);
                var h = (msg[i]["description"]).substr(showChar, (msg[i]["description"]).length - showChar);

                var description = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a class="cursor-pointer" id="morelink-id-' + msg[i]['id'] + '" onClick="morelink(\'' + msg[i]['id'] + '\')">' + moretext + '</a></span>';
            } else {
                var description = msg[i]["description"];
            }

            if (msg[i]['user_id'] != this_user) {
                var ban = '<a title="' + title_report + '" onClick="reportActivity(\'' + msg[i]['id'] + '\',\'' + locale + '\')" class="add-report-activity" data-id="' + msg[i]['id'] + '" data-locale="' + locale + '"><i class="fa fa-ban" aria-hidden="true"></i> </a>';
            } else {
                var ban = '';
            }
            var url_show_me = Routing.generate('show_me', {_locale: locale, id: msg[i]['user_id']});
            var monthNamesEN = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
            var monthNamesFR = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
            var date = (msg[i].created.date).toString();
            if (locale == 'en') {
                var month = monthNamesEN[date.substring(5, 7) - 1];
            } else {
                var month = monthNamesFR[date.substring(5, 7) - 1];
            }
            var parsedDate = parseInt(date.substring(8, 10)) + ' ' + month + ' ' + date.substring(0, 4) + ', ' + date.substring(11, 16);
            var updated = '';
            if (msg[i]['updated']) {
                updated = ' <i title="' + edited + '" class="fa fa-pencil" aria-hidden="true"></i>';
            }

            var comments = '';
            for (var j = 0; j < msg[i].comments.length; j++) {
                var comment = msg[i].comments[j].comment;
                var url_show_me_comment = Routing.generate('show_me', {_locale: locale, id: msg[i].comments[j].user_id});
                var date_comment = (comment.date.date).toString();
                if (locale == 'en') {
                    var month_comment = monthNamesEN[date_comment.substring(5, 7) - 1];
                } else {
                    var month_comment = monthNamesFR[date_comment.substring(5, 7) - 1];
                }
                var parsedDate_comment = parseInt(date_comment.substring(8, 10)) + ' ' + month_comment + ' ' + date_comment.substring(0, 4) + ', ' + date_comment.substring(11, 16);
//                ***********
                if (comment.content.length > showChar) {
                    var c_sub = comment.content.substr(0, showChar);
                    var h_sub = comment.content.substr(showChar, comment.content.length - showChar);
                    var comment_less =
                            '<span id="block-content-' + comment.id + '">' +
                            c_sub + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span>' +
                            '<span class="morecontent">' +
                            '<span>' + h_sub + '</span>&nbsp;&nbsp;' +
                            '<a class="cursor-pointer" id="morelink-id-' + comment.id + '" onclick="morelink(\'' + comment.id + '\')">' + moretext + '</a>' +
                            '</span>' +
                            '</span>';
                    var sub_comment = c + comment_less;
                } else {
                    var sub_comment = comment.content;
                }
//                **********
                var content_comment = '<div class="block-comment">' +
                        '<div class="username-comment"><a href="' + url_show_me_comment + '">' + msg[i].comments[j].user_name + '</a> ' + ' <span class="timestampContent"> ' + parsedDate_comment + '</span>' + '</div>' +
                        '<div class="content-comment break-word">' + sub_comment + '</div>' +
                        '</div>';
                comments += content_comment;
            }
            var Activities = '<div class="gallery-box-2 clearfix" id="block-activitie-' + msg[i]['id'] + '"><div class="gallery-info no-float"><h3><a href="' + url_show_me + '">' + msg[i]["username"] + '</a>' + check_edit + check + '</h3><div><div class="timestampContent">' + parsedDate + updated + '</div><span class="break-word" id="block-content-' + msg[i]['id'] + '">' + description + '</span></div></div>' +
                    '<div class="gallery-btn block_' + msg[i]['id'] + '">' +
                    '<a href="#block_' + msg[i]['id'] + '" onClick="like(\'' + msg[i]['id'] + '\')"><span class="color-icon-1" id="likers_' + msg[i]['id'] + '">' + msg[i]['like_text'] + '</span></a>' +
                    '<span class="color-icon-1" id="comments_' + msg[i]['id'] + '">' + msg[i]['comment_text'] + '</span>' +
                    ban +
                    '</div>' +
                    '<div class="comment-block center-text-align">' +
                    '<div class="row">' +
                    '<div class="col-md-10 col-xs-9">' +
                    '<textarea maxlength="2000" id="comment-to-' + msg[i]['id'] + '" onkeyup="auto_grow(this); maxlength(\'' + msg[i]['id'] + '\')" class="comment-activity" placeholder="' + add_comments + '..."></textarea>' +
                    '</div>' +
                    '<div class="col-md-2 col-xs-3">' +
                    '<a class="left-margin-30 btn color-1 size-2 hover-1 send-comment-btn" onClick="addComment(\'' + msg[i]['id'] + '\',\'' + locale + '\')">' + send + '</a>' +
                    '</div>' +
                    '<div class="status-count" id="status-count-' + msg[i]['id'] + '"></div>' +
                    '</div>' +
                    '<div class="row" id="list-comments-' + msg[i]['id'] + '">' +
                    comments +
                    '</div>' +
                    '<div id="block-show-more-comment-' + msg[i]['id'] + '" >' +
                    '<span class="data-comments-' + msg[i]['id'] + '"><input id="data-comments-' + msg[i]['id'] + '" type="hidden" data-comments="' + msg[i].comments.length + '"></span><span class="show-more-comment-' + msg[i]['id'] + '"><a onClick="showMoreComment(\'' + msg[i]['id'] + '\',\'' + locale + '\')" class="show-more-comment">' + show_more_comment + '</a></span>' +
                    '<span class="less-comments-' + msg[i]['id'] + '"></span>' +
                    '</div>' +
                    '</div>';

            $(".Activities").append(Activities);
        }
        if (msg.length < numberActivity)
            $(".drm").hide();
        else {
            positionActivity = positionActivity + numberActivity;
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert(global_error + " : " + textStatus);
    });
}

function like(id_post) {

    var datas = {'id': id_post};

    var request = $.ajax({
        url: url_vote,
        data: datas,
        type: "POST",
    });

    request.done(function (msg) {
        if (msg.plus_one == true) {
            var value = parseInt($("#liker_" + id_post).text(), 10) + 1;
            $("#likers_" + id_post).html('<i class="fa fa-thumbs-up color-icon-2"></i> <span id="liker_' + id_post + '">' + value + '</span>');
        }

        if (msg.minus_one == true) {
            var value = parseInt($("#liker_" + id_post).text(), 10) - 1;
            $("#likers_" + id_post).html('<i class="fa fa-thumbs-o-up"></i> <span id="liker_' + id_post + '">' + value + '</span>');

        }

    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}

function morelink(id) {
    $('#morelink-id-' + id).html('');
    $('#morelink-id-' + id).parent().prev().toggle();
    $('#morelink-id-' + id).prev().toggle();
}

function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight) + "px";
}