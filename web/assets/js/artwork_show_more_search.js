$('.show-more-artworks').on('click', function (e) {
    var length = $('.data-length').data('length');
    $.ajax({
        url: Routing.generate('show_more_artworks_search', {_locale: locale}),
        method: 'POST',
        data: {
            first: length,
            tag: $('#tag_filter').val(),
            category: $('#filter-category').val(),
            sort: $('#sort_filter').val(),
            subcategory: $('#sub_category_filter').val(),
            scope: $('#scope_filter').val()
        },
        dataType: 'JSON',
        success: function (rep) {
            var wrapper = $('#container-mix');
            if (rep.length != 0) {
                for (var i = 0; i < rep.length; i++) {
                    var artwork = rep[i];
                    var url_artwork_show = Routing.generate('artwork_show', {_locale: locale, id: artwork.id});
                    var url_show_me = Routing.generate('show_me', {_locale: locale, id: artwork.author[0].id});
                    var url_category_show = Routing.generate('category_show', {_locale: locale, id: artwork.category[0].id});
                    if (artwork.cover != "") {
                        var img = '<img src="' + imagine_filter + artwork.cover + '" alt="' + artwork.title + '">';
                    } else {
                        var img = '<img src="' + default_img + '" alt="' + artwork.title + '">';
                    }
                    var date = (artwork.dateCreation.date).toString();
                    var parsedDate = date.substring(8, 10) + '/' + date.substring(5, 7) + '/' + date.substring(0, 4) + ' ' + date.substring(11, 16).replace(':', 'h');
                    var is_liker = false;
                    var is_comment = false
                    if (user_id) {
                        for (var j = 0; j < artwork.likers.length; j++) {
                            if (user_id == artwork.likers[j]['id'])
                                is_liker = true;
                        }
                        for (var j = 0; j < artwork.comments.length; j++) {
                            if (user_id == artwork.comments[j].user['id'])
                                is_comment = true;
                        }
                    }
                    if (is_liker) {
                        var style_liker = "fa-thumbs-up blue-style-1";
                    } else {
                        var style_liker = "fa-thumbs-o-up";
                    }
                    if (is_comment) {
                        var style_comment = "fa-comment blue-style-1";
                    } else {
                        var style_comment = "fa-comment-o";
                    }
                    var list_sub_category = '';
                    var sub_category_length = artwork.sub_category.length;
                    if (artwork.sub_category.length > 1) {
                        list_sub_category += artwork.sub_category[0].name + ', +' + (artwork.sub_category.length - 1);
                    } else if (artwork.sub_category.length == 1) {
                        list_sub_category += artwork.sub_category[0].name;
                    }
                    var fieldHTML = '<div class="col-ml-12 col-xs-4 col-sm-3">' +
                            '<div class="be-post">' +
                            '<a href="' + url_artwork_show + '" class="be-img-block">' +
                            img +
                            '</a>' +
                            '<a href="' + url_artwork_show + '" class="be-post-title">' + artwork.title + '</a>' +
                            '<span class="center-text-align"><div class="be-text-tags-artwork-date">' +
                            '<a href="' + url_artwork_show + '" class="be-post-tag">' +
                            parsedDate +
                            '</a></div>' +
                            artwork.category[0].name +
                            '<div class="be-text-tags-artwork">' +
                            '<a href="' + url_category_show + '">' + artwork.category[0].name + '</a>' +
                            '</div>' +
                            '<div class="be-text-tags-artwork">' +
                            list_sub_category +
                            '</div>' +
                            '</span>' +
                            '<div class="author-post center-text-align">' +
                            '<span>' + by +
                            ' <a href="' + url_show_me + '">' + artwork.author[0].username + '</a>' +
                            '</span>' +
                            '</div>' +
                            '<div class="info-block">' +
                            '<span class="cursor-auto"><i class="fa ' + style_liker + '"></i>' + artwork.likers.length + '</span>' +
                            '<span class="cursor-auto"><i class="fa fa-eye followers"></i>' + artwork.viewers.length + '</span>' +
                            '<span class="cursor-auto"><i class="fa ' + style_comment + '"></i>' + artwork.comments.length + '</span>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    $(wrapper).append(fieldHTML);
                }
                $('#data-length').html('<input type="hidden" class="data-length" data-length="' + (length + rep.length) + '"/>');
            } else {
                $('.show-more-artworks').addClass('disabled');
                $('.show-more-artworks').html(empty_artworks);
            }
        }
    });

});

$('#filter-category').change(function () {
    if ($('#filter-category').val()) {
        $.ajax({
            url: Routing.generate('list_sub_category', {_locale: locale, id: $('#filter-category').val()}),
            type: 'GET',
            success: function (response) {
                var li = "";
                for (var i = 0; i < response.length; i++) {
                    li += '<option value="' + response[i]['id'] + '">' + response[i]['name'] + '</option>';
                }
                var html = '<div class="col-md-2">' +
                        '<select name="subcategory" class="select-be-drop-down">' +
                        '<option value="">' + subtitle + '</option>' +
                        li +
                        '</select>' +
                        '</div>';
                $('#contents-sub-category').html(html);
            }
        });
    } else {
        $('#contents-sub-category').html('');
    }
});