$(document).ready(function () {

    //Previsualisation image - Page Article New Add
    $('form').find('input[name="shopbundle_article[cover]"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0];
            $image_preview = $('.img_to_send_add');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $("#img_pop").css("width", "60%");
            document.getElementById("img_pop").style.boxShadow = "1px 2px 2px 0px #333";
            $(".delete_img").fadeIn('slow');
            $(".height_form").css("height", "295px");
        }
    });
    //Button delete previsualisation - Page Article New Add
    $('.delete_img').click(function () {
        // $('#shopbundle_article_cover')[0].reset();
        document.getElementById("shopbundle_article_cover").value = "";
        $image_preview.find('#img_pop').attr("src", "");
        $(this).hide();
    });

    $('form').find('input[name="shopbundle_article[image2]"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0];
            $image_preview = $('.img_to_send_add_2');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $("#img_pop_2").css("width", "60%");
            document.getElementById("img_pop").style.boxShadow = "1px 2px 2px 0px #333";
            $(".delete_img_2").fadeIn('slow');
            $(".height_form").css("height", "388px");
        }
    });
    $('.delete_img_2').click(function () {
        // $('#shopbundle_article_cover')[0].reset();
        document.getElementById("shopbundle_article_image2").value = "";
        $image_preview.find('#img_pop_2').attr("src", "");
        $(this).hide();
    });

    $('form').find('input[name="shopbundle_article[image3]"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0];
            $image_preview = $('.img_to_send_add_3');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $("#img_pop_3").css("width", "60%");
            document.getElementById("img_pop").style.boxShadow = "1px 2px 2px 0px #333";
            $(".delete_img_3").fadeIn('slow');
            $(".height_form").css("height", "388px");
        }
    });
    $('.delete_img_3').click(function () {
        // $('#shopbundle_article_cover')[0].reset();
        document.getElementById("shopbundle_article_image3").value = "";
        $image_preview.find('#img_pop_3').attr("src", "");
        $(this).hide();
    });

    $('form').find('input[name="shopbundle_article[image4]"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0];
            $image_preview = $('.img_to_send_add_4');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $("#img_pop_4").css("width", "60%");
            document.getElementById("img_pop").style.boxShadow = "1px 2px 2px 0px #333";
            $(".delete_img_4").fadeIn('slow');
            $(".height_form").css("height", "545px");
        }
    });
    $('.delete_img_4').click(function () {
        // $('#shopbundle_article_cover')[0].reset();
        document.getElementById("shopbundle_article_image4").value = "";
        $image_preview.find('#img_pop_4').attr("src", "");
        $(this).hide();
        $(".height_form").css("height", "383px");
    });

    $('form').find('input[name="shopbundle_article[image5]"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0];
            $image_preview = $('.img_to_send_add_5');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $("#img_pop_5").css("width", "60%");
            document.getElementById("img_pop").style.boxShadow = "1px 2px 2px 0px #333";
            $(".delete_img_5").fadeIn('slow');
            $(".height_form").css("height", "545px");
        }
    });
    $('.delete_img_5').click(function () {
        // $('#shopbundle_article_cover')[0].reset();
        document.getElementById("shopbundle_article_image5").value = "";
        $image_preview.find('#img_pop_5').attr("src", "");
        $(this).hide();
    });

    $('form').find('input[name="shopbundle_article[image6]"]').change(function () {
        var files = $(this)[0].files;
        if (files.length > 0) {
            var file = files[0];
            $image_preview = $('.img_to_send_add_6');
            $image_preview.find('img').attr('src', window.URL.createObjectURL(file));
            $("#img_pop_6").css("width", "60%");
            document.getElementById("img_pop").style.boxShadow = "1px 2px 2px 0px #333";
            $(".delete_img_6").fadeIn('slow');
            $(".height_form").css("height", "545px");
        }
    });
    $('.delete_img_6').click(function () {
        // $('#shopbundle_article_cover')[0].reset();
        document.getElementById("shopbundle_article_image6").value = "";
        $image_preview.find('#img_pop_6').attr("src", "");
        $(this).hide();
    });
});