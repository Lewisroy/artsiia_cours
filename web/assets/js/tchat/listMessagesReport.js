$('.reply-report-message').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var locale = $(this).data('locale');
    var message = $('#message-' + id).val();
    if (message != '' && message != null) {
        $.ajax({
            url: Routing.generate('tchat_reply_report_message', {_locale: locale}),
            type: 'POST',
            dataType: 'JSON',
            data: {
                'id': id,
                'locale': locale,
                'message': message
            },
            success: function (response) {
                Notify(response.message, null, null, (response.status) ? 'success' : 'warning');
                $.ajax({
                    url: Routing.generate('tchat_get_list_report_messages', {id: id, _locale: locale}),
                    type: 'GET',
                    dataType: 'HTML',
                    success: function (response) {
                        $('#list_message').html(response);
                    }
                });
            }
        });
        $('#message-' + id).val('');
    } else {
        $('.alert-warning').hide();
        if (locale == 'fr')
            Notify('Veuillez taper un message d\'abord !', null, null, 'warning');
        else
            Notify('Please enter a message first !', null, null, 'warning');
    }
});