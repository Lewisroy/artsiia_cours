ActionConversation = function (param) {
    var data = [];
    $('input[name="list-conversation"]:checked').each(function () {
        data.push(this.value);
    });
    if (data.length < 1) {
        $('.alert-warning').hide();
        if (locale == 'fr')
            Notify('Veuillez sélectionner au moin une conversation d\'abord !', null, null, 'warning');
        else
            Notify('Please select at least one conversation first !', null, null, 'warning');
    } else {
        if (param == 1) {
            $.ajax({
                url: Routing.generate('unread_conversations', {_locale: locale}),
                dataType: "json",
                data: {
                    ids: data
                },
                success: function (rep) {
                    location.reload();
                }
            });
        } else if (param == 2) {
            $.ajax({
                url: Routing.generate('delete_conversations', {_locale: locale}),
                dataType: "json",
                data: {
                    ids: data
                },
                success: function (rep) {
                    location.reload();
                }
            });
        } else if (param == 3) {
            $.ajax({
                url: Routing.generate('read_conversations', {_locale: locale}),
                dataType: "json",
                data: {
                    ids: data
                },
                success: function (rep) {
                    location.reload();
                }
            });
        }
    }
};