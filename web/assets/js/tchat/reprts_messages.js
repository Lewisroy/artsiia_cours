$('.get-list-messages').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var locale = $(this).data('locale');
    var ids = $("#ids").data('ids');
    var total = $('.messages-reports-count').data('count');
    if (total > 0) {
        total--;
    }
    var content = '<span class="messages-reports-count" data-count="' + total + '">' + total + '</span>';
    $('#content-count').html(content);
    $('#is-read-type-' + id).removeClass('message-isnot-read');
    $('#is-read-name-' + id).removeClass('message-isnot-read');
    $('#is-read-content-' + id).removeClass('message-isnot-read');
    for (var i = 0; i < ids.length; i++) {
        $('#block-message-' + ids[i]).removeClass('active_hover');
    }
    $('#block-message-' + id).addClass('active_hover');
    $.ajax({
        url: Routing.generate('tchat_get_list_report_messages', {id: id, _locale: locale}),
        type: 'GET',
        dataType: 'HTML',
        success: function (response) {
            $('#list_message').html(response);
        }
    });
});