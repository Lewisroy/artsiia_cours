var PULL_INTERVAL = 10000; // 10 secondes
var ACTIVITY_UPDATE_INTERVAL = 30000;  // 30 secondes
var TChat = {
    pullUrl: pullUrl,
    pullMethod: "POST",
    sendUrl: sendUrl,
    sendMethod: "POST",
    activityUrl: activityUrl,
    activityMethod: "POST",
    // Pull des messages
    pull_message: function () {
        var page = $('#page-messages').val();
        $.ajax({
            url: TChat.pullUrl,
            data: {'user_id': user_id, page: page},
            type: TChat.pullMethod,
            success: function (htmlResponse) {
                $('#list_message').html(htmlResponse);
                setTimeout(function () {
                    TChat.pull_message();
                }, PULL_INTERVAL);
            }
            ,
            error: function (error) {
                setTimeout(function () {
                    TChat.pull_message();
                }, PULL_INTERVAL);
            }
        }
        );
    },
    // Send a message
    send_message: function (message) {
        $(".be-loader").show();
        $.ajax({
            url: TChat.sendUrl,
            data: {'message': message},
            type: TChat.sendMethod,
            success: function (htmlResponse) {
                $('#list_message').html(htmlResponse);
                $(".be-loader").hide();
                CKEDITOR.instances["message_text"].setData('');
            },
            error: function (error) {
                $(".be-loader").hide();
                CKEDITOR.instances["message_text"].setData('');
            }
        });
    },
    // push l'activity date
    push_activity: function () {
        $.ajax({
            url: TChat.activityUrl,
            data: {'update': update_true},
            type: TChat.activityMethod,
            success: function (jsonData) {
                setTimeout(function () {
                    TChat.push_activity();
                }, ACTIVITY_UPDATE_INTERVAL);
            },
            error: function (error) {
                setTimeout(function () {
                    TChat.push_activity();
                }, ACTIVITY_UPDATE_INTERVAL);
            }
        });
    },
};
$(document).ready(function () {
    TChat.pull_message();
    if (CKEDITOR.instances["message_text"]) {
        CKEDITOR.instances["message_text"].destroy(true);
        delete CKEDITOR.instances["message_text"];
    }
    CKEDITOR.replace("message_text", {
        "toolbar": ["\/", ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript", "-"],
            ["Image", "SpecialChar", "Smiley", "TextColor", "BGColor"],
            []], "uiColor": "#ffffff", "language": "fr"
    });
    $(".send_message").click(function () {
        var message = CKEDITOR.instances["message_text"].getData();
        if (message != "") {
            TChat.send_message(message);
        }

        return false;
    });
    // notification activity
    TChat.push_activity();
    // Effet background gray
    $('.style-3').on('mouseover', function (e) {
        if (!$(this).hasClass('grayBack')) {
            $(this).addClass('grayBack');
        }
    });
    $('.style-3').on('mouseout', function (e) {
        if ($(this).hasClass('grayBack')) {
            $(this).removeClass('grayBack');
        }
    });
});