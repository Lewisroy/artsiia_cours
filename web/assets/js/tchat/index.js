var PULL_INTERVAL = 10000; // 10 secondes
var ACTIVITY_UPDATE_INTERVAL = 30000; // 30 secondes
var TChat = {
    pullUrl: null,
    timers: Array(),
    pullMethod: "POST",
    sendUrl: null,
    sendMethod: "POST",
    activityUrl: null,
    activityMethod: "POST",
    // Pull des messages
    pull_message: function () {
        if (!(TChat.pullUrl == null || TChat.pullUrl == "")) {
            $.ajax({
                url: TChat.pullUrl,
                data: {'user_id': user_id},
                type: TChat.pullMethod,
                success: function (htmlResponse) {
                    $('#list_message').html(htmlResponse);
                    TChat.timers.push(setTimeout(function () {
                        TChat.pull_message();
                    }, PULL_INTERVAL));
                },
                error: function (error) {
                    TChat.timers.push(setTimeout(function () {
                        TChat.pull_message();
                    }, PULL_INTERVAL));
                }
            });
        }
    },
    // Send a message
    send_message: function (message) {
        if (!(TChat.sendUrl == null || TChat.sendUrl == "")) {
            $(".be-loader").show();
            $.ajax({
                url: TChat.sendUrl,
                data: {'message': message},
                type: TChat.sendMethod,
                success: function (htmlResponse) {
                    $('#list_message').html(htmlResponse);
                    $(".be-loader").hide();
                    CKEDITOR.instances["message_text"].setData('');
                },
                error: function (error) {
                    $(".be-loader").hide();
                    CKEDITOR.instances["message_text"].setData('');
                }
            });
        }
    },
    // push l'activity date
    push_activity: function () {
        if (!(TChat.activityUrl == null || TChat.activityUrl == "")) {
            $.ajax({
                url: TChat.activityUrl,
                data: {'update': update_true},
                type: TChat.activityMethod,
                success: function (jsonData) {
                    TChat.timers.push(setTimeout(function () {
                        TChat.push_activity();
                    }, ACTIVITY_UPDATE_INTERVAL));
                },
                error: function (error) {
                    TChat.timers.push(setTimeout(function () {
                        TChat.push_activity();
                    }, ACTIVITY_UPDATE_INTERVAL));
                }
            });
        }
    },
};
$(document).ready(function () {
    $(".style-3").each(function () {
        if ($(this).hasClass('active_hover')) {
            TChat.pullUrl = $(this).find('.pullPath').val();
            TChat.activityUrl = $(this).find('.pushPath').val();
            TChat.sendUrl = $(this).find('.sendPath').val();
            $(".messageWith").text($(this).find('.messageTitle').val());
        }
    });
    TChat.pull_message();
    if (CKEDITOR.instances["message_text"]) {
        CKEDITOR.instances["message_text"].destroy(true);
        delete CKEDITOR.instances["message_text"];
    }
    CKEDITOR.replace("message_text", {
        "toolbar": ["\/", ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript", "-"],
            ["Image", "SpecialChar", "Smiley", "TextColor", "BGColor"],
            []], "uiColor": "#ffffff", "language": "fr"
    });
    $(".send_message").click(function () {
        var message = CKEDITOR.instances["message_text"].getData();
        if (message != "") {
            TChat.send_message(message);
        }

        return false;
    });
    $(".voir_message").click(function () {
        $(".be-loader").show();
        $(".style-3").each(function () {
            if ($(this).hasClass('active_hover')) {
                $(this).removeClass('active_hover');
            }
        });
        // Suppression des boucles timer en cours
        for (var id = 0; id <= TChat.timers.length; id++) {
            clearTimeout(TChat.timers[id]);
        }
        TChat.timers = Array();
        $(this).parents(".style-3").addClass('active_hover');
        TChat.pullUrl = $(this).parents(".style-3").find('.pullPath').val();
        TChat.activityUrl = $(this).parents(".style-3").find('.pushPath').val();
        TChat.sendUrl = $(this).parents(".style-3").find('.sendPath').val();
        $(".messageWith").text($(this).parents(".style-3").find('.messageTitle').val());
        TChat.pull_message();
        TChat.push_activity();
        $(".be-loader").hide();
    });
    // notification activity
    TChat.push_activity();
    // Commence ici le script de suppression
    $('.form-checkbox input').on('click', function (e) {
        var currentVal = $(this).val();
        currentVal = currentVal == 0 ? 1 : 0;
        $(this).val(currentVal);
    });
    $('.supprimerConv').on('click', function (e) {
        var urlAction = $(this).attr("data-url");
        $(".formAction").attr("action", urlAction);
        $("#ids").val("");
        var listIds = "";
        var count = 0;
        $('.form-checkbox input').each(function () {
            if ($(this).val() == 1) {
                if (count == 0) {
                    listIds += $(this).parents(".form-checkbox").find(".conversationId").val();
                } else {
                    listIds += "," + $(this).parents(".form-checkbox").find(".conversationId").val();
                }
                count++;
            }
        });
        $("#ids").val(listIds);
        if ($.trim(listIds) != "") {
            $(".formAction").submit();
        } else {
            alert("vous devez selectionner au moins une conversation");
            return false;
        }
    });
    // Effet background gray
    $('.style-3').on('mouseover', function (e) {
        if (!$(this).hasClass('grayBack')) {
            $(this).addClass('grayBack');
        }
    });
    $('.style-3').on('mouseout', function (e) {
        if ($(this).hasClass('grayBack')) {
            $(this).removeClass('grayBack');
        }
    });
    // Marquer comme non lu message
    $('.marqueNonLu').on('click', function (e) {
        var urlAction = $(this).attr("data-url");
        $(".formAction").attr("action", urlAction);
        $("#ids").val("");
        var listIds = "";
        var count = 0;
        $('.form-checkbox input').each(function () {
            if ($(this).val() == 1) {
                if (count == 0) {
                    listIds += $(this).parents(".form-checkbox").find(".conversationId").val();
                } else {
                    listIds += "," + $(this).parents(".form-checkbox").find(".conversationId").val();
                }
                count++;
            }
        });
        $("#ids").val(listIds);
        if ($.trim(listIds) != "") {
            $(".formAction").submit();
        } else {
            alert("vous devez selectionner au moins une conversation");
            return false;
        }
    });
});