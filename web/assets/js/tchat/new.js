$(document).ready(function () {
    if (CKEDITOR.instances["message_text"]) {
        CKEDITOR.instances["message_text"].destroy(true);
        delete CKEDITOR.instances["message_text"];
    }
    CKEDITOR.replace("message_text", {
        "toolbar": ["\/", ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript", "-"],
            ["Image", "SpecialChar", "Smiley", "TextColor", "BGColor"],
            []], "uiColor": "#ffffff", "language": "fr"
    });
    $(".send_message").click(function () {
        var message = CKEDITOR.instances["message_text"].getData();
        $("#message_text").val(message);
        var destinataires = $("#destinataires").val();
        if ($.trim(message) != "" && $.trim(destinataires) != "") {
            $("[name^=formMessage]").submit();

            return true;
        } else {
            alert("Veuillez remplir les champs du formulaire (destinataires et message).");
        }
        //

        return false;
    });
    $(".clear_message").click(function () {
        CKEDITOR.instances["message_text"].setData('');
        $("#message_text").val('');

        return false;
    });
    $('#destinataires').tagsInput({
        width: '100%',
        'defaultText': "Ajouter un destinataire",
        autocomplete_url: autocomplete_url,
        autocomplete: {
            source: function (request, response) {
                // Saisie 3 caractère au minimum
                if (request.term != null && request.term != "" && request.term.length > 2) {
                    $.ajax({
                        url: url,
                        dataType: "json",
                        data: {
                            q: request.term
                        },
                        success: function (data) {
                            if (data != null && data.length > 0) {
                                response($.map(data, function (item) {
                                    return {
                                        label: item.name,
                                        value: item.name
                                    }
                                }));
                            } else {
                                alert("Aucun correspondance trouvé");
                            }

                        }
                    })
                }
            }}
    });
});