$('.show-more-people').on('click', function (e) {
    var length = $('.data-length').data('length');
    $.ajax({
        url: Routing.generate('show_more_people', {_locale: locale}),
        method: 'POST',
        data: {
            first: length
        },
        dataType: 'JSON',
        success: function (rep) {
            var wrapper = $('#container-mix');
            if (rep.length != 0) {
                for (var i = 0; i < rep.length; i++) {
                    var user = rep[i];
                    var url_show_me = Routing.generate('show_me', {_locale: locale, id: user.id});
                    var user_city = '';
                    if (user.city) {
                        user_city = '<p class="be-user-info">' + user.city + ',' + user.country + '</p>';
                    }
                    if (user.picture != null) {
                        var img = '<img src="' + uploads_artwork_cover + user.id + '/' + user.picture + '" alt="">';
                    } else {
                        var img = '<img src="' + default_img + '" alt=""';
                    }
                    var category = '';
                    var subCategory = '';
                    var category_length = user.category.length;
                    if (category_length > 0) {
                        for (var j = 0; j < user.category.length; j++) {
                            var cat = user.category[j];
                            var url_category_show = Routing.generate('category_show', {_locale: locale, id: cat.id});

                            if (j < category_length - 1) {
                                var comma = ', ';
                            } else {
                                var comma = '';
                            }
                            var c = '<a href="' + url_category_show + '">' + cat.name + '</a>' + comma;
                            category += c;

                            var subCategory_length = cat.subCategories.length;
                            for (var k = 0; k < cat.subCategories.length; k++) {
                                var sub_cat = cat.subCategories[k];
                                if (k < subCategory_length - 1) {
                                    var comma = ', ';
                                } else {
                                    var comma = '';
                                }
                                var s_c = '<a href="#">' + sub_cat.name + '</a>' + comma;
                                subCategory += s_c;
                            }
                        }
                    } else {
                        category = category_no_existing;
                        subCategory = subCategory_no_existing;
                    }
                    var url_login = Routing.generate('fos_user_security_login', {_locale: locale});
                    var folow = '<a class="btn color-1 size-2 hover-1 followA" href="' + url_login + '">' + follow_trans + '</a>';
                    if (user_id) {
                        var check = false;
                        for (var j = 0; j < user_followings.length; j++) {
                            var follow = user_followings[j];
                            if (follow == user_id) {
                                check = true;
                            }
                        }
                        if (check) {
                            folow = '<label class="be-user-info" style="color:green;">' +
                                    '<i class="fa fa-check" aria-hidden="true"></i>' +
                                    already_following +
                                    '</label>';
                        } else {
                            var url_user_follow = Routing.generate('user_follow', {_locale: locale, id: user_id});
                        }
                        folow = '<div class="followADiv">' +
                                '<a class="btn color-1 size-2 hover-1" href="' + url_user_follow + '">' + follow_trans + '</a>' +
                                '</div>';
                    }

                    var fieldHTML = '<div class="custom-column-5">' +
                            '<div class="be-user-block style-2">' +
                            '<a class="be-ava-user style-2" href="' + url_show_me + '">' +
                            img +
                            '</a>' +
                            '<div class="be-user-counter">' +
                            '<div class="c_number">' + user.artworks.length + '</div>' +
                            '<div class="c_text">' + artworks_trans + 's</div>' +
                            '</div>' +
                            '<a href="' + url_show_me + '" class="be-use-name">' + user.username + '</a>' +
                            user_city +
                            '<div class="be-text-tags">' +
                            category +
                            '</div>' +
                            '<div class="be-text-tags" style="border-top: none;padding: 0;height: 35px;">' +
                            subCategory +
                            '</div>' +
                            folow +
                            '</div>' +
                            '</div>';
                    $(wrapper).append(fieldHTML);
                }
                $('#data-length').html('<input type="hidden" class="data-length" data-length="' + (length + rep.length) + '"/>');
            } else {
                $('.show-more-people').addClass('disabled');
                $('.show-more-people').html(empty_artists);
            }
        }
    });

});

$('#filter-category').change(function () {
    console.log($('#filter-category').val());
    if ($('#filter-category').val()) {
        $.ajax({
            url: Routing.generate('list_sub_category', {_locale: locale, id: $('#filter-category').val()}),
            type: 'GET',
            success: function (response) {
                var li = "";
                for (var i = 0; i < response.length; i++) {
                    li += '<option value="' + response[i]['id'] + '">' + response[i]['name'] + '</option>';
                }
                var html = '<div class="col-md-2">' +
                        '<select name="subcategory" class="select-be-drop-down">' +
                        '<option value="">' + subtitle + '</option>' +
                        li +
                        '</select>' +
                        '</div>';
                $('#contents-sub-category').html(html);
            }
        });
    } else {
        $('#contents-sub-category').html('');
    }
});